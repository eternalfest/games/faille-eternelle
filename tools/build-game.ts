const ANCHORS: ReadonlyMap<string, [number, number]> = new Map([
  ["sablier", [10, 20] as [number, number]],
  ["eclair", [10, 20] as [number, number]],
  ["ticket_bleu", [10, 20] as [number, number]],
  ["ticket_rouge", [10, 20] as [number, number]],
  ["erable", [10, 20] as [number, number]],
  ["urticants", [10, 20] as [number, number]],
  ["bouton", [10, 20] as [number, number]],
  ["pin", [10, 20] as [number, number]],
  ["sifflet", [10, 20] as [number, number]],
  ["avis", [10, 20] as [number, number]],
  ["ciseaux", [10, 20] as [number, number]],
  ["cle_de_sol", [10, 20] as [number, number]],
  ["dynamite", [10, 20] as [number, number]],
  ["marteau", [10, 20] as [number, number]],
  ["potion", [10, 20] as [number, number]],
  ["flamme", [10, 20] as [number, number]],
  ["masque", [10, 20] as [number, number]],
  ["miel", [10, 20] as [number, number]],
  ["gemme", [10, 20] as [number, number]],
  ["emeraude", [10, 20] as [number, number]],
  ["baseball", [10, 20] as [number, number]],
  ["nuage", [10, 20] as [number, number]],
  ["poudre", [10, 20] as [number, number]],
  ["cristallin", [10, 20] as [number, number]],
  ["baguette", [10, 20] as [number, number]],
  ["livre", [10, 20] as [number, number]],
  ["brimstone", [10, 20] as [number, number]],
  ["metronome", [10, 20] as [number, number]],
  ["montre", [10, 20] as [number, number]],
  ["anneau", [10, 20] as [number, number]],
  ["detector", [10, 20] as [number, number]],
  ["bomb", [10, 20] as [number, number]],
  ["glasses_no", [10, 20] as [number, number]],
  ["arrow", [10, 20] as [number, number]],
  ["rave", [10, 20] as [number, number]],
  ["boot", [10, 20] as [number, number]],
  ["multibomb", [10, 20] as [number, number]],
  ["sceptre", [10, 20] as [number, number]],
  ["frozen", [10, 20] as [number, number]],
  ["curse", [10, 20] as [number, number]],
  ["purif", [10, 20] as [number, number]],
  ["holy", [10, 20] as [number, number]],
  ["orb", [10, 20] as [number, number]],
  ["eye", [10, 20] as [number, number]],
  ["bell", [10, 20] as [number, number]],
  ["parapluie", [10, 20] as [number, number]],
  ["black_hole", [10, 20] as [number, number]],
  ["glasses_void", [10, 20] as [number, number]],
  ["mirror", [10, 20] as [number, number]],
  ["crown", [10, 20] as [number, number]],
  ["dice", [10, 20] as [number, number]],

  ["sandwich", [10, 20] as [number, number]],
  ["hotdog", [10, 20] as [number, number]],
  ["falafels", [10, 20] as [number, number]],
  ["panini", [10, 20] as [number, number]],
  ["kfc", [10, 20] as [number, number]],
  ["wrap", [10, 20] as [number, number]],
  ["maki", [10, 20] as [number, number]],
  ["riz", [10, 20] as [number, number]],
  ["doritos", [10, 20] as [number, number]],
  ["donut", [10, 20] as [number, number]],
  ["gateauMT", [10, 20] as [number, number]],
  ["bretzel", [10, 20] as [number, number]],
  ["tropezienne", [10, 20] as [number, number]],
  ["gateauR", [10, 20] as [number, number]],
  ["bonbon", [10, 20] as [number, number]],
  ["pie", [10, 20] as [number, number]],
  ["oreo", [10, 20] as [number, number]],
  ["nougat", [10, 20] as [number, number]],
  ["barbe", [10, 20] as [number, number]],
  ["chouros", [10, 20] as [number, number]],
  ["popcorn", [10, 20] as [number, number]],
  ["pomme", [10, 20] as [number, number]],
  ["pain", [10, 20] as [number, number]],
  ["gauffre", [10, 20] as [number, number]],
  ["lait", [10, 20] as [number, number]],
  ["confiture", [10, 20] as [number, number]],
  ["fourrage", [10, 20] as [number, number]],
  ["verrine", [10, 20] as [number, number]],
  ["buche", [10, 20] as [number, number]],
  ["glace", [10, 20] as [number, number]],
  ["boule_choco", [10, 20] as [number, number]],
  ["gateauF", [10, 20] as [number, number]],
  ["cupcake", [10, 20] as [number, number]],
  ["cupcakeC", [10, 20] as [number, number]],
  ["maracas", [10, 20] as [number, number]],
  ["triangle", [10, 20] as [number, number]],
  ["flute", [10, 20] as [number, number]],
  ["guitare", [10, 20] as [number, number]],
  ["tambourin", [10, 20] as [number, number]],
  ["djembe", [10, 20] as [number, number]],
  ["trompette", [10, 20] as [number, number]],
  ["xylophone", [10, 20] as [number, number]],
  ["banjo", [10, 20] as [number, number]],
  ["violon", [10, 20] as [number, number]],
  ["block", [10, 20] as [number, number]],
  ["mirabelle", [10, 20] as [number, number]],
  ["mechabombino", [10, 20] as [number, number]],
  ["citron", [10, 20] as [number, number]],
  ["mure", [10, 20] as [number, number]],
  ["medal", [10, 20] as [number, number]],
  ["pepper", [10, 20] as [number, number]],
  ["star_broken", [15, 25] as [number, number]],

  ["trois", [6, 11] as [number, number]],
  ["deux", [6, 11] as [number, number]],
  ["un", [6, 11] as [number, number]],
  ["zero", [6, 11] as [number, number]],
  ["bombe", [6, 11] as [number, number]],
  ["brim", [6, 11] as [number, number]],
  ["hand", [6, 11] as [number, number]],
  ["bomberman", [6, 11] as [number, number]],
  ["dizzy", [6, 11] as [number, number]],
  ["eyeC", [6, 11] as [number, number]],

  ["bg_blossom", [0, 0] as [number, number]],

  ["engrenagesB", [5, 0] as [number, number]],
  ["instableJauneHB", [5, 0] as [number, number]],
  ["instableJauneVB", [5, 0] as [number, number]],
  ["instableNoireHB", [5, 0] as [number, number]],
  ["instableNoireVB", [5, 0] as [number, number]],
  ["instableBlancheHB", [5, 0] as [number, number]],
  ["instableBlancheVB", [5, 0] as [number, number]],
  ["crypteBleueB", [5, 0] as [number, number]],
  ["cloudB", [5, 0] as [number, number]],
  ["briqueBleueB", [5, 0] as [number, number]],
  ["colonneBleueB", [5, 0] as [number, number]],
  ["diccyVB", [5, 0] as [number, number]],

  ["large_scroll", [0, 0] as [number, number]],
  ["enter_key", [0, 0] as [number, number]],
  ["space_key", [0, 0] as [number, number]],
  ["simple_key", [0, 0] as [number, number]],
  ["dim_in", [0, 0] as [number, number]],
  ["dim_out", [0, 0] as [number, number]],
  ["map", [0, 0] as [number, number]],

  ["emoji0", [10, 15] as [number, number]],
  ["emoji1", [10, 15] as [number, number]],
  ["emoji2", [10, 15] as [number, number]],
  ["emoji3", [10, 15] as [number, number]],
  ["emoji4", [10, 15] as [number, number]],
  ["emoji5", [10, 15] as [number, number]],
  ["other0", [12, 20] as [number, number]],
  ["other1", [12, 20] as [number, number]],
  ["party_bottom", [0, 20] as [number, number]],

  ["fridge_bg", [0, 0] as [number, number]],
  ["quest_bg", [0, 0] as [number, number]],
  ["boss_gris", [0, 0] as [number, number]],

  ["black", [0, 20] as [number, number]],

  ["hat", [6, 15] as [number, number]],
  ["coffinDance", [2, 17] as [number, number]],
  ["party", [6, 17] as [number, number]],
  
  ["$arrow", [60, 7] as [number, number]],
  ["$maillon1", [6, 36] as [number, number]],
  ["$maillon2", [0, 40] as [number, number]],
  ["$maillon3", [12, 40] as [number, number]],
  
  ["dechire", [0, 0] as [number, number]],
  ["$blockBlue", [0, 20] as [number, number]],
  ["$msg", [0, 18] as [number, number]],
  ["$illuminati", [0, 0] as [number, number]],
  ["$eye", [0, 0] as [number, number]],
  ["starItem", [15, 15] as [number, number]],
  ["timerBg", [0, 20] as [number, number]],
  ["arrow_down", [0, 15] as [number, number]],

  ["$r2", [40, 77]],

  ["$vine", [0, 0] as [number, number]],
  ["$vine2", [0, 0] as [number, number]],
  ["$starlock", [0, 0] as [number, number]],
]);

const EXTRACT_FRAMES: ReadonlyMap<string, FrameRef> = new Map([
  ["boss_reborn_ring", [3401, 7]],
  ["boss_reborn_ring_attack", [3401, 1]],
]);

const CLONE_SPRITES: ReadonlyMap<string, SpriteRef> = new Map([
  ["hammer_bad_spear", ";tse1("],
  ["boss_spike", ";tse1("],
  ["boss_homing_spike", ";tse1("],
  ["boss_zigzag_spike", ";tse1("],
  ["boss_damocles_spike", ";tse1("],
  ["boss_ultimate_spike", ";tse1("],
  ["boss_falling_spike", ";tse1("],
  ["boss_temporary_spike", ";tse1("],
  ["boss_laser", "{W(a9("],
  ["boss_reborn_temporary_homing_spike", ";tse1("],
  ["boss_reborn_zigzag_spike", ";tse1("],
  ["boss_reborn_ice_flower_spike", ";tse1("],
  ["boss_reborn_rotating_laser", "{W(a9("],
  ["boss_reborn_kami_spike", ";tse1("],
  ["boss_reborn_otherwordly_spike", ";tse1("],
  ["boss_reborn_storm_spike", ";tse1("],
  ["boss_reborn_phase3_laser", "+Da3+"],
  ["$engrenage", 520 as SpriteRef],
  ["$engrenageCentre", 1821 as SpriteRef],
]);

const EXTRA_FRAMES: ReadonlyMap<SpriteRef, ExtraFrameOptions> = new Map([
  [
    "9-DjM",
    {
      frames: [
        "sablier",
        "eclair",
        "ticket_bleu",
        "ticket_rouge",
        "erable",
        "urticants",
        "bouton",
        "pin",
        "sifflet",
        "avis",
        "ciseaux",
        "cle_de_sol",
        "dynamite",
        "marteau",
        "potion",
        "flamme",
        "masque",
        "miel",
        "gemme",
        "emeraude",
        "baseball",
        "nuage",
        "poudre",
        "cristallin",
        "baguette",
        "livre",
        "brimstone",
        "metronome",
        "empty",
        "montre",
        "anneau",
        "detector",
        "bomb",
        "glasses_no",
        "arrow",
        "rave",
        "boot",
        "multibomb",
        "sceptre",
        "frozen",
        "curse",
        "purif",
        "holy",
        "orb",
        "eye",
        "bell",
        "parapluie",
        "black_hole",
        "glasses_void",
        "mirror",
        "crown",
        "dice",
      ],
    },
  ],
  [
    "]xUh6",
    {
      frames: [
        "sandwich",
        "hotdog",
        "falafels",
        "panini",
        "kfc",
        "wrap",
        "maki",
        "riz",
        "doritos",
        "donut",
        "gateauMT",
        "bretzel",
        "tropezienne",
        "gateauR",
        "bonbon",
        "pie",
        "oreo",
        "nougat",
        "barbe",
        "chouros",
        "popcorn",
        "pomme",
        "pain",
        "gauffre",
        "lait",
        "confiture",
        "fourrage",
        "verrine",
        "buche",
        "glace",
        "boule_choco",
        "gateauF",
        "cupcake",
        "cupcakeC",
        "maracas",
        "triangle",
        "flute",
        "guitare",
        "tambourin",
        "djembe",
        "trompette",
        "xylophone",
        "banjo",
        "violon",
        "block",
        "mirabelle",
        "mechabombino",
        "citron",
        "mure",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "medal",
        "empty",
        "pepper",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "star_broken",
      ],
    },
  ],
  [
    "6h0D)",
    {
      frames: [
        "trois",
        "deux",
        "un",
        "zero",
        "bombe",
        "brim",
        "hand",
        "bomberman",
        "dizzy",
        "eyeC",
      ],
    },
  ],
  [
    "[Of51(",
    {
      frames: [
        "boss_gris",
      ],
    },
  ],
  [
    "5Jij5(",
    {
      frames: [
        "bg_blossom",
      ],
    },
  ],
  [
    "0S4B/=S7A",
    {
      frames: [
        "engrenages",
        "instableJauneH",
        "instableJauneV",
        "instableNoireH",
        "instableNoireV",
        "instableBlancheH",
        "instableBlancheV",
        "crypteBleue",
        "cloud",
        "briqueBleue",
        "colonneBleue",
        "diccyV",
      ],
    },
  ],
  [
    "0S4B/ nbpb",
    {
      protectedDepths: new Set([1]),
      frames: [
        "engrenagesB",
        "instableJauneHB",
        "instableJauneVB",
        "instableNoireHB",
        "instableNoireVB",
        "instableBlancheHB",
        "instableBlancheVB",
        "crypteBleueB",
        "cloudB",
        "briqueBleueB",
        "colonneBleueB",
        "diccyVB",
      ],
    },
  ],
  [
    "7ZdiZ",
    {
      frames: [
        "party_bottom",
      ],
    },
  ],
  [
    "-0lm4(",
    {
      frames: [
        "dim_in",
        "dim_out",
      ],
    },
  ],
  [
    747 as SpriteRef,
    {
      frames: [
        "hat",
        "coffinDance",
        "party",
      ],
    },
  ],
  [
    814 as SpriteRef,
    {
      frames: [
        "empty",
        "empty",
        "empty",
        "empty",
        "empty",
        "hat",
        "coffinDance",
        "party",
      ],
    },
  ],
]);

////////////////////////////////////////////////////////////////////////////////

import { PlaceObject } from "swf-types/tags";
import { getGamePath } from "@eternalfest/game";
import fs from "fs";
import sysPath from "path";
import { emitSwf } from "swf-emitter";
import { DynMovie, swfMerge } from "swf-merge";
import { parseSwf } from "swf-parser";
import { CompressionMethod, Matrix, Movie, NamedId, Sfixed16P16, SpriteTag, Tag, TagType } from "swf-types";
import { DefineSprite, DefineSprite as DefineSpriteTag } from "swf-types/tags/define-sprite";
import { BlendMode } from "swf-types/blend-mode";
import { Sfixed8P8 } from "swf-types/fixed-point/sfixed8p8";

const PROJECT_ROOT: string = sysPath.join(__dirname, "..");

const INPUT_GAME: string = getGamePath();
const INPUT_ASSETS: string = sysPath.join(PROJECT_ROOT, "build", "assets.swf");
const OUTPUT_GAME: string = sysPath.join(PROJECT_ROOT, "build", "game.swf");

interface ExtraFrameOptions {
  readonly protectedDepths?: ReadonlySet<number>,
  readonly frames: ReadonlyArray<string>,
}

const PIXELS_PER_TWIP: number = 20;

type SpriteRef = string | number;

type FrameRef = [SpriteRef, number];

const IDENTITY_MATRIX: Matrix = {
  scaleX: Sfixed16P16.fromValue(1),
  scaleY: Sfixed16P16.fromValue(1),
  rotateSkew0: Sfixed16P16.fromValue(0),
  rotateSkew1: Sfixed16P16.fromValue(0),
  translateX: 0,
  translateY: 0,
};

function applyAnchors(astMovie: Movie, anchors: ReadonlyMap<string, [number, number]>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  const tags: Tag[] = [...astMovie.tags];
  for (const [linkageName, [posX, posY]] of anchors) {
    const id = exports.get(linkageName);
    if (id === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(linkageName)}`);
    }
    const index = definitions.get(id);
    if (index === undefined) {
      throw new Error(`DefinitionNotFound: id = ${id}`);
    }
    const tag: Tag = tags[index];
    if (tag.type !== TagType.DefineSprite) {
      throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
    }
    const childTags: ReadonlyArray<SpriteTag> = tag.tags.map((childTag: SpriteTag) => {
      if (childTag.type !== TagType.PlaceObject || childTag.isUpdate) {
        return childTag;
      }
      const oldMatrix: Matrix = childTag.matrix !== undefined ? childTag.matrix : IDENTITY_MATRIX;
      const matrix = {
        ...oldMatrix,
        translateX: oldMatrix.translateX - (posX * PIXELS_PER_TWIP),
        translateY: oldMatrix.translateY - (posY * PIXELS_PER_TWIP),
      };
      return {...childTag, matrix};
    });
    tags[index] = {...tag, tags: childTags};
  }
  return {...astMovie, tags};
}

function cloneSprites(astMovie: Movie, sprites: ReadonlyMap<string, SpriteRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [cloneLinkName, spriteRef] of sprites) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const clone: DefineSprite = {...sprite, id: nextId};
    tags.push(clone);
    extraExports.push({id: clone.id, name: cloneLinkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
}

async function main() {
  const game: Movie = await readSwf(INPUT_GAME);
  let assets: Movie = await readSwf(INPUT_ASSETS);
  assets = applyAnchors(assets, ANCHORS);
  // await writeFile("assets.json", JSON.stringify(assets, null, 2) as any);
  let merged: Movie = swfMerge(game, assets);
  merged = extractFrames(merged, EXTRACT_FRAMES);
  merged = cloneSprites(merged, CLONE_SPRITES);
  merged = appendFrames(merged, EXTRA_FRAMES);
  // await writeFile("game.json", JSON.stringify(merged, null, 2) as any);
  await writeSwf(OUTPUT_GAME, merged);
}

function extractFrames(astMovie: Movie, anchors: ReadonlyMap<string, FrameRef>): Movie {
  const movie = DynMovie.fromAst(astMovie);
  let nextId: number = movie.getMaxCharacterId() + 1;
  const tags: Tag[] = [...astMovie.tags];
  const extraExports: NamedId[] = [];
  for (const [linkName, [spriteRef, frameIndex]] of anchors) {
    const {index, sprite} = resolveSpriteRef(tags, movie, spriteRef);
    const frameSprite: DefineSprite = extractFrame(sprite, frameIndex, nextId);
    tags.push(frameSprite);
    extraExports.push({id: frameSprite.id, name: linkName});
    nextId += 1;
  }
  tags.push({type: TagType.ExportAssets, assets: extraExports});
  return {...astMovie, tags};
  function extractFrame(sprite: DefineSprite, frameIndex: number, id: number): DefineSprite {
    let curFrame: number = 0;
    let curTagIndex: number = 0;
    const depths: Map<number, PlaceObject> = new Map();
    for (const tag of sprite.tags) {
      if (curFrame === frameIndex) {
        // We reached the frame we wanted to extract
        const spriteTags: SpriteTag[] = [];
        for (const object of depths.values()) {
          spriteTags.push(object);
        }
        for (let i: number = curTagIndex; i < sprite.tags.length; i++) {
          const spriteTag: SpriteTag = sprite.tags[i];
          if (spriteTag.type === TagType.ShowFrame) {
            break;
          }
          spriteTags.push(spriteTag);
        }
        spriteTags.push({type: TagType.ShowFrame});
        return {type: TagType.DefineSprite, id, frameCount: 1, tags: spriteTags};
      }

      if (tag.type === TagType.ShowFrame) {
        curFrame += 1;
      } else if (tag.type === TagType.PlaceObject) {
        if (!tag.isUpdate) {
          depths.set(tag.depth, tag);
        } else {
          let old: PlaceObject | undefined = depths.get(tag.depth);
          if (old === undefined) {
            old = {
              type: TagType.PlaceObject,
              isUpdate: false,
              depth: tag.depth,
              characterId: tag.characterId,
              className: tag.className,
              matrix: {
                scaleX: Sfixed16P16.fromValue(1),
                scaleY: Sfixed16P16.fromValue(1),
                rotateSkew0: Sfixed16P16.fromValue(0),
                rotateSkew1: Sfixed16P16.fromValue(0),
                translateX: 0,
                translateY: 0,
              },
              colorTransform: {
                redMult: Sfixed8P8.fromValue(1),
                greenMult: Sfixed8P8.fromValue(1),
                blueMult: Sfixed8P8.fromValue(1),
                alphaMult: Sfixed8P8.fromValue(1),
                redAdd: 0,
                greenAdd: 0,
                blueAdd: 0,
                alphaAdd: 0,
              },
              ratio: 0,
              name: tag.name,
              clipDepth: tag.clipDepth,
              filters: undefined,
              blendMode: BlendMode.Normal,
              bitmapCache: undefined,
              visible: undefined,
              backgroundColor: undefined,
              clipActions: undefined,
            };
          }
          const newTag: PlaceObject = {
            ...old,
            isUpdate: false,
            characterId: tag.characterId !== undefined ? tag.characterId : old.characterId,
            className: tag.className !== undefined ? tag.className : old.className,
            matrix: tag.matrix !== undefined ? tag.matrix : old.matrix,
            colorTransform: tag.colorTransform !== undefined ? tag.colorTransform : old.colorTransform,
            ratio: tag.ratio !== undefined ? tag.ratio : old.ratio,
            name: tag.name !== undefined ? tag.name : old.name,
            clipDepth: tag.clipDepth !== undefined ? tag.clipDepth : old.clipDepth,
            filters: tag.filters !== undefined ? tag.filters : old.filters,
            blendMode: tag.blendMode !== undefined ? tag.blendMode : old.blendMode,
            bitmapCache: tag.bitmapCache !== undefined ? tag.bitmapCache : old.bitmapCache,
            visible: tag.visible !== undefined ? tag.bitmapCache : old.visible,
            backgroundColor: tag.backgroundColor !== undefined ? tag.backgroundColor : old.backgroundColor,
            clipActions: tag.clipActions !== undefined ? tag.clipActions : old.clipActions,
          };
          depths.set(tag.depth, newTag);
        }
      } else if (tag.type === TagType.RemoveObject) {
        depths.delete(tag.depth);
      }

      curTagIndex += 1;
    }
    throw new Error("FrameNotFound: " + frameIndex);
  }
}

function appendFrames(astMovie: Movie, extraFrames: ReadonlyMap<SpriteRef, ExtraFrameOptions>) {
  const movie = DynMovie.fromAst(astMovie);
  const exports = movie.getNamedExports();
  const tags: Tag[] = [...astMovie.tags];
  for (const [extendedLinkageName, options] of extraFrames) {
    const protectedDepths: ReadonlySet<number> = options.protectedDepths !== undefined
      ? options.protectedDepths
      : new Set();
    const {index, sprite} = resolveSpriteRef(tags, movie, extendedLinkageName);
    tags[index] = appendFramesToSprite(sprite, options.frames, extendedLinkageName, protectedDepths);
  }
  return {...astMovie, tags};

  function appendFramesToSprite(
    tag: DefineSpriteTag,
    frames: ReadonlyArray<string>,
    extendedLinkageName: SpriteRef,
    protectedDepths: ReadonlySet<number>,
  ): DefineSpriteTag {
    let frameCount: number = tag.frameCount;
    const childTags: SpriteTag[] = [...tag.tags];
    for (const depth of getUsedDepths(childTags)) {
      if (protectedDepths.has(depth)) {
        continue;
      }
      childTags.push({type: TagType.RemoveObject, depth});
    }
    let minDepth = 0;
    for (const depth of protectedDepths) {
      minDepth = Math.max(minDepth, depth);
    }
    const depth = minDepth + 1;
    let first = true;
    for (const frameLinkageName of frames) {
      const id = exports.get(frameLinkageName);
      if (id === undefined) {
        throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(frameLinkageName)}`);
      }
      console.log(`${extendedLinkageName}[${frameCount}] = ${frameLinkageName}`);
      if (first) {
        first = false;
      } else {
        childTags.push({type: TagType.RemoveObject, depth});
      }
      childTags.push(
        {
          type: TagType.PlaceObject,
          isUpdate: false,
          depth,
          characterId: id,
          className: undefined,
          matrix: undefined,
          colorTransform: undefined,
          ratio: undefined,
          name: undefined,
          clipDepth: undefined,
          filters: undefined,
          blendMode: undefined,
          bitmapCache: undefined,
          visible: undefined,
          backgroundColor: undefined,
          clipActions: undefined,
        },
        {type: TagType.ShowFrame},
      );
      frameCount += 1;
    }

    return {...tag, frameCount, tags: childTags};
  }
}

function getUsedDepths(tags: Iterable<Tag>): Set<number> {
  const depths: Set<number> = new Set();
  for (const tag of tags) {
    if (tag.type === TagType.PlaceObject) {
      depths.add(tag.depth);
    } else if (tag.type === TagType.RemoveObject) {
      depths.delete(tag.depth);
    }
  }
  return depths;
}

function resolveSpriteRef(tags: readonly Tag[], movie: DynMovie, ref: SpriteRef): {index: number, sprite: DefineSprite} {
  const exports = movie.getNamedExports();
  const definitions = movie.getDefinitions();
  let resolvedId: number
  if (typeof ref === "number") {
    resolvedId = ref;
  } else {
    const refParts: readonly string[] = typeof ref === "string" ? ref.split("/") : [];
    const spriteName: string = refParts[0];
    const objectName: string | undefined = 1 < refParts.length ? refParts[1] : undefined;
    const spriteId: number | undefined = exports.get(spriteName);
    if (spriteId === undefined) {
      throw new Error(`ExportNotFound: linkageName = ${JSON.stringify(spriteName)}`);
    }
    if (objectName === undefined) {
      resolvedId = spriteId;
    } else {
      const spriteIndex = definitions.get(spriteId);
      if (spriteIndex === undefined) {
        throw new Error(`DefinitionNotFound: id = ${spriteId}`);
      }
      const tag: Tag = tags[spriteIndex];
      if (tag.type !== TagType.DefineSprite) {
        throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${tag.type} (${TagType[tag.type]})`);
      }
      let subSpriteId: number | undefined = undefined;
      for (const childTag of tag.tags) {
        if (childTag.type === TagType.PlaceObject && childTag.name === objectName) {
          subSpriteId = childTag.characterId;
        }
      }
      if (subSpriteId === undefined) {
        throw new Error(`CharacterIdNotFound: extendedLinkageName = ${JSON.stringify(ref)}`);
      }
      resolvedId = subSpriteId;
    }
  }
  const index: number | undefined = definitions.get(resolvedId);
  if (index === undefined) {
    throw new Error(`DefinitionNotFound: id = ${resolvedId}`);
  }
  const spriteTag: Tag = tags[index];
  if (spriteTag.type !== TagType.DefineSprite) {
    throw new Error(`UnexpectTagType: expected = ${TagType.DefineSprite} (DefineSprite), actual = ${spriteTag.type} (${TagType[spriteTag.type]})`);
  }
  return {index, sprite: spriteTag};
}

async function readSwf(filePath: string): Promise<Movie> {
  const buffer: Buffer = await readFile(filePath);
  return parseSwf(buffer);
}

async function readFile(filePath: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, data) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

async function writeSwf(filePath: string, movie: Movie): Promise<void> {
  const bytes: Uint8Array = emitSwf(movie, CompressionMethod.Deflate);
  return writeFile(filePath, bytes);
}

async function writeFile(filePath: string, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

main();
