package custom_pause;
import custom_pause.module.PauseModuleFactory;
import custom_pause.module.PauseModule;
import json_reader.JSONReader;
import hf.mode.GameMode;
import etwin.Obfu;

@:build(patchman.Build.di())
class CustomPauseConfig {
    private static var DEFAULT: String = Obfu.raw("default");
    public var modules: Map<String, Map<String, Array<Array<PauseModule>>>>;
    public var json_object: Map<String, Dynamic>;
    public function new(data: Dynamic) {
        modules = new Map<String, Map<String, Array<Array<PauseModule>>>>();
        json_object = new JSONReader().build(data);
    }

    public function load() {
        for(key in json_object.keys()) {
            modules[key] = loadOptions(json_object[key]);
        }
    }

    private function loadOptions(map: Map<String, Array<Array<Dynamic>>>): Map<String, Array<Array<PauseModule>>> {
        var options = new Map<String, Array<Array<PauseModule>>>();
        for(key in map.keys()) {
            options[key] = loadPlayers(map[key]);
        }
        return options;
    }

    private function loadPlayers(map: Array<Array<Dynamic>>): Array<Array<PauseModule>> {
        var players = new Array<Array<PauseModule>>();
        for(player in map) {
            players.push(loadModules(player));
        }
        return players;
    }

    private function loadModules(map: Array<Dynamic>): Array<PauseModule> {
        var modules = new Array<PauseModule>();
        for(module in map) {
            modules.push(PauseModuleFactory.get().create(module));
        }
        return modules;
    }

    public function getModules(game: GameMode): Array<Array<PauseModule>> {
        for(i in modules.keys()) {
            if(game.manager.isMode(i) ||game.manager.isMode("$" + i) ) {
                for(j in modules[i].keys()) {
                    if(game.root.GameManager.CONFIG.hasOption(j) || game.root.GameManager.CONFIG.hasOption("$" + j))
                        return modules[i][j];
                }
                return modules[i][DEFAULT] != null ? modules[i][DEFAULT] : modules[DEFAULT][DEFAULT];
            }
        }
        return modules[DEFAULT][DEFAULT];
    }

    @:diFactory
    public static function fromHml(data: patchman.module.Data): CustomPauseConfig {
        return new CustomPauseConfig(data.get(Obfu.raw("CustomPause")));
    }
}
