package custom_pause;
import etwin.Obfu;
import hf.mode.GameMode;
class KeyDescriptor {
    public static var description_font: String = "<font face=\"Franklin Gothic Heavy\" size=\"15\" color=\"#ffe6c592\">";
    public var key: KeyImage;
    public var description: String;
    public function new() {
    }

    public function display(self: GameMode, y: Int, offset: Int): Dynamic {
        var key: Dynamic = this.key.display(self, y, offset);
        key.createTextField("_label", 2,  self.root.Data.GAME_WIDTH / 2 - key._x + 10, 0 - offset, 140, 150);
        //key.createTextField("_label", 2, 90, 0 - offset, 150, 150);
        key._label.html = true;
        key._label.wordWrap = true;
        key._label.selectable = false;
        key._label.htmlText = description_font + "<b>" + description + "</b></font>";
        return key;
    }

    public function destroy() {
        key.destroy();
    }
}
