package custom_pause;

import json_reader.JSONReader;
import hf.FxManager;
import patchman.HostModuleLoader;
import patchman.IPatch;
import etwin.Obfu;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class KeyMapper {
    public static var INSTANCE: KeyMapper;
    public var keys: Map<String, String>;
    public function new(data : patchman.module.Data) {
        INSTANCE = this;
        var keys: Dynamic = data.get(Obfu.raw("KeyCodes"));
        var json = new JSONReader();
        this.keys = json.build(keys);
    }
}
