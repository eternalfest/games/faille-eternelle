package custom_pause;

import custom_pause.CustomPauseConfig;
import custom_pause.module.PauseModule;
import custom_pause.module.multi.Character;
import custom_pause.module.multi.MultiBasePage;
import custom_pause.module.multi.Sandy;
import custom_pause.module.multi.Igor;
import merlin.actions.Multi;
import custom_pause.module.solo.SoloBasePage;
import etwin.flash.Key;
import custom_pause.module.keys.EnterKey;
import custom_pause.module.keys.SpaceKey;
import custom_pause.module.keys.CrossKey;
import custom_pause.module.keys.SimpleKey;
import etwin.flash.DynamicMovieClip;
import etwin.flash.MovieClip;
import etwin.Obfu;
import etwin.Obfu;
import patchman.Ref;
import patchman.PatchList;
import patchman.IPatch;
import hf.mode.GameMode;

@:build(patchman.Build.di())
class CustomPause {
    public static var INSTANCE: CustomPause;
    public var id: Int;
    @:diExport
    public var patch(default, null): IPatch;
    public var scroll:Dynamic;
    public var left:Dynamic;
    public var right:Dynamic;
    public var pages: Array<Array<PauseModule>>;
    public var index: Int;
    public var pause: Bool;
    public var pause_screen: Dynamic;
    public var config: CustomPauseConfig;
    public function new(patches: Array<IPatch>, config: CustomPauseConfig) {
        this.config = config;
        var pauseKeyLock: Int = 0;
        INSTANCE = this;
        pause = false;
        id = 0;
        index = 0;
        patches.push(Ref.auto(GameMode.initGame).before(function(hf, self: GameMode) {
            initialize();
            pages = config.getModules(self);
        }));

        patches.push(Ref.auto(GameMode.onPause).wrap(function(hf, self: GameMode, old) {
            pauseKeyLock = -1;
            index = 0;
            if (self.fl_lock) {
                return;
            }
            self.fl_pause = true;
            pause = true;
            self.lock();
            self.world.lock();
            self.pauseMC.removeMovieClip();
            self.pauseMC = cast self.depthMan.attach("hammer_interf_instructions", self.root.Data.DP_INTERF);
            self.pauseMC.gotoAndStop("1");
            self.pauseMC._x = self.root.Data.GAME_WIDTH * 0.5;
            self.pauseMC._y = self.root.Data.GAME_HEIGHT * 0.5;
            self.pauseMC.click.text = "";
            self.pauseMC.title.text = self.root.Lang.get(5);
            self.pauseMC.move.text = self.root.Lang.get(7);
            self.pauseMC.attack.text = self.root.Lang.get(8);
            self.pauseMC.pause.text = self.root.Lang.get(9);
            self.pauseMC.space.text = self.root.Lang.get(10);
            self.pauseMC.sector.text = self.root.Lang.get(14) + "«" + self.root.Lang.getSectorName(self.currentDim, self.world.currentId) + "»";
            if (!self.fl_mute) {
                self.setMusicVolume(0.5);
            }
            var v2 = self.root.Lang.get(301 + self.tipId++);
            if (v2 == null) {
                self.tipId = 0;
                v2 = self.root.Lang.get(301 + self.tipId++);
            }
            //self.pauseMC.tip._visible = false;
            if(scroll != null)
                scroll.removeMovieClip();
            if(pause_screen != null)
                pause_screen.removeMovieClip();
            scroll = cast self.depthMan.attach(Obfu.raw("large_scroll"), self.root.Data.DP_INTERF);
            scroll._x = self.root.Data.GAME_WIDTH / 2 - scroll._width / 2;
            scroll._y = self.root.Data.GAME_HEIGHT / 2 - scroll._height / 2;
            display_arrows(self);
            pages[self.getPlayerList().length - 1][index].create_page(self);

            pause_screen = cast self.depthMan.attach("hammer_interf_instructions", self.root.Data.DP_INTERF);
            pause_screen.gotoAndStop("1");
            pause_screen._x = self.root.Data.GAME_WIDTH * 0.5;
            pause_screen._y = self.root.Data.GAME_HEIGHT * 1.5;
            pause_screen.title.text = pages[self.getPlayerList().length - 1][index].get_title(self);
            pause_screen.title._y -= self.root.Data.GAME_HEIGHT + 50;
        }));

        patches.push(Ref.auto(GameMode.onUnpause).before(function(hf, self: GameMode) {
            pause = false;
            pages[self.getPlayerList().length - 1][index].destroy();
            scroll.removeMovieClip();
            pause_screen.removeMovieClip();
            left.removeMovieClip();
            right.removeMovieClip();
        }));
        patches.push(Ref.auto(GameMode.getControls).before(function(hf, self: GameMode) {
            if(Key.isDown(Key.LEFT) && pauseKeyLock != Key.LEFT && pause && pauseKeyLock != -1) {
                pauseKeyLock = Key.LEFT;
                pages[self.getPlayerList().length - 1][index].destroy();
                index--;
                if(index == -1)
                    index++;
                pages[self.getPlayerList().length - 1][index].create_page(self);
                display_arrows(self);
                pause_screen.title.text = pages[self.getPlayerList().length - 1][index].get_title(self);
            }
            else if(Key.isDown(Key.RIGHT) && pauseKeyLock != Key.RIGHT && pause && pauseKeyLock != -1) {
                pauseKeyLock= Key.RIGHT;
                pages[self.getPlayerList().length - 1][index].destroy();
                index++;
                if(index == pages[self.getPlayerList().length - 1].length)
                    index--;
                pages[self.getPlayerList().length - 1][index].create_page(self);
                display_arrows(self);
                pause_screen.title.text = pages[self.getPlayerList().length - 1][index].get_title(self);
            }
            else if(!Key.isDown(Key.LEFT) && !Key.isDown(Key.RIGHT))
                pauseKeyLock = 0;
        }));

        this.patch = new PatchList(patches);
    }

    public function display_arrows(mode: GameMode) {
        if(left != null)
            left.removeMovieClip();
        if(index > 0) {
            left = mode.depthMan.attach("hammer_fx_exit", mode.root.Data.DP_INTERF);
            left._x = -20;
            left._y = mode.root.Data.GAME_HEIGHT / 2;
            left._rotation = 90;
            left.label._visible = false;
        }

        if(right != null)
            right.removeMovieClip();
        if(index < pages[mode.getPlayerList().length - 1].length - 1) {
            right = cast mode.depthMan.attach("hammer_fx_exit", mode.root.Data.DP_INTERF);
            right._x = mode.root.Data.GAME_WIDTH + 20;
            right._y = mode.root.Data.GAME_HEIGHT / 2;
            right._rotation = 270;
            right.label._visible = false;
        }
    }

    private function initialize() {
        config.load();
    }
}