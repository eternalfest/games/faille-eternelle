package custom_pause.module.solo;
import custom_pause.module.PauseModule;
import etwin.Obfu;
import hf.mode.GameMode;
class SoloBasePage extends PauseModule {
    public function new(data: Dynamic) {
        super();
    }

    public override function create_page(mode: GameMode): Void {
        create_cross_keys(["↑", "↓", "←", "→"], mode.root.Lang.get(10007), -150, mode, -30);
        create_space_key(mode.root.Lang.get(10008), mode.root.Lang.get(10009), -60, mode, 0);
        create_key(Obfu.raw("P"), mode.root.Lang.get(10010), -10, mode, 0);
        create_key(Obfu.raw("C"), mode.root.Lang.get(10020), 40, mode, 0);
        create_key(Obfu.raw("K"), mode.root.Lang.get(10013), 90, mode, 5);
    }
}
