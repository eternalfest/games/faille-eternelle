package custom_pause.module;
import custom_pause.module.multi.Sandy;
import custom_pause.module.multi.Igor;
import custom_pause.module.multi.MultiBasePage;
import custom_pause.module.solo.SoloBasePage;

import etwin.Obfu;
class PauseModuleFactory {
    private static var instance: PauseModuleFactory;

    private var modules: Map<String, Dynamic -> PauseModule>;
    public function new() {
        modules = new Map<String, Dynamic -> PauseModule>();
        addModule(Obfu.raw("SoloControls"), function(data: Dynamic) return new SoloBasePage(data));
        addModule(Obfu.raw("MultiControls"), function(data: Dynamic) return new MultiBasePage(data));
        addModule(Obfu.raw("Igor"), function(data: Dynamic) return new Igor(data));
        addModule(Obfu.raw("Sandy"), function(data: Dynamic) return new Sandy(data));
    }

    public function create(module: Map<String, Dynamic>): PauseModule {
        return modules[module[Obfu.raw("name")]](module);
    }

    public static function get(): PauseModuleFactory {
        if(instance == null)
            instance = new PauseModuleFactory();
        return instance;
    }

    public function addModule(key: String, lambda: Dynamic -> PauseModule) {
        modules[key] = lambda;
    }

    public function get_list(): Iterator<String> {
        return modules.keys();
    }
}
