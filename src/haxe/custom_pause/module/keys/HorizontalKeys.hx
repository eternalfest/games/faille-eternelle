package custom_pause.module.keys;
import hf.mode.GameMode;
import etwin.Obfu;
class HorizontalKeys extends KeyImage {
    public var key_values: Array<String>;
    public var keys: Array<Dynamic>;
    public function new() {
        keys = new Array<Dynamic>();
        key_values = new Array<String>();
    }

    public override function display(self: GameMode, y: Int, offset: Int): Dynamic {
        var left = create_key(self.pauseMC._x + self.pauseMC.pause._x - 110, self.pauseMC._y + self.pauseMC.pause._y + y, key_values[0], self);
        var right = create_key(left._x + 40, left._y, key_values[1], self);
        return left;
    }

    private function create_key(x: Int, y: Int, value: String, self: GameMode): Dynamic {
        var key: Dynamic = cast self.depthMan.attach(Obfu.raw("simple_key"), self.root.Data.DP_INTERF);
        key._x = x;
        key._y = y;
        key.createTextField("_key", 1, -10, 0, 50, 100);
        key._key.html = true;
        key._key.selectable = false;
        key._key.htmlText = "<p align=\"center\">" + KeyImage.key_font + "<b>" + value + "</b></font></p>";
        keys.push(key);
        return key;
    }

    public override function destroy() {
        for(i in 0...keys.length)
            keys[i].removeMovieClip();
        keys = [];
    }
}
