package custom_pause.module.keys;
import etwin.Obfu;
import hf.mode.GameMode;

class SimpleKey extends KeyImage {
    public var key: Dynamic;
    public function new() {
    }

    public override function display(self: GameMode, y: Int, offset: Int): Dynamic {
        key = cast self.depthMan.attach(Obfu.raw("simple_key"), self.root.Data.DP_INTERF);
        key._x = self.pauseMC._x + self.pauseMC.pause._x - 90;
        key._y = self.pauseMC._y + self.pauseMC.pause._y + y;
        key.createTextField("_key", 1, -10, 0, 50, 100);
        key._key.html = true;
        key._key.selectable = false;
        key._key.htmlText = "<p align=\"center\">" + KeyImage.key_font + "<b>" + value + "</b></font></p>";
        return key;
    }

    public override function destroy() {
        key.removeMovieClip();
    }
}
