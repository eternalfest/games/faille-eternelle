package custom_pause.module.keys;
import etwin.Obfu;
import hf.mode.GameMode;
class EnterKey extends KeyImage {
    public var key: Dynamic;
    public function new() {
    }

    public override function display(self: GameMode, y: Int, offset: Int): Dynamic {
        key = cast self.depthMan.attach(Obfu.raw("enter_key"), self.root.Data.DP_INTERF);
        key._x = self.pauseMC._x + self.pauseMC.pause._x - 100;
        key._y = self.pauseMC._y + self.pauseMC.pause._y + y;
        key.createTextField("_key", 1, -10, 0, 50, 100);
        key._key.html = true;
        key._key.selectable = false;
        return key;
    }

    public override function destroy() {
        key.removeMovieClip();
    }
}
