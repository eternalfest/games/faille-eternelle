package custom_pause.module;
import custom_pause.module.keys.HorizontalKeys;
import custom_pause.module.keys.VerticalKeys;
import custom_pause.module.keys.CrossKey;
import custom_pause.module.keys.SpaceKey;
import custom_pause.module.keys.EnterKey;
import custom_pause.module.keys.SimpleKey;
import etwin.Obfu;
import hf.mode.GameMode;
class PauseModule {

    public var keys: Array<KeyDescriptor>;
    public function new() {
        keys = new Array<KeyDescriptor>();
    }

    public function create_page(mode: GameMode): Void {

    }

    public function create_key(key: String, desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        k.key = new SimpleKey();
        k.key.value = key;
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function create_enter_key(desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        k.key = new EnterKey();
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function create_space_key(key: String, desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        k.key = new SpaceKey();
        k.key.value = key;
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function create_cross_keys(key_values: Array<String>, desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        var cross = new CrossKey();
        cross.key_values = key_values;
        k.key = cross;
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function create_vertical(key_values: Array<String>, desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        var cross = new VerticalKeys();
        cross.key_values = key_values;
        k.key = cross;
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function create_horizontal(key_values: Array<String>, desc: String, y: Int, self:GameMode, offset: Int) {
        var k = new KeyDescriptor();
        var cross = new HorizontalKeys();
        cross.key_values = key_values;
        k.key = cross;
        k.description = desc;
        k.display(self, y, offset);
        keys.push(k);
    }

    public function destroy() {
        for(i in 0...keys.length) {
            keys[i].destroy();
        }
        keys = [];
    }

    public function get_title(game: GameMode): String {
        return game.root.Lang.get(5);
    }
}
