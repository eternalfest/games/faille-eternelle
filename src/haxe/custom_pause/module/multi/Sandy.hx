package custom_pause.module.multi;
import etwin.flash.filters.BitmapFilter;
import etwin.Obfu;
class Sandy extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("R"), Obfu.raw("F"), Obfu.raw("D"), Obfu.raw("G"), Obfu.raw("A"), Obfu.raw("F2"), Obfu.raw("é"), Obfu.raw("Sandy"), []);
    }

    public override function get_controls(): Array<Int> {
        return [82, 70, 68, 71];//RFDG
    }

    public override function get_attack(): Int {
        return 65;
    }

    public override function get_costume(): Int {
        return 113;
    }

    public override function get_emoji(): Int {
        return 50;
    }

    public override function get_skin(): Int {
        return 2;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [];
    }

    public override function get_name(): String {
        return Obfu.raw("Sandy");
    }
}
