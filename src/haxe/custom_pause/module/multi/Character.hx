package custom_pause.module.multi;
import custom_pause.module.PauseModule;
import json_reader.JSONReader;
import etwin.flash.filters.BitmapFilter;
import etwin.Obfu;
import hf.mode.GameMode;
import hf.entity.Player;
class Character extends PauseModule {
    private var picture: Player;
    private var left: String;
    private var right: String;
    private var bottom: String;
    private var top: String;
    private var attack: String;
    private var name: String;
    private var costume: String;
    private var emoji: String;
    private var skin: Int;
    private var filter: Array<BitmapFilter>;
    public function new(top: String, bottom: String, left: String, right: String, attack: String, costume: String, emoji: String, name: String, filter: Array<BitmapFilter>) {
        super();
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
        this.attack = attack;
        this.name = name;
        this.filter = filter;
        this.costume = costume;
        this.emoji = emoji;
        this.skin = 2;
    }

    public override function create_page(mode: GameMode): Void {
        var controls: Array<Int> = get_controls();
        create_cross_keys([KeyMapper.INSTANCE.keys[Std.string(controls[0])], KeyMapper.INSTANCE.keys[Std.string(controls[1])], KeyMapper.INSTANCE.keys[Std.string(controls[2])], KeyMapper.INSTANCE.keys[Std.string(controls[3])]], "", -150, mode, -30);
        create_key(KeyMapper.INSTANCE.keys[Std.string(get_attack())], mode.root.Lang.get(10009), -60, mode, 0);
        create_key(KeyMapper.INSTANCE.keys[Std.string(get_costume())], mode.root.Lang.get(10021), 30, mode, 0);
        create_key(KeyMapper.INSTANCE.keys[Std.string(get_emoji())], mode.root.Lang.get(10022), 80, mode, 0);
        picture = cast mode.depthMan.attach("hammer_player", mode.root.Data.DP_INTERF);
        picture.gotoAndStop("1");
        picture.skin = get_skin();
        picture.defaultHead = mode.root.Data.HEAD_SANDY;
        picture.head = picture.defaultHead;
        picture._x = 260;
        picture._y = 190;
        picture.filters = get_filter();
    }

    public override function get_title(game: GameMode): String {
        return get_name();
    }

    public override function destroy() {
        super.destroy();
        picture.destroy();
        picture.removeMovieClip();
    }

    public function get_controls(): Array<Int> {
        return [0, 0, 0, 0];
    }

    public function get_attack(): Int {
        return 0;
    }

    public function get_costume(): Int {
        return 0;
    }

    public function get_emoji(): Int {
        return 0;
    }

    public function get_skin(): Int {
        return 1;
    }

    public function get_filter(): Array<BitmapFilter> {
        return [];
    }

    public function get_name(): String {
        return "";
    }
}
