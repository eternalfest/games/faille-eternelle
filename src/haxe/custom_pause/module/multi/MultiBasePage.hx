package custom_pause.module.multi;
import custom_pause.module.PauseModule;
import hf.mode.GameMode;
import etwin.Obfu;
class MultiBasePage extends PauseModule {
    public function new(data: Dynamic) {
        super();
    }

    public override function create_page(mode: GameMode): Void {
        create_key(Obfu.raw("P"), mode.root.Lang.get(10010), -150, mode, 0);
        create_key(Obfu.raw("V"), Obfu.raw("Voir les vies"), -100, mode, 0);
        create_key(Obfu.raw("N"), Obfu.raw("Voir les points"), -50, mode, 0);
        create_key(Obfu.raw(","), Obfu.raw("Voir Cristal"), 0, mode, 0);
        create_key(Obfu.raw("Z"), Obfu.raw("+ personnage (niveau 0)"), 50, mode, 5);
        create_key(Obfu.raw("E"), Obfu.raw("- personnage (niveau 0)"), 100, mode, 5);
    }
}
