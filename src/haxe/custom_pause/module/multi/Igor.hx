package custom_pause.module.multi;
import custom_pause.module.PauseModule;
import hf.entity.Physics;
import hf.entity.Player;
import hf.mode.GameMode;
import etwin.Obfu;
class Igor extends PauseModule {
    private var picture: Player;
    public function new(data: Dynamic) {
        super();
    }

    public override function create_page(mode: GameMode): Void {
        create_cross_keys(["↑", "↓", "←", "→"], "", -150, mode, -30);
        create_enter_key(mode.root.Lang.get(10009), -60, mode, -20);
        create_key(Obfu.raw("F1"), mode.root.Lang.get(10021), 30, mode, 0);
        create_key(Obfu.raw("&"), mode.root.Lang.get(10022), 80, mode, 0);

        picture = cast mode.depthMan.attach("hammer_player", mode.root.Data.DP_INTERF);
        picture.gotoAndStop("1");
        picture.skin = 1;
        picture._x = 260;
        picture._y = 190;
    }

    public override function get_title(game: GameMode): String {
        return Obfu.raw("Igor");
    }

    public override function destroy() {
        super.destroy();
        picture.destroy();
        picture.removeMovieClip();
    }
}
