package user_data;

import hf.FxManager;
import patchman.PatchList;
import patchman.Ref;
import hf.mode.GameMode;
import etwin.Obfu;
import patchman.HostModuleLoader;
import patchman.IPatch;

@:build(patchman.Build.di())
class UserData {

    private static var instance: UserData;
    public var quests: Array<QuestItem>;

    public var required_quests: Map<Int, Array<QuestItem>>;

    public var inventory:Inventory;
    @:diExport
    public var patch(default, null): IPatch;
    public function new(patches: Array<IPatch>, hml: HostModuleLoader, data : patchman.module.Data) {
        instance = this;
        quests = new Array<QuestItem>();
        required_quests = new Map<Int, Array<QuestItem>>();
        var loaded: Bool = false;
        var quests: Array<Dynamic> = data.get(Obfu.raw("Quests"));
        patches.push(Ref.auto(hf.mode.GameMode.pickUpSpecial).after(function(hf, self: GameMode, id: Int, result: Int) {
            if(inventory.add_item(id))
                attachLevelPop(self.root.Lang.get(10000), self.root.Lang.getItemName(id), self.fxMan);
            quest_notification(id, self.fxMan);
        }));
        patches.push(Ref.auto(hf.mode.GameMode.pickUpScore).after(function(hf, self: GameMode, id: Int, sub: Int, result: Int) {
            if(inventory.add_item(id + 1000))
                attachLevelPop(self.root.Lang.get(10000), self.root.Lang.getItemName(id + 1000), self.fxMan);
            quest_notification(id + 1000, self.fxMan);
        }));

        patches.push(Ref.auto(hf.mode.GameMode.initWorld).before(function(hf, self: GameMode) {
            if(!loaded) {
                inventory = new Inventory(get_inventory(hml), self);
                loaded = true;
                for(quest in quests) {
                    var item = new QuestItem();
                    item.id = Std.parseInt(Reflect.field(quest, Obfu.raw("id")));
                    item.name = Reflect.field(quest, Obfu.raw("name"));
                    item.description = Reflect.field(quest, Obfu.raw("description"));
                    var require: Dynamic = Reflect.field(quest, Obfu.raw("require"));
                    for(i in Reflect.fields(require)) {
                        var n : Int = Reflect.field(require, i);
                        var key: Int = Std.parseInt(i);
                        item.require[key] = n;
                        if(required_quests[key] == null)
                            required_quests[key] = new Array<QuestItem>();
                        required_quests[key].push(item);
                    }

                    var give: Dynamic = Reflect.field(quest, Obfu.raw("give"));
                    var remove: Dynamic = Reflect.field(quest, Obfu.raw("remove"));

                    for(i in 0...Reflect.fields(give).length) {
                        var structure : Dynamic = Reflect.field(give, cast i);
                        var reward = new RewardItem();
                        for(j in Reflect.fields(structure)) {
                            var val : String = Reflect.field(structure, j);
                            var key: String = j;
                            reward.reward[key] = val;
                        }
                        item.give.push(reward);
                    }

                    for(i in 0...Reflect.fields(remove).length) {
                        var structure : Dynamic = Reflect.field(remove, cast i);
                        var reward = new RewardItem();
                        for(j in Reflect.fields(structure)) {
                            var val : String = Reflect.field(structure, j);
                            var key: String = j;
                            reward.reward[key] = val;
                        }
                        item.remove.push(reward);
                    }
                    this.quests.push(item);
                }
            }
        }));

        this.patch = new PatchList(patches);
    }


    public static function get_inventory(hml: HostModuleLoader): Map<Int, Int> {
        var start: Dynamic = hml.require("run-start");
        var data: Null<Dynamic> = Reflect.callMethod(start, Reflect.field(start, Obfu.raw("get")), []);
        if (data == null) {
            return null;
        }
        var items: Dynamic = Reflect.field(data, Obfu.raw("items"));
        var map: Map<Int, Int> = new Map<Int, Int>();
        for(j in Reflect.fields(items)) {
            var val : Int = Std.parseInt(Reflect.field(items, j));
            var key: Int = Std.parseInt(j);
            map[key] = val;
        }
        return map;
    }

    public static function get(): UserData {
        return instance;
    }

    public function get_completed_quests(): Array<QuestItem> {
        var array: Array<QuestItem> = new Array<QuestItem>();
        for(i in 0...quests.length) {
            quests[i].state = QuestItem.STATE_BASE;
            if(quests[i].get_state(inventory) == QuestItem.STATE_OVER)
                array.push(quests[i]);
        }
        return array;
    }

    public function get_started_quests(): Array<QuestItem> {
        var array: Array<QuestItem> = new Array<QuestItem>();
        for(i in 0...quests.length) {
            quests[i].state = QuestItem.STATE_BASE;
            if(quests[i].get_state(inventory) == QuestItem.STATE_STARTED)
                array.push(quests[i]);
        }
        return array;
    }

    public function quest_notification(id: Int, man: FxManager): Void {
        var quest: Null<QuestItem> = null;
        var current: Int = QuestItem.STATE_BASE;
        var message = 10000;
        var list = required_quests[id];
        if(list != null) {
            for(i in 0...list.length) {
                var old_state = list[i].state;
                list[i].state = QuestItem.STATE_BASE;
                var state:Int = list[i].get_state(inventory);
                if(old_state != state) {
                    if(state > current) {
                        current = state;
                        quest = list[i];
                        message = (state == QuestItem.STATE_OVER) ? 10003 : 10001;
                    }
                    //Commencée ou terminée
                }
                else if(inventory.get(id) <= list[i].require[id]) {
                    //Progres
                    if(state > current) {
                        current = state;
                        quest = list[i];
                        message = 10002;
                    }
                }
            }
        }
        if(current > QuestItem.STATE_NOT_STARTED) {
            attachLevelPop(man.game.root.Lang.get(message), quest.name, man);
        }

    }

    public function attachLevelPop(label: String, name: String, self: FxManager): Void {
        if (name != null) {
            self.levelName.removeMovieClip();
            self.levelName = ((cast self.game.depthMan.attach("hammer_interf_zone", self.game.root.Data.DP_INTERF)): etwin.flash.DynamicMovieClip);
            self.levelName._x = -10.;
            self.levelName._y = cast self.game.root.Data.GAME_HEIGHT - 1;
            self.levelName.field.text = name;
            self.game.root.FxManager.addGlow(self.levelName, 0, 2);
            self.levelName.label.text = label;
            self.nameTimer = cast self.game.root.Data.SECOND * 5;
        }
    }

}
