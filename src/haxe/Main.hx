import bugfix.Bugfix;
import debug.Debug;
import faille.Items;
import faille.Bads;
import faille.Actions;
import faille.FilterItem;
import faille.LevelJump;
import faille.DisplayTexts;
import faille.DeathCount;
import faille.LevelCount;
import faille.StarCount;
import faille.QuestBottomBar;
import faille.QuestTopHat;
import faille.QuestCoffinDanceHat;
import faille.QuestPartyHat;
import faille.QuestEtoiles;
import faille.FireBallTimerCustom;
import faille.ChangeHat;
import faille.NonRandomfestReducedLives;
import faille.Pic;
import faille.Disapear;
import faille.WhiteFading;
import faille.FadingScreen;
import faille.FadingIgor;
import faille.StuckDim;
import faille.TimeStar;
import faille.FixLag;
import faille.Kill;
import faille.DisabledControls;
import faille.SpinningIgor;
import faille.SpawnVortexes;
import faille.SpawnSimpleVortex;
import faille.CustomBugFix;
import faille.boss.EternalSadistSpirit;
import faille.boss.EternalSadistReborn;
import faille.boss.CustomPlayerHitbox;
import faille.custom_map.CustomMap;
import faille.ReducedFireballTimer;
import faille.ChangeRingCooldown;
import faille.LevelWarp;
import bottom_bar.modules.BottomBarModuleFactory;
import bottom_bar.BottomBar;
import custom_pause.CustomPause;
import custom_pause.KeyMapper;
import party.Party;
import quests.QuestManager;
import item_browser.CustomItemBrowser;
import user_data.UserData;
import quest_browser.CustomQuestBrowser;
import atlas.Atlas;
import better_script.BetterScript;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;
import vault.Vault;
import game_params.GameParams;
import shadow_clones.ShadowClones;
import randomfest.Randomfest;
import etwin.Obfu;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    items: Items,
    bads: Bads,
    actions: Actions,
    pic: Pic,
    customBugFix: CustomBugFix,
    changeHat: ChangeHat,
    reducedFireballTimer: ReducedFireballTimer,
    filteritem: FilterItem,
    leveljump: LevelJump,
    displayTexts: DisplayTexts,
    disapear: Disapear,
    whiteFading: WhiteFading,
    fadingScreen: FadingScreen,
    fadingIgor: FadingIgor,
    stuckDim: StuckDim,
    fixLag: FixLag,
    kill: Kill,
    timeStar: TimeStar,
    disabledControls: DisabledControls,
    spinningIgor: SpinningIgor,
    spawnVortexes: SpawnVortexes,
    spawnSimpleVortex: SpawnSimpleVortex,
    levelWarp: LevelWarp,
    wellBoss: EternalSadistSpiritManager,
    labyrinthBoss: EternalSadistRebornManager,
    atlasBoss: atlas.props.Boss,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    customPlayerHitbox: CustomPlayerHitbox,
    changeRingCooldown: ChangeRingCooldown,
    bottomBar:BottomBar,
    randomfest: Randomfest,
    vault: Vault,
    game_params: GameParams,
    shadow_clones: ShadowClones,
    betterScript: BetterScript,
    pause: CustomPause,
    customMap: CustomMap,
    party: Party,
    mapper: KeyMapper,
    quests: QuestManager,
    items_browser: CustomItemBrowser,
    quests_browser: CustomQuestBrowser,
    merlin: Merlin,
    user_data: UserData,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    BottomBarModuleFactory.get().addModule(Obfu.raw("DeathCount"), function(data: Dynamic) return new DeathCount(data));
    BottomBarModuleFactory.get().addModule(Obfu.raw("LevelCount"), function(data: Dynamic) return new LevelCount(data));
    BottomBarModuleFactory.get().addModule(Obfu.raw("StarCount"), function(data: Dynamic) return new StarCount(data));
    BottomBarModuleFactory.get().addModule(Obfu.raw("FireBallTimerCustom"), function(data: Dynamic) return new FireBallTimerCustom(data));
    BottomBarModuleFactory.get().addModule(Obfu.raw("IgnoringRandomfestCustomLives"), function(data: Dynamic) return new NonRandomfestReducedLives(data, randomfest));
    quests.register_reward(Obfu.raw("fireBallTimer"), new QuestBottomBar());
    quests.register_reward(Obfu.raw("topHat"), new QuestTopHat());
    quests.register_reward(Obfu.raw("coffinDanceHat"), new QuestCoffinDanceHat());
    quests.register_reward(Obfu.raw("partyHat"), new QuestPartyHat());
    quests.register_reward(Obfu.raw("etoiles"), new QuestEtoiles());
    Patchman.patchAll(patches, hf);
  }
}
