package item_browser;
import custom_pause.module.PauseModule;
import etwin.Obfu;
import hf.mode.GameMode;
class ObjectPause extends PauseModule {
    public function new(data: Dynamic) {
        super();
    }

    public override function create_page(mode: GameMode): Void {
        create_key(Obfu.raw("O"), mode.root.Lang.get(10012), -150, mode, 0);
        create_horizontal(["←", "→"], mode.root.Lang.get(10040), -100, mode, 0);
        create_space_key(mode.root.Lang.get(10008), mode.root.Lang.get(10041), -50, mode, 0);
    }

    public override function get_title(game: GameMode): String {
        return game.root.Lang.get(10019);
    }
}
