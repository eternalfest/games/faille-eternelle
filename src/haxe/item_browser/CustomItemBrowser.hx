package item_browser;
import custom_pause.module.PauseModuleFactory;
import etwin.flash.MovieClip;
import etwin.Obfu;
import user_data.UserData;
import hf.Mode;
import hf.mode.ItemBrowser;
import hf.Hf;
import etwin.flash.Key;
import patchman.Ref;
import hf.mode.GameMode;
import patchman.IPatch;
import patchman.PatchList;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
@:build(patchman.Build.di())
class CustomItemBrowser {
    @:diExport
    public var patch(default, null): IPatch;

    public var mode: GameMode;
    public var can_quit: Bool;
    public var out: Bool;
    private var hue: Int = 0;
    private var mc: MovieClip = null;
    public function new(patches: Array<IPatch>) {
        PauseModuleFactory.get().addModule(Obfu.raw("ObjectPause"), function(data: Dynamic) return new ObjectPause(data));

        patches.push(Ref.auto(GameMode.getControls).before(function(hf: Hf, self: GameMode): Void {
            if (Key.isDown(79) && out && !self.fl_pause && !self.fl_lock) {
                out = false;
                mode = self;
                self.manager.current.lock();
                self.manager.current.onSleep();
                self.manager.current.hide();
                self.manager.current = hf.mode.ItemBrowser._new(self.manager);
                var browser: ItemBrowser = cast self.manager.current;
                browser.footer._y -= 2;
            }
            else if (!Key.isDown(79))
                out = true;
        }));


        patches.push(Ref.auto(ItemBrowser.endMode).wrap(function(hf: Hf, self: ItemBrowser, old): Void {
            can_quit = false;
            var manager = self.manager;
            manager.current.destroy();
            manager.current = mode;
            if(Key.isDown(80))
                mode.keyLock = 80;
            if(Key.isDown(67))
                mode.keyLock = 67;
            manager.current.unlock();
            manager.current.show();
        }));

        patches.push(Ref.auto(ItemBrowser.main).wrap(function(hf: Hf, self: ItemBrowser, old): Void {
            Reflect.callMethod(self, untyped hf.Mode.prototype.main, []);
            if(!Key.isDown(79) && !can_quit)
                can_quit = true;
            else if((Key.isDown(79) && can_quit) || Key.isDown(80) || Key.isDown(67))
                self.endMode();
            else {
                if (self.klock != null && !Key.isDown(self.klock)) {
                    self.klock = null;
                }
                if (Key.isDown(Key.LEFT) && self.page > 0 && self.klock != Key.LEFT) {
                    --self.page;
                    self.refresh();
                    self.klock = Key.LEFT;
                }
                if (Key.isDown(Key.RIGHT) && self.page < self.max_pages - 1 && self.klock != Key.RIGHT) {
                    ++self.page;
                    self.refresh();
                    self.klock = Key.RIGHT;
                }
                if (Key.isDown(Key.SPACE) && self.klock != Key.SPACE) {
                    self.fl_score = !self.fl_score;
                    self.ffilter = -1;
                    self.page = 0;
                    self.klock = Key.SPACE;
                    self.refresh();
                }
                if (self.fl_names != Key.isDown(78)) {
                    self.fl_names = Key.isDown(78);
                    self.refresh();
                }
                if (Key.isDown(Key.PGUP) && self.ffilter > -1 && self.klock != Key.PGUP) {
                    --self.ffilter;
                    self.refresh();
                    self.klock = Key.PGUP;
                }
                if (Key.isDown(Key.PGDN) && self.klock != Key.PGDN) {
                    ++self.ffilter;
                    self.refresh();
                    self.klock = Key.PGDN;
                }
            }
            this.hue += 3;
            if(this.hue > 360) this.hue = 0;
            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(this.hue, 1, 1).toFilter();
            mc.filters = [filter];
        }));

        patches.push(Ref.auto(ItemBrowser.onOver).wrap(function(hf: Hf, self: ItemBrowser, id, old): Void {
            var owned = UserData.get().inventory.get(id);
            if(owned == null)
                owned = 0;
            self.header.field.text = owned < 10 ? "???" : self.manager.root.Lang.getItemName(id);
        }));

        patches.push(Ref.auto(ItemBrowser.refresh).wrap(function(hf: Hf, self: ItemBrowser, old): Void {
            hf.entity.Item = cast self.depthMan.attach(Obfu.raw("fridge_bg"), self.manager.root.Data.DP_BACK_LAYER);
            var offset;
            var sprite;
            var keys: Array<Int>;
            for (i in 0...self.mcList.length) {
                self.mcList[i].removeMovieClip();
            }
            self.mcList = new Array();
            if (self.fl_score) {
                sprite = "hammer_item_score";
                keys =  UserData.get().inventory.get_score_keys();
                offset = 1000;
            } else {
                offset = 0;
                sprite = "hammer_item_special";
                keys =  UserData.get().inventory.get_special_keys();
            }
            // Change la place du dr pepper dans le frigo
            if(keys.indexOf(1304) != -1) {
                var pepper = false;
                keys.remove(1304);
                var keys2: Array<Int> = keys;
                keys = [];
                for(i in keys2) {
                    if(i > 1247 && !pepper) {
                        keys.push(1304);
                        pepper = true;
                    } 
                    keys.push(i);
                }
            }
            self.max_pages = Math.ceil(keys.length/*self.manager.root.Data.MAX_ITEMS*/ / 40);// self.manager.root.mode.ItemBrowser.PAGE_LENGTH);
            var v5 = self.manager.root.mode.ItemBrowser.ITEM_WIDTH / 2 + 10;
            var v6 = self.manager.root.mode.ItemBrowser.ITEM_HEIGHT / 6 - 5;
            var index = Math.floor(self.page * 40);
            var base = index;
            while (index < keys.length && index < base + 40) {
                var owned = UserData.get().inventory.get(keys[index]);
                if(owned == null)
                    owned = 0;
                if(owned > 0) {
                    var v8: hf.entity.Item = cast self.depthMan.attach(sprite, self.manager.root.Data.DP_ITEMS);
                    var idCustomObject: Int = keys[index];
                    switch (keys[index]) {
                        case 1288:
                            keys[index] = 1169;
                        case 1289:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-35, 1, 1.3).toFilter();
                            v8.filters = [filter];
                        case 1290:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-60, 1, 1.8).toFilter();
                            v8.filters = [filter];
                        case 1291:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-136, 1, 1).toFilter();
                            v8.filters = [filter];
                        case 1292:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(184, 1, 1.2).toFilter();
                            v8.filters = [filter];
                        case 1293:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(142, 1, 1).toFilter();
                            v8.filters = [filter];
                        case 1294:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(56, 1, 1).toFilter();
                            v8.filters = [filter];
                        case 1295 :
                            keys[index] = 1051;
                            v8._xscale = 130;
                            v8._yscale = 130;
                        case 1296 :
                            keys[index] = 1179;
                        case 1297 :
                            keys[index] = 1180;
                        case 1298 :
                            keys[index] = 1183;
                        case 1299 :
                            keys[index] = 1178;
                        case 1300 :
                            keys[index] = 1181;
                        case 1301 :
                            keys[index] = 1182;
                        case 146 :
                            keys[index] = 103;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(200, 1, 1).toFilter();
                            v8.filters = [filter];
                        case 1303 :
                            keys[index] = 1169;
                            this.mc = v8;
                        case 1305:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(0, 1, 0.5).toFilter();
                            v8.filters = [filter];
                        case 1306:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-35, 1, 0.8).toFilter();
                            v8.filters = [filter];
                        case 1307:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-60, 1, 1.3).toFilter();
                            v8.filters = [filter];
                        case 1308:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-136, 1, 0.5).toFilter();
                            v8.filters = [filter];
                        case 1309:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(184, 1, 0.7).toFilter();
                            v8.filters = [filter];
                        case 1310:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(142, 1, 0.5).toFilter();
                            v8.filters = [filter];
                        case 1311:
                            keys[index] = 1169;
                            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(56, 1, 0.5).toFilter();
                            v8.filters = [filter];
                    }
                    v8.blendMode = self.manager.root.BlendMode.LAYER;
                    v8._x = v5 + self.manager.root.mode.ItemBrowser.ITEM_WIDTH * 0.5;
                    v8._y = v6 + self.manager.root.mode.ItemBrowser.ITEM_HEIGHT - 7;
                    v8.gotoAndStop("" + (keys[index] - offset + 1));
                    v8.onRollOver = self.manager.root.Std.callback(self, "onOver", idCustomObject);
                    v8.sub.stop();
                    if (keys[index] - offset + 1 > v8._totalframes) {
                        v8._visible = false;
                    }
                    self.mcList.push(v8);
                    if (self.fl_names) {
                        if (!(self.fl_score && self.manager.root.Data.ITEM_VALUES[idCustomObject] == null) && !(!self.fl_score && self.manager.root.Data.FAMILY_CACHE[idCustomObject] == null)) {
                            var item_name = (owned < 10 ? "???" : self.manager.root.Lang.getItemName(idCustomObject));
                            if (item_name != null || item_name == "") {
                                var name_clip = self.manager.root.Std.createTextField(v8, self.manager.uniq++);
                                name_clip.border = true;
                                name_clip.background = true;
                                if (item_name == null) {
                                    name_clip.borderColor = 16711680;
                                    name_clip.backgroundColor = 7798784;
                                } else {
                                    name_clip.borderColor = 11184810;
                                    name_clip.backgroundColor = 0;
                                }
                                name_clip._x = -20;
                                name_clip._y = -35;
                                name_clip._width = 60;
                                name_clip._height = 40;
                                name_clip.textColor = 16777215;
                                name_clip.text = item_name;
                                name_clip.wordWrap = true;
                                name_clip.selectable = false;
                                name_clip._xscale = 90;
                                name_clip._yscale = name_clip._xscale;
                            }
                        }
                    } else {
                        if (self.ffilter < 0) {
                            if (self.fullRandList[idCustomObject] == null) {
                                v8._alpha = 75.;
                            }
                        } else {
                            if (self.ffilter + offset != self.manager.root.Data.FAMILY_CACHE[idCustomObject]) {
                                var v11 = new etwin.flash.filters.BlurFilter();
                                v11.blurX = 8.;
                                v11.blurY = v11.blurX;
                                v8.filters = [v11];
                                v8._alpha = 40.;
                            }
                        }
                    }
                    var v12: etwin.flash.DynamicMovieClip = cast self.depthMan.attach("hammer_editor_simple_label", self.manager.root.Data.DP_INTERF);
                    v12._x = v5 + self.manager.root.mode.ItemBrowser.ITEM_WIDTH * 0.5;
                    v12._y = v6 + self.manager.root.mode.ItemBrowser.ITEM_HEIGHT;
                    var quantity = v12.field;

                    if (owned == 0) {
                        v8._visible = false;
                        quantity.text = "";
                    }
                    else {
                        quantity.text = "" + owned;
                    }
                    quantity.textColor = 0;
                    self.mcList.push(v12);
                    v5 += self.manager.root.mode.ItemBrowser.ITEM_WIDTH;
                    if (v5 >= self.manager.root.Data.DOC_WIDTH - self.manager.root.mode.ItemBrowser.ITEM_WIDTH) {
                        v5 = self.manager.root.mode.ItemBrowser.ITEM_WIDTH / 2 + 10;
                        v6 += self.manager.root.mode.ItemBrowser.ITEM_HEIGHT;
                    }
                }
                ++index;
            }
            self.footer.field.text = "";
            if (self.fl_score) {
                self.footer.field.text += self.root.Lang.get(10014) + ", ";
            } else {
                self.footer.field.text += self.root.Lang.get(10015) + ", ";
            }
            self.footer.field.text += self.root.Lang.get(10017) + Obfu.raw(" ") + (self.page + 1) + "/" + Math.max(1, self.max_pages);
            self.footer.scriptIcon._visible = false;
        }));
        this.patch = new PatchList(patches);
    }
}
