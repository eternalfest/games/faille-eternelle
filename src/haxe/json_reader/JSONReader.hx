package json_reader;

class JSONReader {
    public function new() {
    }

    public function build(object: Dynamic): Dynamic {
        if(Std.is(object, Array)) {
            var items: Array<Dynamic> = cast object;
            var array: Array<Dynamic> = new Array<Dynamic>();
            for(i in items) {
                array.push(build(i));
            }
            if(array.length > 0)
                return array;
        }
        var o: Map<String, Dynamic> = new Map<String, Dynamic>();
        var is_o: Bool = false;
        for(i in Reflect.fields(object)) {
            is_o = true;
            o[i] = build(Reflect.field(object, i));
        }
        if(is_o)
            return o;
        return object;
    }
}
