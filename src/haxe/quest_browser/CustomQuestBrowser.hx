package quest_browser;


import custom_pause.module.PauseModuleFactory;
import etwin.flash.Key;
import hf.mode.GameMode;
import user_data.QuestItem;
import user_data.UserData;
import patchman.HostModuleLoader;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;
import hf.Hf;
import hf.mode.QuestBrowser;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import patchman.Ref;
import patchman.PatchList;
import patchman.IPatch;

@:build(patchman.Build.di())
class CustomQuestBrowser {

    @:diExport
    public var patch(default, null): IPatch;
    public var mode: GameMode;
    public var type: Bool;
    public var can_quit: Bool;
    public var out: Bool;
    public var left:Dynamic;
    public var right:Dynamic;
    public function new(patches: Array<IPatch>) {
        PauseModuleFactory.get().addModule(Obfu.raw("QuestPause"), function(data: Dynamic) return new QuestPause(data));
        type = false;

        patches.push(Ref.auto(GameMode.getControls).before(function(hf: Hf, self: GameMode): Void {
            if ((Key.isDown(81) && out && !self.fl_pause && !self.fl_lock)) {
                out = false;
                mode = self;
                self.manager.current.lock();
                self.manager.current.onSleep();
                self.manager.current.hide();
                self.manager.current = hf.mode.QuestBrowser._new(self.manager);
            }
            else if (!Key.isDown(81)){
                out = true;
            }
        }));


        patches.push(Ref.auto(QuestBrowser.endMode).wrap(function(hf: Hf, self: QuestBrowser, old): Void {
            can_quit = false;
            var manager = self.manager;
            manager.current.destroy();
            manager.current = mode;
            if(Key.isDown(80))
                mode.keyLock = 80;
            if(Key.isDown(67))
                mode.keyLock = 67;
            manager.current.unlock();
            manager.current.show();
        }));

        patches.push(Ref.auto(QuestBrowser.main).wrap(function(hf: Hf, self: QuestBrowser, old): Void {
            if (Key.isDown(Key.RIGHT) && self.klock != Key.RIGHT) {
                var quests: Array<QuestItem> = (!type) ? UserData.get().get_started_quests() : UserData.get().get_completed_quests();
                if(self.qfilter < quests.length - 1) {
                    ++self.qfilter;
                    self.refresh();
                    self.klock = Key.RIGHT;
                }
            }
            else if(!Key.isDown(81) && !can_quit)
                can_quit = true;
            else if((Key.isDown(81) && can_quit) || Key.isDown(80) || Key.isDown(67))
                self.endMode();
            else if (Key.isDown(Key.SPACE) && self.klock != Key.SPACE) {
                this.type = !this.type;
                self.qfilter = 0;
                self.klock = Key.SPACE;
                self.refresh();
            }
            else
                old(self);
        }));


        patches.push(Ref.auto(QuestBrowser.refresh).wrap(function(hf: Hf, self: QuestBrowser, old): Void {
            var background: Dynamic = cast self.depthMan.attach(Obfu.raw("quest_bg"), self.manager.root.Data.DP_BACK_LAYER);
            var item_font = "<font face=\"Verdana\" size=\"10\" color=\"#e6be86\" letterSpacing=\"0.000000\" kerning=\"0\">";
            var name_font = "<font face=\"Verdana\" size=\"14\" color=\"#e6be86\" letterSpacing=\"0.000000\" kerning=\"0\">";
            var description_font = "<font face=\"Verdana\" size=\"12\" color=\"#e6be86\" letterSpacing=\"0.000000\" kerning=\"0\">";
            background.createTextField("_label", 2, 100, 45, 230, 150);
            background.createTextField("_description", 3, 30, 440, self.root.Data.GAME_WIDTH - 40, 150);
            //key.createTextField("_label", 2, 90, 0 - offset, 150, 150);
            background._label.html = true;
            background._label.wordWrap = true;
            background._label.selectable = false;
            background._description.html = true;
            background._description.wordWrap = true;
            background._description.selectable = false;

            for (i in 0...self.mcList.length) {
                self.mcList[i].removeMovieClip();
            }
            /*
            var started = UserData.get().get_started_quests();
            var ended = UserData.get().get_completed_quests();
            var quests: Array<QuestItem> = new Array<QuestItem>();
            for(i in started)
                quests.push(i);
            for(i in ended)
                quests.push(i);*/
            var quests: Array<QuestItem> = (!type) ? UserData.get().get_started_quests() : UserData.get().get_completed_quests();

            self.mcList = new Array();
            var cpt = 0;
            var require: Map<Int, Int> = quests[self.qfilter].require;
            var clip: Dynamic = null;
            display_arrows(self);
            for(item in require.keys()) {
                var id: Int;
                id = cast item;
                var quantity = require[item];
                var owned = UserData.get().inventory.get(item);
                var unlock: Bool = false;
                if(owned != null)
                    unlock = owned >= 10;
                if(owned == null)
                    owned = 0;
                if(owned > quantity)
                    owned = quantity;
                var sprite = id < 1000 ? "hammer_item_special" : "hammer_item_score";
                clip = self.depthMan.attach(sprite, self.manager.root.Data.DP_ITEMS);
                clip.blendMode = self.manager.root.BlendMode.LAYER;
                clip._x = 20 + 70; //TODO: find constant
                clip._y = 50 + cpt * (self.manager.root.mode.QuestBrowser.ITEM_HEIGHT * 1.5) + 110; //TODO: find constant
                clip._xscale = 75;
                clip._yscale = clip._xscale;
                var text = self.manager.root.Std.createTextField(clip, self.manager.uniq++);
                text._x = self.manager.root.mode.QuestBrowser.ITEM_WIDTH;  //TODO: find constant
                text._y = -30;
                text._width = 350;
                text._height = self.manager.root.mode.QuestBrowser.ITEM_HEIGHT;
                text.html = true;
                var textName = self.manager.root.Std.createTextField(clip, self.manager.uniq++);
                textName._x = self.manager.root.mode.QuestBrowser.ITEM_WIDTH + 80;
                textName._y = -30;
                textName._width = 350;
                textName._height = self.manager.root.mode.QuestBrowser.ITEM_HEIGHT;
                textName.html = true;
                if(owned >= quantity) {
                    text.htmlText = item_font +"<b>"+ owned + " / " + quantity + "</b></font>";
                    textName.htmlText = item_font +"<b>"+ (unlock ? self.manager.root.Lang.getItemName(id) : Obfu.raw("???")) + "</b></font>";
                } else {
                    text.htmlText = item_font + owned + " / " + quantity + "</font>";
                    textName.htmlText = item_font + (unlock ? self.manager.root.Lang.getItemName(id) : Obfu.raw("???")) + "</font>";
                }
                text.wordWrap = false;
                text.selectable = false;
                text._xscale = 250 - clip._xscale;
                text._yscale = text._xscale;
                textName.wordWrap = false;
                textName.selectable = false;
                textName._xscale = 250 - clip._xscale;
                textName._yscale = text._xscale;
                ++cpt;
                switch (id) {
                    case 1288:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(1, 1, 1).toFilter();
                        clip.filters = [filter];
                    case 1289:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-35, 1, 1.3).toFilter();
                        clip.filters = [filter];
                    case 1290:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-60, 1, 1.8).toFilter();
                        clip.filters = [filter];
                    case 1291:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-136, 1, 1).toFilter();
                        clip.filters = [filter];
                    case 1292:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(184, 1, 1.2).toFilter();
                        clip.filters = [filter];
                    case 1293:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(142, 1, 1).toFilter();
                        clip.filters = [filter];
                    case 1294:
                        id = 1169;
                        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(56, 1, 1).toFilter();
                        clip.filters = [filter];
                }
                if (id >= 1000) {
                    clip.gotoAndStop("" + (id - 1000 + 1));
                } else {
                    clip.gotoAndStop("" + (id + 1));
                }
                self.mcList.push(clip);
            }

            var name: String = quests[self.qfilter] != null ? quests[self.qfilter].name : self.manager.root.Lang.get(10004);
            var d: String = quests[self.qfilter] != null ? quests[self.qfilter].description : self.manager.root.Lang.get(type  ? 10042 : 10043);
            var description: String = quests[self.qfilter] != null ? (quests[self.qfilter].state == QuestItem.STATE_OVER ? d : self.manager.root.Lang.get(10006)) : d;
            /*
            self.desc.htmlText = "<p align=\"left\"><font face=\"Verdana\" size=\"10\" color=\"#e6be86\" letterSpacing=\"0.000000\" kerning=\"0\">" + description + "</p>";
            self.footer.field.text = name;
            self.footer.field.textColor = (quests[self.qfilter].state == QuestItem.STATE_OVER ? 65280 : 4474111);*/
            self.footer.scriptIcon._visible = false;
            self.footer._visible = false;

            background._label.htmlText = name_font + "<p align=\"center\"><b>" + name + "</b></p></font>";
            background._description.htmlText = description_font + "<p align=\"justify\">" + description + "</b></font>";
            background._label._y -= (background._label.textHeight - 17) / 2;
        }));

        this.patch = new PatchList(patches);
    }

    public function get(key: String, object: Dynamic): Dynamic {
        return Reflect.field(object, key);
    }

    public function display_arrows(mode: QuestBrowser) {

        var quests: Array<QuestItem> = (!type) ? UserData.get().get_started_quests() : UserData.get().get_completed_quests();
        if(left != null)
            left.removeMovieClip();
        if(mode.qfilter > 0) {
            left = mode.depthMan.attach("hammer_fx_exit", mode.root.Data.DP_INTERF);
            left._x = 0;
            left._y = mode.root.Data.GAME_HEIGHT / 2;
            left._rotation = 90;
            left.label._visible = false;
        }

        if(right != null)
            right.removeMovieClip();
        if(mode.qfilter < quests.length - 1) {
            right = cast mode.depthMan.attach("hammer_fx_exit", mode.root.Data.DP_INTERF);
            right._x = mode.root.Data.GAME_WIDTH + 20;
            right._y = mode.root.Data.GAME_HEIGHT / 2;
            right._rotation = 270;
            right.label._visible = false;
        }
    }
}
