package quest_browser;
import custom_pause.module.PauseModule;
import etwin.Obfu;
import hf.mode.GameMode;
class QuestPause extends PauseModule {
    public function new(data: Dynamic) {
        super();
    }

    public override function create_page(mode: GameMode): Void {
        create_key(Obfu.raw("Q"), mode.root.Lang.get(10011), -150, mode, 0);
        create_vertical(["↑", "↓"], mode.root.Lang.get(10030), -100, mode, -30);
        create_horizontal(["←", "→"], mode.root.Lang.get(10031), -10, mode, 0);
        create_space_key(mode.root.Lang.get(10008), mode.root.Lang.get(10032), 40, mode, 0);
    }

    public override function get_title(game: GameMode): String {
        return game.root.Lang.get(10018);
    }
}
