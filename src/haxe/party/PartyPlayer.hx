package party;
import etwin.flash.filters.BitmapFilter;
import etwin.flash.filters.ColorMatrixFilter;

class PartyPlayer {
    public var skin: Int;
    public var controls: Array<Int>;
    public var color: BitmapFilter;
    public var name: String;
    public var costume_key: Int;
    public var emoji_key: Int;
    public function new(name: String, skin: Int, controls: Array<Int>, color: BitmapFilter, emoji: Int, costume: Int) {
        this.skin = skin;
        this.name = name;
        this.color = color;
        this.controls = controls;
        this.costume_key = costume;
        this.emoji_key = emoji;
    }
}
