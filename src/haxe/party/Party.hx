package party;

import party.pause.Emoji;
import custom_pause.module.PauseModuleFactory;
import party.pause.Blue;
import party.pause.Green;
import party.pause.Cyan;
import party.pause.Black;
import party.pause.Gray;
import party.pause.Pink;
import custom_pause.CustomPauseConfig;
import party.players.GrayPlayer;
import party.players.BlackPlayer;
import party.players.CyanPlayer;
import party.players.GreenPlayer;
import party.players.BluePlayer;
import party.players.PinkPlayer;
import etwin.flash.MovieClip;
import party.bottom_bar.PScore;
import party.bottom_bar.PInterface;
import bottom_bar.modules.BottomBarModuleFactory;
import bottom_bar.BottomBar;
import etwin.Obfu;
import hf.mode.Adventure;
import hf.mode.MultiCoop;
import etwin.flash.Key;
import hf.Hf;
import hf.mode.MultiCoop;
import hf.mode.GameMode;
import patchman.Ref;
import patchman.PatchList;
import patchman.IPatch;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Party {
    @:diExport
    public var patch(default, null): IPatch;
    public var players: Array<PartyPlayer>;
    private var key_map: Map<Int, Bool>;
    public var score: Int;
    private var count: Map<Int, Int>;
    private var value: Map<Int, Int>;
    private var timer: Map<Int, Bool>;
    private var emojis: Map<Int, MovieClip>;

    public function pop_player(game: GameMode): Void {
        var list = game.getPlayerList();
        if(list.length > 2) {
            game.getPlayerList().pop().destroy();
            BottomBar.get().refresh();
            //game.gi.initMulti();
        }
    }

    public function push_player(game: GameMode): Void {
        var size: Int;
        size = game.getPlayerList().length - 2;
        if(size < this.players.length) {
            var p = game.insertPlayer(8, 0);
            p.ctrl.setKeys(this.players[size].controls[0], this.players[size].controls[1], this.players[size].controls[2], this.players[size].controls[3], this.players[size].controls[4]);
            p.name = this.players[size].name;
            p.skin = this.players[size].skin;
            p.head = game.root.Data.HEAD_SANDY;
            p.baseColor = game.root.Data.BASE_COLORS[p.pid];
            p.darkColor = game.root.Data.DARK_COLORS[p.pid];
            p.filters = [this.players[size].color];
            p.lives = game.getPlayerList()[0].lives;
            p.moveToCase(9.5, -4);
            p.show();
            game.killPop();
            game.soundMan.playSound(']zPUC', game.root.Data.CHAN_BOMB);
            //game.attachPointer(8, p.cy + 4, p.cx, p.cy);
            BottomBar.get().refresh();
        }
    }

    public function new(patches: Array<IPatch>) {
        key_map = new Map<Int, Bool>();
        count  = new Map<Int, Int>();
        value = new Map<Int, Int>();
        timer = new Map<Int, Bool>();
        emojis = new Map<Int, MovieClip>();
        BottomBarModuleFactory.get().addModule(Obfu.raw("PartyBackground"), function(data: Dynamic) return new PInterface(data));
        BottomBarModuleFactory.get().addModule(Obfu.raw("PartyScore"), function(data: Dynamic) return new PScore(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Pink"), function(data: Dynamic) return new Pink(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Blue"), function(data: Dynamic) return new Blue(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Green"), function(data: Dynamic) return new Green(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Cyan"), function(data: Dynamic) return new Cyan(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Black"), function(data: Dynamic) return new Black(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Gray"), function(data: Dynamic) return new Gray(data));
        PauseModuleFactory.get().addModule(Obfu.raw("Emoji"), function(data: Dynamic) return new Emoji(data));
        this.score = 0;
        players = [new PinkPlayer(), new BluePlayer(), new GreenPlayer(), new CyanPlayer(), new BlackPlayer(), new GrayPlayer()];

        patches.push(Ref.auto(MultiCoop.getControls).before(function(hf: Hf, self: MultiCoop): Void {
            if (self.world.fl_mainWorld && self.world.currentId == 0 && !self.fl_pause) {
                if (Key.isDown(69) && self.keyLock != 69) {
                    this.pop_player(self);
                    self.keyLock = 69;
                }
                else if (Key.isDown(90) && self.keyLock != 90) {
                    this.push_player(self);
                    self.keyLock = 90;
                }
            }
            if (Key.isDown(78)) {
                var players = self.getPlayerList();
                self.root.Log.print('---- SCORES ----');
                for(i in 0...players.length)
                    self.root.Log.print(players[i].name + ': ' + players[i].score);
            }
            if (Key.isDown(86)) {
                var players = self.getPlayerList();
                self.root.Log.print('---- VIES ----');
                for(i in 0...players.length)
                    self.root.Log.print(players[i].name + ': ' + players[i].lives);
            }
            if (Key.isDown(188)) {
                var players = self.getPlayerList();
                self.root.Log.print('---- LETTRES ----');
                for(i in 0...players.length)
                    print_letters(self, i);
            }

            check_emoji_costume(self, 49, 112, 0);
            check_emoji_costume(self, 50, 113, 1);
            for(i in 0...players.length)
                check_emoji_costume(self, players[i].emoji_key, players[i].costume_key, i + 2);

            var players = self.getPlayerList();
            for(i in value.keys()) {
                if(timer[i]) {
                    if(value[i] == 0) {
                        emojis[i].gotoAndStop("1");
                        players[i].stick(emojis[i], 0, -45);
                    }
                    else if(value[i] == -self.root.Data.SECOND*2) {
                        players[i].unstick();
                        count[i] = 0;
                        timer[i] = false;
                    }
                    value[i] = value[i] - 1;
                }
            }
        }));

        patches.push(Ref.auto(MultiCoop.onPause).wrap(function(hf: Hf, self: MultiCoop, old): Void {
            Reflect.callMethod(self, untyped hf.mode.GameMode.prototype.onPause, []);
        }));

        patches.push(Ref.auto(MultiCoop.startLevel).before(function(hf: Hf, self: MultiCoop): Void {
            var players = self.getPlayerList();
            for (i in 2...players.length) {
                self.addHole(self.root.Entity.x_ctr(10) + self.root.Data.CASE_WIDTH * 0.5, self.root.Entity.y_ctr(12) - self.root.Data.CASE_HEIGHT * 0.5, 360);
            }
        }));

        patches.push(Ref.auto(hf.mode.GameMode.onRestore).before(function(hf: Hf, self: GameMode) {
            self.latePlayers = [];
        }));

        patches.push(Ref.auto(hf.entity.Player.onShieldOut).wrap(function(hf: Hf, self: hf.entity.Player, old) {
            if ( Reflect.field(self, "shield_pop"))
                self.game.fxMan.attachFx(self.x, self.y, 'popShield');
            else
                Reflect.setField(self, "shield_pop", true);
            self.shieldMC.destroy();
            self.checkHits();
        }));

        patches.push(Ref.auto(hf.entity.Player.onStartLevel).before(function(hf: Hf, self: hf.entity.Player) {
            if(self.world.fl_mainWorld && self.world.currentId < 2) {
                self.shieldTimer = 1.;
                self.fl_shield = true;
                Reflect.setField(self, "shield_pop", false);
                return;
            }
            if (self.y < -400) {
                self.shieldTimer = 130.;
                self.fl_shield = true;
                Reflect.setField(self, "shield_pop", false);
            }
            if ((self.game.getPlayerList()).length > 1) {
                if (self.world.currentId == 214 || self.world.currentId == 94 || self.world.currentId == 88) {
                    self.shieldTimer = 55.;
                    self.fl_shield = true;
                    Reflect.setField(self, "shield_pop", false);
                }
            }
        }));
        this.patch = new PatchList(patches);
    }

    public static function print_letters(game: GameMode, id: Int): Void {
        var letters = Obfu.raw("CRISTAL");
        var players = game.getPlayerList();
        var cristal: String = "";
        for (i in 0...7) {
            if (players[id].extendList[i]) {
                cristal += letters.charAt(i);
            } else {
                cristal += ". ";
            }
        }
        game.root.Log.print(players[id].name + ': ' + cristal);
    }

    public function check_emoji_costume(self: MultiCoop, key: Int, costume_id: Int, player: Int) {
        if (!Key.isDown(key)) {
            key_map[key] = false;
        }
        if (Key.isDown(key) && (key_map[key] == false || key_map[key] == null)) {
            if (Key.isDown(16))
                emoji(self, player, 1);
            else
                emoji(self, player, 0);
            key_map[key] = true;
        }

        if (Key.isDown(costume_id) && self.keyLock != costume_id) {
            costume(self, player);
            self.keyLock = costume_id;
        }
    }


    public function emoji(self: MultiCoop, player: Int, id: Int) {
        var players = self.getPlayerList();
        var emoji: MovieClip;
        if (players[player].fl_stick) {
            return;
        }
        if(count[player] == null)
            count[player] = 0;
        count[player] += 1;
        value[player] = Std.int(self.root.Data.SECOND/2);
        timer[player] = true;
        emojis[player].removeMovieClip();
        if (id == 0)
            emoji = self.depthMan.attach(Obfu.raw('emoji') + (count[player] - 1), self.root.Data.DP_PLAYER);
        else
            emoji = self.depthMan.attach(Obfu.raw('other') + (count[player] - 1), self.root.Data.DP_PLAYER);
        emojis[player] = emoji;
    }

    public function costume(self: MultiCoop, player: Int) {
        var player = self.getPlayerList()[player];
        var head = player.head;
        if (player == null) {
            return;
        }
        if (player.head > 5) {
            player.head = self.root.Data.HEAD_NORMAL;
        } else {
            ++player.head;
        }
        if (head != player.head) {
            player.replayAnim();
        }
    }
}
