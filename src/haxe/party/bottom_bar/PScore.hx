package party.bottom_bar;

import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;
class PScore extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        if(bottomBar.game.getPlayerList().length > 2) {
            var mc = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 2);
            untyped bottomBar.mc.score0 = mc.field;
            untyped bottomBar.mc.score0._visible = false;
            untyped mc.label._visible = false;
            untyped bottomBar.mc.score0._width = 400;
            untyped bottomBar.mc.score0._xscale = 115;
            untyped bottomBar.mc.score0._yscale = 115;
            untyped bottomBar.mc.score0._y = 499;
            untyped bottomBar.mc.score0._visible = true;
            untyped bottomBar.mc.score0.text = "000000";
            untyped bottomBar.mc.score0._x = bottomBar.game.root.Data.GAME_WIDTH / 2;
        }
        bottomBar.scores = [untyped bottomBar.mc.score0, untyped bottomBar.mc.score1];
        bottomBar.fakeScores = new Array();
        bottomBar.realScores = new Array();
        var players = (bottomBar.game.getPlayerList());
        for(i in 0...players.length) {
            bottomBar.fakeScores[i] = 0;
            bottomBar.realScores[i] = 0;
            bottomBar.setScore(i, players[i].score);
            untyped bottomBar.scores[i].textColor = bottomBar.game.root.Data.BASE_COLORS[0];
            bottomBar.game.root.FxManager.addGlow(bottomBar.scores[i], bottomBar.game.root.gui.GameInterface.GLOW_COLOR, 2);
        }
    }

    public override function setScore(pid: Int, value: Int, bottomBar: BottomBarInterface): Bool {
        if(bottomBar.game.getPlayerList().length == 2)
            return true;
        else {
            var players = bottomBar.game.getPlayerList();
            bottomBar.realScores[0] = 0;
            for(i in 0...players.length) {
                bottomBar.realScores[0] += players[i].score;
            }
            var str: String = "" + bottomBar.realScores[0];
            untyped bottomBar.mc.score0._x = bottomBar.game.root.Data.GAME_WIDTH / 2 - (str.length / 2 * 10);
            return false;
        }
    }
}
