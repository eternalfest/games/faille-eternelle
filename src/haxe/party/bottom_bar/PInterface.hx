package party.bottom_bar;

import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;
class PInterface extends BottomBarModule {
    public function new(data: Dynamic) {
    }

    public override function init(bottomBar: BottomBarInterface): Void {
        bottomBar.fl_multi = cast true;
        untyped bottomBar.mc.score0._visible = false;
        bottomBar.mc.removeMovieClip();
        bottomBar.mc = ((cast bottomBar.game.depthMan.attach("hammer_interf_game", bottomBar.game.root.Data.DP_TOP)): Dynamic);
        bottomBar.mc._x = -bottomBar.game.xOffset;
        bottomBar.mc._y = cast bottomBar.game.root.Data.DOC_HEIGHT;
        var index: Int;
        index = bottomBar.game.getPlayerList().length;
        if(index > 2)
            index = 4;
        bottomBar.mc.gotoAndStop(""+index);
        bottomBar.mc.cacheAsBitmap = true;
        bottomBar.letters = new Array();
        bottomBar.letters[0] = new Array();
        var mc: Dynamic;
        if(index == 4) {
            var party_top = ((cast bottomBar.game.depthMan.attach("hammer_interf_game", bottomBar.game.root.Data.DP_TOP)): Dynamic);
            var party_bottom = ((cast bottomBar.game.depthMan.attach("hammer_interf_game", bottomBar.game.root.Data.DP_TOP)): Dynamic);
            party_bottom.gotoAndStop("2");
            party_top.gotoAndStop("2");

            party_top._y = cast bottomBar.game.root.Data.DOC_HEIGHT + party_top._height;
            party_bottom._y = cast bottomBar.game.root.Data.DOC_HEIGHT + party_top._height;
            party_bottom._x = -bottomBar.game.xOffset;
            party_top._x = -bottomBar.game.xOffset;
            mc = cast party_top;
            bottomBar.letters[0].push(mc.letter0_0);
            bottomBar.letters[0].push(mc.letter0_1);
            bottomBar.letters[0].push(mc.letter0_2);
            bottomBar.letters[0].push(mc.letter0_3);
            bottomBar.letters[0].push(mc.letter0_4);
            bottomBar.letters[0].push(mc.letter0_5);
            bottomBar.letters[0].push(mc.letter0_6);
            bottomBar.letters[1] = new Array();
            bottomBar.letters[1].push(mc.letter1_0);
            bottomBar.letters[1].push(mc.letter1_1);
            bottomBar.letters[1].push(mc.letter1_2);
            bottomBar.letters[1].push(mc.letter1_3);
            bottomBar.letters[1].push(mc.letter1_4);
            bottomBar.letters[1].push(mc.letter1_5);
            bottomBar.letters[1].push(mc.letter1_6);

            mc = cast party_bottom;
            bottomBar.letters[2] = new Array();
            bottomBar.letters[2].push(mc.letter0_0);
            bottomBar.letters[2].push(mc.letter0_1);
            bottomBar.letters[2].push(mc.letter0_2);
            bottomBar.letters[2].push(mc.letter0_3);
            bottomBar.letters[2].push(mc.letter0_4);
            bottomBar.letters[2].push(mc.letter0_5);
            bottomBar.letters[2].push(mc.letter0_6);
            bottomBar.letters[3] = new Array();
            bottomBar.letters[3].push(mc.letter1_0);
            bottomBar.letters[3].push(mc.letter1_1);
            bottomBar.letters[3].push(mc.letter1_2);
            bottomBar.letters[3].push(mc.letter1_3);
            bottomBar.letters[3].push(mc.letter1_4);
            bottomBar.letters[3].push(mc.letter1_5);
            bottomBar.letters[3].push(mc.letter1_6);
            if(index == 4) {
                var height = party_top._height;
                for(i in 0...bottomBar.letters.length) {
                    for(j in 0...bottomBar.letters[i].length) {
                        bottomBar.letters[i][j].gotoAndStop("1");
                        bottomBar.letters[i][j]._visible = true;
                        bottomBar.letters[i][j]._alpha = 255.;
                        bottomBar.letters[i][j]._y -= height;
                        bottomBar.letters[i][j]._x -= 1.;
                    }
                }

                for(i in 0...bottomBar.letters[0].length)
                    bottomBar.letters[0][i]._y -= 9.;
                for(i in 0...bottomBar.letters[1].length)
                    bottomBar.letters[1][i]._y -= 9.;
            }
        }
        else {
            mc = cast bottomBar.mc;
            bottomBar.letters[0].push(mc.letter0_0);
            bottomBar.letters[0].push(mc.letter0_1);
            bottomBar.letters[0].push(mc.letter0_2);
            bottomBar.letters[0].push(mc.letter0_3);
            bottomBar.letters[0].push(mc.letter0_4);
            bottomBar.letters[0].push(mc.letter0_5);
            bottomBar.letters[0].push(mc.letter0_6);
            bottomBar.letters[1] = new Array();
            bottomBar.letters[1].push(mc.letter1_0);
            bottomBar.letters[1].push(mc.letter1_1);
            bottomBar.letters[1].push(mc.letter1_2);
            bottomBar.letters[1].push(mc.letter1_3);
            bottomBar.letters[1].push(mc.letter1_4);
            bottomBar.letters[1].push(mc.letter1_5);
            bottomBar.letters[1].push(mc.letter1_6);
        }
    }


    public override function getExtend(pid: Int, id: Int, bottomBar: BottomBarInterface): Bool {
        if(bottomBar.game.getPlayerList().length > 2) {
            var letter = bottomBar.letters[pid][id];
            if (!letter._visible) {
                var animation = bottomBar.game.root.Std.attachMC(letter._parent, "hammer_fx_letter_pop", bottomBar.game.manager.uniq++);
                animation._x = letter._x + letter._width * 0.5;
                animation._y = letter._y;
                letter._visible = true;
            }
            return false;
        }
        return true;
    }
}