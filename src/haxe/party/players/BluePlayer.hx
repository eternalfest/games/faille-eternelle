package party.players;
import etwin.Obfu;
import color_matrix.ColorMatrix;

class BluePlayer extends PartyPlayer {
    public static var INSTANCE: BluePlayer;
    public function new() {
        super(Obfu.raw("Bleu"), 2, [101, 98, 97, 99, 186], ColorMatrix.fromHsv(160, 1, 1).toFilter(), 52, 115);
        INSTANCE = this;
    }
}
