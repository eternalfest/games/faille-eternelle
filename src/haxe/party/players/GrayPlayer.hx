package party.players;
import etwin.Obfu;
import color_matrix.ColorMatrix;

class GrayPlayer extends PartyPlayer {
    public static var INSTANCE: GrayPlayer;
    public function new() {
        super(Obfu.raw("Gris"), 1, [36, 35, 46, 34, 8], ColorMatrix.fromHsv(0, 0.3, 0.50).toFilter(), 56, 120);
        INSTANCE = this;
    }
}
