package party.players;
import etwin.Obfu;
import color_matrix.ColorMatrix;

class BlackPlayer extends PartyPlayer {
    public static var INSTANCE: BlackPlayer;
    public function new() {
        super(Obfu.raw("Noir"), 2, [83, 87, 226, 88, 222], ColorMatrix.fromHsv(150, 1, -1).toFilter(), 55, 119);
        INSTANCE = this;
    }
}
