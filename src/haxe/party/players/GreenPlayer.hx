package party.players;
import etwin.Obfu;
import color_matrix.ColorMatrix;

class GreenPlayer extends PartyPlayer {
    public static var INSTANCE: GreenPlayer;
    public function new() {
        super(Obfu.raw("Vert"), 2, [111, 104, 103, 105, 187], ColorMatrix.fromHsv(300, 1, 1).toFilter(), 53, 117);
        INSTANCE = this;
    }
}
