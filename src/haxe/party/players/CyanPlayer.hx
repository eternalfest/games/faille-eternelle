package party.players;
import etwin.Obfu;
import color_matrix.ColorMatrix;

class CyanPlayer extends PartyPlayer {
    public static var INSTANCE: CyanPlayer;
    public function new() {
        super(Obfu.raw("Cyan"), 2, [221, 192, 77, 220, 85], ColorMatrix.fromHsv(200, 1.5, 1).toFilter(), 54, 118);
        INSTANCE = this;
    }
}
