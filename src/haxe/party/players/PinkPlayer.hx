package party.players;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class PinkPlayer extends PartyPlayer {
    public static var INSTANCE: PinkPlayer;
    public function new() {
        super(Obfu.raw("Rose"), 2, [73, 75, 74, 76, 84], ColorMatrix.fromHsv(60, 1, 1).toFilter(), 51, 114);
        INSTANCE = this;
    }
}
