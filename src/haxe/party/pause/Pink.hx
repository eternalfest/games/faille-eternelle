package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.PinkPlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Pink extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("I"), Obfu.raw("K"), Obfu.raw("J"), Obfu.raw("L"), Obfu.raw("T"), Obfu.raw("F3"), Obfu.raw("\""), Obfu.raw("Rose"), [ColorMatrix.fromHsv(60, 1, 1).toFilter()]);
    }

    public override function get_controls(): Array<Int> {
        return PinkPlayer.INSTANCE.controls.slice(0, 4);
    }
    public override function get_attack(): Int {
        return PinkPlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return PinkPlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return PinkPlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return PinkPlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [PinkPlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return PinkPlayer.INSTANCE.name;
    }
}
