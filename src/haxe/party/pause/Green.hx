package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.GreenPlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Green extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("/"), Obfu.raw("8"), Obfu.raw("7"), Obfu.raw("9"), Obfu.raw("="), Obfu.raw("F6"), Obfu.raw("("), Obfu.raw("Vert"), [ColorMatrix.fromHsv(300, 1, 1).toFilter()]);
    }

    public override function get_controls(): Array<Int> {
        return GreenPlayer.INSTANCE.controls.slice(0, 4);
    }

    public override function get_attack(): Int {
        return GreenPlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return GreenPlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return GreenPlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return GreenPlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [GreenPlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return GreenPlayer.INSTANCE.name;
    }
}
