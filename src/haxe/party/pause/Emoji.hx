package party.pause;
import custom_pause.module.PauseModule;
import hf.entity.Player;
import hf.mode.GameMode;
import etwin.Obfu;
class Emoji extends PauseModule {

    private var pictures: Array<Dynamic>;
    public function new(data: Dynamic) {
        super();
        pictures = new Array<Dynamic>();
    }

    public override function create_page(mode: GameMode): Void {
        text_bloc(mode, -10, Obfu.raw('emoji0'), mode.root.Lang.get(10023));
        text_bloc(mode, 80, Obfu.raw('emoji2'), mode.root.Lang.get(10024));
        text_bloc(mode, 170, Obfu.raw('other1'), mode.root.Lang.get(10025));
    }

    public function text_bloc(mode: GameMode, y: Int, emoji_id: String, text: String) {
        var description_font = "<font face=\"Verdana\" size=\"12\" color=\"#e6be86\" letterSpacing=\"0.000000\" kerning=\"0\">";
        var emoji = cast mode.depthMan.attach(emoji_id, mode.root.Data.DP_INTERF);
        emoji.gotoAndStop("1");
        emoji._x = 100;
        emoji._y = 150 + y;
        var picture = cast mode.depthMan.attach("hammer_player", mode.root.Data.DP_INTERF);
        picture.gotoAndStop("1");
        picture.skin = 1;
        picture._x = 100;
        picture._y = 200 + y;
        picture.createTextField("_label", 2, 30, -40, 190, 150);
        picture._label.html = true;
        picture._label.wordWrap = true;
        picture._label.selectable = false;
        picture._label.htmlText = description_font + "<p align=\"left\"><b>" + text + "</b></p></font>";
        pictures.push(emoji);
        pictures.push(picture);
    }

    public override function get_title(game: GameMode): String {
        return game.root.Lang.get(10022);
    }

    public override function destroy() {
        super.destroy();
        for(picture in pictures) {
            picture.destroy();
            picture.removeMovieClip();
        }
    }
}
