package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.BlackPlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Black extends Character {
    public function new(data: Dynamic) {
        super(Obfu.raw("S"), Obfu.raw("W"), Obfu.raw("&#60;"), Obfu.raw("X"), Obfu.raw("²"), Obfu.raw("F8"), Obfu.raw("è"), Obfu.raw("Noir"), [ColorMatrix.fromHsv(150, 1, -1).toFilter()]);
    }

    public override function get_controls(): Array<Int> {
        return BlackPlayer.INSTANCE.controls.slice(0, 4);
    }

    public override function get_attack(): Int {
        return BlackPlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return BlackPlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return BlackPlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return BlackPlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [BlackPlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return BlackPlayer.INSTANCE.name;
    }
}
