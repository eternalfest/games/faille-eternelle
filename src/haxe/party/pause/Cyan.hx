package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.CyanPlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Cyan extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("^"), Obfu.raw("ù"), Obfu.raw("M"), Obfu.raw("*"), Obfu.raw("U"), Obfu.raw("F7"), Obfu.raw("-"), Obfu.raw("Cyan"), [ColorMatrix.fromHsv(200, 1.5, 1).toFilter()]);
    }

    public override function get_controls(): Array<Int> {
        return CyanPlayer.INSTANCE.controls.slice(0, 4);
    }

    public override function get_attack(): Int {
        return CyanPlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return CyanPlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return CyanPlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return CyanPlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [CyanPlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return CyanPlayer.INSTANCE.name;
    }
}
