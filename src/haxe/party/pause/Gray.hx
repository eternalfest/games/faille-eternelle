package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.GrayPlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Gray extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("?"), Obfu.raw("?"), Obfu.raw("?"), Obfu.raw("?"), Obfu.raw("?"), Obfu.raw("F9"), Obfu.raw("_"), Obfu.raw("Gris"), [ColorMatrix.fromHsv(0, 0.3, 0.50).toFilter()]);
        this.skin = 1;
    }

    public override function get_controls(): Array<Int> {
        return GrayPlayer.INSTANCE.controls.slice(0, 4);
    }

    public override function get_attack(): Int {
        return GrayPlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return GrayPlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return GrayPlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return GrayPlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [GrayPlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return GrayPlayer.INSTANCE.name;
    }
}
