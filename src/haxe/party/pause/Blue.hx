package party.pause;
import etwin.flash.filters.BitmapFilter;
import party.players.BluePlayer;
import custom_pause.module.multi.Character;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
class Blue extends Character {

    public function new(data: Dynamic) {
        super(Obfu.raw("5"), Obfu.raw("2"), Obfu.raw("1"), Obfu.raw("3"), Obfu.raw("$"), Obfu.raw("F4"), Obfu.raw("'"), Obfu.raw("Blue"), [ColorMatrix.fromHsv(160, 1, 1).toFilter()]);
    }

    public override function get_controls(): Array<Int> {
        return BluePlayer.INSTANCE.controls.slice(0, 4);
    }

    public override function get_attack(): Int {
        return BluePlayer.INSTANCE.controls[4];
    }

    public override function get_costume(): Int {
        return BluePlayer.INSTANCE.costume_key;
    }

    public override function get_emoji(): Int {
        return BluePlayer.INSTANCE.emoji_key;
    }

    public override function get_skin(): Int {
        return BluePlayer.INSTANCE.skin;
    }

    public override function get_filter(): Array<BitmapFilter> {
        return [BluePlayer.INSTANCE.color];
    }

    public override function get_name(): String {
        return BluePlayer.INSTANCE.name;
    }
}
