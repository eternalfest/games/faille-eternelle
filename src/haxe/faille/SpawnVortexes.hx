package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class SpawnVortexes {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var launch: Bool;
  public var randomNumber: Int;
  public var sid: Int = 1;

  public function new() {
    this.actions = new LaunchVortexes(this);
      
    this.launch = false;

    this.patches = new PatchList([
        
      Ref.auto(GameMode.main).after(function(hf, self: GameMode): Void {
        if (this.launch && !self.fl_pause) {
          if(this.randomNumber <= 0) {
            this.randomNumber = Std.random(20) + 10;
            // spawn vortex
            var x = (Std.random(400) + 1);
            var y = 26 * 20;
            var scaleX = Std.random(70) + 30;
            var scaleY = scaleX;
            var back = true;
            var name = "hammer_portal";
            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(0, 1, (scaleX * scaleX) / 10000).toFilter();
            var sprite = self.world.view.attachSprite(name, x, y, back);
            sprite._xscale = scaleX;
            sprite._yscale = scaleY;
            sprite.play();
            sprite.filters = [filter];
            self.world.scriptEngine.mcList.push({sid: this.sid, mc: (cast sprite)});
            this.sid++;
          } else {
            this.randomNumber--;
          }
          if(!self.fl_pause) {
            for(vortex in self.world.scriptEngine.mcList) {
              var scale = vortex.mc._xscale;
              if(vortex.sid != 10000) {
                vortex.mc._y -= (scale * scale) / 2500;
                if(vortex.mc._y < -80) {
                  self.world.scriptEngine.killById(vortex.sid);
                }
              } else {
                this.randomNumber = 1000000;
                vortex.mc._y -= 4;
                if(vortex.mc._y < 230) {
                  self.world.scriptEngine.mcList = [];
                }
              }
            }
          }
        }
      }),
        
    ]);
  }
}

class LaunchVortexes implements IAction {
  public var name(default, null): String = Obfu.raw("launchVortexes");
  public var isVerbose(default, null): Bool = false;

  private var mod: SpawnVortexes;
    
  public function new(mod: SpawnVortexes) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var launch: Bool = ctx.getOptBool(Obfu.raw("n")).or(false); 
    mod.launch = launch;
    mod.randomNumber = 1;

    return false;
  }
}