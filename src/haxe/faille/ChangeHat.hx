package faille;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.Mode;
import hf.GameManager;
import hf.mode.GameMode;
import keyboard.Key;
import keyboard.KeyCode;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;
import merlin.Merlin;

@:build(patchman.Build.di())
class ChangeHat {

	public static var topHat: Bool = false;
	public static var coffinDanceHat: Bool = false;
	public static var partyHat: Bool = false;
	public var coffinDanceOn: Bool = false;
	public var currentTrack: Null<Int>;

	@:diExport
	public var patch(default, null): IPatch;

	public function new(): Void {
    	this.patch = new PatchList([
      		Ref.auto(GameMode.getControls).wrap(function(hf: Hf, self: GameMode, old): Void {

      			var hf: Hf = self.root;

        		if (self.fl_disguise && Key.isDown(KeyCode.D) && self.keyLock != 68) {

					var player = (self.getPlayerList())[0];
					var v4 = player.head;

					if (player.head == hf.Data.HEAD_TUB) {
				        if(topHat) {
			        		player.head = 13;
			        	} else if(coffinDanceHat) {
			        		player.head = 14;
			        		if(self.fl_music) {
			        			self.playMusic(22);
			        			this.coffinDanceOn = true;
			        		}
			        	} else if(partyHat) {
			        		player.head = 15;
			        	} else {
			        		player.head = 1;
			        	}
				    }
					else if (player.head == 13) {
						if(coffinDanceHat) {
			        		player.head = 14;
			        		if(self.fl_music) {
			        			self.playMusic(22);
			        			this.coffinDanceOn = true;
			        		}
			        	} else if(partyHat) {
			        		player.head = 15;
			        	} else {
			        		player.head = 1;
			        	}
					}
					else if (player.head == 14) {
						if(self.fl_music) {
							this.coffinDanceOn = false;
							self.playMusic(this.currentTrack);
						}
						if(partyHat) {
			        		player.head = 15;
			        	} else {
			        		player.head = 1;
			        	}
					}
					else if (player.head == 15) {
						player.head = 1;
					}
					else {
						player.head++;
					}

					if (v4 != player.head) {
				    	player.replayAnim();
				    }
					self.keyLock = 68;
				}

				old(self);
     		}),

     		Ref.auto(Mode.playMusic).wrap(function(hf: Hf, self: Mode, id: Int, old): Void {
     			if(id != 22) this.currentTrack = id;
     			if(!this.coffinDanceOn) old(self, id);
     		}),

        	Ref.auto(hf.mode.GameMode.initGame).before(function(hf, self: GameMode) {
            	Merlin.setGlobalVar(self, Obfu.raw("ETOILES"), cast 0);
        	}),
    	]);
  	}
}