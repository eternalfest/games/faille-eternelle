package faille;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;

import faille.actions.FreezeBall;
import faille.actions.Death;
import faille.actions.SetSpawn;
import faille.actions.SetItems;
import faille.actions.EnterExt;
import faille.actions.McGears;
import faille.actions.ArrowBall;
import faille.actions.StartShakeScreen;
import faille.actions.Ending;
import faille.actions.MPlay;
import faille.actions.MDraw;
import faille.actions.ItemSprite;
import faille.actions.CustomShield;
import faille.actions.CustomVortex;
import faille.actions.NotMulti;
import faille.actions.ResetLaby;
import faille.actions.Reset;
import faille.actions.DoABarrelRoll;
import faille.actions.AddLives;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var death(default, null): IAction;
  @:diExport
  public var freezeBall(default, null): IAction;
  @:diExport
  public var setSpawn(default, null): IAction;
  @:diExport
  public var setItems(default, null): IAction;
  @:diExport
  public var enterExt(default, null): IAction;
  @:diExport
  public var mcGears(default, null): IAction;
  @:diExport
  public var arrowBall(default, null): IAction;
  @:diExport
  public var startShakeScreen(default, null): IAction;
  @:diExport
  public var ending(default, null): IAction;
  @:diExport
  public var mplay(default, null): IAction;
  @:diExport
  public var mdraw(default, null): IAction;
  @:diExport
  public var itemsprite(default, null): IAction;
  @:diExport
  public var customShield(default, null): IAction;
  @:diExport
  public var customVortex(default, null): IAction;
  @:diExport
  public var notMulti(default, null): IAction;
  @:diExport
  public var resetLaby(default, null): IAction;
  @:diExport
  public var reset(default, null): IAction;
  @:diExport
  public var doABarrelRoll(default, null): IAction;
  @:diExport
  public var addLives(default, null): IAction;

  public function new() {
    this.patches = FrozenArray.from(patches);
    this.death = new Death();
    this.freezeBall = new FreezeBall();
    this.setSpawn = new SetSpawn();
    this.setItems = new SetItems();
    this.enterExt = new EnterExt();
    this.mcGears = new McGears();
    this.arrowBall = new ArrowBall();
    this.startShakeScreen = new StartShakeScreen();
    this.ending = new Ending();
    this.mplay = new MPlay();
    this.mdraw = new MDraw();
    this.itemsprite = new ItemSprite();
    this.customShield = new CustomShield();
    this.customVortex = new CustomVortex();
    this.notMulti = new NotMulti();
    this.resetLaby = new ResetLaby();
    this.reset = new Reset();
    this.doABarrelRoll = new DoABarrelRoll();
    this.addLives = new AddLives();
  }
}
