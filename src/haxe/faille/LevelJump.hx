package faille;

import hf.entity.Player;
import hf.mode.GameMode;
import hf.levels.ScriptEngine;
import etwin.ds.FrozenArray;
import etwin.ds.WeakMap;
import etwin.ds.Set;
import etwin.ds.Nil;

import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import merlin.IAction;
import merlin.IActionContext;

import color_matrix.ColorMatrix;
import randomfest.Randomfest;

@:build(patchman.Build.di())
class LevelJump {

  public static inline var ROOM_DID: Int = 1;
  public static inline var ROOM_LID: Int = 1;
  public static inline var MAIN_DID: Int = 0;

  public static var VORTEX_COLOR_FILTER: ColorMatrix = ColorMatrix.fromHsv(60, 1, 1);

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  private var curJumpLevel: Null<Int>;
  private var jumpLevels: Set<Int>;

  public function new(randomfest: Randomfest) {
    this.actions = FrozenArray.of(
      new PrepareJump(this),
      new JumpPortal(this)
    );

    this.jumpLevels = new Set();

    this.patches = new PatchList([
      Ref.auto(hf.Data.getLink).prefix(function(hf, did, lid, pid) {
        var to_did = null;
        var to_lid = null;
        if (pid == 999) {
          to_did = ROOM_DID;
          to_lid = ROOM_LID;
        } else if (pid >= 1000 && this.curJumpLevel != null) {
          to_did = MAIN_DID;
          to_lid = this.curJumpLevel + pid - 1000;
          if(this.curJumpLevel < 50 && to_lid > 50) {
          	to_lid = 50;
          }
          if(this.curJumpLevel < 100 && to_lid > 100) {
          	to_lid = 100;
          }
          if(this.curJumpLevel < 150 && to_lid > 150) {
          	to_lid = 150;
          }
          if(this.curJumpLevel < 200 && to_lid > 200) {
          	to_lid = 200;
          }
          if(this.curJumpLevel < 290 && to_lid > 290) {
          	to_lid = 290;
          }
          this.curJumpLevel = null;
        }

        if (to_did != null && to_lid != null) {
          var link = hf.levels.PortalLink._new();
          link.from_did = did;
          link.from_lid = lid;
          link.from_pid = pid;
          link.to_did = to_did;
          link.to_lid = to_lid;
          link.to_pid = -1;
          return Nil.some(link);
        }

        return Nil.none();
      }),

      Ref.auto(Player.killPlayer).before(function(hf, self: Player): Void {
        if (randomfest.isRandomfestPlayer(self))
          return;
        var numberDeath: MerlinValue = Merlin.getGlobalVar(self.game, Obfu.raw("DEATH")).or(null);
        var numberDeathFloat: Float = numberDeath.unwrapFloat();
        if(numberDeathFloat == null)
          numberDeathFloat = 0;
        numberDeathFloat++;
        Merlin.setGlobalVar(self.game, Obfu.raw("DEATH"), new MerlinFloat(numberDeathFloat));
      }),
    ]);
  }

  public function prepareJump(game: GameMode, lid: Int): Void {
    this.jumpLevels.add(lid);
  }

  public function usePreparedJump(game: GameMode, lid: Int): Bool {
    if (this.jumpLevels.exists(lid)) {
      this.jumpLevels.remove(lid);
      this.curJumpLevel = lid;
      return true;
    }
    return false;
  }

  public function spawnJumpPortal(game: GameMode, x: Float, y: Float, offset: Null<Int>): Void {
    var pid = offset == null ? 999 : 1000 + offset;
    game.openPortal(x, y, pid);

    game.portalMcList[pid].mc.filters = [VORTEX_COLOR_FILTER.toFilter()];
  }
    
  public function getNextJumpPortal(lid: Int): Int {
    var nextPortal = lid;
    while(!this.jumpLevels.exists(nextPortal) && nextPortal < 290) {
      nextPortal++;
    }
    if(!this.jumpLevels.exists(nextPortal)) nextPortal = null;
    return nextPortal;
  }
}

private typedef LevelJumpState = {
	curJumpLevel: Null<Int>,
	jumpLevels: Set<Int>,
};

class PrepareJump implements IAction {
  public var name(default, null): String = Obfu.raw("prepareJump");
  public var isVerbose(default, null): Bool = false;

  private var mod: LevelJump;

  public function new(mod: LevelJump) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var min = ctx.getInt(Obfu.raw("min"));
    var max = ctx.getInt(Obfu.raw("max"));
    var r = (max - min + 1) / 2.0;
    var lid = Std.int(min + r*Math.random() + r*Math.random());
    this.mod.prepareJump(ctx.getGame(), lid);
    return false;
  }
}

class JumpPortal implements IAction {
  public var name(default, null): String = Obfu.raw("jumpPortal");
  public var isVerbose(default, null): Bool = false;

  private var mod: LevelJump;

  public function new(mod: LevelJump) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game = ctx.getGame();
    var offset: Int = ctx.getOptInt(Obfu.raw("offset")).or(null);
    var lid = game.world.currentId;

    if (offset == null) {
      if (!this.mod.usePreparedJump(game, lid))
        return false;
    }

    var portalX = ctx.getFloat(Obfu.raw("x"));
    var portalY = ctx.getFloat(Obfu.raw("y"));
    this.mod.spawnJumpPortal(game, portalX, portalY, offset);
    return false;
  }
}
