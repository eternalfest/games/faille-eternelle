package faille;

import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import etwin.flash.MovieClip;
import hf.Hf;
import hf.FxManager;
import hf.mode.GameMode;

import faille.actions.WhiteScreen;

@:build(patchman.Build.di())
class FadingScreen {
    
   @:diExport
  public var action(default, null): IAction;
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public var active: Bool = false;
  public var hasBeenTraced: Bool = false;
  public var square_mc: MovieClip;

  public function new() {
    this.action = new WhiteScreen(this);

    var patches = [
    
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if(this.active) {
            if (!this.hasBeenTraced) {
              square_mc = self.depthMan.empty(10000);
              square_mc.beginFill(0xFFFFFF);
              square_mc._alpha = 0;
              square_mc.moveTo(0, 0);
              square_mc.lineTo(-20, 520);
              square_mc.lineTo(420, 520);
              square_mc.lineTo(420, 0);
              square_mc.lineTo(-20, 0);
              square_mc.endFill();
              hasBeenTraced = true;
            } else {
              square_mc._alpha = square_mc._alpha + 0.4;
              if (square_mc._alpha >= 100) {
                square_mc._alpha = 0;
                square_mc.removeMovieClip();
                active = false;
                hasBeenTraced = false;
              }
            }
            
          }
        }
      }),
        
    ];
      
    this.patches = FrozenArray.from(patches);
  }
}