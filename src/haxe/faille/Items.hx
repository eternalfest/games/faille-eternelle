package faille;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.ISpec;
import vault.ISkin;
import randomfest.Randomfest;
import custom_clips.CustomClips;

import faille.items.Sablier;
import faille.items.Eclair;
import faille.items.TicketB;
import faille.items.TicketR;
import faille.items.Feuille;
import faille.items.Urticants;
import faille.items.Bouton;
import faille.items.Sadist;
import faille.items.Sifflet;
import faille.items.Avis;
import faille.items.Ciseaux;
import faille.items.Cle;
import faille.items.Dynamite;
import faille.items.Marteau;
import faille.items.Clonage;
import faille.items.Flamme;
import faille.items.Masque;
import faille.items.Miel;
import faille.items.Rouge;
import faille.items.Vert;
import faille.items.Gant;
import faille.items.Nuage;
import faille.items.Poudre;
import faille.items.Cristallin;
import faille.items.Baguette;
import faille.items.Livre;
import faille.items.Brimstone;
import faille.items.Metronome;
import faille.items.Montre;
import faille.items.Anneau;
import faille.items.Igor;
import faille.items.Detecteur;
import faille.items.Sceptre;
import faille.items.Orbe;
import faille.items.L_Invisible;
import faille.items.Purification;
import faille.items.Eau;
import faille.items.Malediction;
import faille.items.BlackHole;
import faille.items.RaveParty;
import faille.items.BombingBomb;
import faille.items.Bomberman;
import faille.items.Speed;
import faille.items.Couronne;
import faille.items.Cloche;
import faille.items.Freeze;
import faille.items.Parapluie;
import faille.items.Miroir;
import faille.items.L_Controles;
import faille.items.Oeil;
import faille.items.Hasard;
import faille.items.Dash;

import faille.skins.Etoile;
import faille.skins.EtoileMulticolore;
import faille.skins.IgorSkin;
import faille.skins.Piece;
import faille.skins.Statue_Bombino;
import faille.skins.Statue_Poire;
import faille.skins.Statue_Bondissante;
import faille.skins.Statue_Citron;
import faille.skins.Statue_Fraise;
import faille.skins.Statue_Kiwi;

import faille.snow.Ice;
import faille.LevelJump;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var items(default, null): FrozenArray<ISpec>;
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public function new(sadist: Sadist,
                      sifflet: Sifflet, 
                      avis: Avis, 
                      ciseaux: Ciseaux, 
                      dynamite: Dynamite, 
                      marteau: Marteau,
                      flamme: Flamme,
                      masque: Masque,
                      gant: Gant,
                      brimstone: Brimstone,
                      metronome: Metronome,
                      montre: Montre,
                      detecteur: Detecteur,
                      clips: CustomClips,
                      randomfest: Randomfest,
                      eau: Eau,
                      bombingBomb: BombingBomb,
                      bomberman: Bomberman,
                      speed: Speed,
                      couronne: Couronne,
                      miroir: Miroir,
                      l_controles: L_Controles,
                      oeil: Oeil,
                      ice: Ice,
                      levelJump: LevelJump,
                      dash: Dash,
                      etoileMulticolore: EtoileMulticolore,
                      patches: Array<IPatch>) {
    this.patches = FrozenArray.from(patches);
    this.items = FrozenArray.of(
      new Sablier(),
      new Eclair(),
      new TicketB(),
      new TicketR(),
      new Feuille(),
      new Urticants(),
      new Bouton(),
      sadist,
      sifflet,
      avis,
      ciseaux,
      new Cle(),
      dynamite,
      marteau,
      new Clonage(clips),
      flamme,
      masque,
      new Miel(),
      new Rouge(),
      new Vert(clips),
      gant,
      new Nuage(),
      new Poudre(),
      new Cristallin(),
      new Baguette(),
      new Livre(),
      brimstone,
      metronome,
      montre,
      new Anneau(randomfest),
      new Igor(),
      detecteur,
      new Sceptre(),
      new Orbe(),
      new L_Invisible(),
      new Purification(),
      eau,
      new Malediction(),
      new BlackHoleItem(),
      new RaveParty(),
      bombingBomb,
      bomberman,
      speed,
      couronne,
      new Cloche(),
      new Freeze(ice),
      new Parapluie(levelJump),
      miroir,
      l_controles,
      oeil,
      new Hasard(),
      dash
    );

    this.skins = FrozenArray.of(
      (new Etoile(1288, 0, 1): ISkin),
      new Etoile(1289, -35, 1.3),
      new Etoile(1290, -60, 1.8),
      new Etoile(1291, -136, 1),
      new Etoile(1292, 184, 1.2),
      new Etoile(1293, 142, 1),
      new Etoile(1294, 56, 1),
      etoileMulticolore,
      new Etoile(1305, 0, 0.5),
      new Etoile(1306, -35, 0.8),
      new Etoile(1307, -60, 1.3),
      new Etoile(1308, -136, 0.5),
      new Etoile(1309, 184, 0.7),
      new Etoile(1310, 142, 0.5),
      new Etoile(1311, 56, 0.5),
      new Piece(),
      new IgorSkin(),
      new Statue_Bombino(),
      new Statue_Poire(),
      new Statue_Bondissante(),
      new Statue_Citron(),
      new Statue_Fraise(),
      new Statue_Kiwi()
    );
  }
}