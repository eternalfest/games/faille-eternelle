package faille;

import hf.Hf;
import merlin.IAction;
import etwin.ds.FrozenArray;
import faille.pic.PicCustom;

@:build(patchman.Build.di())
class Pic {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;

    public function new(hf: Hf): Void {
        actions = FrozenArray.of(cast new PicCustomSpawn(hf));
    }
}
