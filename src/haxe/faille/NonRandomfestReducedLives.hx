package faille;

import hf.entity.Player;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.custom.ReducedLives;
import randomfest.Randomfest;

class NonRandomfestReducedLives extends ReducedLives {
    private var randomfest: Randomfest;

    public function new(module: Map<String, Dynamic>, randomfest: Randomfest) {
        super(module);
        this.randomfest = randomfest;
    }

    public override function setLives(pid: Int, lives: Int, bottomBar: BottomBarInterface): Bool {
        var correspondingPlayer: Null<Player> = null;
        for (player in bottomBar.game.getPlayerList()) {
            if (player.pid == pid) {
                correspondingPlayer = player;
                break;
            }
        }

        if (correspondingPlayer != null && randomfest.isRandomfestPlayer(correspondingPlayer))
            return false;

        return super.setLives(pid, lives, bottomBar);
    }
}
