package faille.boss;

import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Hf;
import hf.mode.GameMode;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

@:build(patchman.Build.di())
class CustomPlayerHitbox {
    private var playerHitbox: Map<Player, Array<Array<Float>>>;

    @:diExport
    public var updateCustomHitbox(default, null): IPatch;

    /* Need to do -3 in x if player looks to the left. */
    static private var basePlayerHitbox(default, never): Array<Array<Float>> = [[-4, -5], [8, -5], [8, -20], [-4, -20]];
    private static function computePlayerCustomHitbox(player: Player): Array<Array<Float>> {
        var hitbox: Array<Array<Float>> = [];
        // TODO: Would doing a diff between previous and new position and applying that faster ?
        //       However then you need to pay attention to not diverge over time...
        for (point in basePlayerHitbox) {
            var offsetX: Float = player.x + (player.dir == 1 ? 0 : -3);
            hitbox.push([point[0] + offsetX, point[1] + player.y]);
        }
        return hitbox;
    }

    public function getHitbox(player: Player): Array<Array<Float>> {
        return playerHitbox.get(player);
    }

    public function new(): Void {
        playerHitbox = new Map<Player, Array<Array<Float>>>();

        updateCustomHitbox = Ref.auto(Player.update).after(function(hf: Hf, self: Player): Void {
            var hitbox: Array<Array<Float>> = computePlayerCustomHitbox(self);
            playerHitbox.set(self, hitbox);
        });
    }
}
