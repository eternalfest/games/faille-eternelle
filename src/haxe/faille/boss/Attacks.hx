package faille.boss;

import etwin.flash.MovieClip;
import etwin.Obfu;
import hf.entity.Player;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import faille.boss.Constants;
import faille.boss.EternalSadistSpirit;
import faille.boss.Spikes;

enum AttackState {
    Waiting;
    Attacking;
    Done;
}

class OffsetMovieClip {
    public var mc: MovieClip;
    public var offsetX: Float = 0;
    public var offsetY: Float = 0;
    public function new(mc: MovieClip, offsetX: Float, offsetY: Float): Void {
        this.mc = mc;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
}

class SpecialAttack {
    private var hf: Hf;
    private var game: GameMode;
    private var controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;};

    private var remainingWaitTime: Float = 40;
    private var remainingAttackTime: Float;
    private var sprite: Null<OffsetMovieClip>;

    public function new(hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalAttackDuration: Float, sprite: Null<OffsetMovieClip>): Void {
        this.hf = hf;
        this.game = game;
        this.controller = controller;
        this.remainingAttackTime = totalAttackDuration;
        this.sprite = sprite;
    }

    public function update(): AttackState {
        if (sprite != null) {
            sprite.mc._x = controller.getX() + sprite.offsetX;
            sprite.mc._y = controller.getY() + 15 + sprite.offsetY;
        }
        if (remainingWaitTime > 0) {
            remainingWaitTime -= hf.Timer.tmod;
            return AttackState.Waiting;
        }
        else if (remainingAttackTime > 0) {
            remainingAttackTime -= hf.Timer.tmod;
            return AttackState.Attacking;
        }
        return AttackState.Done;
    }

    public function destroy(): Void {
        if (sprite != null)
            sprite.mc.removeMovieClip();
    }
}

class TigerEyeAttack extends SpecialAttack {
    private var timeBetweenWaves: Float;
    private var timeUntilNextWave: Float;
    private var spikeAngles: Array<Float>;
    private var spikeSpeed: Float;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalTime: Float, timeBetweenWaves: Float, spikeAngles: Array<Float>, spikeSpeed: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(10);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, 0));
        this.timeBetweenWaves = timeBetweenWaves;
        this.timeUntilNextWave = timeBetweenWaves;
        this.spikeAngles = spikeAngles;
        this.spikeSpeed = spikeSpeed;
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextWave -= hf.Timer.tmod;
        if (timeUntilNextWave <= 0) {
            var target: Entity = game.getOne(hf.Data.PLAYER);
            var x: Float = controller.getX();
            var y: Float = controller.getY();
            var radianAngle: Float = Math.atan2(target.y - y, target.x - x);
            var degreeAngle: Float = radianAngle * Constants.radianToDegreeFactor;
            for (angleOffset in spikeAngles) {
                var spike: Spike = Spike.attach(hf, game, x, y);
                spike.throwAtAngle(degreeAngle + angleOffset, spikeSpeed);
                spike.filters = [Constants.redFilter];
                controller.addSpike(spike);
            }
            timeUntilNextWave += timeBetweenWaves;
        }

        return state;
    }
}

class JadeAttack extends SpecialAttack {
    private var timeBetweenWaves: Float;
    private var timeUntilNextWave: Float;
    private var additionalSpawns: Array<{function getX(): Float; function getY(): Float;}>;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalTime: Float, timeBetweenWaves: Float, additionalSpawns: Array<{function getX(): Float; function getY(): Float;}>): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(11);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, -5));
        this.timeBetweenWaves = timeBetweenWaves;
        this.timeUntilNextWave = timeBetweenWaves;
        this.additionalSpawns = additionalSpawns;
    }

    private function addHomingSpike(spawn: {function getX(): Float; function getY(): Float;}): Void {
        var spike: HomingSpike = HomingSpike.attach(hf, game, spawn.getX(), spawn.getY());
        spike.filters = [Constants.greenFilter];
        controller.addSpike(spike);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextWave -= hf.Timer.tmod;
        if (timeUntilNextWave <= 0) {
             timeUntilNextWave += timeBetweenWaves;
             addHomingSpike(controller);
             for (spawn in additionalSpawns)
                 addHomingSpike(spawn);
        }

        return state;
    }
}

class MoonPieceAttack extends SpecialAttack {
    private var timeBetweenWaves: Float;
    private var timeUntilNextWave: Float;
    private var speed: Float;
    private var additionalSpawns: Array<{function getX(): Float; function getY(): Float;}>;
    private var additionalSpawnsSpeed: Float;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalTime: Float, timeBetweenWaves: Float, speed: Float,  additionalSpawns: Array<{function getX(): Float; function getY(): Float;}>, additionalSpawnsSpeed: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(12);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, -1));
        this.timeBetweenWaves = timeBetweenWaves;
        this.timeUntilNextWave = timeBetweenWaves;
        this.speed = speed;
        this.additionalSpawns = additionalSpawns;
        this.additionalSpawnsSpeed = additionalSpawnsSpeed;
    }

    private function addZigZagSpike(spawn: {function getX(): Float; function getY(): Float;}, speed: Float): Void {
        var spike: ZigZagSpike = ZigZagSpike.attach(hf, game, spawn.getX(), spawn.getY(), 145 - Std.random(110), speed);
        spike.filters = [Constants.purpleFilter];
        controller.addSpike(spike);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextWave -= hf.Timer.tmod;
        if (timeUntilNextWave <= 0) {
             timeUntilNextWave += timeBetweenWaves;
             addZigZagSpike(controller, speed);
             for (spawn in additionalSpawns)
                 addZigZagSpike(spawn, additionalSpawnsSpeed);
        }

        return state;
    }
}

class DamoclesAttack extends SpecialAttack {
    private static var nbSpikes(default, never): Int = 8;
    private var timeBeforeSpikes: Float = 10;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(9);
        super(hf, game, controller, timeBeforeSpikes + 100, new OffsetMovieClip(mc, 0, -5));
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || timeBeforeSpikes <= 0)
            return state;

        timeBeforeSpikes -= hf.Timer.tmod;
        if (timeBeforeSpikes <= 0) {
            var angleOffset: Float = 360 / nbSpikes;
            for (player in game.getPlayerList()) {
                for (i in 0...nbSpikes) {
                    var spike: DamoclesSpike = DamoclesSpike.attach(hf, game, player, 100, i * angleOffset, 2, 150 + i * 15);
                    spike.filters = [Constants.darkFilter];
                    controller.addSpike(spike);
                }
            }
        }

        return state;
    }
}

class UltimateAttack extends SpecialAttack {
    private static var nbSpikes(default, never): Int = 12;
    private var timeBeforeSpikes: Float = 10;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalTime: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(117);
        super(hf, game, controller, timeBeforeSpikes + 100, new OffsetMovieClip(mc, 0, 0));
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || timeBeforeSpikes <= 0)
            return state;

        timeBeforeSpikes -= hf.Timer.tmod;
        if (timeBeforeSpikes <= 0) {
            for (i in 0...nbSpikes) {
                var angle: Float = i * (160 / nbSpikes);
                angle -= (nbSpikes - 1) / nbSpikes * 80;
                var spike: UltimateSpike = UltimateSpike.attach(hf, game, controller.getX(), controller.getY(), 90 + angle);
                spike.filters = [Constants.lightBlueFilter];
                controller.addSpike(spike);
            }
        }

        return state;
    }
}

class RainAttack extends SpecialAttack {
    private var timeBeforeSpikes: Float = 10;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(2);
        super(hf, game, controller, timeBeforeSpikes, new OffsetMovieClip(mc, 0, 0));
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || timeBeforeSpikes <= 0)
            return state;

        timeBeforeSpikes -= hf.Timer.tmod;
        if (timeBeforeSpikes <= 0) {
            var hole: Float = (Std.random(18) + 1) * 20;
            for (i in 0...20) {
                var x: Float = 20 * i;
                if (Math.abs(x - hole) > 30) {
                    var spike: AcceleratingFallingSpike = AcceleratingFallingSpike.attach(hf, game, x, -20);
                    spike.throwAtAngle(90, 2);
                    controller.addSpike(spike);
                }
            }
        }

        return state;
    }
}

class UnlimitedSpikesWorks extends SpecialAttack {
    private var timeBeforeSpikes: Float = 10;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}): Void {
        super(hf, game, controller, timeBeforeSpikes, null);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || timeBeforeSpikes <= 0)
            return state;

        timeBeforeSpikes -= hf.Timer.tmod;
        if (timeBeforeSpikes <= 0) {
            for (i in 0...7) {
                var x, y;
                do {
                    x = Std.random(20);
                    y = Std.random(25);
                } while (game.world.getCase({x: x, y: y}) != 1);
                controller.addSpike(TemporarySpike.attach(hf, game, (x + 0.5) * hf.Data.CASE_WIDTH, (y - 0.5) * hf.Data.CASE_HEIGHT, 400));
            }
        }

        return state;
    }
}

class LaserAttack extends SpecialAttack {
    private var timeBeforeLaser: Float = 10;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, totalTime: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(3);
        super(hf, game, controller, Math.max(timeBeforeLaser + 5, totalTime), new OffsetMovieClip(mc, 0, 0));
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        if (timeBeforeLaser > 0) {
            timeBeforeLaser -= hf.Timer.tmod;
            if (timeBeforeLaser <= 0) {
                /* We have the game handle the laser, because this will be done before the end of the laser (so as not to have a big delay before the next attack),
                 * and the controller only handles spikes. */
                var onLaserEnd: Laser -> Void = function(instance: Laser): Void {
                    /* If we are here this, and its member game, probably don't exist anymore, so only access things from the instance. */

                    /* Have the game call removeMovieClip later, so as to avoid it calling "endUpdate" on something we have "removeMovieClip"ed
                     * (since we get here from "update" of Laser). */
                    instance.getGame().killList.push(cast instance);
                    instance.getGame().removeFromList(hf.Data.ENTITY, cast instance);
                };
                var laser: Laser = Laser.attach(hf, game, controller.getX(), controller.getY(), onLaserEnd);
                game.addToList(hf.Data.ENTITY, cast laser);
            }
        }

        return state;
    }
}

class DevilSpiral extends SpecialAttack {
    private static var timeBetweenSpikes(default, never): Float = 2;

    private var timeUntilNextSpike: Float = timeBetweenSpikes;
    private var currentDegreesAngle: Float = Std.random(360);

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}): Void {
        super(hf, game, controller, Math.POSITIVE_INFINITY, null);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
            var spike: Spike = Spike.attach(hf, game, controller.getX(), controller.getY());
            spike.throwAtAngle(currentDegreesAngle, 4);
            controller.addSpike(spike);
            currentDegreesAngle += 11;
            timeUntilNextSpike += timeBetweenSpikes;
        }

        return state;
    }
}

/* EternalSadistReborn attacks. */

class RoseAttack extends SpecialAttack {
    private var nbWavesRemaining: Int;
    private var timeBetweenWaves: Float;
    private var timeUntilNextWave: Float;
    private var nbSpikesPerWave: Int;
    private var spikeSpeed: Float;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, nbWaves: Int, timeBetweenWaves: Float, nbSpikesPerWave: Int, spikeSpeed: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(10);
        nbWavesRemaining = nbWaves;
        this.timeBetweenWaves = timeBetweenWaves;
        timeUntilNextWave = timeBetweenWaves;
        this.nbSpikesPerWave = nbSpikesPerWave;
        this.spikeSpeed = spikeSpeed;
        /* The attacks ends when the waves are done, not after a specific amount of time.
         * While we could compute the time it should take, it would not be exact because it depends on hf.Timer.tmod, so let's not. */
        super(hf, game, controller, Math.POSITIVE_INFINITY, new OffsetMovieClip(mc, 0, 0));
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextWave -= hf.Timer.tmod;
        if (timeUntilNextWave <= 0) {
            var diffAngle: Float = 360 / nbSpikesPerWave;
            for (i in 0...nbSpikesPerWave) {
                var spike: Spike = Spike.attach(hf, game, controller.getX(), controller.getY());
                spike.throwAtAngle(i * diffAngle, spikeSpeed);
                spike.filters = [Constants.redFilter];
                controller.addSpike(spike);
            }

            timeUntilNextWave += timeBetweenWaves;
            --nbWavesRemaining;
            if (nbWavesRemaining == 0)
                return AttackState.Done;
        }

        return state;
    }
}

class RiffleAttackExplosion {
    private static inline var timeUntilSpike: Float = 40;
    private static inline var timeUntilDisparitionAfterSpike: Float = 20;

    private var hf: Hf;
    private var game: GameMode;
    private var controller: {function addSpike(spike: Spike): Void;};
    private var sprite: MovieClip;

    private var remainingTimeUntilSpike: Float = timeUntilSpike;
    private var remainingTimeUntilDisparition: Float = timeUntilDisparitionAfterSpike;
    private var spikeLifetime: Float;

    public function new(hf: Hf, game: GameMode, controller: {function addSpike(spike: Spike): Void;}, x: Float, y: Float, spikeLifetime: Float): Void {
        this.hf = hf;
        this.game = game;
        this.controller = controller;
        sprite = cast game.depthMan.attach("hammer_supa_smoke", hf.Data.DP_BADS);
        sprite._x = x;
        sprite._y = y;
        sprite.play();

        this.spikeLifetime = spikeLifetime;
    }

    public function update(): Bool {
        if (remainingTimeUntilSpike > 0) {
            remainingTimeUntilSpike -= hf.Timer.tmod;
            if (remainingTimeUntilSpike <= 0) {
                var spike: TemporaryHomingSpike = TemporaryHomingSpike.attach(hf, game, sprite._x, sprite._y, 6, 1.2, 3, spikeLifetime);
                spike.filters = [Constants.greenFilter];
                controller.addSpike(spike);
            }
        }
        else {
            remainingTimeUntilDisparition -= hf.Timer.tmod;
            if (remainingTimeUntilDisparition <= 0)
                return true;
        }

        return false;
    }

    public function destroy(): Void {
        sprite.removeMovieClip();
    }
}

class RiffleAttack extends SpecialAttack {
    private static inline var totalTime: Float = 400;

    private var timeBetweenMainSpikes: Float;
    private var timeUntilNextMainSpikes: Float;
    private var nbSimultaneousExplosions: Int;
    private var timeBetweenExplosions: Float;
    private var timeUntilNextExplosions: Float;
    private var spikeLifetime: Float;

    private var explosions: Array<RiffleAttackExplosion>;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, timeBetweenControllerSpikes: Float,
                         nbSimultaneousExplosions: Int, timeBetweenExplosions: Float, spikeLifetime: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(11);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, -5));

        timeBetweenMainSpikes = timeBetweenControllerSpikes;
        timeUntilNextMainSpikes = timeBetweenMainSpikes;
        this.nbSimultaneousExplosions = nbSimultaneousExplosions;
        this.timeBetweenExplosions = timeBetweenExplosions;
        timeUntilNextExplosions = 0; /* Spawn new explosions at the beginning. */
        this.spikeLifetime = spikeLifetime;

        explosions = [];
    }

    private function addHomingSpike(spawn: {function getX(): Float; function getY(): Float;}): Void {
        var spike: TemporaryHomingSpike = TemporaryHomingSpike.attach(hf, game, spawn.getX(), spawn.getY(), 4.5, 1.3, 1, spikeLifetime);
        spike.filters = [Constants.greenFilter];
        controller.addSpike(spike);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextMainSpikes -= hf.Timer.tmod;
        if (timeUntilNextMainSpikes <= 0) {
            addHomingSpike(controller);
            timeUntilNextMainSpikes += timeBetweenMainSpikes;
        }

        if (explosions.length == 0) {
            timeUntilNextExplosions -= hf.Timer.tmod;
            if (timeUntilNextExplosions <= 0) {
                for (i in 0...nbSimultaneousExplosions)
                    explosions.push(new RiffleAttackExplosion(hf, game, controller, Std.random(400), Std.random(500), spikeLifetime));
                timeUntilNextExplosions += timeBetweenExplosions;
            }
        }

        /* We assume they are all done at the same time. Even if wrong, they should be close enough and should have already thrown their spike anyway. */
        var done: Bool = false;
        for (explosion in explosions) {
            if (explosion.update())
                done = true;
        }
        if (done) {
            for (explosion in explosions)
                explosion.destroy();
            explosions = [];
        }

        return state;
    }

    override public function destroy(): Void {
        for (explosion in explosions)
            explosion.destroy();
        explosions = [];
        super.destroy();
    }
}

enum DistortionDirections {
    Four;
    Eight;
}
class DistortionAttack extends SpecialAttack {
    private static inline var totalTime: Float = 400;
	/* Left, Right, Up, Down, TopLeft, TopRight, BottomLeft, BottomRight */
    private static function workaroundTooManyStaticData7(): Array<Float> {
        return [-45, 135, 45, -135, 30, 100, -80, -150];
    }
    private static var angles(default, never): Array<Float> = workaroundTooManyStaticData7();
    private static function workaroundTooManyStaticData8(): Array<Null<Float>> {
        return [0, 400, null, null, 0, 400, 0, 400];
    }
    private static var xPositions(default, never): Array<Null<Float>> = workaroundTooManyStaticData8();
    private static function workaroundTooManyStaticData9(): Array<Null<Float>> {
        return [null, null, 0, 500, 0, 0, 500, 500];
    }
    private static var yPositions(default, never): Array<Null<Float>> = workaroundTooManyStaticData9();

    private var timeBetweenSpikes: Float;
    private var timeUntilNextSpike: Float;
    private var speed: Float;
    private var nbDirections: DistortionDirections;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, timeBetweenSpikes: Float, speed: Float, nbDirections: DistortionDirections): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(12);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, -1));
        this.timeBetweenSpikes = timeBetweenSpikes;
        this.timeUntilNextSpike = timeBetweenSpikes;
        this.speed = speed;
        this.nbDirections = nbDirections;
    }

    private function addZigZagSpike(x: Float, y: Float, angleInDegrees: Float, speed: Float): Void {
        var spike: ZigZagSpikeReborn = ZigZagSpikeReborn.attach(hf, game, x, y, angleInDegrees, speed);
        spike.filters = [Constants.purpleFilter];
        controller.addSpike(spike);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
             timeUntilNextSpike += timeBetweenSpikes;

             var index: Int = 0;
             switch (nbDirections) {
             case Four:
                 index = Std.random(4);
             case Eight:
                 index = Std.random(8);
             }
             var x: Float = xPositions[index] != null ? xPositions[index] : Std.random(300) + 50;
             var y: Float = yPositions[index] != null ? yPositions[index] : Std.random(400) + 50;

             addZigZagSpike(x, y, angles[index], speed);
        }

        return state;
    }
}

typedef StormDirectionData = {centerX: Float,
                              centerY: Float,
                              throwAngleInDegrees: Float,
                              nbSpikesWithoutHole: Int};

class StormAttack extends SpecialAttack {
    private static inline var centerToDiagonalLineCenterDistance: Float = 340;
    private static function workaroundTooManyStaticData11(): Array<StormDirectionData> {
        return [
            /* Top to bottom. */
            {centerX: 200, centerY: -20, throwAngleInDegrees: 90, nbSpikesWithoutHole: 20},
            /* Bottom to top. */
            {centerX: 200, centerY: 520, throwAngleInDegrees: -90, nbSpikesWithoutHole: 20},
            /* Left to right. */
            {centerX: -20, centerY: 250, throwAngleInDegrees: 0, nbSpikesWithoutHole: 25},
            /* Right to left. */
            {centerX: 420, centerY: 250, throwAngleInDegrees: 180, nbSpikesWithoutHole: 25},

            /* For the diagonals, the center of the line is computed by taking the center of the screen (200, 250) and moving it along the opposite direction to the planned movement. */

            /* From top left. */
            {centerX: 200 + centerToDiagonalLineCenterDistance * Math.cos(3 * Math.PI / 4), centerY: 250 + centerToDiagonalLineCenterDistance * Math.sin(3 * Math.PI / 4), throwAngleInDegrees: -45, nbSpikesWithoutHole: 32},
            /* From top right. */
            {centerX: 200 + centerToDiagonalLineCenterDistance * Math.cos(Math.PI / 4), centerY: 250 + centerToDiagonalLineCenterDistance * Math.sin(Math.PI / 4), throwAngleInDegrees: -135, nbSpikesWithoutHole: 32},
            /* From bottom left. */
            {centerX: 200 + centerToDiagonalLineCenterDistance * Math.cos(-3 * Math.PI / 4), centerY: 250 + centerToDiagonalLineCenterDistance * Math.sin(-3 * Math.PI / 4), throwAngleInDegrees: 45, nbSpikesWithoutHole: 32},
            /* From bottom right. */
            {centerX: 200 + centerToDiagonalLineCenterDistance * Math.cos(-Math.PI / 4), centerY: 250 + centerToDiagonalLineCenterDistance * Math.sin(-Math.PI / 4), throwAngleInDegrees: 135, nbSpikesWithoutHole: 32},
        ];
    }
    private static var stormDirectionsData(default, never): Array<StormDirectionData> = workaroundTooManyStaticData11();

    private var timeBetweenWalls: Float;
    private var timeUntilNextWall: Float;
    private var wallSpeed: Float;
    private var nbDirections: Int;

    public function new(hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, nbWalls: Int, timeBetweenWalls: Float, wallSpeed: Float, includeDiagonals: Bool): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(2);
        super(hf, game, controller, nbWalls * timeBetweenWalls, new OffsetMovieClip(mc, 0, 0));

        this.timeBetweenWalls = timeBetweenWalls;
        timeUntilNextWall = timeBetweenWalls;
        this.wallSpeed = wallSpeed;
        nbDirections = includeDiagonals ? 8 : 4;
    }

    private inline function createWall(): Void {
        var data: StormDirectionData = stormDirectionsData[Std.random(nbDirections)];
        var lineAngleInDegrees: Float = data.throwAngleInDegrees - 90;
        var lineAngleInRadians: Float = lineAngleInDegrees * Constants.degreeToRadianFactor;
        var spikeOffsetX: Float = 20 * Math.cos(lineAngleInRadians);
        var spikeOffsetY: Float = 20 * Math.sin(lineAngleInRadians);
        var distanceBetweenMiddleAndFirstSpike: Float = data.nbSpikesWithoutHole / 2 * 20;
        var firstSpikeX: Float = data.centerX + distanceBetweenMiddleAndFirstSpike * Math.cos(lineAngleInRadians - Math.PI);
        var firstSpikeY: Float = data.centerY + distanceBetweenMiddleAndFirstSpike * Math.sin(lineAngleInRadians - Math.PI);

        var holeSize: Int = Std.int(data.nbSpikesWithoutHole / 2.3);
        var holeStartIndex: Int = Std.random(data.nbSpikesWithoutHole - holeSize);
        var holeEndIndex: Int = holeStartIndex + holeSize;
        for (i in 0...data.nbSpikesWithoutHole) {
            if (i >= holeStartIndex && i < holeEndIndex)
                continue;
            var spike: StormSpike = StormSpike.attach(hf, game, firstSpikeX + i * spikeOffsetX, firstSpikeY + i * spikeOffsetY);
            spike.throwAtAngle(data.throwAngleInDegrees, wallSpeed);
            spike.filters = [Constants.yellowSpikeFilter];
            controller.addSpike(spike);
        }
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextWall -= hf.Timer.tmod;
        if (timeUntilNextWall <= 0) {
            timeUntilNextWall += timeBetweenWalls;
            createWall();
        }

        return state;
    }
}

class DamoclesRebornAttack extends SpecialAttack {
    private static inline var firstSpikeDelay: Float = 150;
    private static inline var initialDistanceToPlayer: Float = 100;
    private static inline var firingSpeed: Float = 3;

    private var nbSpikes: Int;
    private var timeBeforeSpikes: Float = 50;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, nbSpikes: Int): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(9);
        /* Considered in flight until half the spikes are thrown. */
        super(hf, game, controller, firstSpikeDelay + (nbSpikes / 2) * 30, new OffsetMovieClip(mc, 0, -5));
        this.nbSpikes = nbSpikes;
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || timeBeforeSpikes <= 0)
            return state;

        timeBeforeSpikes -= hf.Timer.tmod;
        if (timeBeforeSpikes <= 0) {
            var angleOffset: Float = 360 / nbSpikes;
            for (player in game.getPlayerList()) {
                var delays: Array<Float> = [];
                for (i in 0...nbSpikes)
                    delays.push(firstSpikeDelay + i * 30);
                for (i in 0...nbSpikes) {
                    var index = Std.random(delays.length);
                    var delay: Float = delays[index];
                    delays.splice(index, 1);
                    var spike: DamoclesSpike = DamoclesSpike.attach(hf, game, player, initialDistanceToPlayer, i * angleOffset, firingSpeed, delay, true);
                    spike.filters = [Constants.darkFilter];
                    controller.addSpike(spike);
                }
            }
        }

        return state;
    }
}

class IceFlowerBall {
    private static inline var timeUntilSpikeSpawn: Float = 40;
    private static inline var spikesStayingStillDuration: Float = 60;
    private static inline var nbSpikes: Int = 5;

    private var hf: Hf;
    private var game: GameMode;
    private var controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;};
    private var sprite: MovieClip;
    private var x: Float;
    private var y: Float;
    private var spikeSpeed: Float;
    private var spikeRadialSpeed: Float;
    private var remainingTimeUntilSpikeSpawn: Float = timeUntilSpikeSpawn;
    private var remainingLifetime: Float = 200;

    public function new(hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, x: Float, y: Float, spikeSpeed: Float, spikeRadialSpeed: Float): Void {
        this.hf = hf;
        this.game = game;
        this.controller = controller;
        sprite = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        sprite.gotoAndStop(10);
        sprite._x = x - 1;
        sprite._y = y + 11;
        this.x = x;
        this.y = y;
        this.spikeSpeed = spikeSpeed;
        this.spikeRadialSpeed = spikeRadialSpeed;
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");

        game.addToList(hf.Data.ENTITY, cast this);
    }

    @:keep
    public function update(): Void {
        if (remainingTimeUntilSpikeSpawn > 0) {
            remainingTimeUntilSpikeSpawn -= hf.Timer.tmod;
            if (remainingTimeUntilSpikeSpawn <= 0) {
                var diffAngle: Float = 360 / nbSpikes;
                var initialAngle: Float = Math.random() * 360;
                for (i in 0...nbSpikes) {
                    var spike: IceFlowerSpike = IceFlowerSpike.attach(hf, game, x, y, 20, initialAngle + i * diffAngle, spikeSpeed, spikeRadialSpeed);
                    spike.filters = [Constants.darkBlueSpikeFilter];
                    controller.addSpike(spike);
                }
            }
        }

        if (remainingLifetime > 0) {
            remainingLifetime -= hf.Timer.tmod;
            if (remainingLifetime <= 0)
                destroy();
        }
    }

    @:keep
    public function endUpdate(): Void {
    }

    @:keep
    public function destroy(): Void {
        sprite.removeMovieClip();
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
        game.removeFromList(hf.Data.ENTITY, cast this);
    }
}

class IceFlower extends SpecialAttack {
    private var nbBalls: Int;
    private var spikeSpeed: Float;
    private var spikeRadialSpeed: Float;
    private var delays: Array<Float>;
    private var innerTime: Float = 0;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, nbBalls: Int, spikeSpeed: Float, spikeRadialAbsoluteSpeed: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(25);
        super(hf, game, controller, 500, new OffsetMovieClip(mc, 0, -5));
        this.nbBalls = nbBalls;
        this.spikeSpeed = spikeSpeed;
        this.spikeRadialSpeed = (Std.random(2) * 2 - 1) * spikeRadialAbsoluteSpeed;

        var timeBetweenBalls: Float = 400 / nbBalls;
        delays = [];
        for (i in 0...nbBalls)
            delays.push(timeBetweenBalls * i + (timeBetweenBalls / 2));
        delays.sort(function(delay1, delay2) {return Std.int(delay1 - delay2);});
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done || delays.length == 0)
            return state;

        innerTime += hf.Timer.tmod;
        if (innerTime >= delays[0]) {
            delays.splice(0, 1);
            new IceFlowerBall(hf, game, controller, Std.random(300) + 50, Std.random(400) + 50, spikeSpeed, spikeRadialSpeed);
        }

        return state;
    }
}

class HeavenlyPunishment extends SpecialAttack {
    private static inline var timeBetweenSpikes: Float = 5;
    private static inline var spikeSpeed: Float = 6;

    private var timeBeforeTeleportation: Float = 100;
    private var timeUntilNextSpike: Float = timeBetweenSpikes;
    private var lasers: Array<RotatingLaser>;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void; function preventTeleporting(): Void; function forceTeleport(x: Float, y: Float): Void; function stopTemporarySpikesSpawning(): Void; function resumeTemporarySpikesSpawning(): Void;}): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mc.gotoAndStop(117);
        super(hf, game, controller, 800, new OffsetMovieClip(mc, 0, 0));
        controller.preventTeleporting();
        controller.stopTemporarySpikesSpawning();
        lasers = [];
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        if (timeBeforeTeleportation > 0) {
            timeBeforeTeleportation -= hf.Timer.tmod;
            if (timeBeforeTeleportation <= 0) {
                (cast controller).forceTeleport(200, 250); // TODO: What's the best way to properly do that?
                /* We want to own the laser so that we can destroy it if needed (i.e. the boss takes damage and is forced to its next phase).
                 * So we won't let the game control it, so we don't need anything from the callback. */
                var firstLaser: RotatingLaser = RotatingLaser.attach(hf, game, controller.getX(), controller.getY(), function(instance: Laser): Void {}, 300, 0, 40, 600, 6);
                firstLaser.setAngle(0);
                lasers.push(firstLaser);
                var secondLaser: RotatingLaser = RotatingLaser.attach(hf, game, controller.getX(), controller.getY(), function(instance: Laser): Void {}, 300, 0, 40, 600, 6);
                secondLaser.setAngle(90);
                lasers.push(secondLaser);
            }
            else {
                return state; /* Don't throw spikes before teleportation. */
            }
        }

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
            var spike: Spike = Spike.attach(hf, game, controller.getX(), controller.getY());
            spike.throwAtAngle(Math.random() * 360, spikeSpeed);
            spike.filters = [Constants.whiteSpikeFilter];
            controller.addSpike(spike);
            timeUntilNextSpike += timeBetweenSpikes;
        }

        // TODO? Should we use our ugly update-remove loop?
        var toRemoveLasers: Bool = false;
        for (laser in lasers) {
            if (laser.updateReborn()) {
                /* Both lasers should end pretty much at the same time. */
                toRemoveLasers = true;
            }
            else {
                laser.endUpdate();
            }
        }
        if (toRemoveLasers) {
            for (laser in lasers)
                laser.destroy();
            lasers = [];
        }

        return state;
    }

    override public function destroy(): Void {
        (cast controller).resumeTemporarySpikesSpawning();
        for (laser in lasers)
            laser.destroy();
        super.destroy();
    }
}

enum MetaKamiSpikeState {
    Descending;
    Waiting;
}
class MetaKamiSpike {
    private static inline var nbSpikes: Int = 100;
    private static inline var initialY: Float = -100;
    private static inline var totalWaitingTime: Float = 50;
    private static inline var dy: Float = 13;
    private static inline var maxOffsetX: Float = 50;

    private var spikes: Array<KamiSpike>;
    private var offsets: Array<{offsetX: Float, offsetY: Float}>;
    private var controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;};
    private var x: Float;
    private var y: Float = initialY;
    private var state: MetaKamiSpikeState = MetaKamiSpikeState.Descending;
    private var remainingWaitingTime: Float = totalWaitingTime;

    public function new(hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, x: Float): Void {
        this.controller = controller;
        this.x = x;
        spikes = [];
        offsets = [];

        for (i in 0...nbSpikes) {
            var offsetY: Float = Math.random() * 300;
            var randX: Float = Math.min(offsetY, maxOffsetX);
            var offsetX: Float = Math.random() * randX - randX / 2 + maxOffsetX / 2;
            if (i < nbSpikes / 2)
                offsetX -= 50;
            else
                offsetX += 50;
            var spike: KamiSpike = KamiSpike.attach(hf, game, x + offsetX, y - offsetY);
            spike.filters = [Constants.orangeSpikeFilter];
            spikes.push(spike);
            offsets.push({offsetX: offsetX, offsetY: offsetY});
        }
    }

    public function update(hf: Hf, game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        switch (state) {
        case MetaKamiSpikeState.Descending:
            y += dy * hf.Timer.tmod;

            for (i in 0...spikes.length) {
                var offsets: {offsetX: Float, offsetY: Float} = offsets[i];
                spikes[i].moveTo(x + offsets.offsetX, y - offsets.offsetY);
            }

            if (y >= 500)
                state = MetaKamiSpikeState.Waiting;
        case MetaKamiSpikeState.Waiting:
            remainingWaitingTime -= hf.Timer.tmod;
            if (remainingWaitingTime <= 0) {
                /* Transfer the spikes to the controller and inform this can be destroyed. */
                for (spike in spikes) {
                    spike.rethrow();
                    controller.addSpike(spike);
                }
                spikes = [];
                return true;
            }
        }

        for (spike in spikes) {
            spike.update(game, customPlayerHitbox);
            spike.endUpdate();
        }

        return false;
    }

    public function destroy(game: GameMode): Void {
        /* In case the boss was forced into another phase while we still control the spikes. Drop them, it's probably better. */
        for (spike in spikes)
            spike.prettyRemove(game);
        spikes = [];
    }
}

class Kami extends SpecialAttack {
    private static inline var waitingTime: Float = 50;
    private static inline var laserDXScale: Float = 1100 / waitingTime;
    private static inline var laserDAlpha: Float = 50 / waitingTime;

    private var timeUntilMetaKamiSpike: Float = waitingTime;
    private var metaKamiSpike: Null<MetaKamiSpike> = null;
    private var kamiX: Float;
    private var customPlayerHitbox: CustomPlayerHitbox;
    private var laserSprite: Null<MovieClip>;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, customPlayerHitbox: CustomPlayerHitbox): Void {
        var mc: MovieClip = cast game.depthMan.attach(Obfu.raw("boss_reborn_ring_attack"), hf.Data.DP_BADS);
        super(hf, game, controller, 250, new OffsetMovieClip(mc, 0, -15));
        kamiX = 100 + Math.random() * 200;
        this.customPlayerHitbox = customPlayerHitbox;
        laserSprite = cast game.depthMan.attach("hammer_fx_death", hf.Data.DP_BADS);
        laserSprite._x = kamiX;
        laserSprite._y = 0;
        laserSprite._xscale = 0;
        laserSprite._yscale = 200;
        laserSprite.gotoAndStop(2);
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        if (timeUntilMetaKamiSpike > 0) {
            var tmod: Float = hf.Timer.tmod;
            timeUntilMetaKamiSpike -= tmod;
            laserSprite._xscale += tmod * laserDXScale;
            laserSprite._alpha -= tmod * laserDAlpha;
            if (timeUntilMetaKamiSpike <= 0)
                metaKamiSpike = new MetaKamiSpike(hf, game, controller, kamiX);
        }

        if (metaKamiSpike != null) {
            if (metaKamiSpike.update(hf, game, customPlayerHitbox)) {
                metaKamiSpike.destroy(game);
                metaKamiSpike = null;
                /* I like the visual when you let it until it rethrows the spikes. */
                laserSprite.removeMovieClip();
                laserSprite = null;
            }
        }

        return state;
    }

    override public function destroy(): Void {
        if (metaKamiSpike != null)
            metaKamiSpike.destroy(game);
        metaKamiSpike = null;
        if (laserSprite != null)
            laserSprite.removeMovieClip();
        laserSprite = null;
        super.destroy();
    }
}

class OtherworldlyArmy extends SpecialAttack {
    private static inline var totalTime: Float = 400;
    private static inline var spikeWaitingTime: Float = 50;
    private static inline var outscreenSpikeSpeed: Float = 2; /* Doesn't really matter much. */

    private var timeBetweenSpikes: Float;
    private var timeUntilNextSpike: Float;
    private var speed: Float;

    public function new (hf: Hf, game: GameMode, controller: {function getX(): Float; function getY(): Float; function addSpike(spike: Spike): Void;}, timeBetweenSpikes: Float, speed: Float): Void {
        var mc: MovieClip = cast game.depthMan.attach("hammer_item_score", hf.Data.DP_BADS);
        mc.gotoAndStop(12);
        super(hf, game, controller, totalTime, new OffsetMovieClip(mc, 0, -1));
        this.timeBetweenSpikes = timeBetweenSpikes;
        this.timeUntilNextSpike = timeBetweenSpikes;
        this.speed = speed;
    }

    override public function update(): AttackState {
        var state: AttackState = super.update();
        if (state == AttackState.Waiting || state == AttackState.Done)
            return state;

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
             timeUntilNextSpike += timeBetweenSpikes;

             /* Either the x is random, in which case y is either above or below the level, or the y is random, in which case x is either on the left or the right. */
             var randomX: Bool = Std.random(2) == 0;
             var x: Float = randomX ? Math.random() * 400 : (Std.random(2) == 0 ? -40 : 440);
             var y: Float = randomX ? (Std.random(2) == 0 ? -40 : 540) : Math.random() * 500;
             var spike: OtherwordlySpike = OtherwordlySpike.attach(hf, game, x, y, spikeWaitingTime, speed);
             spike.filters = [Constants.purpleFilter];

             /* To throw it toward the screen... */
             var targetX: Float = Math.random() * 360 + 20;
             var targetY: Float = Math.random() * 460 + 20;
             spike.throwAtAngle(Math.atan2(targetY - y, targetX - x) * Constants.radianToDegreeFactor, outscreenSpikeSpeed);
             controller.addSpike(spike);
        }

        return state;
    }
}
