package faille.boss;

import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class Constants {
    public static var radianToDegreeFactor(default, never): Float = 180 / Math.PI;
    public static var degreeToRadianFactor(default, never): Float = Math.PI / 180;
    public static var twoPi(default, never): Float = 2 * Math.PI;
    public static var redFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(30, 1, 1).toFilter();
    public static var greenFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(230, 1, 1).toFilter();
    public static var purpleFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(100, 1, 1).toFilter();
    public static var darkFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, -1).toFilter();
    public static var lightBlueFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(-150, 50, 1).toFilter();
    public static var brownFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1).toFilter(); // TODO
    public static var purpleExplosionFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(120, 9, 1).toFilter();
    public static var redRebornFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(10, 5, 1).toFilter();
    public static var blueRebornFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(160, 1, 1).toFilter();
    public static var greenRebornFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(260, 1, 1).toFilter();
    public static var yellowSpikeFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(300, 2, 1).toFilter();
    public static var darkBlueSpikeFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(140, 8, 1).toFilter();
    public static var whiteSpikeFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 12).toFilter();
    public static var orangeSpikeFilter(default, never): ColorMatrixFilter = ColorMatrix.fromHsv(0, 3, 1).toFilter();
}
