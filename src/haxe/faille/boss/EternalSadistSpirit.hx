package faille.boss;

import etwin.flash.MovieClip;
import hf.entity.bomb.player.Classic;
import hf.entity.Player;
import hf.entity.supa.Smoke;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;
import faille.boss.Attacks;
import faille.boss.Eye;
import faille.boss.Spikes;
import faille.boss.CustomPlayerHitbox;

class EternalSadistSpirit {
    private static var invincibilityTotalDuration(default, never): Float = 50;
    private static var blinkDuration(default, never): Float = 3;
    private static var blinkAlpha(default, never): Float = 40;
    private static var totalLifepoints(default, never): Int = 20;
    private static var toPhase2ChangeLifepoints(default, never): Int = 10;
    private static var phase3TotalDuration(default, never): Float = 750;

    private var hf: Hf;
    private var game: GameMode;
    private var x: Float;
    private var y: Float;

    private var blankSprite: MovieClip;
    private var mainSprite: MovieClip;
    private var attackSprite: Null<MovieClip> = null;
    private var lifeSprite: MovieClip;

    private var customPlayerHitbox: CustomPlayerHitbox;
    private var onDeathCallback: EternalSadistSpirit -> Void;
    private var decorativeSpikes: Array<Spike>;

    private var remainingInvincibility: Float = 0;
    private var timeUntilNextBlink: Float = 0;
    private var alpha: Float = 100;
    private var lifepoints: Int = totalLifepoints;
    private var currentPhase: Int = 1;
    private var remainingLifetime: Null<Float> = null;

    private var timeBetweenSpikes: Float = 70;
    private var timeUntilNextSpike: Float;
    private var spikes: Array<Spike>;

    private var specialAttacks: Array<Array<Void -> SpecialAttack>>;
    private var timeBetweenAttacks: Float = 300;
    private var timeUntilNextAttack: Float;
    private var currentSpecialAttack: Null<SpecialAttack> = null;

    private var eyes: Array<Eye>;

    public function new(hf: Hf, game: GameMode, x: Float, y: Float, customPlayerHitbox: CustomPlayerHitbox, onDeathCallback: EternalSadistSpirit -> Void): Void {
        this.hf = hf;
        this.game = game;
        this.x = x;
        this.y = y;
        this.customPlayerHitbox = customPlayerHitbox;
        this.onDeathCallback = onDeathCallback;
        timeUntilNextSpike = timeBetweenSpikes;
        decorativeSpikes = [];
        spikes = [];
        specialAttacks  = [
            [function(): SpecialAttack {return new TigerEyeAttack(hf, game, this, 180, 20, [-15, 0, 15], 4);},
             function(): SpecialAttack {return new JadeAttack(hf, game, this, 130, 25, []);},
             function(): SpecialAttack {return new MoonPieceAttack(hf, game, this, 200, 30, 3, [], 0);}],
            [function(): SpecialAttack {return new TigerEyeAttack(hf, game, this, 120, 15, [-15, -5, 5, 15], 5);},
             function(): SpecialAttack {return new JadeAttack(hf, game, this, 160, 45, cast eyes);},
             function(): SpecialAttack {return new MoonPieceAttack(hf, game, this, 230, 50, 4, cast eyes, 5);},
             function(): SpecialAttack {return new DamoclesAttack(hf, game, this);},
             function(): SpecialAttack {return new UltimateAttack(hf, game, this, 200);},
             function(): SpecialAttack {return new RainAttack(hf, game, this);},
             function(): SpecialAttack {return new UnlimitedSpikesWorks(hf, game, this);},
             function(): SpecialAttack {return new LaserAttack(hf, game, this, 150);}],
            [function(): SpecialAttack {return new DevilSpiral(hf, game, this);}]
        ];
        timeUntilNextAttack = timeBetweenAttacks;
        eyes = [];

        blankSprite = cast game.depthMan.attach("hammer_interf_extend", hf.Data.DP_BADS);
        blankSprite.gotoAndStop(13);
        blankSprite._xscale = 190;
        blankSprite._yscale = 190;
        blankSprite._x = x;
        blankSprite._y = y + 20;

        mainSprite = cast game.depthMan.attach("hammer_interf_extend", hf.Data.DP_BADS);
        mainSprite.gotoAndStop(6);
        mainSprite._x = x;
        mainSprite._y = y + 10;

        lifeSprite = cast game.depthMan.attach("hammer_interf_boss_bar", hf.Data.DP_SPRITE_BACK_LAYER);
        lifeSprite._x = hf.Data.GAME_WIDTH * 0.5;
        lifeSprite._y = 10;

        var a: Int = 2;
        var lambda: Int = 21;
        for (i in 0...5) {
            var t: Float = a / 2 * (i - 2);
            var spike: Spike = Spike.attach(hf, game, x + lambda * t, y - 1.5 * lambda * Math.sqrt(1 - (t / (a + 0.3)) * (t / (a + 0.3))));
            spike.setAngle((i - 2) * 20 - 90);
            decorativeSpikes.push(spike);
        }
        for (i in 5...10) {
            var t: Float = a / 2 * (i - 7);
            var spike: Spike = Spike.attach(hf, game, x + lambda * t, y + 1.5 * lambda * Math.sqrt(1 - (t / (a + 0.3)) * (t / (a + 0.3))));
            spike.setAngle(90 - (i - 7) * 20);
            decorativeSpikes.push(spike);
        }

        game.addToList(hf.Data.ENTITY, cast this);
    }

    public inline function getX(): Float {
        return x;
    }

    public inline function getY(): Float {
        return y;
    }

    public inline function addSpike(spike: Spike): Void {
        spikes.push(spike);
    }

    private inline function isInvincible(): Bool {
        return remainingInvincibility > 0;
    }

    private inline function onLastPhase(): Bool {
        return currentPhase == 3;
    }

    private function startBlinking(): Void {
        timeUntilNextBlink = blinkDuration;
    }

    private function stopBlinking(): Void {
        alpha = 100;
    }

    private function startInvincibility(duration: Float): Void {
        remainingInvincibility = duration;
        startBlinking();
    }

    private function changePhase(): Void {
        ++currentPhase;
        if (currentPhase == 2) {
            eyes.push(new Eye(hf, game, {x: getX(), y: getY()}, 80, Math.PI));
            eyes.push(new Eye(hf, game, {x: getX(), y: getY()}, 80, 0));
            timeBetweenSpikes = 40;
            timeBetweenAttacks = 150;
        }
        else {
            for (eye in eyes) {
                eye.setTimeBetweenSpikes(40);
                eye.increaseTargetDistanceBy(100);
                eye.setRadialSpeed(3 * Constants.degreeToRadianFactor);
            }
            timeBetweenSpikes = 30;
            timeBetweenAttacks = 70;
            remainingLifetime = phase3TotalDuration;
            startInvincibility(phase3TotalDuration);
        }
        stopSpecialAttack();
    }

    public function takeDamage(): Void {
        if (isInvincible())
            return;
        startInvincibility(invincibilityTotalDuration);
        --lifepoints;
        if (currentPhase == 1 && lifepoints <= toPhase2ChangeLifepoints)
            changePhase();
        if (currentPhase == 2 && lifepoints <= 1)
            changePhase();
        /* The boss shouldn't take damage in phase 3. */
    }

    private function startSpecialAttack(): Void {
        var currentPhaseSpecialAttacks: Array<Void -> SpecialAttack> = specialAttacks[currentPhase - 1];
        currentSpecialAttack = currentPhaseSpecialAttacks[Std.random(currentPhaseSpecialAttacks.length)]();
    }

    private function stopSpecialAttack(): Void {
        timeUntilNextAttack = timeBetweenAttacks;
        currentSpecialAttack.destroy();
        currentSpecialAttack = null;
    }

    @:keep
    public function update(): Void {
        if (isInvincible()) {
            timeUntilNextBlink -= hf.Timer.tmod;
            if (timeUntilNextBlink <= 0) {
                alpha = alpha == 100 ? blinkAlpha : 100;
                timeUntilNextBlink += blinkDuration;
            }

            remainingInvincibility -= hf.Timer.tmod;
            if (remainingInvincibility <= 0)
                stopBlinking();
        }

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
            var target: Entity = game.getOne(hf.Data.PLAYER);
            var radianAngle: Float = Math.atan2(target.y - getY(), target.x - getX());
            var spike: Spike = Spike.attach(hf, game, getX(), getY());
            spike.throwAtAngle(radianAngle * Constants.radianToDegreeFactor, 4);
            spikes.push(spike);
            timeUntilNextSpike += timeBetweenSpikes;
        }

        if (timeUntilNextAttack > 0) {
            timeUntilNextAttack -= hf.Timer.tmod;
            if (timeUntilNextAttack <= 0) {
                startSpecialAttack();
            }
        }
        else {
            if (currentSpecialAttack.update() == AttackState.Done)
                stopSpecialAttack();
        }

        if (remainingLifetime != null) {
            var wasAbove60: Bool = remainingLifetime > 60;
            remainingLifetime -= hf.Timer.tmod;
            game.shake(1, 4);
            if (Std.random(10) == 0)
                game.fxMan.attachExplosion(getX() - 40 + Std.random(80), getY() - 20 + Std.random(40), 30);
            if (remainingLifetime <= 60 && wasAbove60) {
                var smoke: Smoke = hf.entity.supa.Smoke.attach(game);
                smoke.x = getX();
                smoke.y = getY();
                smoke._x = getX();
                smoke._y = getY();
                smoke.lifeTimer = 75;
                smoke.totalLife = 75;
            }
            if (remainingLifetime <= 0)
                destroy();
        }

        for (spike in decorativeSpikes) {
            spike.update(game, customPlayerHitbox); /* Those should not get offscreen. */
            spike.endUpdate();
        }

        var nbSpikes: Int = spikes.length;
        var i: Int = 0;
        while (i < nbSpikes) {
            var spike: Spike = spikes[i];
            if (spike.update(game, customPlayerHitbox)) {
                spike.removeMovieClip();
                spikes.splice(i, 1);
                --nbSpikes;
            }
            else {
                spike.endUpdate();
                ++i;
            }
        }

        for (eye in eyes) {
            eye.update(customPlayerHitbox);
            eye.endUpdate();
        }
    }

    @:keep
    public function endUpdate(): Void {
        blankSprite._alpha = alpha;
        mainSprite._alpha = alpha;
        for (spike in decorativeSpikes)
            spike._alpha = alpha;
        if (attackSprite != null)
            attackSprite._alpha = alpha;

        var visualHealth: Float = onLastPhase() ? remainingLifetime / phase3TotalDuration : lifepoints / totalLifepoints;
        (cast lifeSprite).bar._xscale = visualHealth * 100;
    }

    public function destroy(): Void {
        onDeathCallback(this);

        stopSpecialAttack();
        blankSprite.removeMovieClip();
        mainSprite.removeMovieClip();
        for (spike in decorativeSpikes)
            spike.removeMovieClip();
        decorativeSpikes = [];
        if (attackSprite != null)
            attackSprite.removeMovieClip();
        lifeSprite.removeMovieClip();
        for (spike in spikes)
            spike.prettyRemove(game);
        spikes = [];
        for (eye in eyes)
            eye.destroy();

        game.removeFromList(hf.Data.ENTITY, cast this);
    }
}

class EternalSadistSpiritSpawner implements IAction {
    public var name(default, null): String = Obfu.raw("createPicBoss");
    public var isVerbose(default, null): Bool = false;

    private var manager: EternalSadistSpiritManager;

    public function new(manager: EternalSadistSpiritManager): Void {
        this.manager = manager;
    }

    public function run(ctx: IActionContext): Bool {
        var game: GameMode = ctx.getGame();
        var x: Float = game.flipCoordReal(ctx.getFloat(Obfu.raw("xr")));
        var y: Float = ctx.getFloat(Obfu.raw("yr"));
        manager.CreateBoss(ctx.getHf(), game, x, y);
        return false;
    }
}

class EternalSadistSpiritDeadCheck implements IAction {
    public var name(default, null): String = Obfu.raw("picBossDeath");
    public var isVerbose(default, null): Bool = false;

    private var manager: EternalSadistSpiritManager;

    public function new(manager: EternalSadistSpiritManager): Void {
        this.manager = manager;
    }

    public function run(ctx: IActionContext): Bool {
        return manager.isBossDead();
    }
}

@:build(patchman.Build.di())
class EternalSadistSpiritManager {
    private var customPlayerHitbox: CustomPlayerHitbox;
    private var boss: Null<EternalSadistSpirit> = null;

    public function CreateBoss(hf: Hf, game: GameMode, x: Float, y: Float): Void {
        boss = new EternalSadistSpirit(hf, game, x, y, customPlayerHitbox, function (under6T: EternalSadistSpirit): Void {
            boss = null;
        });
    }

    public function isBossDead(): Bool {
        return boss == null;
    }

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;

    @:diExport
    public var hurtBossOnExplosion(default, null): IPatch;

    public function new(hf: Hf, customPlayerHitbox: CustomPlayerHitbox): Void {
        this.customPlayerHitbox = customPlayerHitbox;

        hf.Std.registerClass(Obfu.raw("boss_spike"), Spike);
        hf.Std.registerClass(Obfu.raw("boss_homing_spike"), HomingSpike);
        hf.Std.registerClass(Obfu.raw("boss_zigzag_spike"), ZigZagSpike);
        hf.Std.registerClass(Obfu.raw("boss_damocles_spike"), DamoclesSpike);
        hf.Std.registerClass(Obfu.raw("boss_ultimate_spike"), UltimateSpike);
        hf.Std.registerClass(Obfu.raw("boss_falling_spike"), AcceleratingFallingSpike);
        hf.Std.registerClass(Obfu.raw("boss_temporary_spike"), TemporarySpike);
        hf.Std.registerClass(Obfu.raw("boss_laser"), Laser);

        actions = FrozenArray.of(new EternalSadistSpiritSpawner(this),
                                 new EternalSadistSpiritDeadCheck(this));

        hurtBossOnExplosion = Ref.auto(Classic.onExplode).before(function(hf: Hf, self: Classic): Void {
            if (boss == null)
                return;

            var diffX: Float = self.x - boss.getX();
            var diffY: Float = self.y - boss.getY();
            var squaredDistance: Float = diffX * diffX + diffY * diffY;
            if (squaredDistance < self.radius * self.radius)
                boss.takeDamage();
        });
    }
}
