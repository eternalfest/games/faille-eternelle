package faille.boss;

import etwin.ds.FrozenArray;
import etwin.flash.filters.ColorMatrixFilter;
import etwin.flash.MovieClip;
import etwin.Obfu;
import hf.entity.Player;
import hf.entity.bomb.PlayerBomb;
import hf.entity.supa.Smoke;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import patchman.IPatch;
import patchman.Ref;
import faille.boss.Constants;
import faille.boss.EyeReborn;
import faille.boss.Spikes;
import faille.boss.CustomPlayerHitbox;
import faille.boss.Attacks;
import color_matrix.ColorMatrix;

private enum BodyPart {
    MainBody;
    LeftWing;
    RightWing;
}

class EternalSadistReborn {
    private static function workaroundTooManyStaticData(): Array<DecorativeSpikeData> {
        var colorNormal: Null<ColorMatrixFilter> = null;
        return [
            // Closest top row
            {xOffset: -40, yOffset: -7, rotation: -30, color: colorNormal}, {xOffset: -20, yOffset: -16, rotation: -15, color: colorNormal}, {xOffset: 0, yOffset: -20, rotation: 0, color: colorNormal},
            {xOffset: 20, yOffset: -16, rotation: 15, color: colorNormal}, {xOffset: 40, yOffset: -7, rotation: 30, color: colorNormal},
            // Closest bottom row
            {xOffset: -40, yOffset: 27, rotation: -150, color: colorNormal}, {xOffset: -20, yOffset: 36, rotation: -165, color: colorNormal}, {xOffset: 0, yOffset: 40, rotation: 180, color: colorNormal},
            {xOffset: 20, yOffset: 36, rotation: 165, color: colorNormal}, {xOffset: 40, yOffset: 27, rotation: 150, color: colorNormal},
            // Other top row
            {xOffset: -50, yOffset: -12, rotation: -40, color: Constants.brownFilter}, {xOffset: -30, yOffset: -21, rotation: -25, color: Constants.brownFilter}, {xOffset: -10, yOffset: -27, rotation: -10, color: Constants.brownFilter},
            {xOffset: 10, yOffset: -27, rotation: 10, color: Constants.brownFilter}, {xOffset: 30, yOffset: -21, rotation: 25, color: Constants.brownFilter}, {xOffset: 50, yOffset: -12, rotation: 40, color: Constants.brownFilter},
            // Other bottom row
            {xOffset: -50, yOffset: 32, rotation: -140, color: Constants.brownFilter}, {xOffset: -30, yOffset: 41, rotation: -155, color: Constants.brownFilter}, {xOffset: -10, yOffset: 47, rotation: -170, color: Constants.brownFilter},
            {xOffset: 10, yOffset: 47, rotation: 170, color: Constants.brownFilter}, {xOffset: 30, yOffset: 41, rotation: 155, color: Constants.brownFilter}, {xOffset: 50, yOffset: 32, rotation: 140, color: Constants.brownFilter},
            // Closest left row
            {xOffset: -85, yOffset: -10, rotation: -45, color: Constants.brownFilter}, {xOffset: -95, yOffset: 10, rotation: -90, color: Constants.brownFilter}, {xOffset: -85, yOffset: 30, rotation: -135, color: Constants.brownFilter},
            // Middle left row
            {xOffset: -90, yOffset: -22, rotation: -36, color: colorNormal}, {xOffset: -102, yOffset: -2, rotation: -72, color: colorNormal}, {xOffset: -102, yOffset: 22, rotation: -108, color: colorNormal},
            {xOffset: -90, yOffset: 42, rotation: -144, color: colorNormal},
            // Furthest left row
            {xOffset: -95, yOffset: -30, rotation: -30, color: Constants.brownFilter}, {xOffset: -110, yOffset: -13, rotation: -60, color: Constants.brownFilter}, {xOffset: -115, yOffset: 10, rotation: -90, color: Constants.brownFilter},
            {xOffset: -110, yOffset: 33, rotation: -120, color: Constants.brownFilter}, {xOffset: -95, yOffset: 50, rotation: -150, color: Constants.brownFilter},
            // Closest right row
            {xOffset: 85, yOffset: -10, rotation: 45, color: Constants.brownFilter}, {xOffset: 95, yOffset: 10, rotation: 90, color: Constants.brownFilter}, {xOffset: 85, yOffset: 30, rotation: 135, color: Constants.brownFilter},
            // Middle right row
            {xOffset: 90, yOffset: -22, rotation: 36, color: colorNormal}, {xOffset: 102, yOffset: -2, rotation: 72, color: colorNormal}, {xOffset: 102, yOffset: 22, rotation: 108, color: colorNormal},
            {xOffset: 90, yOffset: 42, rotation: 144, color: colorNormal},
            // Furthest right row
            {xOffset: 95, yOffset: -30, rotation: 30, color: Constants.brownFilter}, {xOffset: 110, yOffset: -13, rotation: 60, color: Constants.brownFilter}, {xOffset: 115, yOffset: 10, rotation: 90, color: Constants.brownFilter},
            {xOffset: 110, yOffset: 33, rotation: 120, color: Constants.brownFilter}, {xOffset: 95, yOffset: 50, rotation: 150, color: Constants.brownFilter}
        ];
    }
    private static var bossDecorativeSpikesData(default, never): Array<DecorativeSpikeData> = workaroundTooManyStaticData();

    private static function workaroundTooManyStaticData2(): Array<EyeData> {
        return [
                {spikes: [{xOffset: -13, yOffset: -13, rotation: -45, color: Constants.redRebornFilter}, {xOffset: 13, yOffset: -13, rotation: 45, color: Constants.redRebornFilter},
                          {xOffset: -13, yOffset: 13, rotation: -135, color: Constants.redRebornFilter}, {xOffset: 13, yOffset: 13, rotation: 135, color: Constants.redRebornFilter}],
                        color: Constants.redRebornFilter, initialAngle: -90, phase2MovingDuration: 50, phase2TargetPlayerProbability: 0, phase2MovementSpeed: 2.7},
                {spikes: [{xOffset: 0, yOffset: -18, rotation: 0, color: Constants.blueRebornFilter}, {xOffset: 18, yOffset: 0, rotation: 90, color: Constants.blueRebornFilter},
                          {xOffset: 0, yOffset: 18, rotation: 180, color: Constants.blueRebornFilter}, {xOffset: -18, yOffset: 0, rotation: 270, color: Constants.blueRebornFilter}],
                        color: Constants.blueRebornFilter, initialAngle: 30, phase2MovingDuration: 45, phase2TargetPlayerProbability: 1, phase2MovementSpeed: 1.2},
                {spikes: [{xOffset: -8, yOffset: -17, rotation: -23, color: Constants.greenRebornFilter}, {xOffset: 17, yOffset: -8, rotation: 67, color: Constants.greenRebornFilter},
                          {xOffset: -17, yOffset: 8, rotation: -113, color: Constants.greenRebornFilter}, {xOffset: 8, yOffset: 17, rotation: 157, color: Constants.greenRebornFilter}],
                        color: Constants.greenRebornFilter, initialAngle: 150, phase2MovingDuration: 40, phase2TargetPlayerProbability: 0.5, phase2MovementSpeed: 1.9},
                ];
    }
    private static var smallEyesData(default, never): Array<EyeData> = workaroundTooManyStaticData2();

    /* [main body, left wing, right wing] */
    private static function workaroundTooManyStaticData4(): Array<Int> {
        return [10, 5, 5];
    }
    private static var phase2InitialLifepoints(default, never): Array<Int> = workaroundTooManyStaticData4();
    private static inline var phase3InitialLifepoints: Int = 750;

    private static inline var simpleHarmonicProbability: Float = 0.005;
    private static inline var startLinkedHarmonicsProbability: Float = 0.001;
    private static inline var nbHarmonicsInLinkedHarmonics: Int = 3;
    /* In original boss, it was random, being in linked harmonics meant a boosted probability to trigger an harmonic (boosted 7% per frame).
     * However that complicates the code for little benefit (if we still want some "randomness" we can apply it on the time between 2 linked harmonics. */
    private static inline var timeBetweenHarmonicsInLinkedHarmonics: Float = 15;

    private static inline var blinkingAlpha: Int = 40;
    private static inline var timeBetweenBlinkingAlphaChange: Float = 3;
    /* Note: Also corresponds to the invulnerability duration. */
    private static inline var blinkingTotalDuration: Float = 50;

    private static function workaroundTooManyStaticData5(): Array<Float> {
        return [90, 50, 20];
    }
    private static var timeBetweenSimpleSpikes(default, never): Array<Float> = workaroundTooManyStaticData5();
    private static inline var nbSpikesInHarmonic: Int = 8;
    private static inline var harmonicSpikeSpeed: Float = 3;

    private static inline var temporarySpikesSpawnInterval: Float = 500;
    private static inline var temporarySpikesLifetime: Float = 160;
    private static function workaroundTooManyStaticData6(): Array<Int> {
        return [5, 8, 8];
    }
    private static var nbTemporarySpikesSpawned(default, never): Array<Int> = workaroundTooManyStaticData6();

    private static inline var timeBetweenPhase3SpiralSpikes: Float = 5;
    private static inline var angleBetweenPhase3SpiralSpikes: Float = 15;
    private static inline var phase3SpiralSpikeSpeed: Float = 5;

    private static inline var phase3LaserSpriteOffset: Float = 46.5;

    /* Note that this is not exact, it's this value + some random duration based it. */
    private static inline var timeBetweenSpecialAttacks: Float = 300;

    private var hf: Hf;
    private var game: GameMode;
    private var x: Float;
    private var y: Float;
    private var customPlayerHitbox: CustomPlayerHitbox;
    private var onDeathCallback: EternalSadistReborn -> Void;

    private var blankSprite: MovieClip;
    private var mainSprite: MovieClip;
    private var ringSprite: MovieClip;
    private var leftArticulationSprite: MovieClip;
    private var rightArticulationSprite: MovieClip;
    private var lifeSprite: MovieClip;

    private var decorativeSpikes: Array<Spike>;

    private var eyes: Array<EyeReborn>;
    private var currentPhase: Int = 1;

    private var phase2Lifepoints: Array<Int>;
    private var phase3Lifepoints: Int = phase3InitialLifepoints;
    private var timeUntilNextPhase3Damage: Float = 1;

    private var isInLinkedHarmonics: Bool = false;
    private var nbHarmonicsRemaining: Int = 0;
    private var timeUntilNextLinkedHarmonic: Float = 0;

    private var remainingBlinkingTime: Float = 0;
    private var timeUntilNextAlphaChange: Float;

    private var timeUntilNextSpike: Float = timeBetweenSimpleSpikes[0];
    private var timeUntilNextTemporarySpikes: Float = temporarySpikesSpawnInterval;
    private var timeUntilNextPhase3SpiralSpike: Float = timeBetweenPhase3SpiralSpikes;
    private var phase3SpiralNextAngle: Float = 0;
    private var spikes: Array<Spike>;

    private var phase3LaserSprites: Array<MovieClip>;

    private var specialAttacks: Array<Array<Void -> SpecialAttack>>;
    private var timeUntilNextSpecialAttack: Float = timeBetweenSpecialAttacks;
    private var currentSpecialAttack: Null<SpecialAttack> = null;
    private var canTeleport: Bool = true;

    public function new(hf: Hf, game: GameMode, x: Float, y: Float, customPlayerHitbox: CustomPlayerHitbox, onDeathCallback: EternalSadistReborn -> Void): Void {
        this.hf = hf;
        this.game = game;
        this.x = x;
        this.y = y;
        this.customPlayerHitbox = customPlayerHitbox;
        this.onDeathCallback = onDeathCallback;
        decorativeSpikes = [];

        blankSprite = cast game.depthMan.attach("hammer_interf_extend", hf.Data.DP_BADS);
        blankSprite.gotoAndStop(13);
        blankSprite._xscale = 190;
        blankSprite._yscale = 190;

        mainSprite = cast game.depthMan.attach("hammer_interf_extend", hf.Data.DP_BADS);
        mainSprite.gotoAndStop(6);

        ringSprite = cast game.depthMan.attach(Obfu.raw("boss_reborn_ring"), hf.Data.DP_BADS);
        ringSprite._rotation = 90;
        ringSprite._xscale = 300;
        ringSprite._yscale = 300;

        leftArticulationSprite = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        leftArticulationSprite.gotoAndStop(10);

        rightArticulationSprite = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        rightArticulationSprite.gotoAndStop(10);

        lifeSprite = cast game.depthMan.attach("hammer_interf_boss_bar", hf.Data.DP_SPRITE_BACK_LAYER);
        lifeSprite._x = hf.Data.GAME_WIDTH * 0.5;
        lifeSprite._y = 10;

        for (decorativeSpikeData in bossDecorativeSpikesData) {
            var spike: Spike = Spike.attach(hf, game, x + decorativeSpikeData.xOffset, y + decorativeSpikeData.yOffset);
            spike.setAngle(decorativeSpikeData.rotation - 90);
            var filter: Null<ColorMatrixFilter> = decorativeSpikeData.color;
            if (filter != null)
                spike.filters = [filter];
            decorativeSpikes.push(spike);
        }

        eyes = [];
        for (data in smallEyesData) {
            eyes.push(new EyeReborn(hf, game, x, y, data, customPlayerHitbox, this));
        }

        phase2Lifepoints = phase2InitialLifepoints.copy();

        spikes = [];

        phase3LaserSprites = [];

        specialAttacks = [
            [function(): SpecialAttack {return new RoseAttack(hf, game, this, 9, 50, 13, 3);},
             function(): SpecialAttack {return new RiffleAttack(hf, game, this, 50, 2, 50, 120);},
             function(): SpecialAttack {return new OtherworldlyArmy(hf, game, this, 10, 2);},
             function(): SpecialAttack {return new StormAttack(hf, game, this, 4, 100, 2, false);},
             function(): SpecialAttack {return new DamoclesRebornAttack(hf, game, this, 7);},
             function(): SpecialAttack {return new IceFlower(hf, game, this, 7, 3, 1.5);}],
            [function(): SpecialAttack {return new RoseAttack(hf, game, this, 12, 30, 18, 4.5);},
             function(): SpecialAttack {return new RiffleAttack(hf, game, this, 40, 3, 20, 200);},
             function(): SpecialAttack {return new OtherworldlyArmy(hf, game, this, 7, 3);},
             function(): SpecialAttack {return new StormAttack(hf, game, this, 6, 80, 3, true);},
             function(): SpecialAttack {return new DamoclesRebornAttack(hf, game, this, 10);},
             function(): SpecialAttack {return new IceFlower(hf, game, this, 10, 4, 2.5);},
             function(): SpecialAttack {return new HeavenlyPunishment(hf, game, this);},
             function(): SpecialAttack {return new Kami(hf, game, this, customPlayerHitbox);}],
        ];

        endUpdate();
        game.addToList(hf.Data.ENTITY, cast this);
    }

    public function getX(): Float {
        return x;
    }

    public function getY(): Float {
        return y;
    }

    public function getMainEyePosition(): {x: Float, y: Float} {
        return {x: x, y: y};
    }

    public function getLeftArticulationPosition(): {x: Float, y: Float} {
        return {x: x - 65, y: y + 10};
    }

    public function getRightArticulationPosition(): {x: Float, y: Float} {
        return {x: x + 65, y: y + 10};
    }

    public function getEyes(): Array<EyeReborn> {
        return eyes;
    }

    public inline function addSpike(spike: Spike): Void {
        spikes.push(spike);
    }

    public function preventTeleporting(): Void {
        canTeleport = false;
        stopLinkedHarmonics();
    }

    public function forceTeleport(x: Float, y: Float): Void {
        teleport(x, y);
    }

    public function stopTemporarySpikesSpawning(): Void {
        timeUntilNextTemporarySpikes = Math.POSITIVE_INFINITY;
    }

    public function resumeTemporarySpikesSpawning(): Void {
        timeUntilNextTemporarySpikes = temporarySpikesSpawnInterval;
    }

    private static function displayExplosionSprite(hf: Hf, game: GameMode, x: Float, y: Float, lifetime: Float): Void {
        var explosion: Smoke = hf.entity.supa.Smoke.attach(game);
        explosion.x = x;
        explosion.y = y;
        explosion.filters = [Constants.purpleExplosionFilter];
        explosion.lifeTimer = lifetime;
    }

    private function teleport(newX: Float, newY: Float): Void {
        displayExplosionSprite(hf, game, x, y, 20);
        x = newX;
        y = newY;
        displayExplosionSprite(hf, game, x, y, 20);
    }

    private function tryTeleport(): Bool {
        if (!canTeleport)
            return false;

        var players: Array<Player> = game.getPlayerList();
        for (i in 0...400) {
            var tentativeX: Float = Std.random(200) + 100;
            var tentativeY: Float = Std.random(300) + 100;
            var valid: Bool = true;
            for (player in players) {
                var diffX: Float = player.x - tentativeX;
                var diffY: Float = player.y - tentativeY;
                if (diffX * diffX + diffY * diffY < 40000) {
                    valid = false;
                    break;
                }
            }
            if (valid) {
                teleport(tentativeX, tentativeY);
                return true;
            }
        }
        return false;
    }

    private function updateLifebar(): Void {
        var phaseLifepoints: Int = 0;
        var phaseInitialLifepoints: Int = 0;
        switch (currentPhase) {
        case 1:
            for (eye in eyes) {
                phaseLifepoints += eye.getPhase1Lifepoints();
                phaseInitialLifepoints += eye.getPhase1InitialLifepoints();
            }
        case 2:
            for (i in 0...3) {
                phaseLifepoints += phase2Lifepoints[i];
                phaseInitialLifepoints += phase2InitialLifepoints[i];
            }
        case 3:
            phaseLifepoints = phase3Lifepoints;
            phaseInitialLifepoints = phase3InitialLifepoints;
        }
        (cast lifeSprite).bar._xscale = (phaseLifepoints / phaseInitialLifepoints) * 100;
    }

    private function nextPhase(): Void {
        ++currentPhase;
        /* Reset sprite to display full life. */
        updateLifebar();
        stopSpecialAttack();

        switch (currentPhase) {
        case 3:
            setPhase3Colors();
            teleport(200, 250);
        default:
        }

        timeUntilNextSpike = timeBetweenSimpleSpikes[currentPhase - 1];

        for (eye in eyes)
            eye.nextPhase();

        /* Here and not in the switch as we need the eyes to have done their phase 3 initialization. */
        // TODO: Can we do something simpler with their initalAngle instead?
        if (currentPhase == 3) {
            for (i in 0...eyes.length) {
                var eye: EyeReborn = eyes[i];
                var nextEye: EyeReborn = eyes[(i + 1) % eyes.length];
                var angle: Float = Math.atan2(nextEye.getY() - eye.getY(), nextEye.getX() - eye.getX());
                var laser: MovieClip = cast game.depthMan.attach(Obfu.raw("boss_reborn_phase3_laser"), hf.Data.DP_BADS);
                laser._x = eye.getX() - phase3LaserSpriteOffset * Math.sin(angle);
                laser._y = eye.getY() - 10 + phase3LaserSpriteOffset * Math.cos(angle);
                laser._xscale = 450;
                laser._yscale = 450;
                laser.gotoAndStop(1);
                (cast laser).skin.gotoAndStop('1');
                laser._rotation = angle * Constants.radianToDegreeFactor - 90;
                phase3LaserSprites.push(laser);
            }
        }
    }

    public function onEyeHit(): Void {
        if (currentPhase != 1)
            return;

        tryTeleport();
        updateLifebar();

        /* Check if all eyes are dead. */
        for (eye in eyes) {
            if (eye.getPhase1Lifepoints() > 0)
                return;
        }

        nextPhase();
    }

    private function harmonic(): Bool {
        var prevX: Float = x;
        var prevY: Float = y;
        var teleported: Bool = tryTeleport();
        if (teleported) {
            var diffAngle: Float = 360 / nbSpikesInHarmonic;
            for (i in 0...nbSpikesInHarmonic) {
                var spike: Spike = Spike.attach(hf, game, prevX, prevY);
                spike.throwAtAngle(i * diffAngle, harmonicSpikeSpeed);
                spikes.push(spike);
            }
        }
        return teleported;
    }

    private function startLinkedHarmonics(): Void {
        isInLinkedHarmonics = true;
        nbHarmonicsRemaining = nbHarmonicsInLinkedHarmonics;
        timeUntilNextLinkedHarmonic = timeBetweenHarmonicsInLinkedHarmonics;
    }

    private function stopLinkedHarmonics(): Void {
        isInLinkedHarmonics = false;
    }

    private function updateLinkedHarmonics(): Void {
        timeUntilNextLinkedHarmonic -= hf.Timer.tmod;
        if (timeUntilNextLinkedHarmonic <= 0) {
            if (harmonic()) {
                --nbHarmonicsRemaining;
                if (nbHarmonicsRemaining <= 0) {
                    stopLinkedHarmonics();
                    return;
                }
            }
            /* Otherwise it doesn't count in the number of linked harmonics, re-attempt after the same wait time. */
            timeUntilNextLinkedHarmonic += timeBetweenHarmonicsInLinkedHarmonics;
        }
    }

    private function startSpecialAttack(): Void {
        var currentPhaseSpecialAttacks: Array<Void -> SpecialAttack> = specialAttacks[currentPhase - 1];
        currentSpecialAttack = currentPhaseSpecialAttacks[Std.random(currentPhaseSpecialAttacks.length)]();
    }

    private function stopSpecialAttack(): Void {
        timeUntilNextSpecialAttack = timeBetweenSpecialAttacks + Math.random() * 0.5 * timeBetweenSpecialAttacks;
        currentSpecialAttack.destroy();
        currentSpecialAttack = null;
        /* Special attacks may prevent teleporting. */
        canTeleport = true;
    }

    @:keep
    public function update(): Void {
        for (spike in decorativeSpikes)
            spike.update(game, customPlayerHitbox); /* Those should not disappear when offscreen. */
        for (eye in eyes)
            eye.update();

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
            var target: Entity = game.getOne(hf.Data.PLAYER);
            var radianAngle: Float = Math.atan2(target.y - getY(), target.x - getX());
            var spike: Spike = Spike.attach(hf, game, getX(), getY());
            spike.throwAtAngle(radianAngle * Constants.radianToDegreeFactor, 4);
            spikes.push(spike);
            timeUntilNextSpike += timeBetweenSimpleSpikes[currentPhase - 1];
        }

        timeUntilNextTemporarySpikes -= hf.Timer.tmod;
        if (timeUntilNextTemporarySpikes <= 0) {
            for (i in 0...nbTemporarySpikesSpawned[currentPhase - 1]) {
                var x, y;
                do {
                    x = Std.random(20);
                    y = Std.random(23);
                } while (game.world.getCase({x: x, y: y}) != 1);
                var spike: TemporarySpike = TemporarySpike.attach(hf, game, (x + 0.5) * hf.Data.CASE_WIDTH, (y - 0.5) * hf.Data.CASE_HEIGHT, temporarySpikesLifetime);
                spikes.push(spike);
            }
            timeUntilNextTemporarySpikes += temporarySpikesSpawnInterval;
        }

        if (currentPhase == 3) {
            timeUntilNextPhase3SpiralSpike -= hf.Timer.tmod;
            if (timeUntilNextPhase3SpiralSpike <= 0) {
                var spike: Spike = Spike.attach(hf, game, getX(), getY());
                spike.throwAtAngle(phase3SpiralNextAngle, phase3SpiralSpikeSpeed);
                spikes.push(spike);
                phase3SpiralNextAngle += angleBetweenPhase3SpiralSpikes;
                if (phase3SpiralNextAngle > 360)
                    phase3SpiralNextAngle -= 360;
                timeUntilNextPhase3SpiralSpike += timeBetweenPhase3SpiralSpikes;
            }

            timeUntilNextPhase3Damage -= hf.Timer.tmod;
            if (timeUntilNextPhase3Damage <= 0) {
                --phase3Lifepoints;
                updateLifebar();
                if (phase3Lifepoints == 80)
                    displayExplosionSprite(hf, game, x, y, 120);
                else if (phase3Lifepoints <= 0)
                    destroy();
                timeUntilNextPhase3Damage += 1;
            }
        }
        else {
            /* No special attack during phase 3. */
            if (timeUntilNextSpecialAttack > 0) {
                timeUntilNextSpecialAttack -= hf.Timer.tmod;
                if (timeUntilNextSpecialAttack <= 0)
                    startSpecialAttack();
            }
            else {
                if (currentSpecialAttack.update() == AttackState.Done)
                    stopSpecialAttack();
            }

            if (canTeleport) {
                if (!isInLinkedHarmonics) {
                    if (Math.random() < startLinkedHarmonicsProbability * hf.Timer.tmod)
                        startLinkedHarmonics();
                    else if (Math.random() < simpleHarmonicProbability * hf.Timer.tmod)
                        harmonic();
                }
                else {
                    updateLinkedHarmonics();
                }
            }
        }

        var nbSpikes: Int = spikes.length;
        var i: Int = 0;
        while (i < nbSpikes) {
            var spike: Spike = spikes[i];
            if (spike.update(game, customPlayerHitbox)) {
                spike.removeMovieClip();
                spikes.splice(i, 1);
                --nbSpikes;
            }
            else {
                spike.endUpdate();
                ++i;
            }
        }
    }

    private function setAlpha(alpha: Int): Void {
        blankSprite._alpha = alpha;
        mainSprite._alpha = alpha;
        ringSprite._alpha = alpha;
        leftArticulationSprite._alpha = alpha;
        rightArticulationSprite._alpha = alpha;
        for (i in 0...decorativeSpikes.length)
            decorativeSpikes[i]._alpha = alpha;
    }

    private function isCurrentAlphaBlinkAlpha(): Bool {
        /* We set the same alpha to all the sprite, so check any.
         * Note that an exact floating comparison will fail (and not just by a machine epsilon),
         * I'm guessing flash must slightly modify the value somehow. In our case we can just check the value is not 100 (fully opaque). */
        return blankSprite._alpha < 100;
    }

    private function startBlinking(): Void {
        remainingBlinkingTime = blinkingTotalDuration;
        timeUntilNextAlphaChange = timeBetweenBlinkingAlphaChange;
    }

    private function stopBlinking(): Void {
        setAlpha(100);
    }

    @:keep
    public function endUpdate(): Void {
        blankSprite._x = x;
        blankSprite._y = y + 20;
        mainSprite._x = x;
        mainSprite._y = y + 10;
        ringSprite._x = x;
        ringSprite._y = y - 60;
        leftArticulationSprite._x = x - 65;
        leftArticulationSprite._y = y + 10;
        rightArticulationSprite._x = x + 65;
        rightArticulationSprite._y = y + 10;
        for (i in 0...bossDecorativeSpikesData.length) {
            var spike: Spike = decorativeSpikes[i];
            var decorativeSpikeData: DecorativeSpikeData = bossDecorativeSpikesData[i];
            spike.moveTo(x + decorativeSpikeData.xOffset, y + decorativeSpikeData.yOffset - 10);
            spike.endUpdate();
        }
        for (eye in eyes)
            eye.endUpdate();

        /* Blinking update. */
        if (remainingBlinkingTime > 0) {
            timeUntilNextAlphaChange -= hf.Timer.tmod;
            if (timeUntilNextAlphaChange <= 0) {
                if (isCurrentAlphaBlinkAlpha())
                    setAlpha(100);
                else
                    setAlpha(blinkingAlpha);
                timeUntilNextAlphaChange += timeBetweenBlinkingAlphaChange;
            }

            remainingBlinkingTime -= hf.Timer.tmod;
            if (remainingBlinkingTime <= 0)
                stopBlinking();
        }
    }

    private function setPhase3Colors(): Void {
        var color: ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1).toFilter();
        blankSprite.filters = [color];
        mainSprite.filters = [color];
        ringSprite.filters = [color];
        leftArticulationSprite.filters = [color];
        rightArticulationSprite.filters = [color];
        for (i in 0...decorativeSpikes.length)
            decorativeSpikes[i].filters = [color];
    }

    private function setColor(bodyPart: BodyPart, color: ColorMatrixFilter): Void {
        switch (bodyPart) {
        case MainBody:
            blankSprite.filters = [color];
            mainSprite.filters = [color];
            ringSprite.filters = [color];
            for (i in 0...22)
                decorativeSpikes[i].filters = [color];
        case LeftWing:
            leftArticulationSprite.filters = [color];
            for (i in 22...34)
                decorativeSpikes[i].filters = [color];
        case RightWing:
            rightArticulationSprite.filters = [color];
            for (i in 34...46)
                decorativeSpikes[i].filters = [color];
        }
    }

    private function isInvincible(): Bool {
        return remainingBlinkingTime > 0;
    }

    public function takeDamage(bodyPart: BodyPart) {
        if (currentPhase != 2 || isInvincible())
            return;

        var id: Int = bodyPart.getIndex();
        if (phase2Lifepoints[id] > 0) {
            --phase2Lifepoints[id];
            startBlinking();
            updateLifebar();
            if (phase2Lifepoints[id] <= 0) {
                setColor(bodyPart, Constants.darkFilter);

                /* Since a part died, check whether all are and thus whether phase 2 is over. */
                var allDead: Bool = true;
                for (i in 0...3) {
                    if (phase2Lifepoints[i] > 0) {
                        allDead = false;
                        break;
                    }
                }
                if (allDead) {
                    nextPhase();
                    return;
                }
            }
            tryTeleport();
        }
    }

    public function destroy(): Void {
        onDeathCallback(this);

        /* Shouldn't be needed as there is no special attack in phase 3. */
        stopSpecialAttack();

        blankSprite.removeMovieClip();
        mainSprite.removeMovieClip();
        ringSprite.removeMovieClip();
        leftArticulationSprite.removeMovieClip();
        rightArticulationSprite.removeMovieClip();
        lifeSprite.removeMovieClip();
        for (spike in decorativeSpikes)
            spike.removeMovieClip();
        decorativeSpikes = [];
        for (eye in eyes)
            eye.destroy();
        eyes = [];
        for (spike in spikes)
            spike.prettyRemove(game);
        spikes = [];
        for (laserSprite in phase3LaserSprites)
            laserSprite.removeMovieClip();
        phase3LaserSprites = [];

        game.removeFromList(hf.Data.ENTITY, cast this);
    }
}

class EternalSadistRebornSpawner implements IAction {
    public var name(default, null): String = Obfu.raw("createPicBossRebirth");
    public var isVerbose(default, null): Bool = false;

    private var manager: EternalSadistRebornManager;

    public function new(manager: EternalSadistRebornManager): Void {
        this.manager = manager;
    }

    public function run(ctx: IActionContext): Bool {
        var game: GameMode = ctx.getGame();
        var x: Float = game.flipCoordReal(ctx.getFloat(Obfu.raw("xr")));
        var y: Float = ctx.getFloat(Obfu.raw("yr"));
        manager.CreateBoss(ctx.getHf(), game, x, y);
        return false;
    }
}

class EternalSadistRebornDeadCheck implements IAction {
    public var name(default, null): String = Obfu.raw("picBossRebirthDeath");
    public var isVerbose(default, null): Bool = false;

    private var manager: EternalSadistRebornManager;

    public function new(manager: EternalSadistRebornManager): Void {
        this.manager = manager;
    }

    public function run(ctx: IActionContext): Bool {
        return manager.isBossDead();
    }
}

@:build(patchman.Build.di())
class EternalSadistRebornManager {
    private var customPlayerHitbox: CustomPlayerHitbox;
    private var boss: Null<EternalSadistReborn> = null;
    private var hasBossSpawned: Bool = false;

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;

    @:diExport
    public var bombsDamageBoss(default, null): IPatch;

    public function CreateBoss(hf: Hf, game: GameMode, x: Float, y: Float): Void {
        boss = new EternalSadistReborn(hf, game, x, y, customPlayerHitbox, function (under6T: EternalSadistReborn): Void {
            boss = null;
        });
        hasBossSpawned = true;
    }

    public function isBossDead(): Bool {
        return hasBossSpawned && boss == null;
    }

    private static inline function isInSquaredRadius(testedEntity: {x: Float, y: Float}, testingEntity: {x: Float, y: Float}, squaredRadius: Float): Bool {
        var diffX: Float = testedEntity.x - testingEntity.x;
        var diffY: Float = testedEntity.y - testingEntity.y;
        return diffX * diffX + diffY * diffY < squaredRadius;
    }

    public function new(hf: Hf, customPlayerHitbox: CustomPlayerHitbox): Void {
        this.customPlayerHitbox = customPlayerHitbox;

        hf.Std.registerClass(Obfu.raw("boss_spike"), Spike);
        hf.Std.registerClass(Obfu.raw("boss_temporary_spike"), TemporarySpike);
        hf.Std.registerClass(Obfu.raw("boss_homing_spike"), HomingSpike);
        hf.Std.registerClass(Obfu.raw("boss_reborn_temporary_homing_spike"), TemporaryHomingSpike);
        hf.Std.registerClass(Obfu.raw("boss_reborn_zigzag_spike"), ZigZagSpikeReborn);
        hf.Std.registerClass(Obfu.raw("boss_reborn_ice_flower_spike"), IceFlowerSpike);
        hf.Std.registerClass(Obfu.raw("boss_reborn_rotating_laser"), RotatingLaser);
        hf.Std.registerClass(Obfu.raw("boss_reborn_kami_spike"), KamiSpike);
        hf.Std.registerClass(Obfu.raw("boss_reborn_otherwordly_spike"), OtherwordlySpike);
        hf.Std.registerClass(Obfu.raw("boss_reborn_storm_spike"), StormSpike);

        actions = FrozenArray.of(new EternalSadistRebornSpawner(this),
                                 new EternalSadistRebornDeadCheck(this));

        bombsDamageBoss = Ref.auto(PlayerBomb.onExplode).before(function(hf: Hf, self: PlayerBomb): Void {
            var squaredRadius: Float = self.radius * self.radius;

            for (eye in boss.getEyes()) {
                if (isInSquaredRadius(eye.getPosition(), self, squaredRadius))
                    eye.takeDamage();
            }

            if (isInSquaredRadius(boss.getMainEyePosition(), self, squaredRadius))
                boss.takeDamage(MainBody);

            if (isInSquaredRadius(boss.getLeftArticulationPosition(), self, squaredRadius))
                boss.takeDamage(LeftWing);

            if (isInSquaredRadius(boss.getRightArticulationPosition(), self, squaredRadius))
                boss.takeDamage(RightWing);
        });
    }
}
