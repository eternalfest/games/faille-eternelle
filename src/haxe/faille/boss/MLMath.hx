package faille.boss;

import hf.entity.Player;
import hf.mode.GameMode;

class MLMath {
    public static inline function SquaredDistance(object1: {x: Float, y: Float}, object2: {x: Float, y: Float}): Float {
        var diffX = object1.x - object2.x;
        var diffY = object1.y - object2.y;
        return diffX * diffX + diffY * diffY;
    }

    public static function DistanceBasedAlpha(object: {x: Float, y: Float}, players: Array<Player>, glowingDivisor: Float): Float {
        var minDistance: Float = 999999;
        for (player in players) {
            var distance: Float = SquaredDistance(player, object);
            minDistance = Math.min(minDistance, distance);
        }
        minDistance = Math.sqrt(minDistance);
        return 100 - Math.min(Math.max(0, (minDistance / glowingDivisor - 20)), 100);
    }

    public static function getAngleWithRandomPlayer(game: GameMode, object: {x: Float, y: Float}): Float {
        var player: Null<Player> = cast game.getOne(game.root.Data.PLAYER);
        /* Can happen during screen transition on game end for example.
         * adjustAngle would do an infinite loop with such input (this would end up as a NaN). */
        if (player == null)
            return 0;
        var angle: Float = Math.atan2(player.y - object.y, player.x - object.x);
        return player.adjustAngle(angle * 180 / Math.PI);
    }

    /* https://stackoverflow.com/questions/10962379/how-to-check-intersection-between-2-rotated-rectangles */
    public static function ArePolygonsIntersecting(polygon1: Array<Array<Float>>, polygon2: Array<Array<Float>>): Bool {
        var polygons: Array<Array<Array<Float>>> = [polygon1, polygon2];
        for (polygon in polygons) {
            for (pointIndex in 0...polygon.length) {
                var point1: Array<Float> = polygon[pointIndex];
                var point2: Array<Float> = polygon[(pointIndex + 1) % polygon.length];
                var normal: Array<Float> = [point2[1] - point1[1], point1[0] - point2[0]];
                normal; /* !ACTUALLY NEEDED! Probably some haxe miscompiling... */
                /* Without it it's as if it's undefined and thus min1/max1/min2/max2 are never set and thus the funcion returns false and Igor is not killed by the bullets.
                 * You can play with this. For example a print of normal gives "[undefined, undefined]" (but still kills Igor), but printing "normal[0]" and normal[1]" gives values. */

                var min1: Float = Math.POSITIVE_INFINITY, max1: Float = Math.NEGATIVE_INFINITY;
                for (p1 in polygon1) {
                    var projection = normal[0] * p1[0] + normal[1] * p1[1];
                    if (projection < min1)
                        min1 = projection;
                    if (projection > max1)
                        max1 = projection;
                }

                var min2: Float = Math.POSITIVE_INFINITY, max2: Float = Math.NEGATIVE_INFINITY;
                for (p2 in polygon2) {
                    var projection = normal[0] * p2[0] + normal[1] * p2[1];
                    if (projection < min2)
                        min2 = projection;
                    if (projection > max2)
                        max2 = projection;
                }

                if (max1 < min2 || max2 < min1)
                    return false;
            }
        }
        return true;
    }

    /* Same logic as ArePolygonsIntersecting except we take advantage of the fact that one is a normal rectangle (with points in specific order):
     *  - The projection is directly getting x or y.
     *  - We only need to check two edges of the rectangle instead of 4.
     *    - In this case we can simplify/bypass some bound checks.
     *  - We start by checking the projections on the rectangle as it will be quicker to exit (return false is the common case). */
    public static function ArePolygonAndNonRotatedRectangleIntersecting(polygon: Array<Array<Float>>, rectangle: Array<Array<Float>>): Bool {
        /* Rectangle check. Moreover we know that our rectangle is in the order [bottom left, bottom right, top right, top left]. */
        var rectangleBottomLeftPoint: Array<Float> = rectangle[0];
        var rectangleTopRightPoint: Array<Float> = rectangle[2];
        var rectangleMinX: Float = rectangleBottomLeftPoint[0];
        var rectangleMaxX: Float = rectangleTopRightPoint[0];
        /* However, don't forget that bottom has a higher y than top... */
        var rectangleMinY: Float = rectangleTopRightPoint[1];
        var rectangleMaxY: Float = rectangleBottomLeftPoint[1];
        var minX: Float = Math.POSITIVE_INFINITY, minY: Float = Math.POSITIVE_INFINITY, maxX: Float = Math.NEGATIVE_INFINITY, maxY: Float = Math.NEGATIVE_INFINITY;
        for (point in polygon) {
            var pointX: Float = point[0];
            var pointY: Float = point[1];
            if (pointX < minX)
                minX = pointX;
            if (pointX > maxX)
                maxX = pointX;
            if (pointY < minY)
                minY = pointY;
            if (pointY > maxY)
                maxY = pointY;
        }
        if (rectangleMaxX < minX || maxX < rectangleMinX || rectangleMaxY < minY || maxY < rectangleMinY)
            return false;

        /* Polygon check, same as in ArePolygonsIntersecting.
         * Maybe there is something clever to do as we know the tested geometry is a normal rectangle;
         * but it's not done if it exists... */
        for (pointIndex in 0...polygon.length) {
            var point1: Array<Float> = polygon[pointIndex];
            var point2: Array<Float> = polygon[(pointIndex + 1) % polygon.length];
            var normal: Array<Float> = [point2[1] - point1[1], point1[0] - point2[0]];
            normal; /* !ACTUALLY NEEDED! Same as in ArePolygonsIntersecting. */

            var min1: Float = Math.POSITIVE_INFINITY, max1: Float = Math.NEGATIVE_INFINITY;
            for (p1 in polygon) {
                var projection = normal[0] * p1[0] + normal[1] * p1[1];
                if (projection < min1)
                    min1 = projection;
                if (projection > max1)
                    max1 = projection;
            }

            var min2: Float = Math.POSITIVE_INFINITY, max2: Float = Math.NEGATIVE_INFINITY;
            for (p2 in rectangle) {
                var projection = normal[0] * p2[0] + normal[1] * p2[1];
                if (projection < min2)
                    min2 = projection;
                if (projection > max2)
                    max2 = projection;
            }

            if (max1 < min2 || max2 < min1)
                return false;
        }

        return true;
    }
}