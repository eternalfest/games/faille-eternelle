package faille.boss;

import etwin.flash.filters.ColorMatrixFilter;
import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import faille.boss.Spikes;
import color_matrix.ColorMatrix;

typedef DecorativeSpikeData = {xOffset: Float,
                               yOffset: Float,
                               rotation: Float,
                               color: Null<ColorMatrixFilter>};

// TODO? Color here redundant with the one in the spikes.
typedef EyeData = {spikes: Array<DecorativeSpikeData>,
                   color: ColorMatrixFilter,
                   initialAngle: Float,
                   phase2MovingDuration: Float,
                   phase2TargetPlayerProbability: Float,
                   phase2MovementSpeed: Float};

class EyeReborn {
    private static inline var phase1Distance: Float = 80;
    private static inline var phase1RotationSpeed: Float = 3;
    private static inline var phase1InitialLifepoints: Int = 5;
    private static inline var phase2DisabledDuration: Float = 120;
    private static inline var phase2StopDuration: Float = 35;
    private static inline var phase3Distance: Float = 150;

    private static inline var blinkingAlpha: Int = 40;
    private static inline var timeBetweenBlinkingAlphaChange: Float = 3;
    /* Note: Also corresponds to the invulnerability duration. */
    private static inline var blinkingTotalDuration: Float = 50;

    private static function workaroundTooManyStaticData3(): Array<Float> {
        return [150, Math.POSITIVE_INFINITY, Math.POSITIVE_INFINITY];
    }
    private static var timeBetweenSimpleSpikes(default, never): Array<Float> = workaroundTooManyStaticData3();
    private static inline var timeBetweenPhase3Lasers: Float = 200;

    private var hf: Hf;
    private var game: GameMode;
    private var x: Float;
    private var y: Float;
    private var data: EyeData;
    private var customPlayerHitbox: CustomPlayerHitbox;
    private var controller: {function getX(): Float; function getY(): Float; function onEyeHit(): Void;};

    private var lifepoints: Int = phase1InitialLifepoints;
    private var angle: Float;
    private var sprite: MovieClip;
    private var decorativeSpikes: Array<Spike>;
    private var currentPhase: Int = 1;
    private var remainingDisabledTime: Float = 0.0;
    private var remainingStopTimer: Float = 0.0;
    private var remainingMovingTimer: Float = 0.0;
    private var dx: Float = 0.0;
    private var dy: Float = 0.0;

    private var remainingBlinkingTime: Float = 0;
    private var timeUntilNextAlphaChange: Float;

    /* Randomize the initial timing of spikes and lasers so that the eyes don't do the same thing at the same time. */

    private var timeUntilNextSpike: Float = Math.random() * timeBetweenSimpleSpikes[0];
    private var spikes: Array<Spike>;

    private var timeUntilNextPhase3Laser: Float = Math.random() * timeBetweenPhase3Lasers;
    private var phase3Lasers: Array<Laser>;

    public function new(hf: Hf, game: GameMode, x: Float, y: Float, data: EyeData, customPlayerHitbox: CustomPlayerHitbox, controller: {function getX(): Float; function getY(): Float; function onEyeHit(): Void;}): Void {
        this.hf = hf;
        this.game = game;
        this.x = x;
        this.y = y;
        this.data = data;
        this.controller = controller;
        this.customPlayerHitbox = customPlayerHitbox;

        angle = data.initialAngle;

        sprite = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        sprite.gotoAndStop(10);
        sprite.filters = [data.color];

        decorativeSpikes = [];
        for (decorativeSpikeData in data.spikes) {
            var spike: Spike = Spike.attach(hf, game, x + decorativeSpikeData.xOffset, y + decorativeSpikeData.yOffset);
            spike.setAngle(decorativeSpikeData.rotation - 90);
            var filter: Null<ColorMatrixFilter> = decorativeSpikeData.color;
            /* Really shouldn't be null, or should we ignore that and directly use the color from the top level eye data? */
            if (filter != null)
                spike.filters = [filter];
            decorativeSpikes.push(spike);
        }

        spikes = [];
        phase3Lasers = [];
    }

    public function getX(): Float {
        return x;
    }

    public function getY(): Float {
        return y;
    }

    public function getPosition(): {x: Float, y: Float} {
        return {x: x, y: y};
    }

    public function getPhase1Lifepoints(): Int {
        return lifepoints;
    }

    public function getPhase1InitialLifepoints(): Int {
        return phase1InitialLifepoints;
    }

    public function nextPhase(): Void {
        ++currentPhase;

        if (currentPhase == 3) {
            /* They are static in the last phase, place them where they should be. */
            dx = 0;
            dy = 0;
            var angleRad: Float = data.initialAngle * Constants.degreeToRadianFactor;
            x = controller.getX() + phase3Distance * Math.cos(angleRad);
            y = controller.getY() + 30 + phase3Distance * Math.sin(angleRad);
            setPhase3Colors();
        }
        else {
            setColor(data.color);
        }

        timeUntilNextSpike = timeBetweenSimpleSpikes[currentPhase - 1];
    }

    private function setPhase3Colors(): Void {
        setColor(ColorMatrix.fromHsv(0, 0, 1).toFilter());
    }

    private function enable(): Void {
        setColor(data.color);
    }

    private function disable(): Void {
        stop();
        setColor(Constants.darkFilter);
    }

    private function chooseTargetAndInitiateMove(): Void {
        remainingMovingTimer = data.phase2MovingDuration;

        var target: {x: Float, y: Float};
        if (Math.random() < data.phase2TargetPlayerProbability)
            target = game.getOne(hf.Data.PLAYER);
        else
            target = {x: Std.random(400), y: Std.random(500)};

        var angle: Float = Math.atan2(target.y - y, target.x - x);
        dx = data.phase2MovementSpeed * Math.cos(angle);
        dy = data.phase2MovementSpeed * Math.sin(angle);
    }

    private function stop(): Void {
        dx = 0;
        dy = 0;
        remainingStopTimer = phase2StopDuration;
    }

    public function isDisabled(): Bool {
        return remainingDisabledTime > 0;
    }

    private function phase2Update(): Void {
        if (remainingDisabledTime > 0) {
            remainingDisabledTime -= hf.Timer.tmod;
            if (remainingDisabledTime <= 0)
                enable();
            return;
        }

        if (remainingStopTimer > 0) {
            remainingStopTimer -= hf.Timer.tmod;
            if (remainingStopTimer <= 0)
                chooseTargetAndInitiateMove();
        }
        else {
            remainingMovingTimer -= hf.Timer.tmod;
            if (remainingMovingTimer <= 0)
                stop();
            x += dx * hf.Timer.tmod;
            y += dy * hf.Timer.tmod;
        }
    }

    public function update(): Void {
        switch (currentPhase) {
        case 1:
            x = controller.getX() + phase1Distance * Math.cos(angle * Constants.degreeToRadianFactor);
            y = controller.getY() + phase1Distance * Math.sin(angle * Constants.degreeToRadianFactor);
            angle += phase1RotationSpeed * hf.Timer.tmod;
        case 2:
            phase2Update();
        case 3:
            timeUntilNextPhase3Laser -= hf.Timer.tmod;
            if (timeUntilNextPhase3Laser <= 0) {
                /* Unlike the normal boss, we want to own the laser here and not give it to the game, as we are in the last phase:
                 * We don't want to have laser still alive and firing after the boss is dead.
                 * So, due to manual handling, we don't need anything from the callback. */
                var laser: Laser = Laser.attach(hf, game, x, y - 10, function(instance: Laser): Void {}, 50, 60, 40, 40);
                phase3Lasers.push(laser);

                timeUntilNextPhase3Laser += timeBetweenPhase3Lasers;
            }
        }

        for (spike in decorativeSpikes)
            spike.update(game, customPlayerHitbox); /* Those should not disappear when offscreen. */

        if (!isDisabled()) {
            timeUntilNextSpike -= hf.Timer.tmod;
            if (timeUntilNextSpike <= 0) {
                var target: Entity = game.getOne(hf.Data.PLAYER);
                var radianAngle: Float = Math.atan2(target.y - getY(), target.x - getX());
                var spike: Spike = Spike.attach(hf, game, getX(), getY());
                spike.throwAtAngle(radianAngle * Constants.radianToDegreeFactor, 4);
                spikes.push(spike);
                timeUntilNextSpike += timeBetweenSimpleSpikes[currentPhase - 1];
            }
        }

        var nbSpikes: Int = spikes.length;
        var i: Int = 0;
        while (i < nbSpikes) {
            var spike: Spike = spikes[i];
            if (spike.update(game, customPlayerHitbox)) {
                spike.removeMovieClip();
                spikes.splice(i, 1);
                --nbSpikes;
            }
            else {
                spike.endUpdate();
                ++i;
            }
        }

        var nbLasers: Int = phase3Lasers.length;
        i = 0;
        while (i < nbLasers) {
            var laser: Laser = phase3Lasers[i];
            if (laser.updateReborn()) {
                laser.removeMovieClip();
                phase3Lasers.splice(i, 1);
                --nbLasers;
            }
            else {
                laser.endUpdate();
                ++i;
            }
        }
    }

    // TODO: The blinking code is the same between the boss and the eye, what is the best way to refactor that?

    private function setAlpha(alpha: Int): Void {
        sprite._alpha = alpha;
        for (i in 0...decorativeSpikes.length)
            decorativeSpikes[i]._alpha = alpha;
    }

    private function isCurrentAlphaBlinkAlpha(): Bool {
        return sprite._alpha < 100;
    }

    private function startBlinking(): Void {
        remainingBlinkingTime = blinkingTotalDuration;
        timeUntilNextAlphaChange = timeBetweenBlinkingAlphaChange;
    }

    private function stopBlinking(): Void {
        setAlpha(100);
    }

    public function endUpdate(): Void {
        sprite._x = x;
        sprite._y = y;
        for (i in 0...data.spikes.length) {
            var spike: Spike = decorativeSpikes[i];
            var decorativeSpikeData: DecorativeSpikeData = data.spikes[i];
            spike.moveTo(x + decorativeSpikeData.xOffset, y + decorativeSpikeData.yOffset - 10);
            spike.endUpdate();
        }

        /* Blinking update. */
        if (remainingBlinkingTime > 0) {
            timeUntilNextAlphaChange -= hf.Timer.tmod;
            if (timeUntilNextAlphaChange <= 0) {
                if (isCurrentAlphaBlinkAlpha())
                    setAlpha(100);
                else
                    setAlpha(blinkingAlpha);
                timeUntilNextAlphaChange += timeBetweenBlinkingAlphaChange;
            }

            remainingBlinkingTime -= hf.Timer.tmod;
            if (remainingBlinkingTime <= 0)
                stopBlinking();
        }
    }

    private function setColor(color: ColorMatrixFilter): Void {
        sprite.filters = [color];
        for (spike in decorativeSpikes)
            spike.filters = [color];
    }

    private function isInvincible(): Bool {
        return remainingBlinkingTime > 0;
    }

    public function takeDamage(): Void {
        switch (currentPhase) {
        case 1:
            if (lifepoints > 0 && !isInvincible()) {
                --lifepoints;
                if (lifepoints == 0)
                    setColor(Constants.darkFilter);
                else
                    startBlinking();
                controller.onEyeHit();
            }
        case 2:
            if (!isDisabled())
                disable();
            remainingDisabledTime = phase2DisabledDuration;
        }
    }

    public function destroy(): Void {
        game.fxMan.attachFx(getX(), getY() - hf.Data.CASE_HEIGHT, "hammer_fx_shine");
        sprite.removeMovieClip();
        for (spike in decorativeSpikes)
            spike.prettyRemove(game);
        decorativeSpikes = [];
        for (spike in spikes)
            spike.prettyRemove(game);
        spikes = [];
        for (laser in phase3Lasers)
            laser.destroy();
        phase3Lasers = [];
    }
}
