package faille.boss;

import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import faille.boss.Spikes;

class Eye {
    private static var minimalSquaredDistanceToThrowSpike(default, never): Float = 120 * 120;

    private var hf: Hf;
    private var game: GameMode;
    private var center: {x: Float, y: Float};
    private var distance: Float;
    private var targetDistance: Float;
    private var angle: Float;
    private var radialSpeed: Float = 1 * Constants.degreeToRadianFactor;

    private var mainSprite: MovieClip;
    private var decorativeSpikes: Array<Spike>;

    private var timeBetweenSpikes: Float = 80;
    private var timeUntilNextSpike: Float;
    private var spikes: Array<Spike>;

    public function new(hf: Hf, game: GameMode, center: {x: Float, y: Float}, distance: Float, radianAngle: Float): Void {
        this.hf = hf;
        this.game = game;
        this.center = center;
        this.distance = distance;
        this.targetDistance = distance;
        this.angle = radianAngle;
        decorativeSpikes = [];
        timeUntilNextSpike = timeBetweenSpikes;
        spikes = [];

        mainSprite = cast game.depthMan.attach("hammer_item_special", hf.Data.DP_BADS);
        mainSprite.gotoAndStop(10);

        var x: Float = getX();
        var y: Float = getY();
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT, "hammer_fx_shine");
        for (i in 0...8) {
            /* They will be correctly placed in endUpdate(). */
            decorativeSpikes.push(Spike.attach(hf, game, x, y));
        }
    }

    private function getX(): Float {
        return center.x + distance * Math.cos(angle);
    }

    private function getY(): Float {
        return center.y + distance * Math.sin(angle);
    }

    public function increaseTargetDistanceBy(value: Float): Void {
        targetDistance += value;
    }

    public function setRadialSpeed(valueInRadians: Float): Void {
        radialSpeed = valueInRadians;
    }

    public function setTimeBetweenSpikes(value: Float): Void {
        timeBetweenSpikes = value;
    }

    public function update(customPlayerHitbox: CustomPlayerHitbox): Void {
        angle += radialSpeed * hf.Timer.tmod;
        if (angle >= Constants.twoPi)
            angle -= Constants.twoPi;

        if (distance < targetDistance) {
            distance += 0.04 * hf.Timer.tmod;
            if (distance > targetDistance)
                distance = targetDistance;
        }
        else if (distance > targetDistance) {
            distance -= 0.04 * hf.Timer.tmod;
            if (distance < targetDistance)
                distance = targetDistance;
        }

        timeUntilNextSpike -= hf.Timer.tmod;
        if (timeUntilNextSpike <= 0) {
            var target: Entity = game.getOne(hf.Data.PLAYER);
            var x: Float = getX();
            var y: Float = getY();
            var diffX: Float = target.x - x;
            var diffY: Float = target.y - y;
            if (diffX * diffX + diffY * diffY > minimalSquaredDistanceToThrowSpike) {
                var radianAngle: Float = Math.atan2(diffY, diffX);
                var spike: Spike = Spike.attach(hf, game, x, y);
                spike.throwAtAngle(radianAngle * Constants.radianToDegreeFactor, 4);
                spikes.push(spike);
            }
            timeUntilNextSpike += timeBetweenSpikes;
        }

        for (spike in decorativeSpikes) {
            spike.update(game, customPlayerHitbox); /* We ignore if they go offscreen and don't despawn them. */
            spike.endUpdate();
        }

        var nbSpikes: Int = spikes.length;
        var i: Int = 0;
        while (i < nbSpikes) {
            var spike: Spike = spikes[i];
            if (spike.update(game, customPlayerHitbox)) {
                spike.removeMovieClip();
                spikes.splice(i, 1);
                --nbSpikes;
            }
            else {
                spike.endUpdate();
                ++i;
            }
        }
    }

    public function endUpdate(): Void {
        var x: Float = getX();
        var y: Float = getY();
        mainSprite._x = x;
        mainSprite._y = y;

        for (i in 0...8) {
            var spike: Spike = decorativeSpikes[i];
            var angleInDegrees: Float = i * (360 / 8);
            var radianAngle: Float = angleInDegrees * Constants.degreeToRadianFactor;
            spike.moveTo(x + 21 * Math.cos(radianAngle), y - 11 + 21 * Math.sin(radianAngle));
            spike.setAngle(angleInDegrees);
        }
    }

    public function destroy(): Void {
        game.fxMan.attachFx(getX(), getY() - hf.Data.CASE_HEIGHT, "hammer_fx_shine");
        for (spike in decorativeSpikes)
            spike.prettyRemove(game);
        decorativeSpikes = [];
        for (spike in spikes)
            spike.prettyRemove(game);
        spikes = [];
        mainSprite.removeMovieClip();
    }
}
