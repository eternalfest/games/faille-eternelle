package faille.boss;

import etwin.flash.filters.GlowFilter;
import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Entity;
import hf.Hf;
import hf.mode.GameMode;
import etwin.Obfu;
import faille.boss.Constants;
import faille.boss.CustomPlayerHitbox;

class Spike extends MovieClip {
    private static function workaroundTooManyStaticData10(): Array<Array<Float>> {
        return [[-7, -8], [7, -8], [7, 8], [-7, 8]];
    }
    private static var unrotatedHitbox(default, never): Array<Array<Float>> = workaroundTooManyStaticData10();
    private var sub: MovieClip;

    private var hf: Hf;
    private var x: Float;
    private var y: Float;
    private var dx: Float = 0;
    private var dy: Float = 0;

    private var rotatedHitbox: Array<Array<Float>>;

    @:keep
    public function new() {}

    public function init(hf: Hf, g: GameMode, x: Float, y: Float): Void {
        this.hf = hf;
        this.x = x;
        this.y = y;
        gotoAndStop(1);
        sub.gotoAndStop(1);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float): Spike {
        var spike: Spike = cast g.depthMan.attach(Obfu.raw("boss_spike"), hf.Data.DP_BADS);
        spike.init(hf, g, x, y);
        return spike;
    }

    private function updateHitbox(newAngleInDegrees: Float): Void {
        var radianRotation: Float = newAngleInDegrees * Constants.degreeToRadianFactor;
        var rotationCos: Float = Math.cos(radianRotation);
        var rotationSin: Float = Math.sin(radianRotation);
        var applyRotation = function(point: Array<Float>): Array<Float> {
            return [point[0] * rotationCos - point[1] * rotationSin, point[0] * rotationSin + point[1] * rotationCos];
        }
        rotatedHitbox = [];
        for (point in unrotatedHitbox)
            rotatedHitbox.push(applyRotation(point));
    }

    public function setAngle(angleInDegrees: Float): Void {
        _rotation = angleInDegrees + 90;
        updateHitbox(angleInDegrees);
    }

    public function getAngle(): Float {
        return _rotation - 90;
    }

    public function throwAtAngle(angleInDegrees: Float, speed: Float): Void {
        var radianAngle: Float = angleInDegrees * Constants.degreeToRadianFactor;
        dx = speed * Math.cos(radianAngle);
        dy = speed * Math.sin(radianAngle);
        setAngle(angleInDegrees);
    }

    override public function moveTo(x: Float, y: Float): Void {
        this.x = x;
        this.y = y;
    }

    public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        var currentHitbox: Array<Array<Float>> = [];
        for (point in rotatedHitbox)
            currentHitbox.push([point[0] + x, point[1] + y]);

        for (player in game.getPlayerList()) {
            if (player.fl_kill)
                continue;
            var playerHitbox: Array<Array<Float>> = customPlayerHitbox.getHitbox(player);
            if (MLMath.ArePolygonAndNonRotatedRectangleIntersecting(currentHitbox, playerHitbox))
                player.killHit(0);
        }

        if (x < 0 || x > 400 || y < 0 || y > 500)
            return true;
        return false;
    }

    public function endUpdate(): Void {
        x += dx * hf.Timer.tmod;
        y += dy * hf.Timer.tmod;
        _x = x - 10 * Math.sin(_rotation * Constants.degreeToRadianFactor);
        _y = y + 10 * Math.cos(_rotation * Constants.degreeToRadianFactor);
    }

    public function prettyRemove(game: GameMode): Void {
        game.fxMan.attachFx(_x, _y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
        removeMovieClip();
    }
}

class HomingSpike extends Spike {
    private static var acceleration(default, never): Float = 1.02;

    private var maxSpeed: Float;
    private var degreesAngleIncrement: Float;

    private var currentSpeed: Float = 1;
    private var timeUntilNextAcceleration: Float = 1;
    private var target: Entity;

    public function getTarget(): Entity {
        return target;
    }

    public function initHomingSpike(hf: Hf, game: GameMode, x: Float, y: Float, maxSpeed: Float, degreesAngleIncrement: Float): Void {
        super.init(hf, game, x, y);
        this.maxSpeed = maxSpeed;
        this.degreesAngleIncrement = degreesAngleIncrement;
        target = game.getOne(hf.Data.PLAYER);
        /* Initially directed toward the bottom. */
        throwAtAngle(90, 0);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, maxSpeed: Float = 3.3, degreesAngleIncrement: Float = 1.2): HomingSpike {
        var spike: HomingSpike = cast g.depthMan.attach(Obfu.raw("boss_homing_spike"), hf.Data.DP_BADS);
        spike.initHomingSpike(hf, g, x, y, maxSpeed, degreesAngleIncrement);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        if (currentSpeed < maxSpeed) {
            /* A bit weird but I don't know a better way to apply acceleration properly with tmod... */
            timeUntilNextAcceleration -= hf.Timer.tmod;
            if (timeUntilNextAcceleration <= 0) {
                timeUntilNextAcceleration += 1;
                currentSpeed *= acceleration;
                currentSpeed = Math.min(currentSpeed, maxSpeed);
            }
        }

        var currentDegreesAngle: Float = _rotation - 90;
        var degreesAngleWithTarget: Float = Math.atan2(target.y - y, target.x - x) * Constants.radianToDegreeFactor;

        if (currentDegreesAngle - degreesAngleWithTarget > 180)
            currentDegreesAngle -= 360;
        else if (degreesAngleWithTarget - currentDegreesAngle > 180)
            currentDegreesAngle += 360;

        if (currentDegreesAngle < degreesAngleWithTarget)
            currentDegreesAngle = Math.min(currentDegreesAngle + degreesAngleIncrement * hf.Timer.tmod, degreesAngleWithTarget);
        else if (currentDegreesAngle > degreesAngleWithTarget)
            currentDegreesAngle = Math.max(currentDegreesAngle - degreesAngleIncrement * hf.Timer.tmod, degreesAngleWithTarget);

        throwAtAngle(currentDegreesAngle, currentSpeed);
        return super.update(game, customPlayerHitbox);
    }
}

class ZigZagSpike extends Spike {
    private static var timeBetweenDirectionChange(default, never): Float = 40;
    private var timeUntilNextDirectionChange: Float = timeBetweenDirectionChange;
    private var currentDegreesAngle: Float;
    private var speed: Float;

    public function initZigZagSpike(hf: Hf, game: GameMode, x: Float, y: Float, angleInDegrees: Float, speed: Float): Void {
        super.init(hf, game, x, y);
        currentDegreesAngle = angleInDegrees;
        this.speed = speed;
        throwAtAngle(currentDegreesAngle, speed);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, angleInDegrees: Float, speed: Float): ZigZagSpike {
        var spike: ZigZagSpike = cast g.depthMan.attach(Obfu.raw("boss_zigzag_spike"), hf.Data.DP_BADS);
        spike.initZigZagSpike(hf, g, x, y, angleInDegrees, speed);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        timeUntilNextDirectionChange -= hf.Timer.tmod;
        if (timeUntilNextDirectionChange <= 0) {
            if (currentDegreesAngle < -90)
                currentDegreesAngle = -90 + Math.abs(currentDegreesAngle + 90);
            else
                currentDegreesAngle = -90 - Math.abs(currentDegreesAngle + 90);
            throwAtAngle(currentDegreesAngle, speed);
            timeUntilNextDirectionChange += timeBetweenDirectionChange;
        }
        return super.update(game, customPlayerHitbox);
    }
}

class DamoclesSpike extends Spike {
    private static var startGlowingTime(default, never): Float = 40;

    private static function getAttackFilter(): GlowFilter {
        var filter: GlowFilter = new GlowFilter();
        filter.color = 16733440;
        filter.alpha = 1.0;
        filter.strength = 100;
        filter.blurX = 2;
        filter.blurY = 2;
        return filter;
    }
    public static var attackFilter(default, never): GlowFilter = getAttackFilter();

    private var target: Entity;
    private var distance: Float;
    private var angleInDegrees: Float;
    private var remainingLoomingTime: Float;
    private var speed: Float;
    private var glowBeforeAttack: Bool;

    private inline function getLoomingPosition(): {x: Float, y: Float} {
        var angleInRadians: Float = angleInDegrees * Constants.degreeToRadianFactor;
        return {x: target.x + distance * Math.cos(angleInRadians), y: target.y + distance * Math.sin(angleInRadians)};
    }

    public function initDamoclesSpike(hf: Hf, game: GameMode, target: Entity, distance: Float, angleInDegrees: Float, speed: Float, delay: Float, glowBeforeAttack: Bool): Void {
        this.target = target;
        this.distance = distance;
        this.angleInDegrees = angleInDegrees;
        var position: {x: Float, y: Float} = getLoomingPosition();
        super.init(hf, game, position.x, position.y);
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT, "hammer_fx_shine");
        throwAtAngle(angleInDegrees - 180, 0);
        remainingLoomingTime = delay;
        this.speed = speed;
        this.glowBeforeAttack = glowBeforeAttack;
    }

    public static function attach(hf: Hf, g: GameMode, target: Entity, distance: Float, angleInDegrees: Float, speed: Float, delay: Float, glowBeforeAttack: Bool = false): DamoclesSpike {
        var spike: DamoclesSpike = cast g.depthMan.attach(Obfu.raw("boss_damocles_spike"), hf.Data.DP_BADS);
        spike.initDamoclesSpike(hf, g, target, distance, angleInDegrees, speed, delay, glowBeforeAttack);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        if (remainingLoomingTime > 0) {
            var position: {x: Float, y: Float} = getLoomingPosition();
            moveTo(position.x, position.y);

            var previousLoomingTime: Float = remainingLoomingTime;
            remainingLoomingTime -= hf.Timer.tmod;
            if (remainingLoomingTime <= 0)
                throwAtAngle(angleInDegrees - 180, speed);

            if (glowBeforeAttack && previousLoomingTime > startGlowingTime && remainingLoomingTime <= startGlowingTime) {
                var previousFilters = filters.copy();
                previousFilters.push(attackFilter);
                filters = previousFilters;
            }

            /* Don't despawn them if they get out of screen while looming over the player. */
            super.update(game, customPlayerHitbox);
            return false;
        }

        return super.update(game, customPlayerHitbox);
    }
}

class UltimateSpike extends Spike {
    private static var speed(default, never): Float = 5;

    private var angle: Float;
    private var remainingLifetime: Float = 500;
    private var isMovingForward: Bool = true;
    private var timeUntilNextDirectionChange: Float = 20;
    private var directionSign: Float = 1;

    public function initUltimateSpike(hf: Hf, game: GameMode, x: Float, y: Float, angle: Float): Void {
        super.init(hf, game, x, y);
        this.angle = angle;
        throwAtAngle(angle, speed);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, angle: Float): UltimateSpike {
        var spike: UltimateSpike = cast g.depthMan.attach(Obfu.raw("boss_ultimate_spike"), hf.Data.DP_BADS);
        spike.initUltimateSpike(hf, g, x, y, angle);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        remainingLifetime -= hf.Timer.tmod;
        if (remainingLifetime <= 0)
            return true;

        timeUntilNextDirectionChange -= hf.Timer.tmod;
        if (timeUntilNextDirectionChange <= 0) {
            if (isMovingForward) {
                throwAtAngle(angle + directionSign * 90, speed);
                directionSign *= -1;
            }
            else {
                throwAtAngle(angle, speed);
            }
            timeUntilNextDirectionChange += 20;
            isMovingForward = !isMovingForward;
        }

        /* Don't despawn when out of screen. */
        super.update(game, customPlayerHitbox);
        return false;
    }
}

class AcceleratingFallingSpike extends Spike {
    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float): AcceleratingFallingSpike {
        var spike: AcceleratingFallingSpike = cast g.depthMan.attach(Obfu.raw("boss_falling_spike"), hf.Data.DP_BADS);
        spike.init(hf, g, x, y);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        dy *= 1.01;
        var outsideOfScreen: Bool = super.update(game, customPlayerHitbox);
        if (y > 200)
            return outsideOfScreen;
        return false; /* Do not despawn it initially. */
    }
}

class TemporarySpike extends Spike {
    private var timeUntilApparition: Float = 70;
    private var lifetime: Float;

    public function initTemporarySpike(hf: Hf, game: GameMode, x: Float, y: Float, lifetime: Float): Void {
        super.init(hf, game, x, y);
        this.lifetime = lifetime;
        _visible = false;
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_shine");
        /* To properly set the hitbox. */
        throwAtAngle(-90, 0);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, lifetime: Float): TemporarySpike {
        var spike: TemporarySpike = cast g.depthMan.attach(Obfu.raw("boss_temporary_spike"), hf.Data.DP_BADS);
        spike.initTemporarySpike(hf, g, x, y, lifetime);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        if (timeUntilApparition > 0) {
            timeUntilApparition -= hf.Timer.tmod;
            if (timeUntilApparition <= 0) {
                game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
                _visible = true;
            }
        }
        else if (lifetime > 0) {
            lifetime -= hf.Timer.tmod;
            if (lifetime <= 0) {
                _visible = false;
                game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
                return true;
            }
            return super.update(game, customPlayerHitbox);
        }
        return false; /* No hitbox, no visual, no despawning. */
    }
}

enum LaserState {
    Tracking;
    Waiting;
    Firing;
    Done;
}

class Laser extends MovieClip {
    private var hf: Hf;
    private var game: GameMode;
    private var target: Entity;
    private var angle: Float;
    private var scale: Float;
    private var trackingTime: Float;
    private var waitingTime: Float;
    private var firingTime: Float;
    private var state: LaserState = LaserState.Tracking;
    private var remainingStateTime: Float;
    private var onDoneCallback: Laser -> Void;

    private var timeBetweenSubLasers: Float;
    private var timeUntilNextSubLaser: Float;

    public function new() {}

    private function updateAngle(): Void {
        var radianAngle: Float = Math.atan2(target.y - _y, target.x - _x);
        angle = radianAngle * Constants.radianToDegreeFactor;
    }

    public function setAngle(angleInDegrees: Float): Void {
        angle = angleInDegrees;
    }

    public function getTotalTime(): Float {
        return trackingTime + waitingTime + firingTime;
    }

    public function init(hf: Hf, game: GameMode, x: Float, y: Float, onDoneCallback: Laser -> Void, scale: Float = 100, trackingTime: Float = 80, waitingTime: Float = 50, firingTime: Float = 120, timeBetweenSubLasers: Float = 4): Void {
        this.hf = hf;
        this.game = game;
        _x = x;
        _y = y;
        this.scale = scale;
        this.trackingTime = trackingTime;
        this.waitingTime = waitingTime;
        this.firingTime = firingTime;
        this.timeBetweenSubLasers = timeBetweenSubLasers;
        this.timeUntilNextSubLaser = Math.random() * timeBetweenSubLasers;
        this.remainingStateTime = trackingTime;
        this.onDoneCallback = onDoneCallback;
        _xscale = scale / 5;
        _yscale = 500;
        target = game.getOne(hf.Data.PLAYER);
        if (trackingTime > 0) {
            updateAngle();
        }
        else {
            state = LaserState.Waiting;
            remainingStateTime = waitingTime;
        }
        gotoAndStop(2);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, onDoneCallback: Laser -> Void, scale: Float = 100, trackingTime: Float = 80, waitingTime: Float = 50, firingTime: Float = 120): Laser {
        var laser: Laser = cast g.depthMan.attach(Obfu.raw("boss_laser"), hf.Data.DP_BADS);
        laser.init(hf, g, x, y, onDoneCallback, scale, trackingTime, waitingTime, firingTime);
        return laser;
    }

    public function getGame(): GameMode {
        return game;
    }

    private inline function getRadianAngle(): Float {
        return angle * Constants.degreeToRadianFactor;
    }

    private function hitPlayers(): Void {
        /* https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line, "Line defined by two points" */
        var radianAngle: Float = getRadianAngle();
        var x2: Float = _x + Math.cos(radianAngle);
        var y2: Float = _y + Math.sin(radianAngle);
        var diffX: Float = x2 - _x;
        var diffY: Float = y2 - _y;
        var divisor: Float = Math.sqrt(diffX * diffX + diffY * diffY);
        for (player in game.getPlayerList()) {
            var distance: Float = Math.abs(diffX * (_y - (player.y - 10)) - diffY * (_x - player.x)) / divisor;
            if (distance < scale / 10)
                player.killHit(0);
        }
    }

    private inline function getLineBounds(): {min: Float, max: Float} {
        var min: Float = Math.NEGATIVE_INFINITY;
        var max: Float = Math.POSITIVE_INFINITY;

        var radianAngle: Float = getRadianAngle();
        var cosAngle: Float = Math.cos(radianAngle);
        if (cosAngle != 0) {
            var param0: Float = -_x / cosAngle;
            var param400: Float = (400 - _x) / cosAngle;
            min = Math.min(param0, param400);
            max = Math.max(param0, param400);
        }
        var sinAngle: Float = Math.sin(radianAngle);
        if (sinAngle != 0) {
            var param0: Float = -_y / sinAngle;
            var param500: Float = (500 - _y) / sinAngle;
            min = Math.max(min, Math.min(param0, param500));
            max = Math.min(max, Math.max(param0, param500));
        }
        return {min: min, max: max};
    }

    public function updateReborn(): Bool {
        switch (state) {
        case Tracking:
            updateAngle();
            remainingStateTime -= hf.Timer.tmod;
            if (remainingStateTime <= 0) {
                state = LaserState.Waiting;
                remainingStateTime = waitingTime;
            }
        case Waiting:
            remainingStateTime -= hf.Timer.tmod;
            if (remainingStateTime <= 0) {
                _xscale = scale;
                state = LaserState.Firing;
                remainingStateTime = firingTime;
            }
        case Firing:
            hitPlayers();
            timeUntilNextSubLaser -= hf.Timer.tmod;
            if (timeUntilNextSubLaser <= 0) {
                var subLaser: MovieClip = cast game.depthMan.attach("hammer_fx_death", hf.Data.DP_BADS);
                var bounds: {min: Float, max: Float} = getLineBounds();
                var param = bounds.min + Math.random() * (bounds.max - bounds.min);
                var radianAngle: Float = getRadianAngle();
                /* Correct place along the line. */
                subLaser._x = _x + param * Math.cos(radianAngle);
                subLaser._y = _y + param * Math.sin(radianAngle);
                subLaser._rotation = Std.random(360);
                /* Fix placement due to rotation and anchor point. */
                var subLaserRadianAngle = (subLaser._rotation + 90) * Constants.degreeToRadianFactor;
                subLaser._x -= 257 * Math.cos(subLaserRadianAngle);
                subLaser._y -= 257 * Math.sin(subLaserRadianAngle);
                subLaser._xscale = Math.min(20, scale / 10);
                timeUntilNextSubLaser += timeBetweenSubLasers;
            }
            remainingStateTime -= hf.Timer.tmod;
            if (remainingStateTime <= 0) {
                state = LaserState.Done;
                play();
            }
        case Done:
            if (_currentframe == _totalframes) {
                onDoneCallback(this);
                return true;
            }
        }

        return false;
    }

    @:keep
    public function update(): Void {
        /* For the usual update needed when owned by the game, it will have to deal with the lifetime handling through the callback only. */
        updateReborn();
    }

    @:keep
    public function endUpdate(): Void {
        _rotation = angle + 90;
    }

    public function destroy(): Void {
        if (state != LaserState.Done) {
            state = LaserState.Done;
            play();
        }
    }

    override public function onRollOver(): Void {
        /* Do nothing, in particular don't grow the sprite in size when the mouse is over it. */
    }
}

/* Unlike HomingSpike it also starts directly toward the player with some base speed. */
class TemporaryHomingSpike extends HomingSpike {
    private var remainingLifetime: Float;

    @:keep
    public function new(): Void {
        super();
    }

    public function initTemporaryHomingSpike(hf: Hf, game: GameMode, x: Float, y: Float, maxSpeed: Float, degreesAngleIncrement: Float, initialSpeed: Float, lifetime: Float): Void {
        super.initHomingSpike(hf, game, x, y, maxSpeed, degreesAngleIncrement);
        this.remainingLifetime = lifetime;

        var targetAngleRadian: Float = Math.atan2(target.y - y, target.x - x);
        throwAtAngle(targetAngleRadian * Constants.radianToDegreeFactor, initialSpeed);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, maxSpeed: Float, degreesAngleIncrement: Float, initialSpeed: Float, lifetime: Float): TemporaryHomingSpike {
        var spike: TemporaryHomingSpike = cast g.depthMan.attach(Obfu.raw("boss_reborn_temporary_homing_spike"), hf.Data.DP_BADS);
        spike.initTemporaryHomingSpike(hf, g, x, y, maxSpeed, degreesAngleIncrement, initialSpeed, lifetime);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        remainingLifetime -= hf.Timer.tmod;
        if (remainingLifetime <= 0) {
            game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
            return true;
        }
        return super.update(game, customPlayerHitbox);
    }
}

/* Yeah, how it handles the direction change has been changed in the boss reborn and it's not compatible...
 * The alternative would be taking a "isReborn" flag and acting on it, which doesn't sound much better than a near copy/paste of ZigZagSpike... */
class ZigZagSpikeReborn extends Spike {
    private static inline var timeBetweenDirectionChange: Float = 50;
    private var timeUntilNextDirectionChange: Float = timeBetweenDirectionChange;
    private var initialDegreesAngle: Float;
    private var currentDegreesAngle: Float;
    private var speed: Float;

    public function initZigZagSpike(hf: Hf, game: GameMode, x: Float, y: Float, angleInDegrees: Float, speed: Float): Void {
        super.init(hf, game, x, y);
        initialDegreesAngle = angleInDegrees;
        currentDegreesAngle = angleInDegrees;
        this.speed = speed;
        throwAtAngle(currentDegreesAngle, speed);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, angleInDegrees: Float, speed: Float): ZigZagSpikeReborn {
        var spike: ZigZagSpikeReborn = cast g.depthMan.attach(Obfu.raw("boss_reborn_zigzag_spike"), hf.Data.DP_BADS);
        spike.initZigZagSpike(hf, g, x, y, angleInDegrees, speed);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        timeUntilNextDirectionChange -= hf.Timer.tmod;
        if (timeUntilNextDirectionChange <= 0) {
            if (currentDegreesAngle == initialDegreesAngle)
                currentDegreesAngle = initialDegreesAngle + 90;
            else
                currentDegreesAngle = initialDegreesAngle;
            throwAtAngle(currentDegreesAngle, speed);
            timeUntilNextDirectionChange += timeBetweenDirectionChange;
        }
        return super.update(game, customPlayerHitbox);
    }
}

class IceFlowerSpike extends Spike {
    private var remainingLifetime: Float = 200;
    private var timeBeforeMoving: Float = 60;
    private var angle: Float;
    private var speed: Float;
    private var radialSpeed: Float;

    public function initIceFlowerSpike(hf: Hf, game: GameMode, xSpawnCenter: Float, ySpawnCenter: Float, spawnDistance: Float, initialAngle: Float, speed: Float, radialSpeed: Float): Void {
        var radianAngle: Float = initialAngle * Constants.degreeToRadianFactor;
        var x: Float = xSpawnCenter + spawnDistance * Math.cos(radianAngle);
        var y: Float = ySpawnCenter + spawnDistance * Math.sin(radianAngle);
        super.init(hf, game, x, y);
        angle = initialAngle;
        this.speed = speed;
        this.radialSpeed = radialSpeed;
        game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
        throwAtAngle(initialAngle, 0);
    }

    public static function attach(hf: Hf, g: GameMode, xSpawnCenter: Float, ySpawnCenter: Float, spawnDistance: Float, initialAngle: Float, speed: Float, radialSpeed: Float): IceFlowerSpike {
        var spike: IceFlowerSpike = cast g.depthMan.attach(Obfu.raw("boss_reborn_ice_flower_spike"), hf.Data.DP_BADS);
        spike.initIceFlowerSpike(hf, g, xSpawnCenter, ySpawnCenter, spawnDistance, initialAngle, speed, radialSpeed);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        if (timeBeforeMoving > 0)
            timeBeforeMoving -= hf.Timer.tmod;

        if (timeBeforeMoving <= 0) {
            this.angle += radialSpeed * hf.Timer.tmod;
            throwAtAngle(angle, speed);
        }

        if (remainingLifetime > 0) {
            remainingLifetime -= hf.Timer.tmod;
            if (remainingLifetime <= 0) {
                game.fxMan.attachFx(x, y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
                return true;
            }
            return super.update(game, customPlayerHitbox);
        }
        return false; /* No hitbox, no visual, no despawning. */
    }
}

class RotatingLaser extends Laser {
    private var timeUntilDirectionChange: Float = 300;

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, onDoneCallback: Laser -> Void, scale: Float = 100, trackingTime: Float = 80, waitingTime: Float = 50, firingTime: Float = 120, timeBetweenSubLasers: Float = 4): RotatingLaser {
        var laser: RotatingLaser = cast g.depthMan.attach(Obfu.raw("boss_reborn_rotating_laser"), hf.Data.DP_BADS);
        laser.init(hf, g, x, y, onDoneCallback, scale, trackingTime, waitingTime, firingTime, timeBetweenSubLasers);
        return laser;
    }

    override public function updateReborn(): Bool {
        if (state == Firing) {
            timeUntilDirectionChange -= hf.Timer.tmod;
            var radialSpeed: Float = (timeUntilDirectionChange > 0 ? 1 : -1) * hf.Timer.tmod;
            angle += radialSpeed;
        }
        return super.updateReborn();
    }
}

class KamiSpike extends Spike {
    private static inline var rethrowSpeed: Float = 5;

    private var hasBeenRethrown: Bool = false;

    public function initKamiSpike(hf: Hf, game: GameMode, x: Float, y: Float): Void {
        super.init(hf, game, x, y);
        /* Use instead of throwAtAngle to properly set the hitbox. */
        throwAtAngle(Math.random() * 360, 0);
    }

    public function rethrow(): Void {
        hasBeenRethrown = true;
        throwAtAngle(getAngle(), rethrowSpeed);
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float): KamiSpike {
        var spike: KamiSpike = cast g.depthMan.attach(Obfu.raw("boss_reborn_kami_spike"), hf.Data.DP_BADS);
        spike.initKamiSpike(hf, g, x, y);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        var outsideOfScreen: Bool = super.update(game, customPlayerHitbox);
        if (hasBeenRethrown)
            return outsideOfScreen;
        return false; /* Do not despawn it initially. */
    }
}

class OtherwordlySpike extends Spike {
    private var hasBeenInScreen: Bool = false;

    private var remainingWaitingTime: Float;
    private var rethrowSpeed: Float;

    public function initOtherwordlySpike(hf: Hf, game: GameMode, x: Float, y: Float, waitingTime: Float, rethrowSpeed: Float): Void {
        super.init(hf, game, x, y);
        remainingWaitingTime = waitingTime;
        this.rethrowSpeed = rethrowSpeed;
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float, waitingTime: Float, rethrowSpeed: Float): OtherwordlySpike {
        var spike: OtherwordlySpike = cast g.depthMan.attach(Obfu.raw("boss_reborn_otherwordly_spike"), hf.Data.DP_BADS);
        spike.initOtherwordlySpike(hf, g, x, y, waitingTime, rethrowSpeed);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        var outsideOfScreen: Bool = super.update(game, customPlayerHitbox);

        /* Stop them when they "peek" inside of the screen. */
        if (!hasBeenInScreen && x > 12 && x < 388 && y > 8 && y < 492) {
            hasBeenInScreen = true;
            dx = 0;
            dy = 0;
        }

        if (hasBeenInScreen && remainingWaitingTime > 0) {
            remainingWaitingTime -= hf.Timer.tmod;
            if (remainingWaitingTime <= 0)
                throwAtAngle(getAngle(), rethrowSpeed);
        }

        /* Guarantee not to leak movie clips in the corner cases where it doesn't make it inside the screen somehow...
         * (it should be spawned closer to the screen than that)
         * I dislike needing to do that, but better be safe than sorry. */
        if (x < -400 || x > 800 || y < -500 || y > 1000)
            return true;

        return hasBeenInScreen && outsideOfScreen; /* Do not despawn until it has been in the screen. */
    }
}

/* Similarly to OtherwordlySpike, wait until it has been in screen before considering it for removal. Otherwise a normal spike. */
class StormSpike extends Spike {
    private var hasBeenInScreen: Bool = false;

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float): StormSpike {
        var spike: StormSpike = cast g.depthMan.attach(Obfu.raw("boss_reborn_storm_spike"), hf.Data.DP_BADS);
        spike.init(hf, g, x, y);
        return spike;
    }

    override public function update(game: GameMode, customPlayerHitbox: CustomPlayerHitbox): Bool {
        var outsideOfScreen: Bool = super.update(game, customPlayerHitbox);

        if (!hasBeenInScreen && !outsideOfScreen)
            hasBeenInScreen = true;

        /* Guarantee not to leak movie clips in the corner cases where it doesn't make it inside the screen somehow...
         * (it should be spawned closer to the screen than that)
         * I dislike needing to do that, but better be safe than sorry. */
        if (x < -400 || x > 800 || y < -500 || y > 1000)
            return true;

        return hasBeenInScreen && outsideOfScreen;
    }
}
