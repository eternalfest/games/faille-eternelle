package faille;

import hf.Hf;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class LevelWarp {

  @:diExport
  public var patches(default, null): IPatch;

  public function new() {
  
    this.patches = new PatchList([

      Ref.auto(GameMode.nextLevel).wrap(function(hf, self: GameMode, old): Void {
          
        var link = hf.Data.getLink(self.currentDim, self.world.currentId, -2);
        if(link == null) {
          old(self);
          return;
        }
          
        if(link.to_pid != -2) {
          self.switchDimensionById(link.to_did, link.to_lid, link.to_pid);
          old(self);
          return;
        }
          
        if(link.to_did != self.currentDim) {
          old(self);
          return;
        }
          
        self.fakeLevelId = 0;
        self.goto(link.to_lid);
        old(self);
        return;
      }),
	  
    ]);
  }
}