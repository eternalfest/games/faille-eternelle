package faille;

import hf.Hf;
import hf.mode.GameMode;
import hf.entity.Player;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class FadingIgor {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var fading: Bool = false;
  public var frames: Float;
  //public var player: Player;

  public function new() {
    this.actions = new CreateFadingIgor(this);
      
    this.patches = new PatchList([
        
      Ref.auto(Player.update).before(function(hf, self: Player): Void {
        if (this.fading) {
          self.minAlpha = 0;
          self.alpha -= this.frames;
	      if(Std.random(3) == 0)
	        var ting = self.game.fxMan.attachFx(self._x - 10 + Std.random(20), self._y - 30 + Std.random(20), "$ting");
          if(self.alpha <= 0) {
            this.fading = false;
          } else {
            self.minAlpha = 0;
          }
        }
      }),
        
    ]);
  }
}

class CreateFadingIgor implements IAction {
  public var name(default, null): String = Obfu.raw("fadePlayer");
  public var isVerbose(default, null): Bool = false;

  private var mod: FadingIgor;
    
  public function new(mod: FadingIgor) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var fading: Bool = ctx.getOptBool(Obfu.raw("n")).or(true); 
    mod.fading = fading;
	var frames: Int = ctx.getOptInt(Obfu.raw("f")).or(40);
	mod.frames = 100 / frames;

    return false;
  }
}