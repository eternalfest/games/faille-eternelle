package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import etwin.flash.MovieClip;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class WhiteFading {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var launch: Bool;
  public var square_mc: MovieClip = null;
  public var hasBeenTraced = false;

  public function new() {
    this.actions = new CreateWhiteFading(this);
      
    this.launch = false;
      
    this.patches = new PatchList([
        
      Ref.auto(GameMode.main).after(function(hf, self: GameMode): Void {
        if (this.launch) {
          if (!this.hasBeenTraced) {
            square_mc = self.depthMan.empty(10000);
            square_mc.beginFill(0xFFFFFF);
            square_mc._alpha = 0;
            square_mc.moveTo(-20, 0);
            square_mc.lineTo(-20, 520);
            square_mc.lineTo(420, 520);
            square_mc.lineTo(420, 0);
            square_mc.lineTo(-20, 0);
            square_mc.endFill();
            hasBeenTraced = true;
          } else {
            square_mc._alpha = square_mc._alpha + 0.9;
          }
        
          self.shake(10, 5);
    
          if(self.fl_flipX) {
            self.mc._x = hf.Data.GAME_WIDTH + self.xOffset - Math.round((Std.random(2) * 2 - 1) * (Std.random(Math.round(self.shakePower * 10)) / 10) * self.shakeTimer / self.shakeTotal);
          } else {
            self.mc._x = Math.round(self.xOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(self.shakePower * 10)) / 10) * self.shakeTimer / self.shakeTotal);
          }
        
          if (self.fl_flipY) {
            self.mc._y = hf.Data.GAME_HEIGHT + 20 + Math.round(self.yOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(self.shakePower * 10)) / 10) * self.shakeTimer / self.shakeTotal);
          } else {
            self.mc._y = Math.round(self.yOffset + (Std.random(2) * 2 - 1) * (Std.random(Math.round(self.shakePower * 10)) / 10) * self.shakeTimer / self.shakeTotal);
          }
            
        } else {
          square_mc._alpha = 0;
          square_mc.removeMovieClip();
          square_mc = null;
          hasBeenTraced = false;
        }
      }),
        
    ]);
  }
}

class CreateWhiteFading implements IAction {
  public var name(default, null): String = Obfu.raw("createWhiteFading");
  public var isVerbose(default, null): Bool = false;

  private var mod: WhiteFading;
    
  public function new(mod: WhiteFading) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var launch: Bool = ctx.getOptBool(Obfu.raw("n")).or(true); 
    mod.launch = launch;

    return false;
  }
}