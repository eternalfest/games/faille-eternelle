package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import etwin.flash.Key;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Kill {

  @:diExport
  public var patches(default, null): IPatch;
  public var can_suicide: Bool;

  public function new() {
    can_suicide = true;
    this.patches = new PatchList([

      Ref.auto(GameMode.getControls).before(function(hf: Hf, self: GameMode): Void {
        if(self.getPlayerList().length < 3) {
          if (Key.isDown(75) && self.keyLock != 75 && can_suicide && !self.fl_pause && !self.fl_lock) {
            self.getPlayerList()[0].killHit(0);
            self.keyLock = 75;
          }
        }
      }),
 
    ]);
  }
}