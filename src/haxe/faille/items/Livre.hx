package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Livre implements ISpec {
  public var id(default, null): Int = 143;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for (i in 0...6) {
      var randomScore = specMan.game.randMan.draw(hf.Data.RAND_SCORES_ID);
      var newItem = hf.entity.item.ScoreItem.attach(specMan.game, item.x, item.y, randomScore, null);
      newItem.moveFrom(item, 10);
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
