package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Orbe implements ISpec {
  public var id(default, null): Int = 161;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      var burn = specMan.game.fxMan.attachFx(fruit.x, fruit.y, "hammer_fx_burning");
      fruit.destroy();
    }
    specMan.game.callEvilOne(0);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}