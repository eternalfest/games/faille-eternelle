package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.entity.Player;
import randomfest.Randomfest;
import vault.ISpec;

class Anneau implements ISpec {
	public var id(default, null): Int = 148;

	private var randomfest: Randomfest;
	private var players: Array<Player>;

	public function new(randomfest: Randomfest): Void {
		this.randomfest = randomfest;
		this.players = [];
	}

	public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
		specMan.permanent(item.id);
		players.push(randomfest.spawnRandomPlayer(hf, specMan.game, item.x / 20, item.y / 20 - 1));
	}

	public function interrupt(hf: Hf, specMan: SpecialManager): Void {
		specMan.clearRec();
		specMan.actives[this.id] = false;
		for(player in players) {
			player.destroy();
		}
		players = [];
	}
}