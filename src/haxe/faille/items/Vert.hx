package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.entity.Bad;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;
import custom_clips.CustomClips;

class Vert implements ISpec {
  public var id(default, null): Int = 137;

  private var clips(default, null): CustomClips;

  public function new(clips: CustomClips) {
    this.clips = clips;
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      
      if (Std.is(fruit, hf.entity.bad.walker.Poire)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveDown(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveUp(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.ww.Crawler)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveDown(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveUp(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.flyer.Baleine)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveDown(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveUp(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.Kiwi)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveDown(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveUp(fireBall.shootSpeed);
      } else if (Std.is(fruit, faille.bads.CitronVert)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveDown(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveUp(fireBall.shootSpeed);
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
