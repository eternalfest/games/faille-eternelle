package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;
import hf.Entity;
import hf.entity.Bad;
import hf.entity.bad.Flyer;
import hf.entity.bad.walker.Ananas;
import hf.entity.bad.walker.Fraise;
import hf.entity.bad.walker.Bombe;
import hf.entity.bad.Shooter;
import hf.entity.bad.ww.Crawler;
import hf.entity.bad.ww.Saw;
import hf.entity.shoot.FramBall;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Montre implements ISpec {
  public var id(default, null): Int = 147;
  public var isActive(default, null): Bool = false;
  public static var ACTIVE(default, null): Bool = false;
  public var cooldown(default, null): Int = 32 * 14;
  public var state(default, null): Int = 3;
  public var square_mc: MovieClip;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(Bad.calcSpeed).wrap(function(hf: Hf, self: Bad, old): Void {
        if (this.isActive) {
          self.speedFactor = 0;
        } else {
          old(self);
        }
      }),

      Ref.auto(Bad.hit).wrap(function(hf: Hf, self: Bad, e: Entity, old): Void {
        if (this.isActive) {
          if ((e.types & hf.Data.PLAYER) > 0) {} else {
            old(self, e);
          }
        } else {
          old(self, e);
        }
      }),

      Ref.auto(Bad.update).wrap(function(hf: Hf, self: Bad, old): Void {
        if (this.isActive) {
          var badList = self.game.getBadClearList();
          for (bad in badList) {
            bad.next = null;
          }
        } else {
          old(self); 
        }
      }),

      Ref.auto(Ananas.startAttack).wrap(function(hf: Hf, self: Ananas, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Fraise.onShoot).wrap(function(hf: Hf, self: Fraise, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Bombe.trigger).wrap(function(hf: Hf, self: Bombe, old): Void {
        if (!this.isActive) {
          old(self);
        } else {
          self.destroy();
          self.dropReward();
        }
      }),

      Ref.auto(Shooter.startShoot).wrap(function(hf: Hf, self: Shooter, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Shooter.onNext).wrap(function(hf: Hf, self: Shooter, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(FramBall.update).wrap(function(hf: Hf, self: FramBall, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(FramBall.hit).wrap(function(hf: Hf, self: FramBall, e: Entity, old): Void {
        if (this.isActive) {
          if ((e.types & hf.Data.PLAYER) > 0) {} else {
            old(self, e);
          }
        } else {
          old(self, e);
        }
      }),

      Ref.auto(Crawler.prepareAttack).wrap(function(hf: Hf, self: Crawler, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Crawler.attack).wrap(function(hf: Hf, self: Crawler, old): Void {
          if (!this.isActive) {
            old(self);
          } else {
            self.fl_attack = false;
          }
      }),
        
      Ref.auto(Saw.update).wrap(function(hf: Hf, self: Saw, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if(this.isActive) {

            this.updateTimers();

            for(player in self.getPlayerList()) {
              if (player.specialMan.actives[this.id] && !player.game.fl_clear) {

                if(this.cooldown <= 0) {
                  switch(this.state) {
                    case 3:
                      var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                      curse.gotoAndStop("" + 10);
                      player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                      this.cooldown = hf.Data.SECOND;
                      this.state--;
                      break;
                    case 2:
                      var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                      curse.gotoAndStop("" + 11);
                      player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                      this.cooldown = hf.Data.SECOND;
                      this.state--;
                      break;
                    case 1:
                      var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                      curse.gotoAndStop("" + 12);
                      player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                      this.cooldown = hf.Data.SECOND;
                      this.state--;
                      break;
                    case 0:
                      var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                      curse.gotoAndStop("" + 13);
                      player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                      this.cooldown = hf.Data.SECOND;
                      break;
                  }
                }
              }
            }
          }
          if(self.fl_clear) this.square_mc._alpha = 0;
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    this.isActive = true;
    Montre.ACTIVE = true;
    specMan.temporary(item.id, 32 * 14);
    var badList = specMan.game.getBadClearList();
    for (bad in badList) {
      bad.updateSpeed();
      bad.animFactor *= 0;
    }

    var badBombList = specMan.game.getList(hf.Data.BAD_BOMB);
    for (badBomb in badBombList) {
      badBomb.destroy();
    }

    var shootList = specMan.game.getList(hf.Data.SHOOT);
    for (shoot in shootList) {
      if (!Std.is(shoot, hf.entity.shoot.FramBall)) {
        shoot.destroy();
      }
    }

    this.square_mc = specMan.game.depthMan.empty(10000);
    this.square_mc._alpha = 20;
    this.square_mc.beginFill(0xFFFFFF);
    this.square_mc.moveTo(-100, -100);
    this.square_mc.lineTo(-100, 620);
    this.square_mc.lineTo(520, 620);
    this.square_mc.lineTo(520, -100);
    this.square_mc.lineTo(-100, -100);
    this.square_mc.endFill();
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    this.isActive = false;
    Montre.ACTIVE = false;
    this.square_mc._alpha = 0;
    this.state = 3;
    this.cooldown = 32 * 14;
    specMan.actives[this.id] = false;
    var badList = specMan.game.getBadClearList();

    for(player in specMan.game.getPlayerList()) {
     player.unstick();
    }

    for (bad in badList) {
      var fruit: hf.entity.Bad = cast bad;
      bad.updateSpeed();
      bad.animFactor = 1;

      if (Std.is(fruit, hf.entity.bad.walker.Ananas)) {
        var ananas: hf.entity.bad.walker.Ananas = cast fruit;
        if(ananas.fl_attack) {
          ananas.startAttack();
        }
      }
    }
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function updateTimers(): Void {
    if(this.cooldown > 0) {
      this.cooldown--;
    }
  }

}