package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Item;
import hf.entity.Player;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Marteau implements ISpec {
  public var id(default, null): Int = 131;
  public var isActive(default, null): Bool = false;
  public var radius(default, null): Float = 65;
  public var cooldown(default, null): Int = 0;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {

    this.patch = new PatchList([

      Ref.auto(Player.attack).wrap(function(hf: Hf, player: Player, old): Null<Entity> {
        if (player.specialMan.actives[this.id] && this.isActive && !player.game.fl_clear) {
          if(this.cooldown <= 0) {
            player.game.fxMan.attachExplodeZone(player.x, player.y, this.radius);
            var arrayBad = player.game.getClose(hf.Data.BAD, player.x, player.y, this.radius, player.fl_stable);
            this.cooldown = hf.Data.SECOND * 2;
            for(bad in arrayBad) {
              var fruit: hf.entity.Bad = cast bad;
              fruit.forceKill(null);
            }
          }
          return null;
        }
        return old(player);
      }),

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          for(player in self.getPlayerList()) {
            if (player.specialMan.actives[this.id]) {
              this.updateTimers();
            }
          }
          if(self.fl_clear) {
            for(player in self.getPlayerList()) {
              if (player.specialMan.actives[this.id]) {
                player.unstick();
              }
            }
          }
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    this.isActive = true;
    specMan.player.curse(14);
    specMan.temporary(item.id, hf.Data.WEAPON_DURATION);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    this.isActive = false;
    specMan.actives[this.id] = false;
    specMan.clearRec();
    specMan.player.unstick();
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function updateTimers(): Void {
    if(this.cooldown > 0) {
      this.cooldown--;
    }
  }
}