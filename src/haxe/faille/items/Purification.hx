package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Purification implements ISpec {
  public var id(default, null): Int = 159;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      hf.entity.bad.flyer.Tzongre.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT);
      fruit.destroy();
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}