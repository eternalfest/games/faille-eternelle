package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Cristallin implements ISpec {
  public var id(default, null): Int = 141;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {

    var arrayLetters = specMan.player.extendList;
    while(arrayLetters.length < 7) {
      arrayLetters.push(false);
    }

    var arrayLettersFalse = new Array();
    var idLetter = 0;

    for(letter in arrayLetters){
      if(!letter) arrayLettersFalse.push(idLetter);
      idLetter++;
    }

    var randomLetter = arrayLettersFalse[hf.Std.random(arrayLettersFalse.length)];
    var randomCoord = {x: hf.Std.random(hf.Data.LEVEL_WIDTH), y: hf.Std.random(hf.Data.LEVEL_HEIGHT)};
    randomCoord = specMan.game.world.getGround(randomCoord.x, randomCoord.y);
    hf.entity.item.SpecialItem.attach(specMan.game, randomCoord.x * hf.Data.CASE_WIDTH, randomCoord.y * hf.Data.CASE_HEIGHT, 0, randomLetter);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
