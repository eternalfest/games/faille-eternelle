package faille.items;

import etwin.ds.Nil;
import etwin.flash.Key;
import hf.Entity;
import hf.entity.Item;
import hf.entity.Player;
import hf.entity.PlayerController;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;
import patchman.IPatch;
import patchman.Ref;

import vault.ISpec;

@:build(patchman.Build.di())
class Dash implements ISpec {
    private static var ALLOWED_DURATION(default, never): Float = 7;
    private static var DASH_DURATION(default, never): Float = 5;
    private static var DASH_SPEED(default, never): Float = 20;

    public var id(default, null): Int = 152;

    private var timeSinceLastDirection: Float = 0;
    private var lastDirectionPressed: Null<Int> = null;
    private var hasReleased: Bool = false;

    private var isDashing: Bool = false;
    private var isDashingLeft: Bool = false;
    private var dashingRemainingDuration: Float = DASH_DURATION;

    @:diExport
    public var dashOnDoubleTap(default, null): IPatch;
    @:diExport
    public var preventDeathDuringDash(default, null): IPatch;
    @:diExport
    public var preventStunDuringDash(default, null): IPatch;
    @:diExport
    public var killBadsDuringDash(default, null): IPatch;

    private function resetKeysState(): Void {
        timeSinceLastDirection = 0;
        lastDirectionPressed = null;
        hasReleased = false;
    }

    private function stopDashing(player: Player): Void {
        isDashing = false;
        player.fl_gravity = true;
    }

    public function new() {
        dashOnDoubleTap = Ref.auto(PlayerController.getControls).after(function(hf: Hf, self: PlayerController): Void {
            if (!self.player.specialMan.actives[id])
                return;

            if (isDashing) {
                self.player.dx = DASH_SPEED * (isDashingLeft ? -1 : 1);
                dashingRemainingDuration -= hf.Timer.tmod;
                if (dashingRemainingDuration <= 0) {
                    stopDashing(self.player);
                    self.player.dx = 0;
                }
                return;
            }

            var leftPressed: Bool = Key.isDown(self.left);
            var rightPressed: Bool = Key.isDown(self.right);
            if (leftPressed && rightPressed) {
                resetKeysState();
                return;
            }

            var newlyPressedDirection: Null<Int> = leftPressed ? self.left : rightPressed ? self.right : null;
            if (lastDirectionPressed != null && !hasReleased && newlyPressedDirection == null)
                hasReleased = true;

            if (lastDirectionPressed != null && hasReleased) {
                timeSinceLastDirection += hf.Timer.tmod;
                if (timeSinceLastDirection > ALLOWED_DURATION) {
                    resetKeysState();
                    return;
                }
            }

            if (newlyPressedDirection != null && lastDirectionPressed != newlyPressedDirection) {
                resetKeysState();
                lastDirectionPressed = newlyPressedDirection;
                return;
            }

            if (lastDirectionPressed != null && lastDirectionPressed == newlyPressedDirection && hasReleased) {
                isDashing = true;
                isDashingLeft = lastDirectionPressed == self.left;
                dashingRemainingDuration = DASH_DURATION;
                self.player.fl_gravity = false;
                self.player.dy = 0;
                resetKeysState();
            }
        });

        preventDeathDuringDash = Ref.auto(Player.killHit).prefix(function(hf: Hf, self: Player, dx: Null<Float>): Nil<Void> {
            if (!self.specialMan.actives[id] || !isDashing)
                return Nil.none();
            return Nil.some(null);
        });

        preventStunDuringDash = Ref.auto(Player.knock).prefix(function(hf: Hf, self: Player, d: Float): Nil<Void> {
            if (!self.specialMan.actives[id] || !isDashing)
                return Nil.none();
            return Nil.some(null);
        });

        killBadsDuringDash = Ref.auto(Player.hit).after(function(hf: Hf, self: Player, e: Entity): Void {
            if (!self.specialMan.actives[id] || !isDashing)
                return;

            if ((e.types & hf.Data.BAD) > 0)
                (cast e).killHit(null);
        });
    }

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        specMan.temporary(item.id, null);
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
        specMan.clearRec();
        specMan.actives[id] = false;
        stopDashing(specMan.player);
    }
}
