package faille.items;

import hf.entity.Item;
import hf.mode.GameMode;
import hf.Hf;
import hf.SpecialManager;
import etwin.flash.MovieClip;
import etwin.Obfu;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Eau implements ISpec {
  public var id(default, null): Int = 160;
  public var effect(default, null): MovieClip;
  public var timer(default, null): Int = 0;
    
  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(GameMode.main).wrap(function(hf: Hf, self: GameMode, old): Void {
        if(timer <= 0) {
          this.effect.removeMovieClip();
        } else {
          timer--;
        }
        old(self);
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    Merlin.setGlobalVar(specMan.game, Obfu.raw("DEATH"), new MerlinFloat(0));
    this.effect = specMan.game.depthMan.attach('shine', hf.Data.DP_SOUNDS);
    this.effect._x = 165;
    this.effect._y = 510;
    this.timer = 15;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}