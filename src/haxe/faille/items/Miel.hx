package faille.items;

import etwin.flash.MovieClip;
import hf.Pos;
import hf.entity.Item;
import hf.entity.Player;
import hf.entity.item.ScoreItem;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;

class Miel implements ISpec {
  public var id(default, null): Int = 135;
  public var isActive(default, null): Bool = false;

  public function new() {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.permanent(item.id);

    for (i in 0...10) {
      var randomCandy = hf.Std.random(3);
      randomCandy += 1003;
      var newItem = hf.entity.item.ScoreItem.attach(specMan.game, item.x, item.y, randomCandy, null);
      newItem.moveFrom(item, 10);
    }

    if(!this.isActive) {
      this.isActive = true;
      hf.Data.ITEM_VALUES[3 + 1000] *= 5;
      hf.Data.ITEM_VALUES[4 + 1000] *= 5;
      hf.Data.ITEM_VALUES[5 + 1000] *= 5;
      hf.Data.ITEM_VALUES[23 + 1000] *= 5;
      hf.Data.ITEM_VALUES[63 + 1000] *= 5;
      hf.Data.ITEM_VALUES[64 + 1000] *= 5;
      hf.Data.ITEM_VALUES[65 + 1000] *= 5;
      hf.Data.ITEM_VALUES[66 + 1000] *= 5;
      hf.Data.ITEM_VALUES[67 + 1000] *= 5;
      hf.Data.ITEM_VALUES[68 + 1000] *= 5;
      hf.Data.ITEM_VALUES[160 + 1000] *= 5;
      hf.Data.ITEM_VALUES[253 + 1000] *= 5;
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    this.isActive = false;
    hf.Data.ITEM_VALUES[3 + 1000] /= 5;
    hf.Data.ITEM_VALUES[4 + 1000] /= 5;
    hf.Data.ITEM_VALUES[5 + 1000] /= 5;
    hf.Data.ITEM_VALUES[23 + 1000] /= 5;
    hf.Data.ITEM_VALUES[63 + 1000] /= 5;
    hf.Data.ITEM_VALUES[64 + 1000] /= 5;
    hf.Data.ITEM_VALUES[65 + 1000] /= 5;
    hf.Data.ITEM_VALUES[66 + 1000] /= 5;
    hf.Data.ITEM_VALUES[67 + 1000] /= 5;
    hf.Data.ITEM_VALUES[68 + 1000] /= 5;
    hf.Data.ITEM_VALUES[160 + 1000] /= 5;
    hf.Data.ITEM_VALUES[253 + 1000] /= 5;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
