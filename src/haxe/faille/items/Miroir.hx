package faille.items;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Miroir implements ISpec {
  public var id(default, null): Int = 167;
  private var levelJump(default, null): LevelJump;
  public var isActive(default, null): Bool = false;
  public var obj(default, null): DynamicMovieClip;
  public var text(default, null): DynamicMovieClip;
  public var lid(default, null): Int;
    
  public static var VORTEX_COLOR_FILTER: ColorMatrix = ColorMatrix.fromHsv(60, 1, 1);

  @:diExport
  private var patch(default, null): IPatch;

  public function new(levelJump: LevelJump): Void {
    this.levelJump = levelJump;
      
    this.patch = new PatchList([

      Ref.auto(GameMode.main).after(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if (this.isActive) {
            if (self.currentDim != 1) {
              var lid = self.world.currentId;
              if(this.lid != lid) {
                  
                this.lid = lid;
                var nextPortalLevelText2: String = "";
                var nextPortalLevelText3: String = "";
                var nextPortalLevel3 = null;
                var nextPortalLevel = this.levelJump.getNextJumpPortal(lid);
                var nextPortalLevelText: String = cast nextPortalLevel; 
                var nextPortalLevel2 = this.levelJump.getNextJumpPortal(nextPortalLevel + 1);
                if(nextPortalLevel2 != null) {
                  nextPortalLevelText2 += cast nextPortalLevel2;
                  nextPortalLevel3 = this.levelJump.getNextJumpPortal(nextPortalLevel2 + 1);
                  if(nextPortalLevel3 != null) {
                    nextPortalLevelText3 += cast nextPortalLevel3;
                  }
                }
                
	            if(nextPortalLevel < 100) nextPortalLevelText = "0" + cast nextPortalLevel;
	            if(nextPortalLevel2 < 100) nextPortalLevelText2 = "0" + cast nextPortalLevel2;
	            if(nextPortalLevel3 < 100) nextPortalLevelText3 = "0" + cast nextPortalLevel3;
                  
                if(nextPortalLevel2 != null) nextPortalLevelText2 = " - " + nextPortalLevelText2;
                if(nextPortalLevel3 != null) nextPortalLevelText3 = " - " + nextPortalLevelText3;
              
                if(nextPortalLevel != null) {
                  var x = 10;
                  var y = 20;
                  var o: MovieClip = self.depthMan.attach("hammer_portal", hf.Data.DP_INTERF);
                  this.obj = cast o;
                  obj._x = x;
                  obj._xscale = 30;
                  obj._y = y;
                  obj._yscale = 30;
                  obj.filters = [VORTEX_COLOR_FILTER.toFilter()];
                  self.fxMan.mcList.push(obj);
              
                  var t: MovieClip = cast self.depthMan.attach('hammer_interf_inGameMsg', hf.Data.DP_INTERF);
                  this.text = cast t;
                  text.field._width = 400;
                  text.label.text = "";
                  text.field.text = nextPortalLevelText + nextPortalLevelText2 + nextPortalLevelText3;
                  text.field._xscale = 90;
                  text.field._yscale = 90;
                  text.field._x = x - 3;
                  text.field._y = y - 9;
                  hf.FxManager.addGlow(text, 0, 2);
                  self.fxMan.mcList.push(text);
                }
              }
            }
          }
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.permanent(item.id);
    this.isActive = true;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    this.isActive = false;
    this.obj.removeMovieClip();
    this.text.removeMovieClip();
  }
}