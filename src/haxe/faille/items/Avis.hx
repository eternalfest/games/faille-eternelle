package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Bad;
import hf.entity.Item;
import hf.entity.bad.walker.Litchi;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import etwin.flash.filters.GlowFilter;
import etwin.flash.filters.BitmapFilter;
import hf.Entity;
import hf.entity.Bad;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Avis implements ISpec {
  public var id(default, null): Int = 127;
  public var fruitChosen(default, null): Entity = null;
  public var filter(default, null): etwin.flash.filters.GlowFilter = null;
  public var filterRecap(default, null): Array<etwin.flash.filters.BitmapFilter> = null;
  public static var CHOSEN_BADS(default, never): WeakMap<hf.entity.Bad, Int> = new WeakMap();

  private var chosenId: Int = 0;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(Bad.onKill).before(function(hf: Hf, self: Bad): Void {
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id]) {
            if(CHOSEN_BADS.get(self) == this.chosenId) {
              ++player.lives;
              self.game.gi.setLives(player.pid, player.lives);
            } else {
              this.filter.strength = 0;
              this.fruitChosen.filters = [this.filterRecap[0]];
              this.filter = null;
              this.filterRecap = null;
              this.fruitChosen = null;
              CHOSEN_BADS.remove(self);
              this.chosenId++;
            }
          }
        }
      }),

      // Gestion du litchi
      Ref.auto(Litchi.weaken).wrap(function(hf: Hf, self: Litchi, old): Void {
        old(self);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id]) {
            if(self == this.fruitChosen) {
              var listBad: Array<Entity> = self.game.getList(hf.Data.BAD_CLEAR);
              var fruit: hf.entity.Bad = cast listBad[listBad.length - 1];
              this.filterRecap = fruit.filters;
              this.fruitChosen = fruit;
              CHOSEN_BADS.set(fruit, this.chosenId);
              this.filter = new etwin.flash.filters.GlowFilter();
              this.filter.color = 16776757;
              this.filter.alpha = 0.5;
              this.filter.strength = 100;
              this.filter.blurX = 3;
              this.filter.blurY = 3;
              if(fruit.filters == null)
                fruit.filters = [this.filter];
              else {
                var f = fruit.filters;
                f.push(this.filter);
                fruit.filters = f;
              }
            }
          }
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    var randomFruit = specMan.game.getOne(hf.Data.BAD_CLEAR);
    this.fruitChosen = randomFruit;
    var fruit: hf.entity.Bad = cast randomFruit;
    CHOSEN_BADS.set(fruit, this.chosenId);
    this.filterRecap = randomFruit.filters;
    this.filter = new etwin.flash.filters.GlowFilter();
    this.filter.color = 16776757;
    this.filter.alpha = 0.5;
    this.filter.strength = 100;
    this.filter.blurX = 3;
    this.filter.blurY = 3;
    if(randomFruit != null){
      if(randomFruit.filters == null)
        randomFruit.filters = [this.filter];
      else {
        var f = randomFruit.filters;
        f.push(this.filter);
        randomFruit.filters = f;
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void { 
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.filter.strength = 0;
    this.fruitChosen.filters = [this.filterRecap[0]];
    this.filter = null;
    this.filterRecap = null;
    this.fruitChosen = null;
    this.chosenId++;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}