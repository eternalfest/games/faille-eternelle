package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Baguette implements ISpec {
  public var id(default, null): Int = 142;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {

    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      var randomScore = specMan.game.randMan.draw(hf.Data.RAND_SCORES_ID);
      hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y, randomScore, null);
      fruit.destroy();
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
