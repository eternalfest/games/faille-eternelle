package faille.items;

import hf.entity.Item;
import hf.entity.Player;
import hf.mode.GameMode;
import hf.Hf;
import hf.SpecialManager;
import etwin.Obfu;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Speed implements ISpec {
  public var id(default, null): Int = 154;
  public var initSpeed(default, null): Float;
  public var player(default, null): Player;
  public var isSet(default, null): Bool = false;
    
  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(GameMode.main).wrap(function(hf: Hf, self: GameMode, old): Void {
        if(this.isSet && this.player.speedFactor <= 5 && !self.fl_pause) {
          this.player.speedFactor += 0.0017;
        }
        old(self);
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    this.initSpeed = specMan.player.speedFactor;
    specMan.player.speedFactor = 0.05;
    this.player = specMan.player;
    this.isSet = true;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.player.speedFactor = this.initSpeed;
    this.player = null;
    this.initSpeed = null;
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.isSet = false;
  }
}