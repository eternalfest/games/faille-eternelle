package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;

class Sablier implements ISpec {
  public var id(default, null): Int = 118;

  public function new(): Void {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    specMan.game.huTimer -= 1000;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
      specMan.clearRec();
      specMan.actives[this.id] = false;
  }
}