package faille.items;

import etwin.flash.MovieClip;
import etwin.flash.filters.BitmapFilter;
import etwin.flash.filters.ColorMatrixFilter;
import hf.Entity;
import hf.entity.Item;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;

import color_matrix.ColorMatrix;
import vault.ISpec;

class RaveParty implements ISpec {
    public var id(default, null): Int = 153;

    private var hf: Hf;
    private var originalFilters: Map<Entity, Array<BitmapFilter>>;

    public function new() {}

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        this.hf = hf;
        originalFilters = new Map<Entity, Array<BitmapFilter>>();
        specMan.registerRecurring(function () {
            var entities: Array<Entity> = specMan.game.getList(hf.Data.ENTITY);
            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(Std.random(360), 1, 1).toFilter();
            for (entity in entities) {
                if (!originalFilters.exists(entity))
                    originalFilters.set(entity, entity.filters);
                entity.filters = [filter];
            }
            specMan.game.shake(hf.Data.SECOND / 8, 7);
        }, hf.Data.SECOND / 2, true);
        specMan.temporary(item.id, null);
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
        specMan.clearRec();
        specMan.actives[id] = false;
        for (entity in originalFilters.keys())
            entity.filters = originalFilters.get(entity);
    }
}
