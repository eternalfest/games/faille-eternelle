package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class TicketR implements ISpec {
  public var id(default, null): Int = 121;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var nbPoints: Int = hf.Std.random(50) + 1;
    nbPoints *= 1000;
    specMan.player.getScore(specMan.player, nbPoints);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
