package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Bouton implements ISpec {
  public var id(default, null): Int = 124;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var randomCoord = {x: hf.Std.random(hf.Data.LEVEL_WIDTH), y: hf.Std.random(hf.Data.LEVEL_HEIGHT)};
    randomCoord = specMan.game.world.getGround(randomCoord.x, randomCoord.y);
    var umbrella = hf.Std.random(100) + 1;
	  if(umbrella <= 65) {
	    umbrella = 11;
	  } else if(umbrella > 65 && umbrella <= 89){
	    umbrella = 12;
	  } else if(umbrella > 89){
	    umbrella = 108;
	  }
    hf.entity.item.SpecialItem.attach(specMan.game, randomCoord.x * hf.Data.CASE_WIDTH, randomCoord.y * hf.Data.CASE_HEIGHT, umbrella, 0);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
