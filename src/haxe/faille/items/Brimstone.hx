package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Item;
import hf.entity.Player;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Brimstone implements ISpec {
  public var id(default, null): Int = 144;
  public var isActive(default, null): Bool = false;
  public var cooldown(default, null): Float = 0;
  public var currentBomb: Int;
  public var hasChangedWeapon: Bool = false;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(Player.attack).wrap(function(hf: Hf, player: Player, old): Null<Entity> {
        if (player.specialMan.actives[this.id] && this.isActive && !player.game.fl_clear) {
          if(this.cooldown <= 0) {

          	var dirPlayer = player.dir;
          	var lightning = player.game.depthMan.attach('hammer_fx_death', hf.Data.DP_FX);
    		lightning._x = player.x + (dirPlayer * 250);
    		lightning._y = player.y - 15;
    		lightning._height = 500;
    		lightning._xscale = 150;
    		lightning._rotation = 90;
    		var filter: ColorMatrixFilter = ColorMatrix.fromHsv(180, 500, 1).toFilter();
          	lightning.filters = [filter];

            this.cooldown = hf.Data.SECOND * 1.7;

            for(e in player.game.getList(hf.Data.BAD_CLEAR)) {
              var fruit: hf.entity.Bad = cast e;
              if(dirPlayer > 0) {
  		        if (fruit.x > player.x && fruit.y < player.y + 20 && fruit.y > player.y - 60) {
  			      fruit.burn();
  			    }
              } else {
    		    if (fruit.x < player.x && fruit.y < player.y + 20 && fruit.y > player.y - 60) {
                  fruit.burn();
                }
              }
            }
         
          }
          return null;
        }
        return old(player);
      }),

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          for(player in self.getPlayerList()) {
            if (player.specialMan.actives[this.id]) this.updateTimers();
          }
        }
        if(self.fl_clear) {
          for(player in self.getPlayerList()) {
            if (player.specialMan.actives[this.id]) {
              player.unstick();
              if(!this.hasChangedWeapon) player.currentWeapon = this.currentBomb;
            }
          }
        }
      }),

      Ref.auto(Player.changeWeapon).before(function(hf: Hf, player: Player, id: Int): Void {
        if (player.specialMan.actives[this.id] && this.isActive && !player.game.fl_clear) {
          this.hasChangedWeapon = true;
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    this.isActive = true;
    specMan.player.curse(15);
    specMan.temporary(item.id, hf.Data.WEAPON_DURATION);
    this.currentBomb = specMan.player.currentWeapon;
    if(this.currentBomb == -1) specMan.player.currentWeapon = 1;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  	this.isActive = false;
    specMan.actives[this.id] = false;
    specMan.clearRec();
    specMan.player.unstick();
    if(!this.hasChangedWeapon) specMan.player.currentWeapon = this.currentBomb;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function updateTimers(): Void {
    if(this.cooldown > 0) {
      this.cooldown--;
    }
  }
}