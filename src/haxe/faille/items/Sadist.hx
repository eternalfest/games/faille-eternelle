package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.Pos;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Sadist implements ISpec {
  public var id(default, null): Int = 125;
  private var entities: Array<{ e: hf.entity.bad.Spear, t: Float }> = new Array();

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          for(player in self.getPlayerList()) {
            if (player.specialMan.actives[this.id] && !self.fl_clear) this.updateTimers();
          }

          if(self.fl_clear) {
            for(entry in this.entities) {
              entry.e.destroy();
              this.entities.remove(entry);
            }
          }
        }
      }),   
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    for(pic in 0...5) {
      var randomCoord = {x: hf.Std.random(hf.Data.LEVEL_WIDTH), y: hf.Std.random(hf.Data.LEVEL_HEIGHT)};
      randomCoord = this.getGroundSpike(randomCoord.x, randomCoord.y, specMan, hf);
      while(randomCoord.x/20 == item.x && randomCoord.y/20 == item.y) {
        randomCoord = {x: hf.Std.random(hf.Data.LEVEL_WIDTH), y: hf.Std.random(hf.Data.LEVEL_HEIGHT)};
        randomCoord = this.getGroundSpike(randomCoord.x, randomCoord.y, specMan, hf);
      }
      var e = hf.entity.bad.Spear.attach(specMan.game, randomCoord.x * 20, randomCoord.y * 20);
      this.entities.push({ e: e, t: 500 });
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    for(entry in this.entities) {
      entry.t = 0;
    }
    updateTimers();
    this.entities = new Array();
  }

  public function getGroundSpike(cx: Int, cy: Int, specMan: SpecialManager, hf: Hf): Pos<Int> {
    var count = 0, 
        height = cy;

    while (count <= hf.Data.LEVEL_HEIGHT) {
      if (height > 0 && specMan.game.world.getCase({x: cx, y: height}) == hf.Data.GROUND) {
        return {x: cx, y: height - 1};
      }
      if (height >= hf.Data.LEVEL_HEIGHT) {
        height = 0;
        cy = 24;
      }
      ++count; ++height;
    }
    return {x: cx, y: cy};
  }

  public function updateTimers(): Void {
    for(entry in this.entities) {
      if (entry.e == null) continue;
      entry.t--;
      if(entry.t <= 0) {
        entry.e.destroy();
        entry.e = null;
      }
    }
  }

}
