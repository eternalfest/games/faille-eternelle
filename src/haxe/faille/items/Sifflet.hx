package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.Entity;
import hf.entity.Bomb;
import hf.entity.bomb.player.Classic;
import hf.entity.bomb.player.Blue;
import hf.entity.bomb.player.Red;
import hf.entity.bomb.player.Black;
import hf.entity.bomb.player.Green;
import hf.entity.bomb.bad.PoireBomb;
import hf.entity.bomb.bad.Mine;
import hf.entity.Bad;
import hf.entity.bad.walker.Bombe;
import hf.entity.bad.Spear;
import faille.bads.MechaBombi;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Sifflet implements ISpec {
  public var id(default, null): Int = 126;
  public var isRed: Bool = false;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(Classic.onExplode).before(function(hf: Hf, self: Classic): Void {
        this.explosion(hf, self, 0.8);
      }),

      Ref.auto(Red.onExplode).before(function(hf: Hf, self: Red): Void {
        this.isRed = true;
        this.explosion(hf, self, 0.35);
      }),

      Ref.auto(Blue.onExplode).before(function(hf: Hf, self: Blue): Void {
        this.explosion(hf, self, 0.05);
      }),

      Ref.auto(Black.onExplode).before(function(hf: Hf, self: Black): Void {
        this.explosion(hf, self, 0.9);
      }),

      Ref.auto(Green.onExplode).before(function(hf: Hf, self: Green): Void {
        this.explosion(hf, self, 0.5);
      }),

      Ref.auto(PoireBomb.onExplode).before(function(hf: Hf, self: PoireBomb): Void {
        this.explosion(hf, self, 0.9);
      }),

      Ref.auto(Mine.onExplode).before(function(hf: Hf, self: Mine): Void {
        this.explosion(hf, self, 0.5);
      }),

      Ref.auto(Bombe.selfDestruct).before(function(hf: Hf, self: Bombe): Void {
        var ballons = self.game.getClose(hf.Data.SOCCERBALL, self.x, self.y, hf.entity.bad.walker.Bombe.RADIUS, false);
        for(i in 0...ballons.length){
          ballons[i].destroy();
          self.game.fxMan.attachFx(ballons[i].x, ballons[i].y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
        }
      }),

      Ref.auto(Bad.hit).before(function(hf: Hf, self: Bad, e: Entity): Void {
        if ((e.types & hf.Data.SOCCERBALL) > 0) {
          var ballons = self.game.getList(hf.Data.SOCCERBALL);

          if (Std.is(e, hf.entity.bad.walker.Bombe)) {
            var bombinos: hf.entity.bad.walker.Bombe = cast e;
            for(i in 0...ballons.length) {
              var ballon = ballons[i];
              var ball: hf.entity.bomb.player.SoccerBall = cast ballon;
              if(ball.dx > 0 || ball.dy > 0) bombinos.trigger();
            }

          } else {
            if(self.isHealthy()){
              for(i in 0...ballons.length) {
                var ballon = ballons[i];
                var ball: hf.entity.bomb.player.SoccerBall = cast ballon;
                if(ball.dx > 0 || ball.dy > 0) self.forceKill(null);
              }
            }
          }
        }
      }),

      Ref.auto(Bad.hit).before(function(hf: Hf, self: Bad, e: Entity): Void {
        if ((e.types & hf.Data.SOCCERBALL) > 0) {
          var ballons = self.game.getList(hf.Data.SOCCERBALL);
          if (Std.is(self, hf.entity.bad.Spear)) {
            var spike: hf.entity.bad.Spear = cast self;
            for(i in 0...ballons.length) {
              if(ballons[i].distance(self.x, self.y) < 15) {
                ballons[i].destroy();
                self.game.fxMan.attachFx(e.x, e.y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
              }
            }
          }
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    hf.entity.bomb.player.SoccerBall.attach(specMan.game, item.x, item.y);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    var ballons = specMan.game.getList(hf.Data.SOCCERBALL);
    for(i in 0...ballons.length) {
      ballons[i].destroy();
      specMan.game.fxMan.attachFx(ballons[i].x, ballons[i].y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
    }
    specMan.clearRec();
    specMan.actives[this.id] = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function explosion(hf:Hf, self: Bomb, coefficient: Float): Void {
    var ballons = self.game.getList(hf.Data.SOCCERBALL);

    for(i in 0...ballons.length) {
      var ballon = ballons[i];
      var ball: hf.entity.bomb.player.SoccerBall = cast ballon;
      var d = self.distance(ball.x, ball.y);

      if (d <= self.radius) {
        var coeff = (self.radius - d) / self.radius;
        var angle = Math.atan2(ball.y - self.y, ball.x - self.x);

        if(!this.isRed){
          ball.dx = Math.cos(angle) * self.power * coeff * 1.5 * coefficient;
          ball.dy = Math.sin(angle) * self.power * coeff * 1.5 * coefficient;

          if (ball.dy < 4 && ball.dy > -8) {
            ball.dy = -8;
          }

        } else {
          ball.dy = -self.power * coeff;

          if (!ball.fl_stable) {
            ball.dy *= 1.5;
          }

          ball.dx = Math.cos(angle) * self.power * coeff * coefficient;
        }

        ball.burn();

        if (ball.fl_stable) {
          ball.dx *= 2;

        } else {
          ball.dx *= 1.5;

        }
      }
    }
  }

}