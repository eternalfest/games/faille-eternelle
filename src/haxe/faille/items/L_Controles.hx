package faille.items;

import hf.entity.Item;
import hf.entity.Player;
import hf.entity.PlayerController;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class L_Controles implements ISpec {
  public var id(default, null): Int = 166;
  public var isActive(default, null): Bool = false;
  public var player(default, null): Player;
  public var jump(default, null): Int;
  public var down(default, null): Int;
  public var left(default, null): Int;
  public var right(default, null): Int;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
    
      Ref.auto(PlayerController.getControls).before(function(hf: Hf, self: PlayerController): Void {
        if (this.isActive && this.player == self.player) {
          self.jump = this.down;
          self.down = this.jump;
          self.left = this.right;
          self.right = this.left;
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, hf.Data.SECOND * 20);
    this.isActive = true;
    this.player = specMan.player;
    this.jump = specMan.player.ctrl.jump;
    this.down = specMan.player.ctrl.down;
    this.left = specMan.player.ctrl.left;
    this.right = specMan.player.ctrl.right;
    specMan.player.curse(18);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    this.isActive = false;
    this.player = null;
    specMan.player.ctrl.jump = this.jump;
    specMan.player.ctrl.down = this.down;
    specMan.player.ctrl.left = this.left;
    specMan.player.ctrl.right = this.right;
    specMan.player.unstick();
  }
}