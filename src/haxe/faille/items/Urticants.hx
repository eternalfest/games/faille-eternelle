package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Urticants implements ISpec {
  public var id(default, null): Int = 123;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      fruit.anger > 0 ? fruit.calmDown() : fruit.angerMore();
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}