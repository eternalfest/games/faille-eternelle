package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Hasard implements ISpec {
  public var id(default, null): Int = 169;
  private var itemsAllowed(default, null): Array<Int> = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 64, 65, 66, 67, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 106, 107, 108, 112, 114, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168];

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var randomItem = Std.random(itemsAllowed.length);
    item.id = itemsAllowed[randomItem];
    specMan.execute(cast item);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}