package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.entity.bomb.player.Classic;
import hf.entity.bomb.player.Black;
import vault.ISpec;

class Poudre implements ISpec {
  public var id(default, null): Int = 140;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.registerRecurring(function() {
      var randomBomb = hf.Std.random(10);
      var randomCoord = {x: hf.Std.random(hf.Data.LEVEL_WIDTH), y: hf.Std.random(hf.Data.LEVEL_HEIGHT)};
      randomCoord; /* Fix compiler bug. */
      randomBomb == 9 ?
        hf.entity.bomb.player.Black.attach(specMan.game, randomCoord.x * 20, randomCoord.y * 20) :
        hf.entity.bomb.player.Classic.attach(specMan.game, randomCoord.x * 20, randomCoord.y * 20);
    }, hf.Data.SECOND/8, true);
    specMan.temporary(item.id, hf.Data.SECOND * 7.5);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
