package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Sceptre implements ISpec {
  public var id(default, null): Int = 156;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var random = hf.Std.random(100) + 1;
      var fruit: hf.entity.Bad = cast e;
      if(random > 30)
        var life = hf.entity.item.SpecialItem.attach(specMan.game, fruit.x, fruit.y, 146, null);
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}