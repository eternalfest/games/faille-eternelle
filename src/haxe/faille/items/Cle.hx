package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Cle implements ISpec {
  public var id(default, null): Int = 129;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var dirPlayer = specMan.player.dir; //-1 gauche et 1 droite

    for(i in 1...4) {
      var letterPos = {x: dirPlayer*i*20 + item.x, y: item.y};
      letterPos; /* Fix compiler bug. */
      if(letterPos.x > 0 && letterPos.x < 410) {
        var letter = hf.entity.item.SpecialItem.attach(specMan.game, letterPos.x, letterPos.y, 0, Std.random(7));
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
