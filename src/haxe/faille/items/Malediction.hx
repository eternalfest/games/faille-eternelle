package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.Pos;
import vault.ISpec;

class Malediction implements ISpec {
  public var id(default, null): Int = 158;
  private var entities: Array<hf.entity.bad.ww.Saw> = new Array();

  public function new(): Void {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, hf.Data.SECOND * 25);
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      if(fruit.fl_stable) {
        var saw = hf.entity.bad.ww.Saw.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT);
        this.entities.push(saw);    
      }
      fruit.destroy();
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    for(entry in this.entities) {
      specMan.game.fxMan.attachFx(entry.x, entry.y, "hammer_fx_pop");
      entry.destroy();
    }
    this.entities = new Array();
  }
}
