package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Item;
import hf.entity.Bad;
import hf.entity.bad.walker.Ananas;
import hf.entity.bad.Shooter;
import hf.entity.bad.ww.Crawler;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Masque implements ISpec {
  public var id(default, null): Int = 134;
  public var isActive(default, null): Bool = false;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(Ananas.startAttack).wrap(function(hf: Hf, self: Ananas, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Shooter.startShoot).wrap(function(hf: Hf, self: Shooter, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(Crawler.prepareAttack).wrap(function(hf: Hf, self: Crawler, old): Void {
        if (!this.isActive) {
          old(self);
        }
      }),

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if(self.fl_clear) {
            self.fxMan.clearBg();
          }
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    this.isActive = true;
    specMan.temporary(item.id, null);
    specMan.game.fxMan.attachBg(14, null, 9999);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    this.isActive = false;
    specMan.actives[this.id] = false;
    specMan.game.fxMan.clearBg();
    specMan.clearRec();
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}