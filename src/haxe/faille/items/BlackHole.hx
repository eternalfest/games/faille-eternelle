package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Bad;
import hf.entity.Item;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;

import color_matrix.ColorMatrix;
import vault.ISpec;

import faille.pic.PicCustom;

class BlackHole {
    private static var SCALE(default, never): Float = 550;
    private static var RADIUS(default, never): Float = 30 * (SCALE / 100);
    private static var TOTAL_LIFETIME(default, never): Float = 120;

    private var hf: Hf;
    private var game: GameMode;
    private var sprite: MovieClip;
    private var x: Float;
    private var y: Float;
    private var enemies: Array<Bad>;
    private var lifetime: Float = TOTAL_LIFETIME;
    private var stepDistance: Float;
    private var stepAngle: Float;
    private var stepScale: Float;

    public function new(hf: Hf, game: GameMode, x: Float, y: Float): Void {
        this.hf = hf;
        this.game = game;
        this.x = x;
        this.y = y;
        sprite = cast game.depthMan.attach("hammer_portal", hf.Data.DP_BACK_LAYER);
        sprite._xscale = SCALE;
        sprite._yscale = SCALE;
        sprite.filters = [ColorMatrix.fromHsv(240, 0.55, 0.48).toFilter()];

        enemies = cast game.getClose(hf.Data.BAD, x, y, RADIUS, false);
        /* Don't affect spikes. */
        var nbEnemies: Int = enemies.length;
        var i: Int = 0;
        while (i < nbEnemies) {
            if (Std.is(enemies[i], hf.entity.bad.Spear) || Std.is(enemies[i], PicCustom)) {
                enemies.splice(i, 1);
                --nbEnemies;
            }
            else {
                ++i;
            }
        }

        var furthestSquaredDistance: Float = 0;
        for (enemy in enemies) {
            /* Steal control of those entities from the game. */
            for (i in 0...32) {
                if ((enemy.types & 1 << i) > 0)
                    game.unregList.push({type: Math.round(Math.pow(2, i)), ent: enemy});
            }
            /* Remove from the world triggers (which calls hit for player kills etc...). */
            enemy.tRem(enemy.cx, enemy.cy);

            var diffX: Float = enemy.x - x;
            var diffY: Float = enemy.y - y;
            var squaredDistance: Float = diffX * diffX + diffY * diffY;
            furthestSquaredDistance = Math.max(furthestSquaredDistance, squaredDistance);
        }
        stepDistance = Math.sqrt(furthestSquaredDistance) / TOTAL_LIFETIME;
        stepAngle = 2 * Math.PI / TOTAL_LIFETIME;
        stepScale = 50 / TOTAL_LIFETIME;

        game.addToList(hf.Data.ENTITY, cast this);
        /* For destroy on level end. Though that means it's also destroyed on "cristal" but that's acceptable. */
        game.addToList(hf.Data.SHOOT, cast this);
    }

    @:keep
    public function update(): Void {
        for (enemy in enemies) {
            /* If we're unlucky, the enemy added back an hitbox during the frame it had between our "unregList.push" request and the moment it was actually removed from the game lists.
             * So somewhat give up and remove at each frame. */
            enemy.tRem(enemy.cx, enemy.cy);

            var angle: Float = Math.atan2(enemy.y - y, enemy.x - x);
            var diffX: Float = enemy.x - x;
            var diffY: Float = enemy.y - y;
            var distance: Float = Math.sqrt(diffX * diffX + diffY * diffY);

            var newAngle: Float = angle + stepAngle;
            var newDistance: Float = distance - stepDistance;

            /* Cannot use moveTo, it will re-add it to the hitbox triggers. */
            enemy.x = x + newDistance * Math.cos(newAngle);
            enemy.y = y + newDistance * Math.sin(newAngle);
            var xscaleFactor: Float = enemy._xscale > 0 ? 1 : -1;
            enemy._xscale = xscaleFactor * Math.max(0, Math.abs(enemy._xscale) - stepScale);
            enemy._yscale = Math.max(0, enemy._yscale - stepScale);
            enemy.endUpdate();
        }

        lifetime -= hf.Timer.tmod;
        if (lifetime <= 0) {
            for (enemy in enemies) {
                enemy.dropReward();
                enemy.destroy();
            }
            enemies = [];
            destroy();
        }
    }

    @:keep
    public function endUpdate(): Void {
        sprite._x = x;
        sprite._y = y;
    }

    @:keep
    public function destroy(): Void {
        /* If we're called from the player leaving the level. */
        for (enemy in enemies)
            enemy.destroy();
        enemies = [];
        sprite.removeMovieClip();
        game.removeFromList(hf.Data.ENTITY, cast this);
        game.removeFromList(hf.Data.SHOOT, cast this);
    }
}

class BlackHoleItem implements ISpec {
    public var id(default, null): Int = 165;

    public function new() {}

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        new BlackHole(hf, specMan.game, 200, 250);
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    }
}
