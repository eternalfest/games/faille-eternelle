package faille.items;

import hf.entity.Item;
import hf.entity.Player;
import hf.entity.Bad;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Oeil implements ISpec {
  public var id(default, null): Int = 162;
  public var isActive(default, null): Bool = false;
  public var player(default, null): Player;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
    
      Ref.auto(Bad.update).before(function(hf: Hf, self: Bad): Void {
        if (this.isActive) {
		  if(this.player.dir > 0 && 
             self.dx < 0 && 
             !self.fl_freeze && 
             !self.fl_knock &&
             (self.fl_stable || 
              !self.fl_stable &&
              Std.is(self, hf.entity.bad.flyer.Baleine))) {
  		    if (self.x > this.player.x - 120 && self.y < this.player.y + 15 && self.y > this.player.y - 15) {
  			  self.dx *= -1;
  			}
          } else if(this.player.dir < 0 && 
                    self.dx > 0 && 
                    !self.fl_freeze && 
                    !self.fl_knock &&
                    (self.fl_stable || 
                    !self.fl_stable &&
                    Std.is(self, hf.entity.bad.flyer.Baleine))) {
    		if (self.x < this.player.x + 120 && self.y < this.player.y + 15 && self.y > this.player.y - 15) {
              self.dx *= -1;
            }
          }
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, hf.Data.SECOND * 30);
    this.isActive = true;
    this.player = specMan.player;
    specMan.player.curse(19);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    this.isActive = false;
    this.player = null;
    specMan.player.unstick();
  }
}