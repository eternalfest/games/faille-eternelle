package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;
import custom_clips.CustomClips;

class Clonage implements ISpec {
  public var id(default, null): Int = 132;

  private var clips(default, null): CustomClips;

  public function new(clips: CustomClips) {
    this.clips = clips;
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var randomFruit = specMan.game.getOne(hf.Data.BAD_CLEAR);
    if(randomFruit != null){

      if (Std.is(randomFruit, hf.entity.bad.walker.Cerise)) {
        specMan.game.attachBad(hf.Data.BAD_CERISE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Citron)) {
        specMan.game.attachBad(hf.Data.BAD_CITRON, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Orange)) {
        specMan.game.attachBad(hf.Data.BAD_ORANGE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Pomme)) {
        specMan.game.attachBad(hf.Data.BAD_POMME, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Banane)) {
        specMan.game.attachBad(hf.Data.BAD_BANANE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.flyer.Baleine)) {
        specMan.game.attachBad(hf.Data.BAD_BALEINE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Abricot)) {
        var randomFruit: hf.entity.bad.walker.Abricot = cast randomFruit;
        randomFruit.fl_spawner ? 
          specMan.game.attachBad(hf.Data.BAD_ABRICOT, randomFruit.x, randomFruit.y - 20) :
          specMan.game.attachBad(hf.Data.BAD_ABRICOT2, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Ananas)) {
        specMan.game.attachBad(hf.Data.BAD_ANANAS, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Fraise)) {
        specMan.game.attachBad(hf.Data.BAD_FRAISE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Framboise)) {
        specMan.game.attachBad(hf.Data.BAD_FRAMBOISE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Kiwi)) {
        specMan.game.attachBad(hf.Data.BAD_KIWI, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Litchi)) {
        specMan.game.attachBad(hf.Data.BAD_LITCHI, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.LitchiWeak)) {
        specMan.game.attachBad(hf.Data.BAD_LITCHI_WEAK, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Poire)) {
        specMan.game.attachBad(hf.Data.BAD_POIRE, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.ww.Crawler)) {
        specMan.game.attachBad(hf.Data.BAD_CRAWLER, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, faille.bads.MechaBombi)) {
        specMan.game.attachBad(24, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, hf.entity.bad.walker.Bombe)) {
        specMan.game.attachBad(hf.Data.BAD_BOMBE, randomFruit.x, randomFruit.y - 20);
      } 
      else if (Std.is(randomFruit, faille.bads.Mirabelle)) {
        var randomFruit: faille.bads.Mirabelle = cast randomFruit;
        randomFruit.isBig ? 
          specMan.game.attachBad(22, randomFruit.x, randomFruit.y - 20) :
          faille.bads.Mirabelle.attach(this.clips, specMan.game, randomFruit.x, randomFruit.y - 20, false);
        
      } else if (Std.is(randomFruit, faille.bads.CitronVert)) {
        specMan.game.attachBad(21, randomFruit.x, randomFruit.y - 20);
      } else if (Std.is(randomFruit, faille.bads.R2)) {
        specMan.game.attachBad(23, randomFruit.x, randomFruit.y - 20);
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
