package faille.items;

import hf.entity.bomb.PlayerBomb;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import patchman.IPatch;
import patchman.Ref;

import vault.ISpec;

@:build(patchman.Build.di())
class BombingBomb implements ISpec {
    private static var SMALL_BOMB_SCALE_FACTOR(default, never): Float = 0.6;
    private static var NB_SMALL_BOMBS(default, never): Int = 3;

    public var id(default, null): Int = 155;

    @:diExport
    public var spawnSmallBombsOnExplosion(default, null): IPatch;

    public function new() {
        spawnSmallBombsOnExplosion = Ref.auto(PlayerBomb.onExplode).after(function(hf: Hf, self: PlayerBomb): Void {
            if (self.owner != null && self.owner.specialMan.actives[id]
                && self._xscale > 70 && self._yscale > 70) {
                for (i in 0...NB_SMALL_BOMBS) {
                    var newBomb: PlayerBomb = cast self.duplicate();
                    newBomb._xscale = self._xscale * SMALL_BOMB_SCALE_FACTOR;
                    newBomb._yscale = self._yscale * SMALL_BOMB_SCALE_FACTOR;
                    newBomb.radius = self.radius * SMALL_BOMB_SCALE_FACTOR;
                    newBomb.power = self.power * SMALL_BOMB_SCALE_FACTOR;
                    newBomb.setOwner(self.owner);
                    var xSpeed: Float = 2 + Math.random() * 5;
                    var ySpeed: Float = 2 + Math.random() * 15;
                    var angle: Float = Math.random() * Math.PI; /* In [0, Pi] to only have somewhat upward, no downward. */
                    newBomb.dx = xSpeed * Math.cos(angle);
                    newBomb.dy = -ySpeed * Math.sin(angle);
                }
            }
        });
    }

    public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
        specMan.permanent(item.id);
    }

    public function interrupt(hf: Hf, specMan: SpecialManager): Void {
        specMan.clearRec();
        specMan.actives[id] = false;
    }
}

