package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Item;
import hf.entity.Player;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Metronome implements ISpec {
  public var id(default, null): Int = 145;
  public var cooldown(default, null): Int = 32 * 4;
  public var isActive(default, null): Bool = false;
  public var state(default, null): Int = 3;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          for(player in self.getPlayerList()) {
            if (player.specialMan.actives[this.id] && this.isActive && !player.game.fl_clear) {
              this.updateTimers();

              if(this.cooldown <= 0) {
                switch(this.state) {
                  case 3:
                    var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                    curse.gotoAndStop("" + 10);
                    player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                    this.cooldown = hf.Data.SECOND;
                    this.state--;
                    break;
                  case 2:
                    var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                    curse.gotoAndStop("" + 11);
                    player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                    this.cooldown = hf.Data.SECOND;
                    this.state--;
                    break;
                  case 1:
                    var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                    curse.gotoAndStop("" + 12);
                    player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                    this.cooldown = hf.Data.SECOND;
                    this.state--;
                    break;
                  case 0:
                    var curse = self.depthMan.attach("curse", hf.Data.DP_FX);
                    curse.gotoAndStop("" + 13);
                    player.stick(curse, 0, -hf.Data.CASE_HEIGHT * 2.5);
                    this.cooldown = hf.Data.SECOND;
                    this.state--;
                    self.shake(hf.Data.SECOND * 0.5, 5);

                    var playerList = self.getPlayerList();
                    for (i in 0...playerList.length) {
                      var p = playerList[i];
                      if (p.fl_stable) {
                        if (p.fl_shield) {
                          p.dy = -8;
                        } else {
                          p.knock(hf.Data.PLAYER_KNOCK_DURATION);
                        }
                      }
                    }

                    var badList = self.getBadClearList();
                    for (i in 0...badList.length) {
                      var bad = badList[i];
                      if(bad.fl_stable) {
                        bad.knock(hf.Data.SECOND * 3);
                      }
                    }

                    break;
                  case -1:
                    player.unstick();
                    this.cooldown = hf.Data.SECOND * 4;
                    this.state = 3;
                }
              }
            }
          }
        }
      }),

      Ref.auto(Player.killHit).before(function(hf: Hf, self: Player, dx: Null<Float>): Void {
        if (self.specialMan.actives[this.id] && this.isActive) {
          self.unstick();
          this.isActive = false;
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    this.isActive = true;
    specMan.temporary(item.id, null);
    this.state = -1;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    this.isActive = false;
    specMan.actives[this.id] = false;
    specMan.clearRec();
    specMan.player.unstick();
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function updateTimers(): Void {
    if(this.cooldown > 0) {
      this.cooldown--;
    }
  }
}