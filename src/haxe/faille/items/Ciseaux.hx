package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Ciseaux implements ISpec {
  public var id(default, null): Int = 128;
  public var lightning(default, null): MovieClip = null;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        for(player in self.getPlayerList()) {
          if (player.specialMan.actives[this.id]  && !player.game.fl_clear) {
            var fruits = self.getList(hf.Data.BAD_CLEAR);
            for(entity in fruits) {
              var fruit: hf.entity.Bad = cast entity;
              var calculHauteur = (((this.lightning._rotation - 90) * 20) / 45);
              for(x in 0...hf.Data.LEVEL_WIDTH) {
                if(Math.abs(fruit.x - x*20) < 10 && Math.abs(fruit.y - (this.lightning._y + calculHauteur * (x + 1))) < 15) {
                  fruit.forceKill(null);
                }
              }
            }
          }
          if(player.game.fl_clear) {
            this.lightning.removeMovieClip();
          }
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, 600);

    var pointCoordLeft = {x: 0, y: 0};
    var randomRotation = 90;

    var mode = hf.Std.random(2);
    switch(mode) {
      case 0:
        pointCoordLeft = {x: 0, y: 50};
        randomRotation = 135;
      case 1:
        pointCoordLeft = {x: 0, y: 450};
        randomRotation = 45;
    }

    this.lightning = specMan.game.depthMan.attach('hammer_fx_death', hf.Data.DP_FX);
    this.lightning._x = pointCoordLeft.x;
    this.lightning._y = pointCoordLeft.y;
    this.lightning._height = 1500;
    this.lightning._xscale = 100;
    this.lightning._rotation = randomRotation;
    this.lightning.gotoAndStop(1);

    var lightningAnimation = specMan.game.depthMan.attach('hammer_fx_death', hf.Data.FX);
    lightningAnimation._x = pointCoordLeft.x;
    lightningAnimation._y = pointCoordLeft.y;
    lightningAnimation._height = 1500;
    lightningAnimation._xscale = 50;
    lightningAnimation._rotation = randomRotation;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.lightning.removeMovieClip();
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}