package faille.items;

import etwin.flash.MovieClip;
import etwin.Obfu;
import hf.Pos;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import faille.snow.Ice;

class Freeze implements ISpec {
  public var id(default, null): Int = 157;
  public var isActive(default, null): Bool = false;
  public var square_mc: MovieClip;
  public var clouds(default, null): MovieClip = null;
  private var ice(default, null): Ice;

  public function new(ice: Ice): Void {
    this.ice = ice;
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    if(!this.isActive) {
      this.isActive = true;
      hf.Data.FREEZE_DURATION *= 2;
      specMan.game.setDynamicVar(Obfu.raw("ICE"), 1);
      specMan.game.world.update();
      specMan.game.gi.update();
    }
      
    this.clouds = specMan.game.depthMan.attach("hammer_fx_clouds", hf.Data.DP_FX);
    this.clouds._y += 9;
    specMan.clouds.push(this.clouds);
	
    this.square_mc = specMan.game.depthMan.empty(10000);
    this.square_mc._alpha = 15;
    this.square_mc.beginFill(0x5BDAFF);
    this.square_mc.moveTo(-100, -100);
    this.square_mc.lineTo(-100, 620);
    this.square_mc.lineTo(520, 620);
    this.square_mc.lineTo(520, -100);
    this.square_mc.lineTo(-100, -100);
    this.square_mc.endFill();
	specMan.game.fxMan.mcList.push(this.square_mc);
	
	ice.enableIce(hf, specMan.game, 2.5, 1);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.isActive = false;
    hf.Data.FREEZE_DURATION /= 2;
    specMan.game.setDynamicVar(Obfu.raw("ICE"), null);
    specMan.game.gi.update();
    this.square_mc._alpha = 0;
    this.clouds.removeMovieClip();
    ice.disableIce(specMan.game);
  }

}
