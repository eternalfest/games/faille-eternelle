package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Player;
import hf.entity.Item;
import hf.entity.shoot.BossFireBall;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Flamme implements ISpec {
  public var id(default, null): Int = 133;
  public var isActive(default, null): Bool = false;
  public var fireballArray(default, null): Array<BossFireBall> = new Array();

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(BossFireBall.center).wrap(function(hf: Hf, self: BossFireBall, old): Void {
        var onePlayerHasActive = 0;
        var playerActif: Player = null;
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isActive) {
            onePlayerHasActive++;
            playerActif = player;
          }

          if (onePlayerHasActive > 0) {
            self.moveTo(playerActif.x + Math.cos(self.ang) * self.dist, playerActif.y + Math.sin(self.ang) * self.dist);
          } else {
            old(self);
          }
        }
      }),

      Ref.auto(BossFireBall.hit).wrap(function(hf: Hf, self: BossFireBall, e: Entity, old): Void {
        var onePlayerHasActive = 0;
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isActive) {
            onePlayerHasActive++;
          }
        }

        if (onePlayerHasActive > 0) {
          if ((e.types & hf.Data.BAD_CLEAR) > 0) {
            var fruit: hf.entity.Bad = cast e;
            fruit.burn();
          } else {
            return;
          }
        } else {
          old(self, e);
        }
      }),

      Ref.auto(Player.killHit).before(function(hf: Hf, self: Player, dx: Null<Float>): Void {
        if (self.specialMan.actives[this.id] && this.isActive) {
          for(fireball in this.fireballArray) {
            fireball.destroy();
          }
        }
      }),

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if(self.fl_clear) {
            for(fireball in this.fireballArray) {
              fireball.destroy();
            }
          }
        }
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    this.isActive = true;
    for(player in specMan.game.getPlayerList()) {
      if (player.specialMan.actives[this.id] && this.isActive) {
        this.fireballArray.push(this.spawnFireBall(hf, player, 0));
        this.fireballArray.push(this.spawnFireBall(hf, player, 120));
        this.fireballArray.push(this.spawnFireBall(hf, player, 240));
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.isActive = false;
    for(fireball in this.fireballArray) {
      fireball.destroy();
    }
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function spawnFireBall(hf: Hf, player: Player, ang: Float): BossFireBall {
    var fireball = hf.entity.shoot.BossFireBall.attach(player.game, player.x, player.y);
    fireball.dist = hf.Data.CASE_WIDTH * 0.2;
    fireball.maxDist = hf.Data.CASE_WIDTH * 2.7;
    fireball.distSpeed = 3;
    fireball.ang = ang * Math.PI / 180;
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(190, 1, 1).toFilter();
    fireball.filters = [filter];
    return fireball;
  }
}