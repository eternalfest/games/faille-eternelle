package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.Entity;
import hf.entity.Bomb;
import hf.entity.bomb.PlayerBomb;
import hf.entity.bomb.BadBomb;
import hf.entity.bomb.bad.Mine;
import hf.entity.bomb.player.Classic;
import hf.entity.bomb.player.Blue;
import hf.entity.bomb.player.Red;
import hf.entity.bomb.player.Black;
import hf.entity.bomb.player.Green;
import hf.entity.bad.walker.Bombe;
import hf.entity.Player;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;

@:build(patchman.Build.di())
class Dynamite implements ISpec {
  public var id(default, null): Int = 130;
  public var isSet(default, null): Bool = false;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(PlayerBomb.init).before(function(hf: Hf, self: PlayerBomb, g: GameMode): Void {
        for(player in g.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            self.radius *= 1.4;
          }
        }
      }),

      Ref.auto(BadBomb.onExplode).before(function(hf: Hf, self: BadBomb): Void {
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet) {
            self.radius *= 1.4;
          }
        }
      }),

      Ref.auto(Mine.onExplode).before(function(hf: Hf, self: Mine): Void {
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet) {
            self.radius *= 1.3;
          }
        }
      }),

      Ref.auto(Bombe.selfDestruct).before(function(hf: Hf, self: Bombe): Void {
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet || self.game.fl_bombExpert) {
             hf.entity.bad.walker.Bombe.RADIUS = hf.entity.bad.walker.Bombe.EXPERT_RADIUS;
          } else {
            hf.entity.bad.walker.Bombe.RADIUS = hf.Data.CASE_WIDTH * 5;
          }
        }
      }),

      Ref.auto(Player.drop).before(function(hf: Hf, self: Player, b: PlayerBomb): Void {
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
             b.upgradeBomb(player);
          }
        }
      }),
        
      Ref.auto(Classic.duplicate).replace(function(hf: Hf, self: Classic): Classic {
        var bomb = hf.entity.bomb.player.Classic.attach(self.game, self.x, self.y);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            bomb.upgradeBomb(player);
          }
        }
        return bomb;
      }),

      Ref.auto(Green.duplicate).replace(function(hf: Hf, self: Green): Green {
        var bomb = hf.entity.bomb.player.Green.attach(self.game, self.x, self.y);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            bomb.upgradeBomb(player);
          }
        }
        return bomb;
      }),
        
      Ref.auto(Blue.duplicate).replace(function(hf: Hf, self: Blue): Blue {
        var bomb = hf.entity.bomb.player.Blue.attach(self.game, self.x, self.y);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            bomb.upgradeBomb(player);
          }
        }
        return bomb;
      }),
        
      Ref.auto(Red.duplicate).replace(function(hf: Hf, self: Red): Red {
        var bomb = hf.entity.bomb.player.Red.attach(self.game, self.x, self.y);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            bomb.upgradeBomb(player);
          }
        }
        return bomb;
      }),
        
      Ref.auto(Black.duplicate).replace(function(hf: Hf, self: Black): Black {
        var bomb = hf.entity.bomb.player.Black.attach(self.game, self.x, self.y);
        for(player in self.game.getPlayerList()) {
          if (player.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
            bomb.upgradeBomb(player);
          }
        }
        return bomb;
      }),
        
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    this.isSet = true;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.isSet = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}  