package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.entity.bomb.PlayerBomb;
import hf.entity.Player;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Bomberman implements ISpec {
  public var id(default, null): Int = 150;
  public var isSet(default, null): Bool = false;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(PlayerBomb.onExplode).before(function(hf: Hf, self: PlayerBomb): Void {
          
        if (self.owner.specialMan.actives[this.id] && this.isSet && !self.game.fl_clear) {
          for(e in self.owner.game.getList(hf.Data.BAD_CLEAR)) {
            var fruit: hf.entity.Bad = cast e;
            // kill all fruits in a cross hitbox
            if (fruit.x > self.x - 80 && fruit.x < self.x + 80 && fruit.y > self.y - 15 && fruit.y < self.y + 15) {
              fruit.forceKill(null);
            } else if (fruit.y > self.y - 80 && fruit.y < self.y + 80 && fruit.x > self.x - 15 && fruit.x < self.x + 15) {
              fruit.forceKill(null); 
            }
          }

          var lightningH = self.game.depthMan.attach('hammer_fx_death', hf.Data.DP_FX);
    	  lightningH._x = self.x;
    	  lightningH._y = self.y - 15;
    	  lightningH._height = 160;
    	  lightningH._xscale = 40;
    	  lightningH._rotation = 90;
          var lightningV = self.game.depthMan.attach('hammer_fx_death', hf.Data.DP_FX);
    	  lightningV._x = self.x;
    	  lightningV._y = self.y - 15;
    	  lightningV._height = 160;
          lightningV._xscale = 40;
        }
      }),
        
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        for(player in self.getPlayerList()) {
          if (self.fl_clear && self.cycle > 10) {
            if (player.specialMan.actives[this.id]) {
              player.unstick();
            }
          }
        }
      }),
    
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    this.isSet = true;
    specMan.player.curse(17);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    specMan.player.unstick();
    this.isSet = false;
  }
}  