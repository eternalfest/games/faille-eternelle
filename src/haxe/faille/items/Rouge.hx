package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.entity.Bad;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Rouge implements ISpec {
  public var id(default, null): Int = 136;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      
      if (Std.is(fruit, hf.entity.bad.walker.Cerise)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.Pomme)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.Fraise)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.Framboise)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.Litchi)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      } else if (Std.is(fruit, hf.entity.bad.walker.LitchiWeak)) {
        fruit.destroy();
        hf.entity.item.ScoreItem.attach(specMan.game, fruit.x, fruit.y - hf.Data.CASE_HEIGHT, 0, 0);
        var fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveLeft(fireBall.shootSpeed);
        fireBall = hf.entity.shoot.PlayerFireBall.attach(specMan.game, fruit.x, fruit.y);
        fireBall.moveRight(fireBall.shootSpeed);
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
