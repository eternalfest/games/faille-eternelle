package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Feuille implements ISpec {

  public var id(default, null): Int = 122;
  public var leafPoints = 100;
  public var leafActive = true;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.registerRecurring(function() {
      if(leafActive) specMan.player.getScore(specMan.player, leafPoints);
      leafPoints < 1000 ? leafPoints += 5 : leafActive = false;
    }, hf.Data.SECOND/1.4, true);
    specMan.temporary(item.id, null);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}