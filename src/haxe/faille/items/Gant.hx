package faille.items;

import etwin.flash.MovieClip;
import hf.Entity;
import hf.entity.Player;
import hf.entity.Item;
import hf.entity.shoot.Ball;
import hf.Hf;
import hf.mode.GameMode;
import hf.SpecialManager;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.ds.WeakMap;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class Gant implements ISpec {
  public var id(default, null): Int = 138;
  public var ball(default, null): MovieClip = null;
  public var isActive(default, null): Bool = false;
  public var getBall(default, null): Bool = false;
  public var cooldown(default, null): Int = 0;

  @:diExport
  private var patch(default, null): IPatch;

  public function new() {
    this.patch = new PatchList([

      // On désactive l'effet s'il n'y a plus d'ennemi - cooldown de relance, pour éviter qu'Igor reprenne directement sa balle
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        for(player in self.getPlayerList()) {
          if (!self.fl_pause && this.cooldown > 0 && player.specialMan.actives[this.id]) {
            this.updateTimers();
          }
          if (self.fl_clear && self.cycle > 10) {
            if (player.specialMan.actives[this.id]) {
              player.unstick();
              this.getBall = false;
            }
          }
        }
      }),

      // On peut récupérer les balles de fraise
      Ref.auto(Ball.hit).wrap(function(hf: Hf, self: Ball, e: Entity, old): Void {
        if ((e.types & hf.Data.PLAYER) > 0) {
          var player: hf.entity.Player = cast e;
          if (player.specialMan.actives[this.id] && this.isActive) {

            if(self.shootSpeed == 8.5 || (this.cooldown <= 0 && self.shootSpeed == 8.51)) {
              this.ball = self.game.depthMan.attach('hammer_shoot_ball', hf.Data.DP_FX);
              player.stick(this.ball, 0, -hf.Data.CASE_HEIGHT * 2.5);
              self.destroy();
              this.getBall = true;
            }

          } else if (self.shootSpeed != 8.51) {
            old(self, e);
          }
        }
        if ((e.types & hf.Data.CATCHER) > 0 && self.targetCatcher == e) {
          if(self.shootSpeed == 8.5) {
            old(self, e);
          } else {
            var fruit: hf.entity.Bad = cast e;
            fruit.forceKill(null);
          }
        }
        if ((e.types & hf.Data.BAD) > 0 && self.shootSpeed == 8.51) {
          var fruit: hf.entity.Bad = cast e;
          fruit.forceKill(null);
        }
      }),

      // Renvoi la balle sur un ennemi au hasard pour le tuer
      Ref.auto(Player.attack).wrap(function(hf: Hf, player: Player, old): Null<Entity> {
        if (player.specialMan.actives[this.id] && this.isActive && this.getBall) {
          this.cooldown = 50;
          var randomFruit: Entity = null;
          var bads = player.game.getClose(hf.Data.BAD_CLEAR, player.x, player.y, hf.Data.CASE_WIDTH * 6, false);
          if(bads.length == 0) {
            randomFruit = player.game.getOne(hf.Data.BAD_CLEAR);
          } else {
            randomFruit = bads[hf.Std.random(bads.length)];
          }
          var shoot = hf.entity.shoot.Ball.attach(player.game, player.x, player.y);
          var filter: ColorMatrixFilter = ColorMatrix.fromHsv(70, 3, 1).toFilter();
          shoot.filters = [filter];
          shoot.shootSpeed = 8.51;
          if(randomFruit == null) {
            this.isActive = false;
            player.curse(16);
            shoot.destroy();
          }
          shoot.moveToTarget(randomFruit, shoot.shootSpeed);
          this.getBall = false;
          player.curse(16);
          return null;
        }
        return old(player);
      }),
      
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, null);
    this.isActive = true;

    this.ball = specMan.game.depthMan.attach('hammer_shoot_ball', hf.Data.DP_FX);
    specMan.player.stick(this.ball, 0, -hf.Data.CASE_HEIGHT * 2.5);
    this.getBall = true;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    specMan.player.unstick();
    this.isActive = false;
    this.getBall = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}

  public function updateTimers(): Void {
    if(this.cooldown > 0) {
      this.cooldown--;
    }
  }
}