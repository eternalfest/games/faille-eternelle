package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Eclair implements ISpec {
  public var id(default, null): Int = 119;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var lightning = specMan.game.depthMan.attach('hammer_fx_death', hf.Data.FX);
    lightning._x = item.x;
    lightning._y = hf.Data.GAME_HEIGHT * 0.5;
    lightning._xscale = 150;

    for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
      var fruit: hf.entity.Bad = cast e;
      if (fruit.x > item.x - 55 && fruit.x < item.x + 55) {
        fruit.forceKill(null);
      }
    }
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
