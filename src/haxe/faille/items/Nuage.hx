package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Nuage implements ISpec {
  public var id(default, null): Int = 139;
  public var clouds(default, null): MovieClip = null;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {

    this.clouds = specMan.game.depthMan.attach("hammer_fx_clouds", hf.Data.DP_FX);
    this.clouds._y += 9;
    specMan.clouds.push(this.clouds);

    specMan.registerRecurring(function() {
      var randomX = hf.Std.random(hf.Data.LEVEL_WIDTH) * 20;
      var lightning = specMan.game.depthMan.attach('hammer_fx_death', hf.Data.DP_SPRITE_BACK_LAYER);
      lightning._x = randomX;
      lightning._y = hf.Data.GAME_HEIGHT * 0.5;
      lightning._xscale = 80;

      for(e in specMan.game.getList(hf.Data.BAD_CLEAR)) {
        var fruit: hf.entity.Bad = cast e;
        if (fruit.x > randomX - 20 && fruit.x < randomX + 20) {
          fruit.forceKill(null);
        }
      }
    }, hf.Data.SECOND/1.4, true);

    specMan.temporary(item.id, hf.Data.SECOND * 24);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.clouds.removeMovieClip();
  }
}