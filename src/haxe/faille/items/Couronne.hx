package faille.items;

import hf.entity.Item;
import hf.entity.Player;
import hf.mode.GameMode;
import hf.Hf;
import hf.SpecialManager;
import etwin.Obfu;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Couronne implements ISpec {
  public var id(default, null): Int = 168;
  public var player(default, null): Player;
  public var initHat(default, null): Int;
  public var isSet(default, null): Bool = false;
    
  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(GameMode.startLevel).after(function(hf: Hf, self: GameMode): Void {
        if(this.isSet) {
          if (self.currentDim != 1) {
            this.player.getScore(this.player, 20000);
          }
        }
      }),
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.permanent(item.id);
    this.initHat = specMan.player.head;
    specMan.player.head = 9;
    this.player = specMan.player;
    this.isSet = true;
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    this.player = null;
    specMan.player.head = this.initHat;
    specMan.clearRec();
    specMan.actives[this.id] = false;
    this.isSet = false;
  }
}
