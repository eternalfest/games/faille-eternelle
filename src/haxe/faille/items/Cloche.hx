package faille.items;

import hf.entity.Item;
import hf.entity.item.ScoreItem;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Cloche implements ISpec {
  public var id(default, null): Int = 163;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    
    specMan.registerRecurring(function() {
      var randomX = hf.Std.random(hf.Data.LEVEL_WIDTH) * 20;
      var randomY = hf.Std.random(hf.Data.LEVEL_HEIGHT) * 20;
      var uid = hf.Std.random(100) + 1;
	  if(uid <= 40) {
	    uid = 0;
	  } else if(uid > 39 && uid <= 69){
	    uid = 1;
	  } else if(uid > 69 && uid <= 89){
	    uid = 2;
	  } else if(uid > 89 && uid <= 99){
	    uid = 3;
	  } else if(uid > 99){
	    uid = 6;
      }
      hf.entity.item.ScoreItem.attach(specMan.game, randomX, randomY, 0, uid);  
    }, hf.Data.SECOND/7, true);
    specMan.temporary(item.id, hf.Data.SECOND * 8);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    specMan.actives[this.id] = false;
  }
}