package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.ISpec;

class Parapluie implements ISpec {
  public var id(default, null): Int = 164;
  private var levelJump(default, null): LevelJump;

  public function new(levelJump: LevelJump): Void {
    this.levelJump = levelJump;
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var lid = specMan.game.world.currentId;
    var numberLevels = this.levelJump.getNextJumpPortal(lid);
    specMan.warpZone(numberLevels - lid);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {}
}