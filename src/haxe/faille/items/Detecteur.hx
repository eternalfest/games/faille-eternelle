package faille.items;

import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.RandomManager;
import hf.SpecialManager;
import hf.mode.GameMode;
import hf.mode.Adventure;
import vault.ISpec;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Detecteur implements ISpec {
  public var id(default, null): Int = 149;
  public var isActive(default, null): Bool = false;
  
  private var randomItems(default, null): Array<Int>;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
      Ref.auto(GameMode.startLevel).after(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if (this.isActive) {
            if (self.currentDim != 1) {
              var x = 10;
              var y = 20;
              
              // Si le miroir est actif, on décale l'affichage
              for(player in self.getPlayerList()) {
                if (player.specialMan.actives[167]) {
                  y += 30;
                }
              }
            
              for(item in randomItems) {
                var movieClip: String;
                if(item < 1000) {
                  movieClip = "hammer_item_special";   
                } else {
                  movieClip = "hammer_item_score";
                  item -= 1000;
                }
            
                var o: MovieClip = self.depthMan.attach(movieClip, hf.Data.DP_INTERF);
                var obj: DynamicMovieClip = cast o;
                obj.gotoAndStop('' + (item + 1));
                obj._x = x;
                obj._xscale = 50;
                obj._y = y;
                obj._yscale = 50;
                self.fxMan.mcList.push(obj);
                x+= 20;
              }
            }
          }
        }
      }),
        
      Ref.auto(Adventure.addLevelItems).wrap(function(hf: Hf, self: Adventure, old): Void {
        if(this.isActive) {
          this.randomItems = new Array();
          var v4;
          var v3;
          if(self.world.current.__dollar__specialSlots.length > 0) {
            self.statsMan.spreadExtend();
          }
          if(self.world.current.__dollar__specialSlots.length > 0) {
            v3 = Std.random(self.world.current.__dollar__specialSlots.length);
            v4 = self.world.current.__dollar__specialSlots[v3];
            var randomSpecial = self.randMan.draw(hf.Data.RAND_ITEMS_ID);
            this.randomItems.push(randomSpecial);
            self.world.scriptEngine.insertSpecialItem(randomSpecial, null, v4.__dollar__x, v4.__dollar__y, hf.Data.SPECIAL_ITEM_TIMER, null, false, true);
          }
          if(self.world.current.__dollar__scoreSlots.length > 0) {
            v3 = Std.random(self.world.current.__dollar__scoreSlots.length);
            v4 = self.world.current.__dollar__scoreSlots[v3];
            var randomScore = self.randMan.draw(hf.Data.RAND_SCORES_ID);
            this.randomItems.push(randomScore);
            self.world.scriptEngine.insertScoreItem(randomScore, null, v4.__dollar__x, v4.__dollar__y, hf.Data.SCORE_ITEM_TIMER, null, false, true);
            if(self.globalActives[94]) {
              var v5 = Std.random(hf.Data.LEVEL_WIDTH);
              var v6 = Std.random(hf.Data.LEVEL_HEIGHT - 5);
              var v7 = self.world.getGround(v5, v6);
              var randomScoreAntok = self.randMan.draw(hf.Data.RAND_SCORES_ID);
              this.randomItems.push(randomScoreAntok);
              self.world.scriptEngine.insertScoreItem(randomScoreAntok, null, v7.x, v7.y, hf.Data.SCORE_ITEM_TIMER, null, false, true);
            }
          }
        } else {
          old(self);
        }
      }),
    
    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.permanent(item.id);
    this.isActive = true;
    
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    this.isActive = false;
  }
}