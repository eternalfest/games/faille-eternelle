package faille.items;

import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class L_Invisible implements ISpec {
  public var id(default, null): Int = 151;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, hf.Data.SECOND * 20);
    specMan.game.world.view.fl_hideTiles = true;
    specMan.game.world.view.detach();
    specMan.game.world.view.attach();
    specMan.game.world.view.moveToPreviousPos();
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.actives[this.id] = false;
    specMan.game.world.view.fl_hideTiles = false;
    specMan.game.world.view.detach();
    specMan.game.world.view.attach();
    specMan.game.world.view.moveToPreviousPos();
  }
}