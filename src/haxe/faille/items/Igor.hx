package faille.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISpec;

class Igor implements ISpec {
  public var id(default, null): Int = 146;

  public function new() {}

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    ++specMan.player.lives;
    specMan.game.gi.setLives(specMan.player.pid, specMan.player.lives);
    specMan.game.fxMan.attachShine(item.x, item.y - hf.Data.CASE_HEIGHT * 0.5);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {}
}
