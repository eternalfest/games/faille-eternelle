package faille;

import etwin.flash.MovieClip;
import hf.entity.Player;
import hf.Hf;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

@:build(patchman.Build.di())
class CustomBugFix {
    @:diExport
    public var removeAdditionalPlayerSpritesOnDestroy(default, null): IPatch;

    public function new(): Void {
        removeAdditionalPlayerSpritesOnDestroy = Ref.auto(Player.destroy).before(function(hf: Hf, self: Player): Void {
            /* Remove the shield sprite if it's present. */
            self.unshield();

            /* Remove the player halo.
             * The original game pretty much hardcodes that there are 2 holes for the players, so weird things already happen with more.
             * This doesn't try to fix that... */
            var holes: Array<MovieClip> = self.game.darknessMC.holes;
            if (self.pid < holes.length) {
                var hole: MovieClip = holes[self.pid];
                /* Otherwise the sprite might stay in game until the next GameMode.holeUpdate(), which sets the hole._visible to the player's _visible value. */
                self._visible = false;
                /* Let the game have its hardcoded 2 holes... */
                if (self.pid < 2) {
                    hole._visible = false;
                }
                else {
                    hole.removeMovieClip();
                    holes.splice(self.pid, 1);
                }
            }
        });
    }
}
