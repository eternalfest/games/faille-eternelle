package faille;

import hf.Hf;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;

@:build(patchman.Build.di())
class StuckDim {

  @:diExport
  public var patches(default, null): IPatch;

  public function new() {
    this.patches = new PatchList([

      Ref.auto(GameMode.switchDimensionById).before(function(hf, self: GameMode, id: Int, lid: Int, pid: Int): Void {
        self.fl_clear = true;
      }),
    ]);
  }
}