package faille.custom_map;

import etwin.ds.Nil;
import etwin.flash.MovieClip;
import etwin.Obfu;
import patchman.Ref;
import patchman.PatchList;
import patchman.IPatch;
import hf.Hf;
import hf.entity.Player;
import hf.gui.GameInterface;
import hf.mode.GameMode;
import randomfest.Randomfest;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;

@:build(patchman.Build.di())
class CustomMap {

	private static inline var MAP_LVL_WIDTH: Int = 20;
 	private static inline var MAP_LVL_HEIGHT: Int = 30;
 	private static inline var MAP_WIDTH: Int = 20;
 	private static inline var MAP_HEIGHT: Int = 15;
 	private var levelInfos: Map<Int, LevelInfo> = new Map();
 	private var playerLoc: Int = 0;
 	private var scroll: MovieClip;
 	private var empty: MovieClip;
    private var hue: Int = 0;
    private var mc: MovieClip = null;
    private var mcMulticolore: MovieClip = null;
    private var multicolore = null;

    private var preventOnResurrectFromActing: Bool = false;

    @:diExport
  	public var patches(default, null): IPatch;

    public function new(randomfest: Randomfest) {

        this.patches = new PatchList([

	      	Ref.auto(GameMode.onMap).wrap(function(hf, self: GameMode, old): Void {
	      		if (self.fl_lock) {
                	return;
	            }
	            self.fl_pause = true;
	            self.lock();
	            self.world.lock();
	            if (!self.fl_mute) {
	                self.setMusicVolume(0.5);
	            }

	            // Pause spéciale pour le niveau 0
	            if (self.world.fl_mainWorld && self.world.currentId == 0) {
	            	self.mapMC.removeMovieClip();
	            	self.mapMC = cast self.depthMan.attach("hammer_interf_instructions", self.root.Data.DP_INTERF);
	            	self.mapMC._x = hf.Data.GAME_WIDTH * 0.5;
            		self.mapMC._y = hf.Data.GAME_HEIGHT * 0.5;
            		self.mapMC.sector.text = "";
            		self.mapMC.gotoAndStop("1");
			      	scroll = cast self.depthMan.attach(Obfu.raw("large_scroll"), self.root.Data.DP_INTERF);
		            scroll._x = self.root.Data.GAME_WIDTH / 2 - scroll._width / 2;
		            scroll._y = self.root.Data.GAME_HEIGHT / 2 - scroll._height / 2;
		            var mc = hf.Std.createEmptyMC(scroll, scroll.getNextHighestDepth());
					showText(hf, mc, 10, 30, 120, 120, 400, "Bienvenue dans la Faille Eternelle");
					showText(hf, mc, 10, 80, 90, 90, 400, "Cette contrée ne possède pas de dimensions");
					showText(hf, mc, 10, 95, 90, 90, 400, "cachées,  il  faut  aller  le  plus  loin  possible.");
					showText(hf, mc, 10, 125, 90, 90, 400, "Le  style  des  niveaux  change  tous  les  dix");
					showText(hf, mc, 10, 140, 90, 90, 400, "niveaux,   avec   une    vie    en   récompense.");
					showText(hf, mc, 10, 170, 90, 90, 400, "Un portail  peut apparaître entre  les niveaux");
					showText(hf, mc, 10, 185, 90, 90, 400, "finissant  par  un  2 et  un  8 pour  sauter  des");
					showText(hf, mc, 10, 200, 90, 90, 400, "niveaux  selon  le  nombre  de  vies  que vous");
					showText(hf, mc, 10, 215, 90, 90, 400, "avez perdues.");
					showText(hf, mc, 10, 245, 90, 90, 400, "Les niveaux finissant par un 9 sont plus durs.");
					showText(hf, mc, 10, 275, 90, 90, 400, "Vous gagnez une vie tous les  500.000 points.");
					showText(hf, mc, 90, 315, 120, 120, 400, "Bonne chance !");
			    } else {
			    	self.mapMC.removeMovieClip();
				    self.mapMC = cast this.attach(hf, self, self.depthMan, hf.Data.DP_INTERF);
				    self.mapMC._x = -self.xOffset;
				    empty = cast self.depthMan.attach(Obfu.raw("empty"), self.root.Data.DP_TOP);
				    var mc = hf.Std.createEmptyMC(empty, empty.getNextHighestDepth());
				    showText(hf, mc, 0, 3, 100, 100, 400, hf.Lang.get(14) + "«" + hf.Lang.getSectorName(self.currentDim, self.world.currentId) + "»");
			    }
	      	}),

	      	Ref.auto(GameMode.onUnpause).before(function(hf, self: GameMode): Void {
	      		scroll.removeMovieClip();
	      		empty.removeMovieClip();
	      	}),

	      	Ref.auto(GameMode.startLevel).before(function(hf, self: GameMode): Void {
	      		if (self.world.fl_mainWorld) {
	      			if(self.world.currentId != 0) this.enterLevel(self.world.currentId);
	      		}
	      	}),

	      	Ref.auto(GameMode.registerMapEvent).before(function(hf, self: GameMode, eid: Int, data: Dynamic): Void {
	      		if (!self.world.fl_mainWorld) {
			      return;
			    }
			    
	      		if (eid == hf.Data.EVENT_EXTEND) {
			      	this.registerExtend();
			    } else if (eid == hf.Data.EVENT_DEATH) {
			      	this.registerDeath();
			    }

			    if (eid == hf.Data.EVENT_EXIT_LEFT || 
			    	eid == hf.Data.EVENT_EXIT_RIGHT) {
			    	this.registerDimIn();
			    }

			    if (eid == hf.Data.EVENT_BACK_LEFT || 
			    	eid == hf.Data.EVENT_BACK_RIGHT) {
			    	this.playerLoc = self.world.currentId;
			    	this.registerDimOut();
			    }
	      	}),

            /* Prevent the randomfest deaths from appearing on the map (and other death effects), and also from putting garbage on the bottom bar. */
            Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
                if (randomfest.isRandomfestPlayer(self))
                    preventOnResurrectFromActing = true;
            }),
            Ref.auto(Player.killPlayer).after(function(hf: Hf, self: Player): Void {
                preventOnResurrectFromActing = false;
            }),
            Ref.auto(GameMode.onResurrect).prefix(function(hf: Hf, self: GameMode): Nil<Void> {
                if (preventOnResurrectFromActing) {
                    self.world.scriptEngine.onPlayerBirth();
                    self.world.scriptEngine.onPlayerDeath();
                    return Nil.some(null);
                }
                return Nil.none();
            }),
            Ref.auto(GameInterface.setLives).prefix(function(hf: Hf, self: GameInterface, pid: Int, v: Int): Nil<Void> {
                if (preventOnResurrectFromActing)
                    return Nil.some(null);
                return Nil.none();
            }),
            Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
                this.hue += 3;
                if(this.hue > 360) this.hue = 0;
                mcMulticolore.removeMovieClip();
                mcMulticolore = drawStar(hf, self, this.mc, 200, 280, this.multicolore == 1 ? true : false, 0, 1, 1, 500, true);
            }),
	    ]);
    }

    public function enterLevel(lvl: Int): Void {
	    this.playerLoc = lvl;
	    getCurLevelInfo().discovered = true;
	}

	public function registerExtend(): Void {
	    getCurLevelInfo().icons.push(Extend);
	}

	public function registerDeath(): Void {
	    getCurLevelInfo().icons.push(Death);
	}

	public function registerDimIn(): Void {
	    getCurLevelInfo().icons.push(DimIn);
	}

	public function registerDimOut(): Void {
	    getCurLevelInfo().icons.push(DimOut);
	}

	private function getCurLevelInfo(): LevelInfo {
	    var info = this.levelInfos[this.playerLoc];
	    if (info == null) {
	      	info = {
	        	discovered: false,
	        	icons: [],
	      	};
	      this.levelInfos[this.playerLoc] = info;
	    }
	    return info;
	}

    public function attach(hf: hf.Hf, game: hf.mode.GameMode, depthMan: hf.DepthManager, plan: Int): MovieClip {
        var mc = depthMan.attach(Obfu.raw("map"), plan);

	    var width = MAP_LVL_WIDTH * MAP_WIDTH;
	    var height = MAP_LVL_HEIGHT * MAP_HEIGHT;
	    var map = hf.Std.createEmptyMC(mc, mc.getNextHighestDepth());

        centerMap(mc, map, width, height);
        // Dim 20 uniquement
        if(game.currentDim > 1) {
            drawStars(hf, game, map);
            return mc;
        }
	    drawMap(hf, map, width, height);

	    for (i in this.levelInfos.keys()) {
	        drawIcons(hf, map, i, this.levelInfos[i].icons);
	        drawText(hf, map, i);
	    }

	    return mc;
	}

	private static function centerMap(parent: MovieClip, map: MovieClip, width: Float, height: Float): Void {
	    map._x = (parent._width - width)/2;
	    map._y = (parent._height - height)/2;
	}

	private function drawMap(hf: hf.Hf, mc: MovieClip, width: Int, height: Int): Void {
	    mc = hf.Std.createEmptyMC(mc, mc.getNextHighestDepth());

	    var t = 1;
	    var w = MAP_LVL_WIDTH;
	    var h = MAP_LVL_HEIGHT;
	    var bgColor = 0x4e301d;
	    var wallClosed = 0x0000ff;
	    var cellColor = 0xa17858;

	    for (i in this.levelInfos.keys()) {
		    var lvl = i;
		    var info = this.levelInfos[i];
		    if (lvl == null || !info.discovered)
		        continue;

		    var x = ((lvl % w) * w) - w;
		    var y = Math.floor(lvl / w) * h;
		    if(lvl % w == 0) {
		    	x = 19 * w;
		    	y -= h;
		    }

		    if(lvl <= 300) {
		    	if(this.playerLoc == lvl)
		    		drawRect(mc, x, y, w, h, wallClosed);
		    	drawRect(mc, x+t, y+t, w-2*t, h-2*t, cellColor);
		    }
		    
	    }

	    // Render on bitmap to "flatten" alpha.
	    var bitmap = new hf.native.BitmapData(width, height, true, 0);
	    bitmap.drawMC(mc, 0, 0);
	    mc.clear();
	    mc.attachBitmap(bitmap, 0);
	    mc._alpha = 75;
	}
    
    private function drawStars(hf: hf.Hf, game: GameMode, mc: MovieClip): Void {
        mc = hf.Std.createEmptyMC(mc, mc.getNextHighestDepth());
        this.mc = mc;
        var rouge: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("rouge")).or(new MerlinFloat(0));
        var orange: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("orange")).or(new MerlinFloat(0));
        var jaune: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("jaune")).or(new MerlinFloat(0));
        var verte: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verte")).or(new MerlinFloat(0));
        var turquoise: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("turquoise")).or(new MerlinFloat(0));
        var bleue: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("bleue")).or(new MerlinFloat(0));
        var violette: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("violette")).or(new MerlinFloat(0));
        var multicolore: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("multicolore")).or(new MerlinFloat(0));
        var rouge = cast rouge.unwrapFloat();
        var orange = cast orange.unwrapFloat();
        var jaune = cast jaune.unwrapFloat();
        var verte = cast verte.unwrapFloat();
        var turquoise = cast turquoise.unwrapFloat();
        var bleue = cast bleue.unwrapFloat();
        var violette = cast violette.unwrapFloat();
        var multicolore = cast multicolore.unwrapFloat();
        this.multicolore = multicolore;
        
        // draw all stars
        drawStar(hf, game, mc, 110, 340, rouge == 1 ? true : false, 0, 1, 1, 250);
        drawStar(hf, game, mc, 70,  260, orange == 1 ? true : false, -35, 1.3, 1, 250);
        drawStar(hf, game, mc, 110, 170, jaune == 1 ? true : false, -60, 2.5, 1, 250);
        drawStar(hf, game, mc, 200, 120, verte == 1 ? true : false, -136, 1, 1, 250);
        drawStar(hf, game, mc, 290, 170, turquoise == 1 ? true : false, 184, 1.2, 1, 250);
        drawStar(hf, game, mc, 330, 260, bleue == 1 ? true : false, 142, 1, 1, 250);
        drawStar(hf, game, mc, 290, 340, violette == 1 ? true : false, 56, 1, 1, 250);
        
        // Explications de l'étoile multicolore
        if(multicolore == 1) {
            showText(hf, mc, 120, 350, 140, 140, 400, "Étoile temporelle");
            showText(hf, mc, 10, 380, 100, 100, 400, "Vous pouvez ralentir le temps en appuyant sur MAJ (SHIFT)");
            showText(hf, mc, 10, 400, 100, 100, 400, "Ce pouvoir n'est pas infini et doit se recharger.");
            showText(hf, mc, 10, 420, 100, 100, 400, "La barre rouge au-dessus d'Igor indique la charge restante.");
            showText(hf, mc, 10, 440, 100, 100, 400, "Si le pouvoir est totalement chargé, la barre disparaît.");
        }
    }
    
    private function drawStar(hf: Hf, game: GameMode, mc: MovieClip, x: Int, y: Int, got: Bool, h: Int, s: Float, v: Int, sxy: Int, multi: Bool = false): MovieClip {
        var depth = mc.getNextHighestDepth();
        var iconType = null;
        if(!multi) {
            iconType = got ? "hammer_item_score" : "$starlock";
        } else {
            iconType = got ? "hammer_item_score" : null;
        }
        var starSprite: Dynamic = hf.Std.attachMC(mc, iconType, depth);
        starSprite._x = got ? x : x - 38;
        starSprite._y = got ? y : y - 71;
        starSprite._xscale = sxy;
        starSprite._yscale = sxy;
        if(got) {
            starSprite.gotoAndStop('' + (169 + 1));
            var filter: ColorMatrixFilter;
            if(!multi) {
                filter = ColorMatrix.fromHsv(h, s, v).toFilter();
            } else {
                filter = ColorMatrix.fromHsv(this.hue, s, v).toFilter();
            }
            starSprite.filters = [filter];
        }
        return starSprite;
    }

	private static function drawIcons(hf: hf.Hf, mc: MovieClip, lvl: Int, icons: Array<IconKind>): Void {
	    var size = 6;
	    var maxWidth = 3;
	    var maxIcons = 9;

	    var posX = ((lvl % MAP_LVL_WIDTH)) - 1;
	    var posY = Math.floor(lvl / MAP_LVL_WIDTH);
	    if(lvl % MAP_LVL_WIDTH == 0) {
	    	posX = 19;
	    	posY -= 1;
	    }

	    var x = (posX + .5) * MAP_LVL_WIDTH;
	    var y = (posY + .5) * MAP_LVL_HEIGHT;

	    var len: Int = CustomMap.min(icons.length, maxIcons);
	    var lastRow = Math.ceil(len / maxWidth) - 1;
	    
	    for (idx in 0...len) {
	      	var row = Std.int(idx / maxWidth);
	      	var col = idx % maxWidth;
	      	var lastCol = row == lastRow ? ((len-1) % maxWidth) : (maxWidth - 1);

			if(lvl <= 300) {
				var icon = attachIcon(hf, mc, icons[idx]);
				icon._x += x + size * (col - lastCol/2);
		      	icon._y += y + size * (row - lastRow/2) - 5;
		      	var testIcon: Int = cast icons[idx];
		      	if(testIcon > 1) {
		      		icon._x -= 5;
		      		icon._y -= 5;
				}
			}
	    }
	}

	private static function drawText(hf: hf.Hf, mc: MovieClip, lvl: Int): Void {
		var Text = attachText(hf, mc);
	    (cast Text).field._width = 400;
	    (cast Text).label.text = "";
	    (cast Text).field._xscale = 70;
	    (cast Text).field._yscale = 70;
	  
	    var posX = ((lvl % MAP_LVL_WIDTH) * MAP_LVL_WIDTH) - MAP_LVL_WIDTH;
	    var posY = Math.floor(lvl / MAP_LVL_WIDTH) * MAP_LVL_HEIGHT + 18;
	    if(lvl % MAP_LVL_WIDTH == 0) {
	    	posX = 19 * MAP_LVL_WIDTH;
	    	posY -= MAP_LVL_HEIGHT;
	    }
	    
	    var lvlS: String = cast lvl;
	    if(lvl < 10) lvlS = "00" + lvl;
	    if(lvl > 9 && lvl < 100) lvlS = "0" + lvl;
	    if(lvl <= 300) {
		    (cast Text).field.text = lvlS;
		    (cast Text).field._x = posX;
		    (cast Text).field._y = posY;
        } else {
            (cast Text).field._x = -1000;
		    (cast Text).field._y = -1000;
        }
	}

	private static function drawRect(clip: MovieClip, x: Float, y: Float, width: Float, height: Float, color: Null<Int>, alpha: Int = 100): Void {
	    if (color == null) return;
	    clip.lineStyle();
	    clip.moveTo(x, y);
	    clip.beginFill(color, alpha);
	    clip.lineTo(x+width, y);
	    clip.lineTo(x+width, y+height);
	    clip.lineTo(x, y+height);
	    clip.lineTo(x, y);
	    clip.endFill();
	}

	private static function attachIcon(hf: hf.Hf, mc: MovieClip, icon: IconKind): MovieClip {
	    var depth = mc.getNextHighestDepth();
	    return switch (icon) {
	    	case Death:
	       		var mc = hf.Std.attachMC(mc, "hammer_interf_mapIcon", depth);
	        	mc.gotoAndStop("5");
	        	mc;
	    	case Extend:
	        	var mc = hf.Std.attachMC(mc, "hammer_interf_mapIcon", depth);
	        	mc.gotoAndStop("6");
	        	mc._x -= 1;
	        	mc._y -= 1;
	        	mc;
	        case DimIn:
	        	var mc = hf.Std.attachMC(mc, "hammer_interf_mapIcon", depth);
	        	mc.gotoAndStop("8");
	        	mc;
	        case DimOut:
	        	var mc = hf.Std.attachMC(mc, "hammer_interf_mapIcon", depth);
	        	mc.gotoAndStop("9");
	        	mc;
	    }
	}

	private static function attachText(hf: hf.Hf, mc: MovieClip): MovieClip {
		var depth = mc.getNextHighestDepth();
		var mc = hf.Std.attachMC(mc, "hammer_interf_inGameMsg", depth);
		return mc;
	}

	private static function showText(hf: hf.Hf, mc: MovieClip, x: Int, y: Int, xscale: Int, yscale: Int, size: Int, txt: String) {
		var Text = attachText(hf, mc);
        (cast Text).field._width = size;
	    (cast Text).label.text = "";
	    (cast Text).field._xscale = xscale;
	    (cast Text).field._yscale = yscale;
	    (cast Text).field.text = txt;
		(cast Text).field._x = x;
		(cast Text).field._y = y;
		hf.FxManager.addGlow((cast Text), 0, 2);
	}

	private static inline function min<T: Float>(a: T, b: T): T {
	    return a < b ? a : b;
	}


}

private typedef LevelInfo = {
  discovered: Bool,
  icons: Array<IconKind>,
};

@:enum
private abstract IconKind(Int) {
  var Death = 0;
  var Extend = 1;
  var DimIn = 2;
  var DimOut = 3;
}