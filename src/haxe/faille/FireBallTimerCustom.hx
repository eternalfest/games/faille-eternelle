package faille;

import etwin.Obfu;
import bottom_bar.BottomBarInterface;

class FireBallTimerCustom extends bottom_bar.modules.custom.Timer {

    public static var visible: Bool = false;
    
    public function new(module: Dynamic) {
        super(module);
    }

    public override function init(bottomBar:BottomBarInterface): Void {

        var fireballSprite: Dynamic = bottomBar.game.depthMan.attach('hammer_bad_fireball', bottomBar.game.root.Data.DP_TOP + 1);
        fireballSprite._xscale = 50.;
        fireballSprite._yscale = 50.;
        fireballSprite._x = x;
        fireballSprite._y = 517.;

        timerSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        timerSprite.field._visible = true;
        timerSprite.field._width = 400;
        timerSprite.field._xscale = 115;
        timerSprite.field._yscale = 115;
        timerSprite.field._x = x + 10;
        timerSprite.field._y = 499;
        timerSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(timerSprite, 7366029, 2);

        if(!visible) {
            fireballSprite._visible = false;
            timerSprite._visible = false;
        }
    }

    public override function update(bottomBar:BottomBarInterface): Bool {
        var text: String;
        if (bottomBar.game.huState >= 2) {
            text = "---";
        } else {
            var elapsedTime: Float = Math.max(((((bottomBar.game.huState < 1 ? bottomBar.game.root.Data.HU_STEPS[0] : 0) + bottomBar.game.root.Data.HU_STEPS[1]) / bottomBar.game.diffFactor - bottomBar.game.huTimer) / bottomBar.game.root.Data.SECOND), 0);
            text = Std.string(Math.round(elapsedTime * 10) / 10);
            if (Math.round(elapsedTime * 10) % 10 == 0)
                text += ".0";
        }
        if (text != timerSprite.field.text) {
            timerSprite.field.text = text;
        }
        return true;
    }
}
