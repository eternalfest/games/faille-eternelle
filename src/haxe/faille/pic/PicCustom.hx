package faille.pic;

import etwin.flash.MovieClip;
import hf.mode.GameMode;
import hf.Hf;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class PicCustom extends MovieClip {
    private var sub: MovieClip;

    private var game: GameMode;

    public function new() {}

    public function init(hf: Hf, g: GameMode, x: Float, y: Float): Void {
        _x = x;
        _y = y;
        sub.gotoAndStop(1);
        game = g;
        
        g.addToList(hf.Data.ENTITY, cast this);
        g.addToList(hf.Data.BAD, cast this);

        var cx: Int = hf.Entity.x_rtc(x);
        var cy: Int = hf.Entity.y_rtc(y);
        if (game.world.getCase({x: cx, y: cy + 1}) > 0)
            this.gotoAndStop("1");
        else if (game.world.getCase({x: cx, y: cy - 1}) > 0)
            this.gotoAndStop("3");
        else if (game.world.getCase({x: cx - 1, y: cy}) > 0)
            this.gotoAndStop("2");
        else if (game.world.getCase({x: cx + 1, y: cy}) > 0)
            this.gotoAndStop("4");
    }

    public static function attach(hf: Hf, g: GameMode, x: Float, y: Float): PicCustom {
        var spike: PicCustom = cast g.depthMan.attach(Obfu.raw("hammer_bad_spear"), hf.Data.DP_SPEAR);
        spike.init(hf, g, x, y);
        return spike;
    }

    @:keep
    public function update(): Void {
        for (player in game.getPlayerList()) {
            if (player.x > _x - 10 && player.x < _x + 10 && player.y > _y - 10 && player.y < _y + 10)
                player.killHit(0);
        }
        for(e in game.getList(game.root.Data.BAD_CLEAR)) {
	        var fruit: hf.entity.Bad = cast e;
	        if (fruit.x > _x - 10 && fruit.x < _x + 10 && fruit.y > _y - 10 && fruit.y < _y + 10)
	            fruit.forceKill(null);
	    }
        var ballons = game.getList(game.root.Data.SOCCERBALL);
        for(e in ballons) {
            var ball: hf.entity.bomb.player.SoccerBall = cast e;
            if (ball.x > _x - 10 && ball.x < _x + 10 && ball.y > _y - 10 && ball.y < _y + 10) {
                ball.destroy();
                game.fxMan.attachFx(ball.x, ball.y - game.root.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
            }     
        }
    }

    @:keep
    public function destroy(): Void {
        removeMovieClip();
    }

    @:keep
    override public function onUnload() {
        game.removeFromList(game.root.Data.BAD, cast this);
        game.removeFromList(game.root.Data.ENTITY, cast this);
    }
}

class PicCustomSpawn implements IAction {
    public var name(default, null): String = Obfu.raw("pic");
    public var isVerbose(default, null): Bool = false;

    public function new(hf: Hf) {
        hf.Std.registerClass(Obfu.raw("hammer_bad_spear"), PicCustom);
    }

    public function run(ctx: IActionContext): Bool {
        var hf: Hf = ctx.getHf();
        var game: GameMode = ctx.getGame();

        var x: Float = game.flipCoordReal(hf.Entity.x_ctr(ctx.getFloat(Obfu.raw("x"))));
        var y: Float = hf.Entity.y_ctr(ctx.getFloat(Obfu.raw("y")));
        --y; /* In order for it to be perfectly lined up with normal spikes. */

        var sid: Null<Int> = ctx.getOptInt(Obfu.raw("sid")).toNullable();

        var spike: PicCustom = PicCustom.attach(hf, game, x, y);
        ctx.killById(sid);
        (cast spike).scriptId = sid;
        return false;
    }
}