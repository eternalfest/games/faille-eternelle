package faille;

import hf.Entity;
import hf.entity.Player;
import hf.SpecialManager;
import patchman.PatchList;
import patchman.IPatch;
import patchman.Ref;
@:build(patchman.Build.di())
class FilterItem {

    @:diExport
    public var patch(default, null): IPatch;

    public var filters: Map<Entity, Array<etwin.flash.filters.BitmapFilter>>;
    public function new(patches: Array<IPatch>) {
        filters = new Map<Entity, Array<etwin.flash.filters.BitmapFilter>>();
        patches.push(Ref.auto(hf.SpecialManager.execute).wrap(function(hf, self: SpecialManager, item, old) {
            if(item.id == 86) {
                var filter = new etwin.flash.filters.GlowFilter();
                filter.color = cast 9224447;
                filter.alpha = 0.5;
                filter.strength = cast 100;
                filter.blurX = 2.;
                filter.blurY = 2.;
                filters[self.player] = self.player.filters;
                if(self.player.filters == null)
                    self.player.filters = [filter];
                else {
                    var f = self.player.filters;
                    f.push(filter);
                    self.player.filters = f;
                }
                self.temporary(item.id, self.game.root.Data.SECOND * 60);
                self.game.fxMan.attachBg(self.game.root.Data.BG_GHOSTS, null, self.game.root.Data.SECOND * 57);
                var bads = self.game.getBadList();
                for (bad in 0...bads.length) {
                    filter.alpha = 1.0;
                    filter.color = cast 16733440;
                    filters[bads[bad]] = bads[bad].filters;
                    if(bads[bad].filters == null)
                        bads[bad].filters = [filter];
                    else {
                        var f = bads[bad].filters;
                        f.push(filter);
                        bads[bad].filters = f;
                    }
                }
            }
            else {
                old(self, item);
            }
        }));

        patches.push(Ref.auto(hf.SpecialManager.interrupt).wrap(function(hf, self: SpecialManager, id, old) {
            if(id == 86) {
                self.actives[id] = false;
                var bads = self.game.getBadList();
                for (bad in 0...bads.length) {
                    bads[bad].alpha = 100.;
                    bads[bad].filters = filters[bads[bad]];
                    filters.remove(bads[bad]);
                }
                self.player.filters = filters[self.player];
                filters.remove(self.player);
                self.game.fxMan.clearBg();
            }
            else {
                old(self, id);
            }
        }));
        this.patch = new PatchList(patches);
    }
}
