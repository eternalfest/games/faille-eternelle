package faille;

import hf.Hf;
import hf.mode.GameMode;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;

@:build(patchman.Build.di())
class ReducedFireballTimer {

    private static var LIVES_STOPPING_TIMER: Int = 20;

    @:diExport
    public var patch(default, null): IPatch;

    public function new(): Void {
    this.patch = new PatchList([

      Ref.auto(GameMode.resetHurry).after(function(hf: Hf, self: GameMode): Void {
        var lives = 0;
        var playerList = self.getPlayerList();
        for (player in playerList) {
            lives = player.lives <= lives ? lives : player.lives;
        }
        if(lives >= LIVES_STOPPING_TIMER) {
            self.diffFactor = 1.0 + 0.05 * (LIVES_STOPPING_TIMER - 3);
            if (self.fl_nightmare) {
              self.diffFactor *= 1.4;
            }
        }
      }),

    ]);
  }
}