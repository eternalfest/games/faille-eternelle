package faille;

import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import custom_clips.CustomClips;

import faille.bads.CitronVert;
import faille.bads.Mirabelle;
import faille.bads.R2;
import faille.bads.MechaBombi;

@:build(patchman.Build.di())
class Bads {

  @:diExport
  public var patch(default, null): IPatch;

  public function new(clips: CustomClips, patches: Array<IPatch>) {
    patches.push(Ref.auto(hf.mode.GameMode.attachBad).wrap(function(hf, self, id, x, y, next) {
      var bad = switch (id) {
        case CitronVert.BAD_ID: CitronVert.attach(clips, self, x, y);
        case Mirabelle.BAD_ID: Mirabelle.attach(clips, self, x, y, true);
        case R2.BAD_ID: R2.attach(clips, self, x, y);
        case MechaBombi.BAD_ID: MechaBombi.attach(clips, self, x, y);
        default: return next(self, id, x, y);
      };

      if (bad.isType(hf.Data.BAD_CLEAR)) {
        self.badCount++;
      }
      return bad;
    }));
    this.patch = new PatchList(patches);
  }
}
