package faille;

import etwin.Obfu;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;

class DeathCount extends BottomBarModule {
    
    public var skullSprite: Dynamic;
    public var countSprite: Dynamic;
    public var x: Int;

    public function new(module: Map<String, Dynamic>) {
        x = module[Obfu.raw("x")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        skullSprite.removeMovieClip();
        skullSprite = bottomBar.game.depthMan.attach('hammer_interf_ninjaIcon', bottomBar.game.root.Data.DP_TOP + 1);
        skullSprite._x = x;
        skullSprite._y = 517.;
        skullSprite.stop();

        countSprite.removeMovieClip();
        countSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        countSprite.field._visible = true;
        countSprite.field._width = 400;
        countSprite.field._xscale = 115;
        countSprite.field._yscale = 115;
        countSprite.field._x = x + 10;
        countSprite.field._y = 499;
        countSprite.field.text = "0";
        countSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(countSprite, 7366029, 2);
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        var merlinNumberDeath: MerlinValue = Merlin.getGlobalVar(bottomBar.game, Obfu.raw("DEATH")).or(new MerlinFloat(0));
        var numberDeath: Float = merlinNumberDeath.unwrapFloat();
        var numberDeathStr = Std.string(numberDeath);
        
        if(numberDeathStr != countSprite.field.text) {
          countSprite.field.text = numberDeathStr;
        }
        
        // Retire le compteur de mort dans la dim 20
        if(bottomBar.game.currentDim > 1 && countSprite.field._visible) {
          skullSprite.removeMovieClip();
          countSprite.field._visible = false;
        }
    
        return true;
    }
}