package faille;

import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import etwin.flash.MovieClip;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import hf.Hf;
import hf.FxManager;
import hf.mode.GameMode;
import hf.levels.GameMechanics;

import faille.actions.McDisapear;

@:build(patchman.Build.di())
class Disapear {
    
   @:diExport
  public var action(default, null): IAction;
  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  
  public var minSid: Int;
  public var maxSid: Int;

  public function new() {
    this.action = new McDisapear(this);

    var patches = [
    
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        if (!self.fl_pause) {
          if(this.minSid != null && this.maxSid != null) {
            for(i in this.minSid...this.maxSid) {
              for (sprite in 0...self.world.scriptEngine.mcList.length) {
                if (self.world.scriptEngine.mcList[sprite].sid == i) {
                  self.world.scriptEngine.mcList[sprite].mc._alpha -= 2;
                  if(self.world.scriptEngine.mcList[sprite].mc._alpha == 0) {
                    self.world.scriptEngine.mcList[sprite].mc.removeMovieClip();
                  }
                }
              }
            }
          }
        }
      }),
        
      Ref.auto(GameMechanics.onReadComplete).before(function(hf, self: GameMechanics) {
        minSid = null;
        maxSid = null;
      })
        
    ];
      
    this.patches = FrozenArray.from(patches);
  }
}