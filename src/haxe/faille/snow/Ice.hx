package faille.snow;

import etwin.flash.filters.BitmapFilter;
import etwin.flash.MovieClip;
import etwin.ds.WeakMap;
import etwin.Obfu;
import hf.Hf;
import hf.GameManager;
import hf.levels.Data in LevelData;
import hf.levels.View;
import hf.levels.ViewManager;
import hf.mode.GameMode;
import hf.Mode;
import hf.native.BitmapData;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import custom_clips.CustomClips;

@:build(patchman.Build.di())
class Ice {
  private static var STATES(default, null): WeakMap<Mode, IceState> = new WeakMap();

  // Add support for snow
  private static var SNOW(default, null): IPatch = new PatchList([
    Ref.auto(View.scale).before(function(hf: Hf, self: View, ratio: Float): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      var scale: Float = Math.round(ratio * 100);
      state.snowMC._xscale = scale;
      state.snowMC._yscale = scale;
    }),
    Ref.auto(ViewManager.onDataReady).after(function(hf: Hf, self: ViewManager): Void {
      var state: Null<IceState> = getIceState(self.manager.current);
      if (state == null) {
        return;
      }
      if (self.fl_scrolling) {
        state.onScroll();
      }
    }),
    Ref.auto(View.moveTo).after(function(hf: Hf, self: View, x: Float, y: Float): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      state.snowMC._x = x;
      state.snowMC._y = y;
    }),
    Ref.auto(View.setFilter).after(function(hf: Hf, self: View, f: BitmapFilter): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      state.snowMC.filters = [f];
    }),
    Ref.auto(View.getSnapShot).after(function(hf: Hf, self: View, x: Float, y: Float, res: BitmapData): Void {
      var state: Null<IceState> = getIceState(self.world.manager.current);
      if (state == null) {
        return;
      }
      res.drawMC(state.snowMC, x + 10, y);
    }),
    Ref.auto(Mode.main).after(function(hf: Hf, self: Mode): Void {
      var state: Null<IceState> = getIceState(self);
      if (state == null) {
        return;
      }
      if (!self.fl_lock) {
        state.update();
      }
    })
  ]);

  @:diExport
  public var snow(default, null): IPatch;

  private var customClips(default, null): CustomClips;

  public function new(customClips: CustomClips, patches: Array<IPatch>): Void {
    this.customClips = customClips;
    patches.push(SNOW);
    this.snow = new PatchList(patches);
  }

  public function enableIce(hf: Hf, _game: Mode, density: Float = 30, speed: Float = 3.5): Void {
    var game: GameMode = cast _game;
    if(hf.GameManager.CONFIG.fl_detail) {
      var DP_BORDERS: Int = hf.Data.DP_BORDERS;
      var snowMC: MovieClip = game.depthMan.empty(DP_BORDERS);
      var state: IceState = new IceState(hf, customClips, snowMC, density, speed);
      STATES.set(game, state);
      redraw(game.world.view);
    }
  }

  public function disableIce(game: Mode): Void {
    var state = getIceState(game);
    if (state == null) return;
    state.snowMC.removeMovieClip();
    STATES.remove(game);
  }

  public static function isIce(game: Mode): Bool {
    return STATES.get(game) != null;
  }

  public static function getIceState(game: Mode): Null<IceState> {
    return STATES.get(game);
  }

  private static function redraw(view: View): Void {
    view.detachLevel();
    view.displayCurrent();
    view.moveToPreviousPos();
  }
}