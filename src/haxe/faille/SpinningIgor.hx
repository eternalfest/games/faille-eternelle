package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.entity.PlayerController;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class SpinningIgor {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var setControls: Bool;

  public function new() {
    this.actions = new Spin(this);
      
    this.setControls = false;

    this.patches = new PatchList([

      Ref.auto(PlayerController.getControls).wrap(function(hf, self: PlayerController, old): Void {
        if (!this.setControls) {
		  old(self);
        } else {
          self.player.dx = 0;
          self.player.dy = 0;
          self.player.fl_hitWall = false;
          self.player.fl_hitGround = false;
          self.player.fl_gravity = false;
          self.player.fl_friction = false;
          self.player.knock(1200);
        }
      }),
        
      Ref.auto(GameMode.onRestore).wrap(function(hf, self: GameMode, old): Void {
        if (!this.setControls) {
		  old(self);
        } else {
          self.unlock();
          var lvl = self.getPortalEntrance(self.portalId);
          for (player in self.getPlayerList()) {
            player.moveToCase(lvl.x, lvl.y);
            player.show();
            player.unshield();
          }
        }
      }),
          
    ]);
  }
}

class Spin implements IAction {
  public var name(default, null): String = Obfu.raw("spin");
  public var isVerbose(default, null): Bool = false;

  private var mod: SpinningIgor;
    
  public function new(mod: SpinningIgor) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var controls: Bool = ctx.getOptBool(Obfu.raw("n")).or(true);
    this.mod.setControls = controls;
    hf.Data.WAIT_TIMER = (controls ? hf.Data.SECOND * 99999 : hf.Data.SECOND * 8);

    return false;
  }
}
