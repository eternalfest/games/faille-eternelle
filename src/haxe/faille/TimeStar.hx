package faille;

import hf.Hf;
import hf.entity.PlayerController;
import hf.mode.GameMode;
import hf.entity.Player;
import etwin.ds.FrozenArray;
import etwin.flash.MovieClip;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import etwin.flash.Key;

@:build(patchman.Build.di())
class TimeStar {
 
  public var MAX_CHARGE: Float = 330;
  public var DECREASE: Float = 3;
  public var INCREASE: Float = 1.2;
  public var KEY: Int = 16;            // 9 = Tab, 16 = Shift
  public var charge: Float = 330;
  
  public var timerBg: MovieClip = null;
  public var timer: MovieClip = null;
  public var player: Player = null;

  @:diExport
  public var patches(default, null): IPatch;

  public function new() {
    this.patches = new PatchList([

      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
        
        var multicolore: MerlinValue = Merlin.getGlobalVar(self, Obfu.raw("multicolore")).or(new MerlinFloat(0));
        var multicolore = cast multicolore.unwrapFloat();
        
        if(multicolore == 1 && !self.fl_pause) {
          
          if(Key.isDown(KEY)) {
            if(this.charge > DECREASE) {
              hf.Timer.tmod = 0.3;
              this.charge -= DECREASE;
            }
          }
            
        }
      }),
        
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
          
        var multicolore: MerlinValue = Merlin.getGlobalVar(self, Obfu.raw("multicolore")).or(new MerlinFloat(0));
        var multicolore = cast multicolore.unwrapFloat();
        this.player = self.getPlayerList()[0];
        
        if(multicolore == 1 && !self.fl_pause) {
          
          if (!Key.isDown(KEY)) {
            if(this.charge < MAX_CHARGE) {
              this.charge += INCREASE;
            }
          }
          
          if(this.charge < MAX_CHARGE) {
            timerBg.removeMovieClip();
            timerBg = self.depthMan.attach(Obfu.raw('timerBg'), hf.Data.DP_FX);
            timerBg._yscale = 95;
            timerBg._x = player.x - hf.Data.CASE_WIDTH * 1.28;
            timerBg._y = player.y - hf.Data.CASE_HEIGHT * 1.58;
              
            timer.removeMovieClip();
            timer = self.depthMan.attach('26E,J', hf.Data.DP_FX + 1);
            timer.gotoAndStop(cast Math.floor(this.charge / 11));
            timer._xscale = 11.5;
            timer._yscale = 0.95;
            timer._x = player.x - hf.Data.CASE_WIDTH * 1.25;
            timer._y = player.y - hf.Data.CASE_HEIGHT * 2.5;

          } else {
            timerBg.removeMovieClip();
            timer.removeMovieClip();
            timerBg = null;
            timer = null;
          }
        }
        if(multicolore == 0) {
          timerBg.removeMovieClip();
          timer.removeMovieClip();
          timerBg = null;
          timer = null; 
        }
          
      }),
        
      Ref.auto(Player.resurrect).before(function(hf: Hf, self: Player): Void {
        this.charge = MAX_CHARGE;
      }),
        
    ]);
  }
}