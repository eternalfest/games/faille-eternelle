package faille.bads;

import etwin.flash.filters.ColorMatrixFilter;
import hf.Entity;
import hf.mode.GameMode;
import hf.Animation;

import color_matrix.ColorMatrix;
import custom_clips.CustomClips;

import hf.mode.Adventure;
import faille.items.Avis;
import etwin.ds.WeakMap;
import etwin.flash.filters.GlowFilter;
import etwin.flash.filters.BitmapFilter;

@:hfTemplate
class Mirabelle extends hf.entity.bad.Jumper {

  public static var BAD_ID(default, never): Int = 22;
  public static var TIMER_TING = 5 + Std.random(6);

  private static var FILTER: ColorMatrixFilter = ColorMatrix.of([
    2, 0, 0, 0, 0,
    0, 1.5, 0, 0, 0,
    0, 0, 1, 0, 0,
    0, 0, 0, 1, 0
  ]).toFilter();

  private static var REUNITING_DELAY: Float = 12;

  public var clips: CustomClips;
  public var isBig: Bool = true;
  private var reunitingDelay: Float = 0;
  public var tingTimer: Float = TIMER_TING;
  public var ting: Animation = null;

  @:keep
  private function new() {
    super(null);
    this.animFactor = 0.65;
    this.setJumpUp(5);
    this.setJumpH(100);
    this.setClimb(100, 3);
    this.setFall(20);
  }

  public static function attach(clips: CustomClips, g: GameMode, x: Float, y: Float, isBig: Bool): Mirabelle {
    var hf = g.root;
    var symbol = hf.Data.LINKAGES[hf.Data.BAD_ABRICOT];
    var bad = clips.attach(g.depthMan, symbol, hf.Data.DP_BADS, Mirabelle.getClass(hf));
    bad.clips = clips;
    bad.filters = [FILTER];
    bad.isBig = isBig;
    bad.initBad(g, x, y);
    bad.scale(115);
    if (!isBig) {
      bad.scale(85);
    }
    return bad;
  }

  private function makeChild(x: Float, y: Float, isMain: Bool): Mirabelle {
    var child = Mirabelle.attach(this.clips, this.game, x, y, false);
    child.dir = isMain ? this.dir : -this.dir;
    if (isMain) {
      child.fl_ninFoe = this.fl_ninFoe;
      child.fl_ninFriend = this.fl_ninFriend;
    }
    child.anger = this.anger;
    child.updateSpeed();
    child.reunitingDelay = REUNITING_DELAY;
    child.fl_moveable = false;
    var adventure: hf.mode.Adventure = cast this.game;
    adventure.perfectOrder.push(child.uniqId);
    return child;
  }

  public override function freeze(timer: Float): Void {
    this.fl_moveable = true;
    if (this.canSplit()) {
      this.split();
    } else {
      super.freeze(timer);
    }
  }

  public override function knock(timer: Float): Void {
    if (this.canSplit() && 
        (timer != 120 &&     // Tzongre
         timer != 320 &&     // Bibelot en argent
         timer != 360 &&     // Ballon de banquise
         timer != 32 * 3)) { // Métronome
      this.split();
    } else {
      super.knock(timer);
    }
  }

  public override function forceKill(dx: Null<Float>): Void {
    this.reunitingDelay = 0;
    this.killHit(dx);
  }

  public override function killHit(dx: Null<Float>): Void {
    if (this.reunitingDelay > 0) {
      return; // ignore
    } else if (this.canSplit()) {
      this.split();
    } else {
      super.killHit(dx);
    }
  }

  private inline function canSplit(): Bool {
    return this.isBig && !this.fl_kill;
  }

  private function split(): Void {
    var hf = this.game.root;
    var isWanted: Bool = false;
    for(player in this.game.getPlayerList()) {
      if (player.specialMan.actives[127]) {
        if(Avis.CHOSEN_BADS.get(this) == 0) {
          ++player.lives;
          this.game.gi.setLives(player.pid, player.lives);
        }
      }
    }
    var adventure: hf.mode.Adventure = cast this.game;
    adventure.perfectOrder.remove(this.uniqId);
    ++adventure.perfectCount;
    this.makeChild(this.x, this.y - hf.Data.CASE_HEIGHT, true);
    this.makeChild(this.x, this.y - hf.Data.CASE_HEIGHT, false);
    this.game.fxMan.attachExplosion(this.x, this.y - 5, 25);
    this.game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, this.x, this.y, Std.random(4));
    this.destroy();
  }

  private function reunite(other: Mirabelle): Void {
    var hf = this.game.root;
    var mx = (this.x + other.x)/2;
    var my = (this.y + other.y)/2;

    var combined = Mirabelle.attach(this.clips, this.game, mx, my - hf.Data.CASE_HEIGHT, true);
    if(this.dir == other.dir) {
      combined.dir = this.dir;
    } else if (this.anger != other.anger) {
      combined.dir = this.anger > other.anger ? this.dir : other.dir;
    }

    combined.fl_ninFriend = this.fl_ninFriend || other.fl_ninFriend;
    if (this.fl_ninFoe || other.fl_ninFoe) {
      combined.fl_ninFoe = true;
      combined.fl_ninFriend = false;
    }

    combined.anger = Std.int(Math.max(this.anger, other.anger));
    combined.updateSpeed();

    var adventure: hf.mode.Adventure = cast this.game;
    adventure.perfectOrder.push(combined.uniqId);
    adventure.perfectOrder.remove(this.uniqId);
    adventure.perfectOrder.remove(other.uniqId);
    --adventure.perfectCount;

    this.game.fxMan.attachExplosion(mx, my - 10, 25);
    this.game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, mx, my, Std.random(4));
    this.destroy();
    other.destroy();
  }

  public override function hit(e: Entity): Void {
    if(Std.is(e, this.game.root.entity.Bad)) {
      var e: hf.entity.Bad = cast e;
      if(!this.fl_freeze && e.iceMc._name != null && this.getDepth() > e.iceMc.getDepth()) {
        this.swapDepths(e.iceMc.getDepth());
      }

      if (Std.is(e, Mirabelle)) {
        var e: Mirabelle = cast e;
        if (!this.isBig && !e.isBig
            && this.getDepth() > e.getDepth()
            && this.reunitingDelay <= 0 && e.reunitingDelay <= 0
            && !this.fl_knock && !e.fl_knock
            && !this.fl_freeze && !e.fl_freeze
            && !this.fl_kill && !e.fl_kill
            && this.fl_stable && e.fl_stable) {
          this.reunite(e);
          return;
        }
      }
    }
    
    super.hit(e);
  }

  public override function update(): Void {
    if (!this.fl_kill && this.reunitingDelay > 0) {
      this.fl_moveable = false;
      this.reunitingDelay -= this.game.root.Timer.tmod;
      if (this.dy <= -10) {
        this.dy = 0;
      }
    } else {
      this.fl_moveable = true;
    }
    super.update();
  }

  public override function endUpdate(): Void {
    if (this.tingTimer > 0) {
      this.tingTimer -= this.game.root.Timer.tmod;
    } else {
      this.ting.destroy();
      this.tingTimer = TIMER_TING;
      this.ting = this.game.fxMan.attachFx(this.x + Std.random(15) * (Std.random(2) * 2 - 1), this.y - Std.random(10), "hammer_fx_star");
      this.ting.mc._xscale = 70;
      this.ting.mc._yscale = 70;
    }
    super.endUpdate();
  }
}
