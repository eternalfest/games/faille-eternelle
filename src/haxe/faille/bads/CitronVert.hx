package faille.bads;

import etwin.flash.filters.ColorMatrixFilter;
import hf.mode.GameMode;

import color_matrix.ColorMatrix;
import custom_clips.CustomClips;

private typedef ShootProperties = {
  angle: Float,
  increment: Float,
  remaining: Int,
};

@:hfTemplate
class CitronVert extends hf.entity.bad.Shooter {

  public static var BAD_ID(default, never): Int = 21;

  private static var FILTER: ColorMatrixFilter = ColorMatrix.fromHsv(-65, 0.85, 0.8).toFilter();
  	
	private static var NB_SHOOTS: Int = 3;
	private static var SHOOT_ANGLE_INCR: Float = 15;
	private static var SHOOT_SPEED: Float = 6;
	private static var SHOOT_DELAY: Float = 8;
	private static var ATTACK_PROBA: Int = 1;
  private static var ATTACK_DELAY: Float = 40;
  
  private var shootProps: Null<ShootProperties> = null;
  
  @:keep
  private function new() {
    super(null);
    this.setJumpH(50);
    this.setShoot(ATTACK_PROBA);
    this.initShooter(50, 20);
  }

  public static function attach(clips: CustomClips, g: GameMode, x: Float, y: Float): CitronVert {
    var hf = g.root;
    var symbol = hf.Data.LINKAGES[hf.Data.BAD_CITRON];
    var bad = clips.attach(g.depthMan, symbol, hf.Data.DP_BADS, CitronVert.getClass(hf));
    bad.filters = [FILTER];
    bad.initBad(g, x, y);
    return bad;
  }

  public override function onShoot(): Void {
    var hf = this.game.root;

    if (this.shootProps == null) {
      this.shootProps = getShootProperties();
      if (this.shootProps == null) {
        return;
      }
    }

    // Spawn the shoot
    var shoot = hf.entity.shoot.Zeste.attach(this.game, this.x, this.y - 10);
    shoot.moveToAng(this.shootProps.angle, SHOOT_SPEED);
    shoot.rotation = this.shootProps.angle + 90;
    shoot.filters = [FILTER];

    this.shootProps.remaining--;
    this.shootProps.angle += this.shootProps.increment;
    if (this.shootProps.remaining > 0) {
      // Schedule next shoot
      var old = this.shootPreparation;
      this.shootPreparation = SHOOT_DELAY;
      this.startShoot();
      this.shootPreparation = old;
    } else {
      // Done
      this.shootProps = null;
      this.shootCD = ATTACK_DELAY;
    }
  }

  public override function wakeUp() {
    super.wakeUp();
    this.shootProps = null;
  }

  public override function melt() { 
    super.melt(); 
    this.shootProps = null;
  }

  private function getShootProperties(): ShootProperties {
    var hf = this.game.root;
    var p = this.game.getOne(hf.Data.PLAYER);
    if (p == null) {
      return null;
    }

    var props = {
      angle: Math.atan2(p.y - this.y, p.x - this.x) * 180/Math.PI,
      increment: SHOOT_ANGLE_INCR * (Std.random(2) == 0 ? -1 : 1),
      remaining: NB_SHOOTS,
    };

    props.angle -= (props.remaining - 1) * props.increment * 0.5;
    return props;
  }
}

