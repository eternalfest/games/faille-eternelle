package faille.bads;

import etwin.flash.MovieClip;
import color_matrix.ColorMatrix;
import hf.mode.GameMode;

import custom_clips.CustomClips;

@:hfTemplate
@:allow(faille.bads.R2Frag)
class R2 extends hf.entity.bad.Shooter {
  public static var BAD_ID(default, never): Int = 23;

  private static var FILTER = ColorMatrix.fromHsv(66, 0.53, 1).toFilter();
  private static var BOMB_RADIUS: Float = 82;
  private static var FRAGS: Int = 12;
  private static var FRAG_DIST: Float = 90;
  private static var FRAG_SPEED: Float = 0.03;

  private var clips: CustomClips;
  private var tx: Null<Float>;
  private var ty: Null<Float>;
  private var isPhased: Bool;
  private var frags: Array<R2Frag>;

  @:keep
  public function new(hf: hf.Hf): Void {
    super(hf);
    this.speed *= .8;
    this.setJumpUp(3);
    this.setJumpDown(6);
    this.setJumpH(100);
    this.setClimb(100, 3);
    this.setFall(20);
    this.setShoot(0.7);
    this.initShooter(20, 12);
    this.isPhased = false;
    this.frags = [];
  }

  public static function attach(clips: CustomClips, game: GameMode, x: Float, y: Float): R2 {
    var symbol = "$r2";
    game.root.Std.registerClass(symbol, R2.getClass(game.root));
    var r2: R2 = cast game.depthMan.attach(symbol, game.root.Data.DP_BADS);
    r2.clips = clips;
    r2.initBad(game, x, y);
    return r2;
  }

  private function makeFrags(): Void {
    while(this.frags.length < FRAGS) {
      this.frags.push(R2Frag.attach(this, this.frags.length));
    }
  }

  private function spawnParticles(n: Int): Void {
    var hf = this.game.root;
    var particles = this.game.getList(hf.Data.FX);
    var lastId = particles.length > 0 ? particles[particles.length - 1].uniqId : null;

    this.game.fxMan.inGameParticles(hf.Data.PARTICLE_FRAMB_SMALL, this.x, this.y, n);

    // Find the spawned particles and recolor them
    var particles = this.game.getList(hf.Data.FX);
    for (i in 0...n) {
      var p = particles[particles.length - i - 1];
      if (p.uniqId == lastId) {
        break;
      } else {
        p.filters = [FILTER];
      }
    }
  }

  private function phaseOut(): Void {
    if (this.isPhased) return;

    var hf = this.game.root;
    this.spawnParticles(Std.random(3) + 2);
    this.game.fxMan.attachExplosion(this.x, this.y, 40);
    this.isPhased = true;
    this.dx = 0;
    this.dy = 0;
    this.disableShooter();
    this.disableAnimator();
    this.hide();
  }

  private function phaseIn(): Void {
    if (!this.isPhased) return;

    for (f in this.frags) {
      f.park();
    }

    this.isPhased = false;
    this.enableAnimator();
    this.enableShooter();
    this.show();
    this.moveTo(this.tx, this.ty);
    this.game.fxMan.attachExplosion(this.x, this.y, 40);
  }

  override public function destroy(): Void {
    for (f in this.frags) {
      f.destroy();
    }

    super.destroy();
  }

  override public function freeze(d: Float): Void {
    this.phaseIn();
    super.freeze(d);
  }

  override public function isReady(): Bool {
    return super.isReady() && !this.isPhased;
  }

  override public function killHit(dx: Null<Float>): Void {
    this.phaseIn();
    super.killHit(dx);
  }

  override public function knock(d: Float): Void {
    this.phaseIn();
    super.knock(d);
  }

  override public function onHitGround(h: Float): Void {
    var hf = this.game.root;
    super.onHitGround(h);
    if (h >= hf.Data.CASE_HEIGHT * 2) {
      this.spawnParticles(Std.random(4) + 2);
    }
  }

  override public function onShoot(): Void {
    this.tx = x;
    this.ty = y;
    this.phaseOut();
    this.makeFrags();

    var dist = FRAG_DIST;
    var speed = FRAG_SPEED * (1 + this.anger * 0.25);

    var eye1 = Std.random(this.frags.length);
    var eye2 = eye1;
    while (eye2 == eye1) {
      eye2 = Std.random(this.frags.length);
    }

    for (i in 0...this.frags.length) {
      var angle = 2*i*Math.PI / this.frags.length;
      var skin = i == eye1 ? "5" : i == eye2 ? "6" : "1";
      this.frags[i].activate(angle, dist, speed, skin);
    }

    this.frags[eye1].gotoAndStop("5");
    this.frags[eye2].gotoAndStop("6");
  }

  override public function update(): Void {
    if (!this._visible) {
      this.moveTo(100, -200);
      if (!this.isHealthy()) {
        this.phaseIn();
      }
    } else {
      var bombs = this.game.getClose(this.game.root.Data.PLAYER_BOMB, this.x, this.y, BOMB_RADIUS, false);
      if (bombs.length > 0 && this.isReady()) {
        this.startShoot();
      }
    }
    super.update();
  }

  override public function walk(): Void {
    if (!this.isPhased) {
      super.walk();
    }
  }
}
