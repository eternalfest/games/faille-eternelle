package faille.bads;

import faille.items.Montre;

@:hfTemplate
@:allow(faille.bads.R2)
class R2Frag extends hf.Entity {
  private var isParked: Bool = true;
  private var isFinisher: Bool = false;
  private var owner: R2;
  private var angle: Float;
  private var dist: Float;
  private var speed: Float;
  private var timeAnim: Float = 0;

  @:keep
  private function new() {
    super(null);
  }

  public static function attach(owner: R2, id: Int): R2Frag {
    var hf = owner.game.root;

    var frag = owner.clips.attach(owner.game.depthMan, "hammer_shoot_framBall2", hf.Data.DP_BADS, R2Frag.getClass(hf));
    frag.init(owner.game);
    frag.filters = [R2.FILTER];
    frag.owner = owner;
    frag.isFinisher = id == 0;
    frag.park();
    return frag;
  }

  private function park(): Void {
    this.isParked = true;
    this.hide();
    this.moveTo(100, -500);
  }

  private function activate(angle: Float, dist: Float, speed: Float, skin: String) {
    this.isParked = false;
    this.gotoAndStop(skin);
    this.show();
    this.moveTo(owner.tx, owner.ty);
    this.angle = angle;
    this.dist = dist;
    this.speed = speed;
    this.timeAnim = 1;
  }

  public override function update(): Void {
    if (!Montre.ACTIVE) {
      if (this.isParked) {
        return;
      }

      if (this.timeAnim <= 0 && this.isFinisher) {
        this.owner.phaseIn();
      }

      var pos = Math.sin(this.timeAnim * Math.PI) * this.dist;
      this.timeAnim -= this.game.root.Timer.tmod * this.speed;

      this.x = this.owner.tx + Math.cos(this.angle) * pos;
      this.y = this.owner.ty + Math.sin(this.angle) * pos;
      this.updateCoords();

      for (p in this.game.getPlayerList()) {
        if (p.fl_kill)
          continue;
        var diffX: Float = this.x - p.x;
        var diffY: Float = this.y - (p.y - 10);
        if (diffX * diffX + diffY * diffY < 150)
          p.killHit(Math.cos(this.angle));
      }

      super.update();
    }
  }
}
