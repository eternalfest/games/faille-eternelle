package faille.bads;

import hf.Entity;
import etwin.flash.filters.ColorMatrixFilter;
import hf.mode.GameMode;

import color_matrix.ColorMatrix;
import custom_clips.CustomClips;

@:hfTemplate
class MechaBombi extends hf.entity.bad.walker.Bombe {

  public static var BAD_ID(default, never): Int = 24;

  private static var FILTER: ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1).toFilter();
  private static var RAY_SIZE = 16;
  private static var EXPERT_RAY_SIZE = 50;
  private static var EXPLOSION_TIME = 20;
  
  @:keep
  private function new() {
    super(null);
  }

  public static function attach(clips: CustomClips, g: GameMode, x: Float, y: Float): MechaBombi {
    var hf = g.root;
    var symbol = hf.Data.LINKAGES[hf.Data.BAD_BOMBE];
    var bad = clips.attach(g.depthMan, symbol, hf.Data.DP_BADS, MechaBombi.getClass(hf));
    bad.filters = [FILTER];
    bad.initBad(g, x, y);
    return bad;
  }

  public override function trigger(): Void {
    if (this.fl_overheat) {
      return;
    }
    super.trigger();
    this.updateLifeTimer(EXPLOSION_TIME);
  }

  public override function selfDestruct(): Void {
    var hf = this.game.root;
    var rayX = this.x;
    var rayY = this.y - 10;
    var ballons = this.game.getList(hf.Data.SOCCERBALL);

    // Do graphical effects.
    this.game.fxMan.attachExplodeZone(rayX, rayY, 30);
    for (_ in 0...2) {
      var ray = this.game.depthMan.attach("hammer_fx_death", hf.Data.DP_FX);
      ray._rotation = -90;
      ray._x = hf.Data.GAME_WIDTH / 2;
      ray._y = rayY;
      if (this.game.fl_bombExpert || this.isDynamiteActive()) {
        ray._xscale = 260;
      } else {
        ray._xscale = 130;
      }

      ray = this.game.depthMan.attach("hammer_fx_death", hf.Data.DP_FX);
      ray._x = rayX;
      ray._y = hf.Data.GAME_HEIGHT / 2;
      if (this.game.fl_bombExpert || this.isDynamiteActive()) {
        ray._xscale = 260;
      } else {
        ray._xscale = 130;
      }
    }

    // Kill all players touching the rays.
    for (p in this.game.getPlayerList()) {
      if(this.isInRange(rayX, rayY, p) == 1) {
        p.killHit(p.x < rayX ? -1 : 1);
      } else if(this.isInRange(rayX, rayY, p) == 2) {
        p.killHit(Std.random(2) == 0 ? -1 : 1);
      }
    }

    // Kill all soccerfest ball touching the rays.
    for (i in 0...ballons.length) {
      var dx = Math.abs(ballons[i].x - rayX);
      var dy = Math.abs(ballons[i].y - rayY);

      if (this.isInRange(rayX, rayY, ballons[i]) > 0 ) {
        ballons[i].destroy();
        this.game.fxMan.attachFx(ballons[i].x, ballons[i].y - hf.Data.CASE_HEIGHT / 2, "hammer_fx_pop");
      }
    }

    // Kill ourselves.
    this.game.soundMan.playSound("sound_bomb_black", hf.Data.CHAN_BOMB);
    this.dropReward();
    this.game.fxMan.inGameParticles(hf.Data.PARTICLE_METAL, this.x, this.y, Std.random(4) + 5);
    this.game.fxMan.inGameParticles(hf.Data.PARTICLE_SPARK, this.x, this.y, Std.random(4));
    this.onKill();
    this.destroy();
  }

  // Return 0 not in range, 1 dy < RAY_SIZE, 2 dx < RAY_SIZE
  public function isInRange(rayX: Float, rayY: Float, e: Entity): Int {
    var dx = Math.abs(e.x - rayX);
    var dy = Math.abs(e.y - rayY);
    var res = 0;

    if (this.game.fl_bombExpert || this.isDynamiteActive()) {
      faille.bads.MechaBombi.RAY_SIZE = faille.bads.MechaBombi.EXPERT_RAY_SIZE;
    } else {
      faille.bads.MechaBombi.RAY_SIZE = 16;
    }

    if (dy < RAY_SIZE) {
      res = 1;
    } else if (dx < RAY_SIZE) {
      res = 2;
    } 

    return res;
  }

  // Check if the item "Dynamite instable" is active
  public function isDynamiteActive(): Bool {
    var isDynamiteActive = false;
    for (p in this.game.getPlayerList()) {
      if (p.specialMan.actives[130]) {
        isDynamiteActive = true;
      }
    }
    return isDynamiteActive;
  }

}