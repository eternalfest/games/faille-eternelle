package faille.actions;

import hf.mode.GameMode;
import hf.Entity;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class McGears implements IAction {
  public var name(default, null): String = Obfu.raw("mc_gear");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var xr: Int = ctx.getOptInt(Obfu.raw("xr")).or(null);
    var yr: Int = ctx.getOptInt(Obfu.raw("yr")).or(null);
    var scaleX: Int = ctx.getOptInt(Obfu.raw("sx")).or(100);
	var scaleY: Int = ctx.getOptInt(Obfu.raw("sy")).or(100);
    var sid: Int = ctx.getOptInt(Obfu.raw("sid")).or(0);
    var back: Bool = ctx.getOptBool(Obfu.raw("back")).or(false);
    var name: String = ctx.getString(Obfu.raw("n"));
    var rotation: Int = ctx.getOptInt(Obfu.raw("rot")).or(null);
    var flip: Bool = ctx.getOptBool(Obfu.raw("flip")).or(false);
    var animated: Bool = ctx.getOptBool(Obfu.raw("p")).or(false);
    var rotSpeed: Int = ctx.getOptInt(Obfu.raw("rotSpeed")).or(null);
    var onPlayer: Bool = ctx.getOptBool(Obfu.raw("onPlayer")).or(false);

    var game: GameMode = ctx.getGame();

    game.world.scriptEngine.killById(sid);
    var posX: Float;
    var posY: Float;

    if (xr == null) {
      posX = x*20 + 10;
      posY = y*20 + 10;
    } else {
      posX = xr;
      posY = yr;
    }

    posX = game.flipCoordReal(posX);

    if (game.fl_mirror) {
      posX += 20;
    }

    var sprite = game.world.view.attachSprite(name, posX, posY, back);

    if(rotation != null) {
      if (game.fl_mirror) {
        sprite._rotation = -rotation;
      } else {
        sprite._rotation = rotation;
      }
    }

    sprite._xscale = scaleX;
    sprite._yscale = scaleY;
      
    if(flip) {
      sprite._xscale *= -1;
    }

    if (name == "$torch") {
      if (!game.world.scriptEngine.fl_firstTorch)
        game.clearExtraHoles();
      game.addHole(posX + 20 * 0.5, posY - 20 * 0.5, 180);
      game.updateDarkness();
      game.world.scriptEngine.fl_firstTorch = true;
    }
      
    if(onPlayer) {
      for(player in game.getPlayerList()) {
        sprite._x = player.x;
        sprite._y = player.y;
      }
    }
  
    if (animated) {
      sprite.play();
      if(rotation != null) {
        sprite._x = x*20;
        sprite._y = y*20 + 20;
      }
    } else {
      sprite.stop();
      untyped { sprite.sub.stop(); }
    }

    game.world.scriptEngine.mcList.push({sid: sid, mc: (cast sprite)});

    return false;
  }
}