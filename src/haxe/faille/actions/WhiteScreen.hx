package faille.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.Entity;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class WhiteScreen implements IAction {
  public var name(default, null): String = Obfu.raw("whiteScreen");
  public var isVerbose(default, null): Bool = false;

  private var mod: FadingScreen;
    
  public function new(mod: FadingScreen) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    mod.active = true;

    return false;
  }
}