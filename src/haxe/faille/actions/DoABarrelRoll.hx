package faille.actions;

import faille.BarrelRollingEntity;
import etwin.Obfu;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;

class DoABarrelRoll implements IAction {

  public var name(default, null): String = Obfu.raw("doABarrelRoll");
  public var isVerbose(default, null): Bool = false;

  public function new(): Void {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var x1: Float = game.root.Entity.x_ctr(ctx.getInt(Obfu.raw("x1")));
    var y1: Float = game.root.Entity.y_ctr(ctx.getInt(Obfu.raw("y1")));
    var x2: Float = game.root.Entity.x_ctr(ctx.getInt(Obfu.raw("x2")));
    var y2: Float = game.root.Entity.y_ctr(ctx.getInt(Obfu.raw("y2")));
    var name: String = ctx.getString(Obfu.raw("n"));
    var h: Float = ctx.getOptInt(Obfu.raw("h")).or(0);
    var s: Float = ctx.getOptInt(Obfu.raw("s")).or(1);
    var v: Float = ctx.getOptInt(Obfu.raw("v")).or(1);
    BarrelRollingEntity.attach(game, x1, y1, x2, y2, name, h, s, v);
    return false;
  }

}
