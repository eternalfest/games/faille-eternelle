package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class StartShakeScreen implements IAction {
  public var name(default, null): String = Obfu.raw("startShakeScreen");
  public var isVerbose(default, null): Bool = false;

  public var intensity: Int;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var value: Int = ctx.getOptInt(Obfu.raw("v")).or(10);

  	var game: GameMode = ctx.getGame();
    intensity = value;

    game.shake(1, intensity);

    return false;
  }
}