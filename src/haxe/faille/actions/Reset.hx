package faille.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class Reset implements IAction {
  public var name(default, null): String = Obfu.raw("reset");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
      
    game.world.fl_read[8] = false;
      
    return false;
  }
}