package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class AddLives implements IAction {
  public var name(default, null): String = Obfu.raw("addLives");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();
    var lives: Int = ctx.getOptInt(Obfu.raw("add")).or(0);
  	
    for (player in game.getPlayerList()) {
      player.lives += lives;
      game.gi.setLives(player.pid, player.lives);
    }

    return false;
  }
}