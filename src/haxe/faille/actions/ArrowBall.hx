package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ArrowBall implements IAction {
  public var name(default, null): String = Obfu.raw("arrowBall");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();

  	var playerList = game.getPlayerList();
    for (i in 0...playerList.length) {
      var p = playerList[i];
      p.changeWeapon(6);
    }

    return false;
  }
}