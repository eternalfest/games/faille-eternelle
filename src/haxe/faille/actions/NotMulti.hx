package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class NotMulti implements IAction {
  public var name(default, null): String = Obfu.raw("notMulti");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    if(game.getPlayerList().length <= 1) {
      return true;   
    } else {
      return false;
    }
  }
}