package faille.actions;

import hf.mode.GameMode;
import hf.Hf;
import hf.levels.ScriptEngine;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class MDraw implements IAction {
  public var name(default, null): String = Obfu.raw("draw");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();
  	var hf: Hf = ctx.getHf();

  	var shape: Float = ctx.getOptInt(Obfu.raw("shape")).or(0);
  	var x: Int = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Int = ctx.getOptInt(Obfu.raw("y")).or(0);

    // Dessine un X ou un O à l'endroit de la case
    if(shape == 2) {
    	game.world.forceCase(x-1, y, 0);
    	game.world.forceCase(x-1, y-1, 0);
    	game.world.forceCase(x,   y-2, 0);
    	game.world.forceCase(x+1, y-2, 0);
    	game.world.forceCase(x+2, y, 0);
    	game.world.forceCase(x+2, y-1, 0);
    	game.world.forceCase(x+1, y+1, 0);
    	game.world.forceCase(x,   y+1, 0);

    	game.world.forceCase(x,   y, hf.Data.GROUND);
    	game.world.forceCase(x+1, y, hf.Data.GROUND);
    	game.world.forceCase(x,   y-1, hf.Data.GROUND);
    	game.world.forceCase(x+1, y-1, hf.Data.GROUND);
    } else if (shape == 1) {
    	game.world.forceCase(x-1, y+1, 0);
    	game.world.forceCase(x-1, y-2, 0);
    	game.world.forceCase(x+2, y+1, 0);
    	game.world.forceCase(x+2, y-2, 0);
    }
    game.world.scriptEngine.fl_redraw = true;

    return false;
  }
}