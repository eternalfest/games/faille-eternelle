package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class EnterExt implements IAction {
  public var name(default, null): String = Obfu.raw("enter_ext");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var xTile1: Float = ctx.getOptInt(Obfu.raw("x1")).or(0);
    var yTile1: Float = ctx.getOptInt(Obfu.raw("y1")).or(0);
    var xTile2: Float = ctx.getOptInt(Obfu.raw("x2")).or(0);
    var yTile2: Float = ctx.getOptInt(Obfu.raw("y2")).or(0);

    var game: GameMode = ctx.getGame();
    xTile1 = game.flipCoordCase(xTile1);
    xTile2 = game.flipCoordCase(xTile2);

  	var x1 = Math.min(xTile1, xTile2);
    var x2 = Math.max(xTile1, xTile2);
    var y1 = Math.min(yTile1, yTile2);
    var y2 = Math.max(yTile1, yTile2);

    for(player in game.getPlayerList()) {
      if(!player.fl_kill && x1 <= player.cx && player.cx <= x2 && y1 <= player.cy && player.cy <= y2)
        return true;
    }

    return false;
  }
}