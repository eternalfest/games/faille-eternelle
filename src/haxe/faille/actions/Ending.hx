package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class Ending implements IAction {
  public var name(default, null): String = Obfu.raw("ending");
  public var isVerbose(default, null): Bool = false;

  public var coords: Array<{ x: Int, y: Int }> = [];

  public function new() {
    // Remplir le tableau des coordonnées
    for (x in 0...20) {
      for (y in 0...26) {
        if((x == 9 && y == 15) ||
           (x == 10 && y == 15) ||
           (x == 9 && y == 14) ||
           (x == 10 && y == 14)) {
          continue;
        } else {
          this.coords.push({x: x, y: y});
        }
      }
    }
  }

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();
    
    if (coords.length > 0) {
      var i = Std.random(coords.length);
      var coord = coords[i];

      coords[i] = coords[coords.length - 1];
      coords.pop();

      var blackTile = game.depthMan.attach(Obfu.raw('black'), game.root.Data.DP_FX);
      blackTile._x = coord.x * 20;
      blackTile._y = coord.y * 20;

      if (coords.length > 0) {
        var i = Std.random(coords.length);
        var coord = coords[i];

        coords[i] = coords[coords.length - 1];
        coords.pop();

        var blackTile = game.depthMan.attach(Obfu.raw('black'), game.root.Data.DP_FX);
        blackTile._x = coord.x * 20;
        blackTile._y = coord.y * 20;

        if (coords.length > 0) {
          var i = Std.random(coords.length);
          var coord = coords[i];

          coords[i] = coords[coords.length - 1];
          coords.pop();

          var blackTile = game.depthMan.attach(Obfu.raw('black'), game.root.Data.DP_FX);
          blackTile._x = coord.x * 20;
          blackTile._y = coord.y * 20;

          if (coords.length > 0) {
            var i = Std.random(coords.length);
            var coord = coords[i];

            coords[i] = coords[coords.length - 1];
            coords.pop();

            var blackTile = game.depthMan.attach(Obfu.raw('black'), game.root.Data.DP_FX);
            blackTile._x = coord.x * 20;
            blackTile._y = coord.y * 20;

            if (coords.length > 0) {
              var i = Std.random(coords.length);
              var coord = coords[i];

              coords[i] = coords[coords.length - 1];
              coords.pop();

              var blackTile = game.depthMan.attach(Obfu.raw('black'), game.root.Data.DP_FX);
              blackTile._x = coord.x * 20;
              blackTile._y = coord.y * 20;
            }
          }
        }
      }
    }
    return false;
  }
}