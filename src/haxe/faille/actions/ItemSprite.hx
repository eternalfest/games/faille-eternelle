package faille.actions;

import hf.mode.GameMode;
import hf.Entity;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class ItemSprite implements IAction {
  public var name(default, null): String = Obfu.raw("mc_item");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var xr: Int = ctx.getOptInt(Obfu.raw("xr")).or(null);
    var yr: Int = ctx.getOptInt(Obfu.raw("yr")).or(null);
    var sid: Int = ctx.getOptInt(Obfu.raw("sid")).or(0);
    var id: Int = ctx.getOptInt(Obfu.raw("id")).or(0);

    var game: GameMode = ctx.getGame();

    ctx.killById(sid);
    var posX: Float;
    var posY: Float;

    if (xr == null) {
      posX = x*20 + 30;
      posY = y*20 + 20;
    } else {
      posX = xr;
      posY = yr;
    }

    if(id == 1295)
      posY += 7;
    posX = game.flipCoordReal(posX);

    if (game.fl_mirror) {
      posX += 20;
    }

    var sprite;

    if(id >= 1000) {
      sprite = game.world.view.attachSprite('hammer_item_score', posX, posY, true);
      var subid = id - 1000;
      if(subid == 288) {
        subid = 169;
      }
      if(subid == 295) {
        subid = 51;
        sprite._xscale = 130;
        sprite._yscale = 130;
      }
      sprite.gotoAndStop('' + (subid + 1));
      if(subid == 289) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-35, 1, 1.3).toFilter();
        sprite.filters = [filter];
      }
      if(subid == 290) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-60, 1, 1.8).toFilter();
        sprite.filters = [filter];
      }
      if(subid == 291) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(-136, 1, 1).toFilter();
        sprite.filters = [filter];
      }
      if(subid == 292) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(184, 1, 1.2).toFilter();
        sprite.filters = [filter];
      }
      if(subid == 293) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(142, 1, 1).toFilter();
        sprite.filters = [filter];
      }
      if(subid == 294) {
        subid = 169;
        sprite.gotoAndStop('' + (subid + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(56, 1, 1).toFilter();
        sprite.filters = [filter];
      }
      
    } else {
      sprite = game.world.view.attachSprite('hammer_item_special', posX, posY, true);
      sprite.gotoAndStop('' + (id + 1));
      if(id == 146) {
        var id = 103;
        sprite.gotoAndStop('' + (id + 1));
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(200, 1, 1).toFilter();
        sprite.filters = [filter];
      }
    }
    
    sprite.stop();
    untyped { sprite.sub.stop(); }
    game.world.scriptEngine.mcList.push({sid: sid, mc: cast sprite});

    return false;
  }
}