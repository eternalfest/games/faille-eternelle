package faille.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.Entity;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class CustomVortex implements IAction {
  public var name(default, null): String = Obfu.raw("openCustomPortal");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var pid: Int = ctx.getOptInt(Obfu.raw("pid")).or(0);
    var h: Float = ctx.getOptInt(Obfu.raw("h")).or(0);
    var s: Float = ctx.getOptInt(Obfu.raw("s")).or(100);
    var v: Float = ctx.getOptInt(Obfu.raw("v")).or(100);
    var back: Bool = ctx.getOptBool(Obfu.raw("back")).or(false);

    if (game.portalMcList[pid] != null) {
      return false;
    } else {
      game.world.scriptEngine.insertPortal(x, y, pid);
      var x_ctr = hf.Entity.x_ctr(game.flipCoordCase(x));
      var y_ctr = hf.Entity.y_ctr(y) - hf.Data.CASE_HEIGHT * 0.5;
      var vortex = game.depthMan.attach('hammer_portal', back ? hf.Data.DP_BACK_LAYER : hf.Data.DP_SPRITE_BACK_LAYER);
      vortex._x = x_ctr;
      vortex._y = y_ctr;
      game.fxMan.attachExplosion(x_ctr, y_ctr, 40);
      game.fxMan.inGameParticles(hf.Data.PARTICLE_PORTAL, x_ctr, y_ctr, 5);
      game.fxMan.attachShine(x_ctr, y_ctr);
      game.portalMcList[pid] = {x: x_ctr, y: y_ctr, mc: vortex, cpt: 0};
      var filter: ColorMatrixFilter = ColorMatrix.fromHsv(h, s/100, v/100).toFilter();
      vortex.filters = [filter];    
	}
    return false;
  }
}