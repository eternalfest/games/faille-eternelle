package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class SetItems implements IAction {
  public var name(default, null): String = Obfu.raw("setItems");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var spec_x: Float = ctx.getOptInt(Obfu.raw("spx")).or(0);
    var spec_y: Float = ctx.getOptInt(Obfu.raw("spy")).or(0);
    var score_x: Float = ctx.getOptInt(Obfu.raw("scx")).or(0);
    var score_y: Float = ctx.getOptInt(Obfu.raw("scy")).or(0);

  	var game: GameMode = ctx.getGame();

    var lvlData = game.world.current;
    lvlData.__dollar__specialSlots = new Array();
    lvlData.__dollar__scoreSlots = new Array();
	
	lvlData.__dollar__specialSlots.push({__dollar__x: spec_x, __dollar__y: spec_y});
	lvlData.__dollar__scoreSlots.push({__dollar__x: score_x, __dollar__y: score_y});
      
    game.addLevelItems();

    return false;
  }
}