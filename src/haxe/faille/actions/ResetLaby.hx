package faille.actions;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class ResetLaby implements IAction {
  public var name(default, null): String = Obfu.raw("resetLaby");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();

    for(id in 9...65) {
      game.world.fl_read[id] = false;
    }
      
    return false;
  }
}