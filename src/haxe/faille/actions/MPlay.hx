package faille.actions;

import hf.mode.GameMode;
import hf.Hf;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class MPlay implements IAction {
  public var name(default, null): String = Obfu.raw("playM");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();

    var played: Bool = false;

    var upLeftM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("upLeft")).or(null);
    var upMidM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("upMid")).or(null);
    var upRightM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("upRight")).or(null);
    var midLeftM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("midLeft")).or(null);
    var midMidM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("midMid")).or(null);
    var midRightM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("midRight")).or(null);
    var downLeftM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("downLeft")).or(null);
    var downMidM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("downMid")).or(null);
    var downRightM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("downRight")).or(null);
    var upLeft: Float = upLeftM.unwrapFloat();
    var upMid: Float = upMidM.unwrapFloat();
    var upRight: Float = upRightM.unwrapFloat();
    var midLeft: Float = midLeftM.unwrapFloat();
    var midMid: Float = midMidM.unwrapFloat();
    var midRight: Float = midRightM.unwrapFloat();
    var downLeft: Float = downLeftM.unwrapFloat();
    var downMid: Float = downMidM.unwrapFloat();
    var downRight: Float = downRightM.unwrapFloat();

    var verticalLeftBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalLeftBot")).or(null);
    var verticalMidBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalMidBot")).or(null);
    var verticalRightBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalRightBot")).or(null);
    var horizontalUpBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalUpBot")).or(null);
    var horizontalMidBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalMidBot")).or(null);
    var horizontalBotBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalBotBot")).or(null);
    var diagonalUpLeftBotRightBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("diagonalUpLeftBotRightBot")).or(null);
    var diagonalUpRightBotLeftBotM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("diagonalUpRightBotLeftBot")).or(null);
    var verticalLeftBot: Float = verticalLeftBotM.unwrapFloat();
    var verticalMidBot: Float = verticalMidBotM.unwrapFloat();
    var verticalRightBot: Float = verticalRightBotM.unwrapFloat();
    var horizontalUpBot: Float = horizontalUpBotM.unwrapFloat();
    var horizontalMidBot: Float = horizontalMidBotM.unwrapFloat();
    var horizontalBotBot: Float = horizontalBotBotM.unwrapFloat();
    var diagonalUpLeftBotRightBot: Float = diagonalUpLeftBotRightBotM.unwrapFloat();
    var diagonalUpRightBotLeftBot: Float = diagonalUpRightBotLeftBotM.unwrapFloat();

    var verticalLeftIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalLeftIgor")).or(null);
    var verticalMidIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalMidIgor")).or(null);
    var verticalRightIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("verticalRightIgor")).or(null);
    var horizontalUpIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalUpIgor")).or(null);
    var horizontalMidIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalMidIgor")).or(null);
    var horizontalBotIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("horizontalBotIgor")).or(null);
    var diagonalUpLeftBotRightIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("diagonalUpLeftBotRightIgor")).or(null);
    var diagonalUpRightBotLeftIgorM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("diagonalUpRightBotLeftIgor")).or(null);
    var verticalLeftIgor: Float = verticalLeftIgorM.unwrapFloat();
    var verticalMidIgor: Float = verticalMidIgorM.unwrapFloat();
    var verticalRightIgor: Float = verticalRightIgorM.unwrapFloat();
    var horizontalUpIgor: Float = horizontalUpIgorM.unwrapFloat();
    var horizontalMidIgor: Float = horizontalMidIgorM.unwrapFloat();
    var horizontalBotIgor: Float = horizontalBotIgorM.unwrapFloat();
    var diagonalUpLeftBotRightIgor: Float = diagonalUpLeftBotRightIgorM.unwrapFloat();
    var diagonalUpRightBotLeftIgor: Float = diagonalUpRightBotLeftIgorM.unwrapFloat();

    var victoryM: MerlinValue = Merlin.getGlobalVar(game, Obfu.raw("victory")).or(null);
    var victory: Float = victoryM.unwrapFloat();

    // Si Igor gagne
    if(igorWins(verticalLeftIgor, verticalMidIgor, verticalRightIgor, horizontalUpIgor, horizontalMidIgor, horizontalBotIgor, diagonalUpLeftBotRightIgor, diagonalUpRightBotLeftIgor)) {
      victory = 1;
      Merlin.setGlobalVar(game, Obfu.raw("victory"), new MerlinFloat(victory));
      played = true;

    } else {
      if(verticalLeftBot == 2 && !played && (upLeft == 0 || downLeft == 0 || midLeft == 0)) {
        played = true;

        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        } else
        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(midLeft == 0) {
          botMidLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midLeft, victory);
        }

      } else 
      if(verticalMidBot == 2 && !played && (midMid == 0 || upMid == 0 || downMid == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(upMid == 0) {
          botUpMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upMid, victory);
        } else
        if(downMid == 0) {
          botBotMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downMid, victory);
        }

      } else 
      if(verticalRightBot == 2 && !played && (upRight == 0 || downRight == 0 || midRight == 0)) {
        played = true;

        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(midRight == 0) {
          botMidRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midRight, victory);
        }

      } else 
      if(horizontalUpBot == 2 && !played && (upLeft == 0 || upRight == 0 || upMid == 0)) {
        played = true;

        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        } else
        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        } else
        if(upMid == 0) {
          botUpMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upMid, victory);
        }

      } else 
      if(horizontalMidBot == 2 && !played && (midMid == 0 || midLeft == 0 || midRight == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(midLeft == 0) {
          botMidLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midLeft, victory);
        } else
        if(midRight == 0) {
          botMidRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midRight, victory);
        }

      } else 
      if(horizontalBotBot == 2 && !played && (downLeft == 0 || downRight == 0 || downMid == 0)) {
        played = true;

        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(downMid == 0) {
          botBotMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downMid, victory);
        }

      } else
      if(diagonalUpLeftBotRightBot == 2 && !played && (midMid == 0 || downRight == 0 || upLeft == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        }

      } else
      if(diagonalUpRightBotLeftBot == 2 && !played && (midMid == 0 || downLeft == 0 || upRight == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        }

      } else
      if(verticalLeftIgor == 2 && !played && (upLeft == 0 || downLeft == 0 || midLeft == 0)) {
        played = true;

        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        } else
        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(midLeft == 0) {
          botMidLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midLeft, victory);
        }

      } else 
      if(verticalMidIgor == 2 && !played && (midMid == 0 || upMid == 0 || downMid == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(upMid == 0) {
          botUpMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upMid, victory);
        } else
        if(downMid == 0) {
          botBotMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downMid, victory);
        }

      } else 
      if(verticalRightIgor == 2 && !played && (upRight == 0 || downRight == 0 || midRight == 0)) {
        played = true;

        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(midRight == 0) {
          botMidRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midRight, victory);
        }

      } else 
      if(horizontalUpIgor == 2 && !played && (upLeft == 0 || upRight == 0 || upMid == 0)) {
        played = true;

        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        } else
        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        } else
        if(upMid == 0) {
          botUpMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upMid, victory);
        }

      } else 
      if(horizontalMidIgor == 2 && !played && (midMid == 0 || midLeft == 0 || midRight == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(midLeft == 0) {
          botMidLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midLeft, victory);
        } else
        if(midRight == 0) {
          botMidRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midRight, victory);
        }

      } else 
      if(horizontalBotIgor == 2 && !played && (downLeft == 0 || downRight == 0 || downMid == 0)) {
        played = true;

        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(downMid == 0) {
          botBotMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downMid, victory);
        }

      } else
      if(diagonalUpLeftBotRightIgor == 2 && !played && (midMid == 0 || downRight == 0 || upLeft == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(downRight == 0) {
          botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
        } else
        if(upLeft == 0) {
          botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
        }

      } else
      if(diagonalUpRightBotLeftIgor == 2 && !played && (midMid == 0 || downLeft == 0 || upRight == 0)) {
        played = true;

        if(midMid == 0) {
          botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
        } else
        if(downLeft == 0) {
          botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
        } else
        if(upRight == 0) {
          botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
        }

      } else
      if(midMid == 0 && !played) {
        played = true;
        botMidMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midMid, victory);
      } else
      if(upMid == 0 && midMid != 1 && !played) {
        played = true;
        botUpMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upMid, victory);
      } else
      if(upRight == 0 && !played) {
        played = true;
        botUpRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upRight, victory);
      } else
      if(downLeft == 0 && !played) {
        played = true;
        botBotLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downLeft, victory);
      } else
      if(upLeft == 0 && !played) {
        played = true;
        botUpLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, upLeft, victory);
      } else
      if(downRight == 0 && !played) {
        played = true;
        botBotRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downRight, victory);
      } else
      if(midLeft == 0 && !played) {
        played = true;
        botMidLeft(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midLeft, victory);
      } else
      if(downMid == 0 && !played) {
        played = true;
        botBotMid(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, downMid, victory);
      } else
      if(midRight == 0 && !played) {
        played = true;
        botMidRight(game, verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, midRight, victory);
      }
    }

    return false;
  }

  private function igorWins(verticalLeftIgor: Float, verticalMidIgor: Float, verticalRightIgor: Float, horizontalUpIgor: Float, horizontalMidIgor: Float, horizontalBotIgor: Float, diagonalUpLeftBotRightIgor: Float, diagonalUpRightBotLeftIgor: Float): Bool {
    if (verticalLeftIgor == 3 ||
        verticalMidIgor == 3 ||
        verticalRightIgor == 3 ||
        horizontalUpIgor == 3 ||
        horizontalMidIgor == 3 ||
        horizontalBotIgor == 3 ||
        diagonalUpLeftBotRightIgor == 3 ||
        diagonalUpRightBotLeftIgor == 3) {
      return true;
    } else {
      return false;
    }
  }

  private function botWins(verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, victory: Float, game: GameMode): Void {
    if (verticalLeftBot == 3 ||
        verticalMidBot == 3 ||
        verticalRightBot == 3 ||
        horizontalUpBot == 3 ||
        horizontalMidBot == 3 ||
        horizontalBotBot == 3 ||
        diagonalUpLeftBotRightBot == 3 ||
        diagonalUpRightBotLeftBot == 3) {
      victory = -1;
      Merlin.setGlobalVar(game, Obfu.raw("victory"), new MerlinFloat(victory));
    }
  }

  private function botUpLeft(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, upLeft: Float, victory: Float): Void {
    verticalLeftBot += 1;
    horizontalUpBot += 1;
    diagonalUpLeftBotRightBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("verticalLeftBot"), new MerlinFloat(verticalLeftBot));
    Merlin.setGlobalVar(game, Obfu.raw("horizontalUpBot"), new MerlinFloat(horizontalUpBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpLeftBotRightBot"), new MerlinFloat(diagonalUpLeftBotRightBot));
    upLeft = 2;
    Merlin.setGlobalVar(game, Obfu.raw("upLeft"), new MerlinFloat(upLeft));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botMidLeft(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, midLeft: Float, victory: Float): Void {
    verticalLeftBot += 1;
    horizontalMidBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("verticalLeftBot"), new MerlinFloat(verticalLeftBot));
    Merlin.setGlobalVar(game, Obfu.raw("horizontalMidBot"), new MerlinFloat(horizontalMidBot));
    midLeft = 2;
    Merlin.setGlobalVar(game, Obfu.raw("midLeft"), new MerlinFloat(midLeft));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botBotLeft(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, downLeft: Float, victory: Float): Void {
    verticalLeftBot += 1;
    horizontalBotBot += 1;
    diagonalUpRightBotLeftBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("verticalLeftBot"), new MerlinFloat(verticalLeftBot));
    Merlin.setGlobalVar(game, Obfu.raw("horizontalBotBot"), new MerlinFloat(horizontalBotBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpRightBotLeftBot"), new MerlinFloat(diagonalUpRightBotLeftBot));
    downLeft = 2;
    Merlin.setGlobalVar(game, Obfu.raw("downLeft"), new MerlinFloat(downLeft));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botUpMid(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, upMid: Float, victory: Float): Void {
    horizontalUpBot += 1;
    verticalMidBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalUpBot"), new MerlinFloat(horizontalUpBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalMidBot"), new MerlinFloat(verticalMidBot));
    upMid = 2;
    Merlin.setGlobalVar(game, Obfu.raw("upMid"), new MerlinFloat(upMid));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botMidMid(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, midMid: Float, victory: Float): Void {
    horizontalMidBot += 1;
    verticalMidBot += 1;
    diagonalUpLeftBotRightBot += 1;
    diagonalUpRightBotLeftBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalMidBot"), new MerlinFloat(horizontalMidBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalMidBot"), new MerlinFloat(verticalMidBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpLeftBotRightBot"), new MerlinFloat(diagonalUpLeftBotRightBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpRightBotLeftBot"), new MerlinFloat(diagonalUpRightBotLeftBot));
    midMid = 2;
    Merlin.setGlobalVar(game, Obfu.raw("midMid"), new MerlinFloat(midMid));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botBotMid(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, downMid: Float, victory: Float): Void {
    horizontalBotBot += 1;
    verticalMidBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalBotBot"), new MerlinFloat(horizontalBotBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalMidBot"), new MerlinFloat(verticalMidBot));
    downMid = 2;
    Merlin.setGlobalVar(game, Obfu.raw("downMid"), new MerlinFloat(downMid));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botUpRight(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, upRight: Float, victory: Float): Void {
    horizontalUpBot += 1;
    verticalRightBot += 1;
    diagonalUpRightBotLeftBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalUpBot"), new MerlinFloat(horizontalUpBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalRightBot"), new MerlinFloat(verticalRightBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpRightBotLeftBot"), new MerlinFloat(diagonalUpRightBotLeftBot));
    upRight = 2;
    Merlin.setGlobalVar(game, Obfu.raw("upRight"), new MerlinFloat(upRight));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botMidRight(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, midRight: Float, victory: Float): Void {
    horizontalMidBot += 1;
    verticalRightBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalMidBot"), new MerlinFloat(horizontalMidBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalRightBot"), new MerlinFloat(verticalRightBot));
    midRight = 2;
    Merlin.setGlobalVar(game, Obfu.raw("midRight"), new MerlinFloat(midRight));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }

  private function botBotRight(game: GameMode, verticalLeftBot: Float, verticalMidBot: Float, verticalRightBot: Float, horizontalUpBot: Float, horizontalMidBot: Float, horizontalBotBot: Float, diagonalUpLeftBotRightBot: Float, diagonalUpRightBotLeftBot: Float, downRight: Float, victory: Float): Void {
    horizontalBotBot += 1;
    verticalRightBot += 1;
    diagonalUpLeftBotRightBot += 1;
    Merlin.setGlobalVar(game, Obfu.raw("horizontalBotBot"), new MerlinFloat(horizontalBotBot));
    Merlin.setGlobalVar(game, Obfu.raw("verticalRightBot"), new MerlinFloat(verticalRightBot));
    Merlin.setGlobalVar(game, Obfu.raw("diagonalUpLeftBotRightBot"), new MerlinFloat(diagonalUpLeftBotRightBot));
    downRight = 2;
    Merlin.setGlobalVar(game, Obfu.raw("downRight"), new MerlinFloat(downRight));
    botWins(verticalLeftBot, verticalMidBot, verticalRightBot, horizontalUpBot, horizontalMidBot, horizontalBotBot, diagonalUpLeftBotRightBot, diagonalUpRightBotLeftBot, victory, game);
  }
}