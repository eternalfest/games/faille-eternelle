package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class CustomShield implements IAction {
  public var name(default, null): String = Obfu.raw("customShield");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
  	var game: GameMode = ctx.getGame();
    var t: Float = ctx.getOptInt(Obfu.raw("t")).or(30);

    for (player in game.getPlayerList()) {
      if (player.fl_shield)
        player.shield(t);
    }
      
    return false;
  }
}