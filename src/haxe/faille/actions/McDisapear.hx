package faille.actions;

import hf.Hf;
import hf.mode.GameMode;
import hf.Entity;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class McDisapear implements IAction {
  public var name(default, null): String = Obfu.raw("mc_disapear");
  public var isVerbose(default, null): Bool = false;

  private var mod: Disapear;
    
  public function new(mod: Disapear) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var minSid: Int = ctx.getOptInt(Obfu.raw("minSid")).or(0);
    var maxSid: Int = ctx.getOptInt(Obfu.raw("maxSid")).or(0);

    var game: GameMode = ctx.getGame();
      
    mod.minSid = minSid;
    mod.maxSid = maxSid;

    return false;
  }
}