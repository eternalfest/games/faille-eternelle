package faille.actions;

import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

class SetSpawn implements IAction {
  public var name(default, null): String = Obfu.raw("setSpawn");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Int = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Int = ctx.getOptInt(Obfu.raw("y")).or(0);

  	var game: GameMode = ctx.getGame();

    var lvlData = game.world.current;
    lvlData.__dollar__playerX = x;
    lvlData.__dollar__playerY = y;

    if (game.fl_mirror) {
      lvlData.__dollar__playerX = 20 - x - 1;
    }

    return false;
  }
}