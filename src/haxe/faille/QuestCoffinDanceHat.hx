package faille;

import user_data.RewardItem;
import quests.QuestReward;
import faille.ChangeHat;
import etwin.Obfu;
import hf.mode.GameMode;

class QuestCoffinDanceHat implements QuestReward {

  public function new() {}

  public function give(game: GameMode, reward: RewardItem): Void {
    faille.ChangeHat.coffinDanceHat = true;
  }

  public function remove(game: GameMode, reward: RewardItem): Void {
  }
}