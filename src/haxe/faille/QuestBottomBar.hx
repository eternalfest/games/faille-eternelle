package faille;

import user_data.RewardItem;
import quests.QuestReward;
import etwin.Obfu;
import bottom_bar.modules.BottomBarModuleFactory;
import bottom_bar.modules.custom.FireballTimer;
import bottom_bar.BottomBar;
import hf.mode.GameMode;

class QuestBottomBar implements QuestReward {

  public function new() {}

  public function give(game: GameMode, reward: RewardItem): Void {
    faille.FireBallTimerCustom.visible = true;
  }

  public function remove(game: GameMode, reward: RewardItem): Void {
  }
}
