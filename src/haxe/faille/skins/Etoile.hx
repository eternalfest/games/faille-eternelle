package faille.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class Etoile implements ISkin {
  public static var ID_TOULAHO: Int = 169;

  public var id(default, null): Int;
  public var sprite(default, null): Null<String> = null;
  private var hue: Int;
  private var lux: Float;

  public function new(id: Int, hue: Int, lux: Float) {
    this.id = id;
    this.hue = hue;
    this.lux = lux;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_TOULAHO + 1));
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(this.hue, 1, this.lux).toFilter();
    mc.filters = [filter];
  }
}