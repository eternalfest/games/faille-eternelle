package faille.skins;

import etwin.flash.MovieClip;
import hf.mode.GameMode;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class EtoileMulticolore implements ISkin {
  public static var ID_TOULAHO: Int = 169;

  public var id(default, null): Int = 1303;
  public var sprite(default, null): Null<String> = null;
  private var hue: Int = 0;
  private var etoile: MovieClip;
    
  @:diExport
  private var patch(default, null): IPatch;

  public function new() {
    this.patch = new PatchList([
      Ref.auto(GameMode.main).before(function(hf: Hf, self: GameMode): Void {
       hue += 3;
       var filter: ColorMatrixFilter = ColorMatrix.fromHsv(hue, 1, 1).toFilter();
       etoile.filters = [filter];
       if(hue > 360) hue = 0;
      }),
    ]);
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_TOULAHO + 1));
    etoile = mc;
  }
}