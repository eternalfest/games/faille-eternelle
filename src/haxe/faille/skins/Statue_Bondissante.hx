package faille.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class Statue_Bondissante implements ISkin {
  public static var ID_STATUE: Int = 183;

  public var id(default, null): Int = 1298;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_STATUE + 1));
  }
}