package faille.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class Piece implements ISkin {
  public static var ID_PIECE: Int = 51;

  public var id(default, null): Int = 1295;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_PIECE + 1));
    mc._xscale = 130;
    mc._yscale = 130;
  }
}