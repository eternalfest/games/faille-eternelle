package faille.skins;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import vault.ISkin;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class IgorSkin implements ISkin {
  public static var ID_IGOR: Int = 103;

  public var id(default, null): Int = 146;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_IGOR + 1));
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(200, 1, 1).toFilter();
    mc.filters = [filter];
  }
}
