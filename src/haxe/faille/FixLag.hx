package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.GameManager;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;

@:build(patchman.Build.di())
class FixLag {

  @:diExport
  public var patches(default, null): IPatch;

  public function new() {
    this.patches = new PatchList([

      //Ref.auto(GameMode.shake).wrap(function(hf, self: GameMode, duration: Null<Float>, power: Null<Float>, old): Void {
        //if(hf.GameManager.CONFIG.fl_detail)
          //old(self, duration, power);
      //}),
 
    ]);
  }
}