package faille;

import etwin.flash.MovieClip;
import hf.Hf;
import hf.Mode;
import hf.GameManager;
import hf.mode.GameMode;
import hf.entity.Player;
import hf.entity.shoot.PlayerArrow;
import hf.entity.shoot.PlayerPearl;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.Obfu;
import merlin.Merlin;

@:build(patchman.Build.di())
class ChangeRingCooldown {

	@:diExport
	public var patch(default, null): IPatch;

	public function new(): Void {
    	this.patch = new PatchList([
      		Ref.auto(PlayerArrow.init).wrap(function(hf: Hf, self: PlayerArrow, g: GameMode, old): Void {
      			old(self, g);
				self.coolDown = hf.Data.SECOND * 1.2;
     		}),

     		Ref.auto(PlayerPearl.init).wrap(function(hf: Hf, self: PlayerPearl, g: GameMode, old): Void {
      			old(self, g);
				self.coolDown = hf.Data.SECOND * 1.2;
     		}),

     		Ref.auto(Player.changeWeapon).wrap(function(hf: Hf, self: Player, id: Int, old): Void {
     			if(self.world.fl_mainWorld) {
     				if(self.world.currentId > 80 && self.world.currentId <= 90 && id == null) {
     					id = hf.Data.WEAPON_S_ICE;
     				}
     				if(self.world.currentId > 190 && self.world.currentId < 200 && id == null) {
     					id = hf.Data.WEAPON_S_ARROW;
     				}
     			}
				old(self, id);
     		}),
    	]);
  	}
}