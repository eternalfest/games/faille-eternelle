package faille;

import etwin.Obfu;
import merlin.Merlin;
import merlin.value.MerlinFloat;
import merlin.value.MerlinValue;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

class StarCount extends BottomBarModule {
    
    public var starSprite: Dynamic;
    public var countSprite: Dynamic;
    public var x: Int;
    public var colorStar: Int;

    public function new(module: Map<String, Dynamic>) {
        x = module[Obfu.raw("x")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        starSprite.removeMovieClip();
        starSprite = bottomBar.game.depthMan.attach('hammer_item_score', bottomBar.game.root.Data.DP_TOP + 1);
        starSprite._x = x;
        starSprite._y = 517.;
        starSprite._xscale = 56;
        starSprite._yscale = 56;
        starSprite.gotoAndStop('' + (169 + 1));
        starSprite._alpha = 1;
        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1).toFilter();
        starSprite.filters = [filter];

        countSprite.removeMovieClip();
        countSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        countSprite.field._visible = false;
        countSprite.field._width = 400;
        countSprite.field._xscale = 115;
        countSprite.field._yscale = 115;
        countSprite.field._x = x + 10;
        countSprite.field._y = 499;
        countSprite.field.text = "0";
        countSprite.label._visible = false;
        bottomBar.game.root.FxManager.addGlow(countSprite, 7366029, 2);
        
        colorStar = 0;
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        var merlinNumberStars: MerlinValue = Merlin.getGlobalVar(bottomBar.game, Obfu.raw("nbEtoiles")).or(new MerlinFloat(0));
        var numberStars: Float = merlinNumberStars.unwrapFloat();
        var numberStarsStr: String = Std.string(numberStars);

        // Ajoute le compteur d'étoiles dans la dim 20
        if(bottomBar.game.currentDim > 1) {
          starSprite._alpha = 100;
          countSprite.field._visible = true;
            
          if(Std.string(Math.abs(7 - numberStars)) != countSprite.field.text) {
            countSprite.field.text = cast Math.abs(7 - numberStars);
          }
        
          // Effet si toutes les étoiles sont réunies
          if(numberStars == 0) {
            colorStar += 3;
            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(colorStar, 1, 1).toFilter();
            starSprite.filters = [filter];
            if(colorStar > 360) colorStar = 0;
          } else {
            var filter: ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1).toFilter();
            starSprite.filters = [filter];
          }
        }
    
        return true;
    }
}