package faille;

import etwin.Obfu;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;

class LevelCount extends BottomBarModule {
    
    public var levelSprite: Dynamic;
    public var countSprite: Dynamic;
    public var x: Int;

    public function new(module: Map<String, Dynamic>) {
        x = module[Obfu.raw("x")];
    }

    public override function init(bottomBar:BottomBarInterface): Void {
        levelSprite.removeMovieClip();
        levelSprite = bottomBar.game.depthMan.attach(Obfu.raw('arrow_down'), bottomBar.game.root.Data.DP_TOP + 1);
        levelSprite._x = x;
        levelSprite._y = 517.;
        levelSprite._xscale = 90;
        levelSprite._yscale = 90;
        levelSprite.stop();

        countSprite.removeMovieClip();
        countSprite = bottomBar.game.depthMan.attach('hammer_interf_inGameMsg', bottomBar.game.root.Data.DP_TOP + 1);
        countSprite.field._visible = true;
        countSprite.field._width = 400;
        countSprite.field._xscale = 115;
        countSprite.field._yscale = 115;
        countSprite.field._x = x + 20;
        countSprite.field._y = 499;
        countSprite.field.text = "0";
        countSprite.label._visible = false;
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        
        if(bottomBar.game.currentDim > 0) {
            countSprite.field.text = "?";
        } else {
            countSprite.field.text = bottomBar.game.world.currentId;
        }
        return true;
    }
}