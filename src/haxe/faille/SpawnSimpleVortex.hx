package faille;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class SpawnSimpleVortex {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var launch: Bool;
  public var sid: Int;

  public function new() {
    this.actions = new LaunchVortex(this);
      
    this.launch = false;

    this.patches = new PatchList([
        
      Ref.auto(GameMode.main).after(function(hf, self: GameMode): Void {
        if (this.launch && !self.fl_pause) {
          var x = 200;
          var y = 28 * 20;
          var scaleX = 250;
          var scaleY = scaleX;
          var back = true;
          var name = "hammer_portal";
          var filter: ColorMatrixFilter = ColorMatrix.fromHsv(0, 1, 1).toFilter();
          var sprite = self.world.view.attachSprite(name, x, y, back);
          sprite._xscale = scaleX;
          sprite._yscale = scaleY;
          sprite.play();
          sprite.filters = [filter];
          self.world.scriptEngine.mcList.push({sid: this.sid, mc: (cast sprite)});
          this.launch = false;
        }
      }),
          
    ]);
  }
}

class LaunchVortex implements IAction {
  public var name(default, null): String = Obfu.raw("launchSimpleVortex");
  public var isVerbose(default, null): Bool = false;

  private var mod: SpawnSimpleVortex;
    
  public function new(mod: SpawnSimpleVortex) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var launch: Bool = ctx.getOptBool(Obfu.raw("n")).or(false); 
    mod.launch = launch;
    mod.sid = 10000;

    return false;
  }
}