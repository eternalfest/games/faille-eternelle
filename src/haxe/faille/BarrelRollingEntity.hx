package faille;

import etwin.Obfu;
import hf.Hf;
import hf.entity.Physics;
import hf.mode.GameMode;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:hfTemplate
class BarrelRollingEntity extends Physics {

  private var targetX: Float;
  private var targetY: Float;
  private var ang: Float;

  private var speed: Float = 8;
  private var angSpeed: Float = -50;

  @:keep
  public function new(): Void {
    super(null);
    this.fl_gravity = false;
    this.fl_hitGround = false;
    this.fl_hitWall = false;
    this.fl_hitBorder = false;
  }

  public static function attach(g: GameMode, x1: Float, y1: Float, x2: Float, y2: Float, name: String, h: Float, s: Float, v: Float): BarrelRollingEntity {
    var hf: Hf = g.root;
    hf.Std.registerClass(name, BarrelRollingEntity.getClass(hf));
    var entity: BarrelRollingEntity = cast g.depthMan.attach(name, hf.Data.DP_SUPA);
    var filter = ColorMatrix.fromHsv(h, s, v).toFilter();
    entity.filters = [filter];
    entity.init(g);
    entity.initMovement(x1, y1, x2, y2);
    return entity;
  }

  override public function init(g: GameMode) {
    super.init(g);
    this.register(g.root.Data.FX);
  }

  public function initMovement(x1: Float, y1: Float, x2: Float, y2: Float): Void {
    this.moveTo(x1, y1);
    this.targetX = x2;
    this.targetY = y2;
    this.ang = 0;
    this.moveToPoint(this.targetX, this.targetY, this.speed);
  }

  override public function update(): Void {
    super.update();
    this.moveToPoint(this.targetX, this.targetY, this.speed);
    this.ang = this.ang + this.angSpeed;
  }

  override public function postfix(): Void {
    super.postfix();
    if (this.distance(this.targetX, this.targetY) < 5) {
      this.destroy();
    }
  }

  override public function endUpdate(): Void {
    super.endUpdate();
    this._rotation = this.ang;
  };

}
