package quests;

import quests.rewards.QuestLifeReward;
import quests.rewards.QuestBankReward;
import quests.rewards.QuestTokenReward;
import user_data.QuestItem;
import user_data.UserData;
import quests.rewards.QuestFamilyReward;
import quests.rewards.QuestDefaultReward;
import hf.mode.GameMode;
import etwin.Obfu;
import patchman.HostModuleLoader;
import patchman.Ref;
import quests.QuestManager;
import quests.Quest;
import patchman.PatchList;
import patchman.IPatch;
@:build(patchman.Build.di())
class QuestManager {
    private static var instance: QuestManager;

    @:diExport
    public var patch(default, null): IPatch;


    public var rewards: Map<String, Map<String, GameMode -> Dynamic -> Void>>;

    public function new(patches: Array<IPatch>, hml: HostModuleLoader, data : patchman.module.Data) {
        instance = this;
        rewards = new Map<String, Map<String, GameMode -> Dynamic -> Void>>();
        register_reward(Obfu.raw("default"), new QuestDefaultReward());
        register_reward(Obfu.raw("family"), new QuestFamilyReward());
        register_reward(Obfu.raw("token"), new QuestTokenReward());
        register_reward(Obfu.raw("bank"), new QuestBankReward());
        register_reward(Obfu.raw("life"), new QuestLifeReward());
        var loaded: Bool = false;
        var quests: Array<Dynamic> = data.get(Obfu.raw("Quests"));

        patches.push(Ref.auto(hf.mode.GameMode.initWorld).before(function(hf, self: GameMode) {
            if(!loaded && UserData.get().quests.length > 0) {
                var inventory = UserData.get().inventory;
                var quests = UserData.get().quests;
                for(quest in quests) {
                    if(quest.get_state(inventory) == QuestItem.STATE_OVER) {
                        var q = new Quest();
                        q.actions[Obfu.raw("give")] = quest.give;
                        q.actions[Obfu.raw("remove")] = quest.remove;
                        execute_quest(self, q, Obfu.raw("give"));
                        execute_quest(self, q, Obfu.raw("remove"));
                    }
                }
                self.randMan.register(self.root.Data.RAND_ITEMS_ID, self.root.Data.getRandFromFamilies(self.root.Data.SPECIAL_ITEM_FAMILIES, self.root.GameManager.CONFIG.specialItemFamilies));
                self.randMan.register(self.root.Data.RAND_SCORES_ID, self.root.Data.getRandFromFamilies(self.root.Data.SCORE_ITEM_FAMILIES, self.root.GameManager.CONFIG.scoreItemFamilies));

                self.worldKeys = new Array();
                for(family in 5000...5100) {
                    if (self.root.GameManager.CONFIG.hasFamily(family))
                        self.giveKey(family - 5000);
                }
            }
        }));

        this.patch = new PatchList(patches);
    }

    public static function get(): QuestManager {
        return instance;
    }

    public function register_reward(id: String, reward: QuestReward): Void {
        rewards[id] = new Map<String, GameMode -> Dynamic -> Void>();
        rewards[id][Obfu.raw("give")] = reward.give;
        rewards[id][Obfu.raw("remove")] = reward.remove;
    }

    public function execute_quest(game: GameMode, quest: Quest, action: String): Void {
        for(j in 0...quest.actions[action].length) {
            if(quest.actions[action][j].reward[Obfu.raw("type")] != null) {
                var reward = rewards[quest.actions[action][j].reward[Obfu.raw("type")]];
                if(reward == null)
                    reward = rewards[Obfu.raw('default')];
                reward[action](game, quest.actions[action][j]);
            }
        }
    }
}
