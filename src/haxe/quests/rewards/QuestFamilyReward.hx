package quests.rewards;
import user_data.RewardItem;
import etwin.Obfu;
import hf.mode.GameMode;
class QuestFamilyReward implements QuestReward {
    public function new() {
    }

    public function give(game: GameMode, reward: RewardItem): Void {
        var item: Int = Std.parseInt(reward.reward[Obfu.raw("id")]);
        if (item >= 1000) {
            game.root.GameManager.CONFIG.scoreItemFamilies.push(item);
        } else {
            game.root.GameManager.CONFIG.specialItemFamilies.push(item);
        }
    }
    public function remove(game: GameMode, reward: RewardItem): Void {
        var item: Int = Std.parseInt(reward.reward[Obfu.raw("id")]);
        if (item >= 1000) {
            for(i in 0...game.root.GameManager.CONFIG.scoreItemFamilies.length)
                if(game.root.GameManager.CONFIG.scoreItemFamilies[i] == item) {
                    for(j in i...game.root.GameManager.CONFIG.scoreItemFamilies.length)
                        game.root.GameManager.CONFIG.scoreItemFamilies[j] = game.root.GameManager.CONFIG.scoreItemFamilies[j + 1];
                    game.root.GameManager.CONFIG.scoreItemFamilies.pop();
                }
        } else {
            for(i in 0...game.root.GameManager.CONFIG.specialItemFamilies.length)
                if(game.root.GameManager.CONFIG.specialItemFamilies[i] == item) {
                    for(j in i...game.root.GameManager.CONFIG.specialItemFamilies.length)
                        game.root.GameManager.CONFIG.specialItemFamilies[j] = game.root.GameManager.CONFIG.specialItemFamilies[j + 1];
                    game.root.GameManager.CONFIG.specialItemFamilies.pop();
                }
        }
    }
}
