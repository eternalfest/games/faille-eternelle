package quests;
import user_data.RewardItem;
import hf.mode.GameMode;
interface QuestReward {
    public function give(manager: GameMode, reward: RewardItem): Void;
    public function remove(manager: GameMode, reward: RewardItem): Void;
}
